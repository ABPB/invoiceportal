'use strict';

(function(){
    angular.module('abpib').controller('BankDetailsCtrl', ['$scope', '$state', '$rootScope', 'B2bSharedServices', 'B2bAPIServices','adithyaServices','userStroage',BankDetailsCtrl]);

    function BankDetailsCtrl($scope, $state, $rootScope, B2bSharedServices, B2bAPIServices,adithyaServices,userStroage) {
        var token=userStroage.get('access_token');
        var access_token= "bearer" + " " + token;
        $scope.bankDetailsArray = B2bSharedServices.distributorDetails.bankDetails;
        $scope.activeBankDetails = $scope.bankDetailsArray[$scope.bankDetailsArray.length-1];
        // $scope.bankDetails = {};
        // $scope.bankDetailsArray = B2bSharedServices.distributorDetails.bankDetailsArray;
        // $scope.activeBankDetails = B2bSharedServices.distributorDetails.activeBankDetails;
        
        $scope.onIFSCCodeChanged = function() {
            $scope.activeBankDetails.abd_bank_name = "";
            $scope.activeBankDetails.abd_branch_name = "";
        };

        $scope.onIFSCSearchClicked = function(ifscCode) {
            if(ifscCode.length != 11) {
                alert("Please enter the valid ifsc code");
                return;
            }
            $rootScope.ngPopupConfig.isShow = true;
            adithyaServices.getBankDetailsForIFSC(access_token,ifscCode).then(function(data){
                $rootScope.ngPopupConfig.isShow = false;
                console.log(data);
                if(!data.bankIFSC)
                {
                    alert("Please enter a valid ifsc code");
                    return;
                }
                //$scope.$apply(function(){
                $scope.activeBankDetails.abd_bank_name = data.bankIFSC.biName;
                $scope.activeBankDetails.abd_branch_name = data.bankIFSC.biBranch;
                //})

            },function(error){
                $rootScope.ngPopupConfig.isShow = false;
                alert("There is an issue in API. Please contact the administrator");
                console.log(error);
            });


        };

        $scope.addAnotherAccount = function(){

            for(var i=0; i<$scope.bankDetailsArray.length; i++)
            {
                for(var i=0; i<$scope.bankDetailsArray.length; i++)
                {
                    var bankDetail = $scope.bankDetailsArray[i];
                    console.log(bankDetail);
                    if(bankDetail.abd_ifsc_code.length != 11) 
                    {
                        alert("Please enter the valid ifsc code");
                        return;    
                    }
    
                    else if(bankDetail.abd_account_holdername == "")
                    {
                        alert("Please enter valid bank account holder name");
                        return;
                    }

                    else if(!bankDetail.abd_account_no) {
                        alert("Please enter valid bank account number");
                        return;
                    }

                    else if(bankDetail.abd_bank_name == "" || bankDetail.abd_branch_name == "" || bankDetail.abd_account_no == "")
                    {
                        alert("Please enter the valid bank details");
                        return;    
                    }
                    
                    else if(isNaN(bankDetail.abd_account_no)) {
                        alert("Bank account number must be of only numbers");
                        return;    
                    }

                    else if(bankDetail.abd_account_no != bankDetail.accountNumberValidation)
                    {
                        alert("Please check the account number. A/c number not matched with re-entered value");
                        return;    
                    }
                }
            }  

            $scope.activeBankDetails = B2bSharedServices.createNewBankDetails();
            B2bSharedServices.distributorDetails.bankDetails.push($scope.activeBankDetails);

            // var copyTmp = {};
            // angular.copy($scope.activeBankDetails, copyTmp);
            // $scope.bankDetailsArray.pop($scope.activeBankDetails);
            // $scope.bankDetailsArray.push(copyTmp);
            // B2bSharedServices.distributorDetails.activeBankDetails = $scope.activeBankDetails = B2bSharedServices.createNewBankDetails();
            // $scope.bankDetailsArray.push($scope.activeBankDetails);
        }

        $scope.onDefaultFlagChange = function(activeBankDetails) {
            
            console.log(activeBankDetails)

            if(activeBankDetails.abd_preferred_bank_flg == 'Y')
            {
                for(var i=0; i<$scope.bankDetailsArray.length; i++)
                {
                    if($scope.bankDetailsArray[i] !== activeBankDetails) $scope.bankDetailsArray[i].abd_preferred_bank_flg = 'N';
                }   
            }
        }

        $scope.onEditBtnClicked = function(bankDetail) {
            $scope.activeBankDetails = bankDetail;
            console.log("onEditBtnClicked");
        }
        
        $scope.onDeleteBtnClicked = function(index) {
            $scope.bankDetailsArray.splice(index, 1);
        }

        $scope.onSaveBtnPressed = function(){
            
            var bankDetailsArray = B2bSharedServices.distributorDetails.bankDetails;
            var upiDetailsArray = B2bSharedServices.distributorDetails.upiDetails;

            var bankErrorMessage = "";
            var upiErrorMessage = "";

            var hasCreditAccount = false;
            var hasDebitAccount = false;

            for(var i=0; i<bankDetailsArray.length; i++) {
                var bankDetail = bankDetailsArray[i];
                var validResult = B2bSharedServices.checkIsValidBankDetail(bankDetail);
                bankDetail.isValid = validResult.valid;

                if(!validResult.valid && bankDetailsArray.length > 1) {
                    bankDetailsArray.splice(i, 1);
                    continue;
                }
                
                else if(!validResult.valid) bankErrorMessage = validResult.message;

                else bankErrorMessage = "";

                if(validResult.valid)
                {
                    if(bankDetail.abd_crd_dbt_flg == "C") hasCreditAccount = true;
                    
                    else if(bankDetail.abd_crd_dbt_flg == "D") hasDebitAccount = true;
    
                    else if (bankDetail.abd_crd_dbt_flg == "B") {
                        hasCreditAccount = true;
                        hasDebitAccount = true;
                    }
                }
            }

            for(var i=0; i<upiDetailsArray.length; i++)
            {
                if(!upiDetailsArray[i].isVerified && upiDetailsArray.length > 1) {
                    upiDetailsArray.splice(i, 1);
                    continue;
                }
                
                else if(!upiDetailsArray[i].isVerified) upiErrorMessage = "Please verify the existing UPI handler";

                else upiErrorMessage = "";

                if(upiDetailsArray[i].isVerified)
                {
                    if(upiDetailsArray[i].aud_upa_crddbt_flg == "C") hasCreditAccount = true;
                    
                    else if(upiDetailsArray[i].aud_upa_crddbt_flg == "D") hasDebitAccount = true;
    
                    else if (upiDetailsArray[i].aud_upa_crddbt_flg == "B") {
                        hasCreditAccount = true;
                        hasDebitAccount = true;
                    }
                }
            }

            if(bankErrorMessage != "" && upiErrorMessage != "") {
                alert(bankErrorMessage);
            }

            else if(!hasCreditAccount) {
                alert("Please add atleast one credit account");
            }
            
            else if(!hasDebitAccount) {
                alert("Please add atleast one debit account");
            }

            else {
                console.log("Success");
                $state.go('topnavview.confirmation');
            }
        }
    }

})();