'use strict';

(function(){
    angular.module('abpib').controller('SuccessCtrl', ['$scope', '$state', 'B2bSharedServices', SuccessCtrl]);

    function SuccessCtrl($scope, $state, B2bSharedServices) {
        
        $scope.onAddAnotherDistibutorClicked = function() {
            console.log("reload");
            $state.go('onboardentity')
        };
    }

})();