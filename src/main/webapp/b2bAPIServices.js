'use strict';

(function(){
    angular.module('abpib').service('B2bAPIServices', ['$http', '$q', '$httpParamSerializer', B2bAPIServices]);

    function B2bAPIServices($http, $q, $httpParamSerializer) {
        
        var serverPath = 'https://localhost:8441/middleware/merchantOnboarding/';
        var path ='https://localhost:8441/oauth/token&grant_type=client_credentials';

        function callService(methodType, requestBody, path)
        {
            var deferred = $q.defer();
            var req = {
                method: methodType,
                url: path,
                data: requestBody,
                timeout: 50000,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
                    'Authorization': 'Basic bWlkZGxld2FyZTpzZWNyZXQ='
                }
            };

            $http(req).success(function(data){
                deferred.resolve(data);
            })

            .error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        }
    
        function getBankDetailsForIFSC(ifscCode) {
            return callService('GET', {}, serverPath+"bankIFSC?ifscCode="+ifscCode);
        }

        function verifyUPIHandler(handler) {
            return callService('GET', {}, serverPath+"upiDetails?upiName="+handler);
        }

        function postRegistrationDetails(registrationData) {
            console.log(registrationData);
            return callService('POST', registrationData, serverPath+'register')
        }
         
        function apiAuthentication(){
            var req = {
                grant_type :"client_credentials",
                username :'middleware',
                password : 'secret'
                
            };
            console.log($httpParamSerializer(req));
            debugger;
            return callService('POST', $httpParamSerializer(req), path)
        }

        return {
            getBankDetailsForIFSC: getBankDetailsForIFSC,
            postRegistrationDetails: postRegistrationDetails,
            verifyUPIHandler: verifyUPIHandler,
            apiAuthentication  : apiAuthentication
        }
        
    }
})();