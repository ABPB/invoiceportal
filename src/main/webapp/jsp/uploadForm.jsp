<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html style="overflow-x: hidden;">
<head lang="en">
    <title>Invoice Operations</title>
    <link rel="stylesheet" href="../plugins/jQueryUI/jquery-ui.css_1.12.1">
    <link rel="stylesheet" href="../plugins/jQueryUI/jquery-ui.css_them1.12.1.css">
    <script src="../plugins/jQueryUI/jquery-1.12.4.js"></script>
    <script src="../plugins/jQueryUI/jquery-ui.js_1.12.1.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script type="text/javascript" src="../ui/bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <script src="../plugins/jQueryUI/jquery.validate.min.js"></script>
    <script src="../plugins/jQueryUI/additional-methods.min.js"></script>
    <link rel="stylesheet" href="../ui/app.css">
    <script>
        function validate(){

            var file= form.file.value;
            var reg = /(.*?)\.(txt|csv|pdf|jpeg|jpg|png|zip)$/;
            if(file !=''){
                if(!file.match(reg))            {
                    alert("Invalid File");
                    return false;
                }
            }else{
                var x = document.getElementById("file").required;
                document.getElementById("demo").innerHTML = x;
            }

        }
    </script>
    <script type="text/javascript">
        $(document).ready( function() {
            $('#Failed').delay(1000).fadeOut();
            $('#Success').delay(1000).fadeOut();
             $('#error').delay(2000).fadeOut();
        });
    </script>
</head>
<body class="hold-transition skin-blue layout-top-nav" ng-controller="batchuploadCtrl">
<div class="wrapper">
    <section class="content">
        <div class="row">
            <div class="col-sm-2">
                &nbsp;
            </div>
            <div class="col-sm-10">

                <% if(request.getAttribute("error")!=null) {%>
                <label id="error" style="color: orange"><%=request.getAttribute("error")%></label>
                <%}else {%>
                <%
                    if(request.getAttribute("uploadStatus")!=null&&
                            ("Success").equalsIgnoreCase((String)request.getAttribute("uploadStatus"))){%>
                <label id="Success" style="color:green;">File Uploaded Successfully</label>
                <%}else if(request.getAttribute("uploadStatus")!=null&&
                        ("Failed").equalsIgnoreCase((String)request.getAttribute("uploadStatus"))){%>
                <label id="Failed" style="color: red">File Upload Failed</label>
                <%}%>

                <%}%>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9" style="width:60%;margin-left: 20px;">
                <div class="row">
                    <form name="form" method="POST" enctype="multipart/form-data" action="/uploadFile">
                        <div class="col-sm-2" style="text-align: right;background-color: #f1f1f1;height: 38px;">
                            <p class="latest-upload-text" style="margin-left:5px;">Upload File</p>
                        </div>
                        <div class="col-sm-9" style="padding-left: 0px;">
                            <div class="upload" style="height: 35px;">
                                <input type="file" id="file" class="form-control" name="file" required />
                                <i>Please upload in .txt .csv format only.</i><br/>

                            </div>
                        </div>
                        <div class="col-sm-1" style="position: absolute;margin-left: 88%;">
                            <button type="submit" class="glyphicon glyphicon-upload" value="Upload File"
                                    style="cursor:pointer;vertical-align:-webkit-baseline-middle;font-size: 25px;"
                                    role="button" tabindex="0" onClick="return validate();"></button>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                            &nbsp;
                    </div>
                </div>
                <div class="row"
                     style="width: 100%;border-color:black;border-style: solid;border-width: 1px;background-color: yellow">
                    <div class="col-sm-1">
                        <p class="batch-upload-gridtext">Sno</p>
                    </div>
                    <div class="col-sm-3" style="width: 30%;">
                        <p class="batch-upload-gridtext">File Name</p>
                    </div>
                    <div class="col-sm-3" style="width: 30%;">
                        <p class="batch-upload-gridtext">Last Uploaded</p>
                    </div>
                    <div class="col-sm-3" style="width: 30%;">
                        <p class="batch-upload-gridtext">Action</p>
                    </div>
                </div>
                <% String fileName;
                    String fileName1;
                    String strArr[];
                    ArrayList al=(ArrayList)request.getAttribute("files");
                    int i=0;
                    if(al!=null&&al.size()>0){
                        for(Object object:al){
                            fileName=(String)object;
                            fileName1=fileName;
                            fileName=fileName.split("/")[fileName.split("/").length-1];
                            i++;
                %>
                <div class="row" style="width: 93%;padding-top: 5px;">
                    <div class="col-sm-1">
                        <p class="batch-upload-gridtext"><%=i%></p>
                    </div>
                    <div class="col-sm-3" style="width: 30%;">
                        <p class="batch-upload-gridtext"><%=fileName%></p>
                    </div>
                    <div class="col-sm-3" style="width: 30%;">
                        <p class="batch-upload-gridtext"><i class="glyphicon glyphicon-calendar"></i></p>
                    </div>
                    <div class="col-sm-3" style="width: 30%;">
                        <div class="row">
                            <div class="col-sm-1">&nbsp;</div>
                            <div class="col-sm-2">
                                <p class="batch-upload-gridtext"><a class="glyphicon glyphicon-download-alt" href="<%=fileName1%>"></a></p>
                            </div>
                            <div class="col-sm-2">
                                <p class="batch-upload-gridtext"><a  class="glyphicon glyphicon-trash" href="/delete/files/<%=fileName%>"></a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <%} }%>

            </div>
        </div>
    </section>
</div>
</body>
</html>
