    <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
        <%@ page import="java.util.*" %>

        <!DOCTYPE html>
        <head lang="en">
        <title>ABPB Operations</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
         <link rel="stylesheet" href="../ui/app.css">
         <script>
            function goToInitalPage() {
                 window.location.href = "/uploadedFiles/CREATE";
             }
         </script>
        </head>

        <body class="hold-transition skin-blue layout-top-nav" >
        <div class="wrapper">
        <section class="content">
            <%

                    if(request.getAttribute("uploadStatus")!=null&& ("Success").equalsIgnoreCase((String)request.getAttribute("uploadStatus"))){%>

        File Uploaded Successfully
            <%}else if(request.getAttribute("uploadStatus")!=null&& ("Failed").equalsIgnoreCase((String)request.getAttribute("uploadStatus"))){%>
        File Upload Failed
            <%}%>
        <form id="myForm" method="post">
        <div class="box">
        <div class="box-body">
        <div class="form-group">
        <div class="col-md-12">
        <div class="box">
        <form method="POST" enctype="multipart/form-data" action="/uploadFile" NAME="form1">
        <div style="padding: 0px !important;" class="box-body">
        <div class="row" style="margin-bottom:10px">
        <div class="col-sm-12">
        <p class="mandates-upload">Upload Files</p>
        </div>
        </div>
        <div class="row">
        <div class="col-md-6">
                <div class="form-group m-b-0">
                <input type="file" id="upload" class="form-control" name="file" />
                <i>Please upload in .zip format only. (both ACK and RES files) </i><br/>
                </div>
        </div>
                <div class="col-md-2">
                <input type="submit" class="form-control" id="fileUploaad" value="Upload">
                </div>

        </div>
            <div class="row" style="margin-bottom:10px">
                    <div class="col-sm-12">
                        <p class="mandates-upload">Operations Download</p>
                    </div>
            </div>
                    <div class="row">
                            <div class="col-sm-12">
                                    <div class="nav-tabs-custom">
                                            <ul class="nav nav-tabs">
                                            <li class="active"><a id="tab1" data-toggle="tab" href="/uploadedFiles/CREATE">
                                            <p class="nach-mandate">Create Mandate</p></a></li>
                                            <li><a id="tab2" data-toggle="tab" href="/uploadedFiles/AMEND">
                                            <p class="nach-mandate">Amend Mandate</p></a></li>
                                            <li><a id="tab3" data-toggle="tab" href="/uploadedFiles/CANCEL">
                                            <p class="nach-mandate">Cancel Mandate</p>
                                            </a>
                                            </li>
                                            </ul>

                                    </div>

                            </div>

                    </div>
            <div class="row">
            <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                    <% String fileName;
        String fileName1;
        String strArr[];
        ArrayList al=(ArrayList)request.getAttribute("files");
        if(al!=null&&al.size()>0){
        for(Object object:al){
        fileName=(String)object;
        fileName1=fileName;
        fileName=fileName.split("/")[fileName.split("/").length-1];
        %>
            <div class="row">
            <div class="col-md-4">
            <label class="nach-mandates"><%=fileName%></label>
            </div>
            <div class="col-md-4">
            <a class="glyphicon glyphicon-download-alt" href="<%=fileName1%>" aria-hidden="true" style="cursor:
            pointer"></a> <span class="glyphicon-class"></span>
            </div>
            </div>

                    <%} }%>

            </div>
            </div>
            </div>
        </div>

        </form>
        </div>

        </div>
        </div>
        </div>

        </div>
        </form>
        </section>
        </div>
        </div>


        </div>
        </body>
        </html>
