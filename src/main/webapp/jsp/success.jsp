<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" `="abpib" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" ng-app="abpib" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" ng-app="abpib" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" ng-app="abpib" class="no-js"> <!--<![endif]-->
<head>
    <title>Invoice Operations</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Angular Material style sheet -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="../ui/bower_components/bootstrap/dist/css/bootstrap-theme.css">
    <link rel="stylesheet" href="../ui/bower_components/bootstrap/dist/css/bootstrap.css">

    <script type="text/javascript" src="../ui/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="../ui/bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <link rel="stylesheet" href="../plugins/jQueryUI/jquery-ui.css_1.12.1">
    <link rel="stylesheet" href="../plugins/jQueryUI/jquery-ui.css_them1.12.1.css">
    <script src="../plugins/jQueryUI/jquery-1.12.4.js"></script>
    <script src="../plugins/jQueryUI/jquery-ui.js_1.12.1.js"></script>
    <script src="../plugins/jQueryUI/jquery.validate.min.js"></script>
    <script src="../plugins/jQueryUI/additional-methods.min.js"></script>
    <link rel="stylesheet" href="../ui/dist1/css/AdminLTE.css" />
    <script>
        function callView() {
            var viewPath = '/#/initiatePayment';
            var servarPath=window.location.origin;
            window.location = window.location.origin+viewPath;
        }
    </script>
    <style>
        .main-footer {
            position: fixed;
            bottom: 0;
            width: 1250px;
        }

        .background-object {
            position: fixed;
            top: 0;
            bottom: 0;
            width: 1250px;
            background-color: rgba(247, 251, 218, 0.30);
            z-index: -1;
        }
    </style>

</head>

<body class="hold-transition skin-yellow layout-boxed layout-top-nav" ng-app="abpib">
    <div class="wrapper">
        <div class="background-object1"></div>
        <header class="main-header">
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a ui-sref="initiatePayment" class="custLogo navbar-brand"><img src="../ui/dist1/img/vector-smart-object.png" class="" alt="User Image"></a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                    </div>
                </div>
            </nav>
        </header>
        <div id="success">
            <div class="row">
                <div class="col-sm-10">&nbsp</div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-link"  onClick="return callView();"><i class="glyphicon glyphicon-chevron-left"></i><span class="grid-text-font"style="color: #337ab7">Pay More Invoices</span></button>
                </div>
            </div>

        <c:choose>
                     <c:when test = "${status eq 'Success'}">
                        <%-- Success section --%>
                        <div style="text-align: center; margin-bottom: 50px;" class="box-body">
                                            <img src="../ui/dist1/img/SUCCESS.png" style="margin-top: 70px;" class="img-circle" alt="User Image">
                                            <h4>
                                                <b>SUCCESS</b></h4>
                                            <p style="margin-bottom: 70px;">
                                                Payment made successfuly</p>

                                        </div>

                     </c:when>


                     <c:otherwise>
                        <%-- Failure section --%>
                        <div style="text-align: center; margin-bottom: 50px;" class="box-body">
                                            <img src="../ui/dist1/img/failure.png" style="margin-top: 70px;" class="img-circle" alt="User Image">
                                            <h4>
                                                <b>FAILURE</b></h4>
                                            <p style="margin-bottom: 70px;">
                                                Payment failure</p>
                                                <c:out value = "${errCode}"/> :: <c:out value = "${errDesc}"/>

                                        </div>

                     </c:otherwise>
                  </c:choose>
            </div>
       <%-- <footer class="main-footer">
            <div class="container"><strong>Copyright &copy; 2014-2016.</strong> All rights reserved.</div>
        </footer>--%>
    </div>
</body>
</html>
