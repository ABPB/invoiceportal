    <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
        <%@ page import="java.util.*" %>

        <!DOCTYPE html>
        <head lang="en">
        <title>ABPB Operations</title>
        <%-- <!--/*/ <th:block th:include="fragments/headerinc :: head"></th:block> /*/--> --%>
        <%--   <jsp:include page="fragments/headerinc.jspx" /> --%>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
           <link rel="stylesheet" href="../ui/app.css">
        </head>

        <body class="hold-transition skin-blue layout-top-nav">
        <div class="wrapper">
        <%--  <!--/*/ <th:block th:include="fragments/header :: header"></th:block> /*/--> --%>
        <%--  <jsp:include page="fragments/header.jsp" />
         <div class="content-wrapper">
             <div class="container">
                 <section class="content-header">
                     <h1>
                         ABPB Operations
                     </h1>
                     <ol class="breadcrumb">
                         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                         <li class="active">ABPB Operations</li>
                     </ol>
                 </section>
           --%>
        <section class="content">
            <%

            				if(request.getAttribute("uploadStatus")!=null&& ("Success").equalsIgnoreCase((String)request.getAttribute("uploadStatus"))){%>

        File Uploaded Successfully
            <%}else if(request.getAttribute("uploadStatus")!=null&& ("Failed").equalsIgnoreCase((String)request.getAttribute("uploadStatus"))){%>
        File Upload Failed
            <%}%>
        <form id="myForm" th:object="${merchant}" method="post">
        <div class="box">
        <div class="box-body">
        <div class="form-group">
        <div class="col-md-12">
        <div class="box">
        <form method="POST" enctype="multipart/form-data" action="/uploadFile" NAME="form1">
        <div style="padding: 0px !important;" class="box-body">
        <div class="row" style="margin-bottom:10px">
        <div class="col-sm-12">
        <p class="mandates-upload">Upload Files</p>
        </div>
        </div>
        <div class="row">
        <div class="col-md-6">
        <div class="form-group" style="margin-bottom:0px">
        <input type="file" id="upload" class="form-control" name="file" />
        <i>Please upload in .zip format only. (both ACK and RES files) </i><br/>
        </div>
        </div>
        <div class="col-md-2">
        <input type="submit" class="form-control" id="fileUploaad" value="Upload" />
        </div>
        </div>
        <div class="row">
        <div class="col-sm-12">
        <p class="mandates-upload">Operations Download</p>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
        <li class="active"><a id="tab1" data-toggle="tab" href="/uploadedFiles/ACHDEBIT"><p class="mandates-upload">Debit
        Transaction Files</p></a></li>

        </ul>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-sm-12">
        <div class="tab-content">
        <div class="tab-pane active" id="tab_1">

            <%
                                                                                    String fileName;
                                                                                    String fileName1;
                                                                                    String strArr[];
                                                                                    ArrayList al=(ArrayList)request.getAttribute("files");
                                                                                    if(al!=null&&al.size()>0){
                                                                                    for(Object object:al){
                                                                                    fileName=(String)object;
                                                                                    fileName1=fileName;
                                                                                    fileName=fileName.split("/")[fileName.split("/").length-1];
                                                                                    %>

        <div class="row">


        <div class="col-md-4">
        <label><%=fileName%></label>
        </div>
        <div class="col-md-4">
        <a class="glyphicon glyphicon-download-alt" href="<%=fileName1%>" aria-hidden="true" style="cursor:
        pointer"></a> <span class="glyphicon-class"></span>
        </div>
        </div>

            <%} }%>

        </div>
        </div>
        </div>
        </div>

        </div>
        </form>
        </div> </div>
        </div>
        </div>

        </div>
        </form>
        </section>
        </div>
        </div>
        </div>
        </body>
        </html>
