<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PG Redirection</title>
</head>
<body>
<form name="PGRedirection" method="POST">
	<input name="msg" type="hidden" value='${msg}' />
	<input name="txtPayCategory" type="hidden" value='${paymentMode}' />
</form>

<script>
    //var redirectURL = '<%=request.getAttribute("returnURL")%>';
    var redirectURL = '${pgURL}';
    var status = '${status}';
    //alert("redirectURL   " + redirectURL);
    document.PGRedirection.action=redirectURL;
    if(status=="Success"){
        document.PGRedirection.submit();
    }else{
        alert("Not able to get details to redirect to PG" );
    }
</script>

</body>
</html>