<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html ng-app="abpib">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Angular Material style sheet -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css"
          integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
    <link rel="stylesheet" href="../ui/bower_components/bootstrap/dist/css/bootstrap-theme.css">
    <link rel="stylesheet" href="../ui/bower_components/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/4.2.2/ui-grid.css">

    <script type="text/javascript" src="../ui/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="../ui/bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <!-- Angular Material requires Angular.js Libraries -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
    <script src="../ui/bower_components/angular-ui-router/release/angular-ui-router.js"></script>
    <style>
        .login-container {
            max-width: 300px;
            width: 100%;
            margin: 0 auto;
        }

        .make-center {
            text-align: center;

        }

    </style>
</head>

<body style="background-color: #efefef;">
<div class="container-fluid" ng-controller="loginCtrl as vm">
    <div class="row" style="height: 10vh;">
        <div class="col-sm-12 col-xs-12">
            &nbsp;
        </div>
    </div>

    <div class="make-center">
        <div class="card login-container">
            <div class="card-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-2 col-xs-2">&nbsp;</div>
                        <div class="col-sm-8 col-xs-8" style="object-fit: cover;padding:0px;">
                            <img src="../ui/images/abbp-logo.png">
                        </div>
                        <div class="col-sm-2 col-xs-2">&nbsp;</div>
                    </div>
                    <c:if test="${not empty error}">
                        <span class="error">${error}</span>
                    </c:if>
                    <form method="POST" action="<c:url value='/login' />" class="form-signin">

                        <div class="row"
                             style="background-color: rgba(140, 158, 255, 0.13);padding: 5px;margin-bottom: 5px;">
                            <div class="col-sm-12 col-xs-12" style="text-align: center">
                                Invoice Management
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-2 col-xs-2">&nbsp;</div>
                            <div class="col-sm-8 col-xs-8" style="text-align: center">
                                Login
                            </div>
                            <div class="col-sm-2 col-xs-2">&nbsp;</div>
                        </div>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-12 col-xs-12">
                                <input type="text" class="form-control" id="username" name="username"
                                       placeholder="User name"
                                       ng-model="vm.user.userName" required style="font-size: 12px;
    font-family:  Fira sans-serif;">
                                <%--<span class="glyphicon glyphicon-user form-control-feedback"
                                      style="padding-right: 2em;font-size: 10px;color: #808282;"></span>--%>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-12 col-xs-12">
                                <input type="password" class="form-control" id="password" name="password"
                                       ng-model="vm.user.passWord"
                                       placeholder="Password" required style="font-size: 12px;
    font-family: Fira sans-serif;">
                             <%--   <span class="glyphicon glyphicon-lock form-control-feedback"
                                      style="padding-right: 2em;font-size: 10px;color: #808282;"></span>--%>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-12 col-xs-12">
                                <button class="btn  btn-lg btn-block" type="submit">Login</button>

                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12" style="text-align: center">
                                <a>Forgot Password?</a>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12" style="text-align: center">
                                Copyrights@ABPB 2017
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                &nbsp;
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
</div>


</body>

</html>