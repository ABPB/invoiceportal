<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html lang="en" ng-app="abpib" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html lang="en" ng-app="abpib" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html lang="en" ng-app="abpib" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" ng-app="abpib" class="no-js"> <!--<![endif]-->

        <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>Invoice portal</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

        <link href="../ui/bower_components/googleFonts-fira+sans/fonts-google.css" rel="stylesheet">
        <link rel="stylesheet" href="../ui/bower_components/html5-boilerplate/dist/css/normalize.css">
        <script src="../ui/bower_components/html5-boilerplate/dist/js/vendor/modernizr-2.8.3.min.js"></script>
        <%--<script type="text/javascript" src="../ui/bower_components/jquery/dist/jquery.min.js"></script>--%>

        <script type="text/javascript" src="../ui/bower_components/jquery/dist/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>

        <link rel="stylesheet" href="../ui/bower_components/bootstrap/dist/css/bootstrap-theme.css" type="text/css">
            <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" type="text/css">
            <!-- Angular Material style sheet -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css"
                  integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">

            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css">

        <link rel="stylesheet" href="../ui/bower_components/dataTableBootstrap/dataTables.bootstrap.min.css" type="text/css">



    <link rel="stylesheet" href="../ui/bower_components/font-awesome/font-awesome.min.css"/>
    <link rel="stylesheet" href="../ui/bower_components/ionicons/ionicons.min.css" />

    <link rel="stylesheet"
          href="../ui/bower_components/angular-material/angular-materialv1_1_0.min.css">
    <link rel="stylesheet"
          href="../ui/bower_components/font-awesome/font-awesome4_7_0.min.css">

    <link rel="stylesheet" href="../ui/bower_components/ui-grid/ui-grid.min.css">
    <link rel="stylesheet" href="../ui/bower_components/html5-boilerplate/dist/css/main.css">
    <link rel="stylesheet" href="../ui/app.css">

    <link rel="stylesheet" href="../ui/bower_components/angular-growl/angular-growl.css">


    <link rel="stylesheet" href="../plugins/iCheck/all.css" />
    <link rel="stylesheet" href="../ui/dist1/css/AdminLTE.css" />
    <link rel="stylesheet" href="../ui/dist1/css/skins/_all-skins.css" />

    <link rel='stylesheet' href="../ui/dist1/css/ngPopup.css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        var userName = '<c:out value="${userName}"/>';
        var userType = '<c:out value="${userType}"/>';
        var entityId = '<c:out value="${entityId}"/>';
        var merchantId ='<c:out value="${merchantId}"/>';
        var roleId ='<c:out value="${roleId}"/>';

        sessionStorage.setItem('userName',userName);
        sessionStorage.setItem('userType',userType);
        sessionStorage.setItem('entityId',entityId);
        sessionStorage.setItem('merchantId',merchantId);
        sessionStorage.setItem('roleId',roleId);

    </script>
</head>
<body style="background-color:#efefef">
<div ui-view="header"></div>
<div growl></div>
<div ui-view="template"></div>
<div ui-view="footer" style="position: fixed; bottom: -1px; width: 100%"></div>
<ng-pop-up option='ngPopupConfig'></ng-pop-up>




<script src="../ui/bower_components/angular/angular.js"></script>
<script src="../ui/bower_components/chart.js/dist/Chart.js"></script>
<script src="../ui/bower_components/angular-ui-router/release/angular-ui-router.js"></script>
<script src="../ui/bower_components/angular-chart.js/angular-chart.js"></script>
<script src="../ui/bower_components/ui-grid/ui-grid.min.js"></script>
<script src="../ui/bower_components/underscore/underscore-min.js"></script>
<script src="../ui/bower_components/moment/moment.js"></script>


<script src="../ui/bower_components/angular-file-saver/dist/angular-file-saver.bundle.min.js"></script>

<script src="../ui/bower_components/blob-polyfill/Blob.js"></script>



<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>



<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-aria.min.js"></script>


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-messages.min.js"></script>
<!-- Angular Material Library -->
<script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.0.0/ui-bootstrap-tpls.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-growl-2/0.7.9/angular-growl.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-utils/0.1.1/angular-ui-utils.min.js"></script>
<script src="../ui/bower_components/angular-datatables/dist/angular-datatables.min.js"></script>

<script src="../ui/app.js"></script>


<!--serivces -->
<script src="../ui/services/adithyaServices.js" type="text/javascript"></script>
<script src="../ui/services/adtiyasharedService.js" type="text/javascript"></script>
<script src="../ui/services/userStroage.js" type="text/javascript"></script>
<script src="../b2bAPIServices.js"></script>
<script src="../b2bSharedService.js"></script>
<script src="../payments/paymentsAPI.js"></script>
<script src="../payments/paymentsSharedDataServices.js"></script>

<!--Controller Defined -->
<script src="../ui/controller/loginCtrl.js" type="text/javascript"></script>
<script src="../ui/controller/dashboardCtrl.js" type="text/javascript"></script>
<script src="../ui/controller/footerCtrl.js" type="text/javascript"></script>
<script src="../ui/controller/headerCtrl.js" type="text/javascript"></script>


<script type="text/javascript" src="../ui/controller/initiate/imageViewerCtrl.js"></script>
<script type="text/javascript" src="../ui/controller/initiate/initiatePaymentCtrl.js"></script>

<script type="text/javascript" src="../ui/controller/operations/operationCtrl.js"></script>
<script type="text/javascript" src="../ui/controller/operations/paymentCtrl.js"></script>
<script type="text/javascript" src="../ui/controller/operations/mandatesCtrl.js"></script>

<script type="text/javascript" src="../ui/controller/fileupload/batchuploadCtrl.js"></script>

<script type="text/javascript" src="../ui/controller/maker/invoiceMakerCtrl.js"></script>
<script type="text/javascript" src="../ui/controller/maker/invoiceApprovedMakerCtrl.js"></script>
<script type="text/javascript" src="../ui/controller/maker/invoiceMakerpendingCtrl.js"></script>


<script type="text/javascript" src="../ui/controller/checker/invoiceCheckerpendingCtrl.js"></script>
<script type="text/javascript" src="../ui/controller/checker/invoiceCheckerCtrl.js"></script>
<script type="text/javascript" src="../ui/controller/checker/invoiceApprovedcheckerCtrl.js"></script>



<script type="text/javascript" src="../ui/dist1/js/ngPopup.js"></script>
<script type="text/javascript" src="../ui/bower_components/download.js"></script>


<script src="../distributor-details/distributorDetails.ctrl.js"></script>
<script src="../merchant-onboarding/merchantOnboarding.ctrl.js"></script>

<script src="../payments/fromAccount/fromAccount.ctrl.js"></script>

<script src="../payments/pages/payment/payment.ctrl.js"></script>
<script src="../payments/pages/pay/pay.ctrl.js"></script>

<script src="../payments/pages/collect/collect.ctrl.js"></script>
<script src="../payments/pages/selectPayment/selectionRoot/paymentSelection.ctrl.js"></script>
<script src="../payments/pages/selectPayment/quickPay/quickPay.ctrl.js"></script>
<script src="../payments/pages/choosePayment/choosePayment.ctrl.js"></script>

<script src="../payments/pages/selectPayment/cards/cards.ctrl.js"></script>
<script src="../payments/pages/selectPayment/vpaTransfer/vpaTransfer.ctrl.js"></script>
<script src="../payments/pages/selectPayment/internetBanking/internetBanking.ctrl.js"></script>
<script src="../success/success.ctrl.js"></script>






<script src="../top-nav-view/topNavView.ctrl.js"></script>
<script src="../bank-details/bankDetails.ctrl.js"></script>
<script src="../upi-details/upiDetails.ctrl.js"></script>
<script src="../confirmation/confirmation.ctrl.js"></script>
<script src="../success/success.ctrl.js"></script>

<script src="../ui/services/httpService.js"></script>

<script type="text/javascript" src="../plugins/fastclick/fastclick.js"></script>
<script type="text/javascript" src="../plugins/iCheck/icheck.min.js"></script>


</body>
</html>
