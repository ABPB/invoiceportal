'use strict';

(function(){
    angular.module('abpib').controller('UpiDetailsCtrl', ['$scope', '$state', '$rootScope', 'B2bSharedServices', 'B2bAPIServices','adithyaServices','userStroage', UpiDetailsCtrl]);

    function UpiDetailsCtrl($scope, $state, $rootScope, B2bSharedServices, B2bAPIServices,adithyaServices,userStroage) {
        var token=userStroage.get('access_token');
        var access_token= "bearer" + " " + token;
        $scope.upiDetailsArray = B2bSharedServices.distributorDetails.upiDetails;
        if($scope.upiDetailsArray.length != 0)
        {
            $scope.activeUpiDetail = $scope.upiDetailsArray[$scope.upiDetailsArray.length-1];
            if($scope.activeUpiDetail.aud_upa_handler.indexOf("@birla") != -1)
                $scope.activeUpiDetail.aud_upa_handler = $scope.activeUpiDetail.aud_upa_handler.substr(0, $scope.activeUpiDetail.aud_upa_handler.indexOf("@birla"));
        }
        console.log($scope.upiDetailsArray);
        

        $scope.addAnotherHandler = function(){
            for(var i=0; i<$scope.upiDetailsArray.length; i++)
            {
                if(!$scope.upiDetailsArray[i].isVerified) 
                {
                    alert("Please verify the existing UPI handler");
                    return;
                }
            }  

            $scope.activeUpiDetail = B2bSharedServices.createNewUPIDetails();
            B2bSharedServices.distributorDetails.upiDetails.push($scope.activeUpiDetail);
            
        }

        $scope.onVerifyClick = function(upiDetail) {
            
            if(upiDetail.aud_upa_handler.length > 0)
            {
                upiDetail.aud_upa_handler += '@birla';
                var verifyArray = $scope.upiDetailsArray;
                
                for(var i=0; i<verifyArray.length; i++){
                    var currentUPIDetail = verifyArray[i];
                    if(currentUPIDetail != $scope.activeUpiDetail && currentUPIDetail.aud_upa_handler ==  upiDetail.aud_upa_handler){
                        alert("This UPI handle is already added");
                        upiDetail.aud_upa_handler = upiDetail.aud_upa_handler.substr(0, upiDetail.aud_upa_handler.indexOf("@birla"));
                        return;
                    }
                }

                $rootScope.ngPopupConfig.isShow = true;
                adithyaServices.verifyUPIHandler(access_token,upiDetail.aud_upa_handler).then(function(data){
                    $rootScope.ngPopupConfig.isShow = false;
                    console.log(data);
                    if(data.validationStatus != "Success")
                    {
                        $scope.activeUpiDetail.aud_upa_handler = $scope.activeUpiDetail.aud_upa_handler.substr(0, $scope.activeUpiDetail.aud_upa_handler.indexOf("@birla"));
                        alert("Please enter a valid UPI handle");
                        return;
                    }
                    //$scope.$apply(function(){

                    upiDetail.isVerified = true;
                    //})

                },function(error){
                    $scope.activeUpiDetail.aud_upa_handler = $scope.activeUpiDetail.aud_upa_handler.substr(0, $scope.activeUpiDetail.aud_upa_handler.indexOf("@birla"));
                    alert("There is an error in service call. Please contact administrator")
                    console.log(error);
                    $rootScope.ngPopupConfig.isShow = false;
                });

            }

            else
            {
                alert("Please enter valid UPI handler");
            }
        }
        
        $scope.onDefaultFlagChange = function(upiDetail) {

            console.log(upiDetail)

            if(upiDetail.aud_upa_dft_flg == 'Y')
            {
                for(var i=0; i<$scope.upiDetailsArray.length; i++)
                {
                    if($scope.upiDetailsArray[i] !== upiDetail) $scope.upiDetailsArray[i].aud_upa_dft_flg = 'N';
                }   
            }
        }

        $scope.onEditBtnClicked = function(upiDetail) {
            
            if($scope.activeUpiDetail.isVerified == false)
            {
                var confirmBeforeClear = confirm("This will clear the un verified handler");
                if (confirmBeforeClear == false) return;

                for(var i = 0, len = $scope.upiDetailsArray.length; i < len; i++) {
                    if ($scope.upiDetailsArray[i] === $scope.activeUpiDetail) {
                        $scope.upiDetailsArray.splice(i,1);
                    }
                }    
            }

            $scope.activeUpiDetail = upiDetail;
            
            if(upiDetail.aud_upa_handler.indexOf("@birla") != -1)
            
                // upiDetail.aud_upa_handler = upiDetail.aud_upa_handler.substr(0, upiDetail.aud_upa_handler.indexOf("@birla"));
                upiDetail.aud_upa_handler = upiDetail.aud_upa_handler;
                
        }
        
        $scope.onDeleteBtnClicked = function(upiDetail) {
            for(var i = 0, len = $scope.upiDetailsArray.length; i < len; i++) {
                if ($scope.upiDetailsArray[i] === upiDetail) {
                    $scope.upiDetailsArray.splice(i,1);
                }
            }   
        }

        $scope.onUPIEditBtnPressed = function(upiDetail) {
            upiDetail.isVerified = false;
            if(upiDetail.aud_upa_handler.indexOf("@birla") != -1)
                upiDetail.aud_upa_handler = upiDetail.aud_upa_handler.substr(0, upiDetail.aud_upa_handler.indexOf("@birla"));
        }

        $scope.onSaveBtnPressed = function(){
            
            var bankDetailsArray = B2bSharedServices.distributorDetails.bankDetails;
            var upiDetailsArray = B2bSharedServices.distributorDetails.upiDetails;

            var bankErrorMessage = "";
            var upiErrorMessage = "";

            var hasCreditAccount = false;
            var hasDebitAccount = false;

            for(var i=0; i<bankDetailsArray.length; i++) {
                var bankDetail = bankDetailsArray[i];
                var validResult = B2bSharedServices.checkIsValidBankDetail(bankDetail);
                bankDetail.isValid = validResult.valid;

                if(!validResult.valid && bankDetailsArray.length > 1) 
                {
                    bankDetailsArray.splice(i, 1);
                    continue;
                }
                
                else if(!validResult.valid) bankErrorMessage = validResult.message;

                else bankErrorMessage = "";

                if(validResult.valid)
                {
                    if(bankDetail.abd_crd_dbt_flg == "C") hasCreditAccount = true;
                    
                    else if(bankDetail.abd_crd_dbt_flg == "D") hasDebitAccount = true;
    
                    else if (bankDetail.abd_crd_dbt_flg == "B") {
                        hasCreditAccount = true;
                        hasDebitAccount = true;
                    }
                }
            }

            for(var i=0; i<upiDetailsArray.length; i++)
            {
                if(!upiDetailsArray[i].isVerified && upiDetailsArray.length > 1) {
                    upiDetailsArray.splice(i, 1);
                    continue;
                }
                
                else if(!upiDetailsArray[i].isVerified) upiErrorMessage = "Please verify the existing UPI handler";

                else upiErrorMessage = "";

                if(upiDetailsArray[i].isVerified)
                {
                    console.log(upiDetailsArray[i].aud_upa_crddbt_flg);
                    if(upiDetailsArray[i].aud_upa_crddbt_flg == "C") hasCreditAccount = true;
                    
                    else if(upiDetailsArray[i].aud_upa_crddbt_flg == "D") hasDebitAccount = true;
    
                    else if (upiDetailsArray[i].aud_upa_crddbt_flg == "B") {
                        hasCreditAccount = true;
                        hasDebitAccount = true;
                    }
                }
            }

            console.log(hasCreditAccount);

            if(bankErrorMessage != "" && upiErrorMessage != "") {
                alert("Please enter valid UPI handler or bank details");
            }

            else if(!hasCreditAccount) {
                alert("Please add atleast one credit account");
            }
            
            else if(!hasDebitAccount) {
                alert("Please add atleast one debit account");
            }

            else {
                console.log("Success");
                $state.go('topnavview.confirmation');
            }
        }
    }

})();