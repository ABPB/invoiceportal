(function () {
    'use strict';
    var Adbapp = angular.module('abpib');
    Adbapp.factory('adithyaServices', function adithyaServices($http,CONSTANTS,$q) {
        var url = "https://13.126.208.204:8441/oauth/token?grant_type=client_credentials";
        var deferred = $q.defer();
        return {
            loginNec: function (encode) {
                return $http({
                    method: 'POST',
                    url: url,
                    headers: {
                        "Authorization": "Basic " + encode,
                        "Content-Type": "application/json",
                        "Accept": "application/json"
                    }

                }).then(function (result) {

                    return result.data;
                });
            },
            getBankDetailsForIFSC:function(token,ifscCode){
                var url=CONSTANTS.SERVER_PATH+"merchantOnboarding/bankIFSC?";
                return $http({
                    method: "GET",
                    url: url,
                    params: {ifscCode: ifscCode},
                    headers: {
                        "Authorization": token
                    }
                }).then(function (result) {
                    return result.data;
                });
            },
            //payment maker section
            getInvoiceDetails: function (token,merchantId,entityId) {
                var data= {};
                data.merchantId=merchantId;
                data.establishmentId=entityId;
                var url=CONSTANTS.SERVER_PATH+'invoice/getInvoiceDetails';
                return $http({
                    method: "POST",
                    data:data,
                    url: url,
                    headers: {
                        "Authorization": token,
                        "Content-Type": "application/json",
                        "Accept": "application/json"
                    }
                }).then(function (result) {
                    return result.data;
                })

            },
            verifyUPIHandler:function(token,upiName){
                var url=CONSTANTS.SERVER_PATH+"upiDetails?";
                return $http({
                    method: "GET",
                    url: url,
                    params: {upiName: upiName},
                    headers: {
                        "Authorization": token
                    }
                }).then(function (result) {
                    return result.data;
                });
            },
            postRegistrationDetails:function(token,registrationData){
                var url=CONSTANTS.SERVER_PATH+'merchantOnboarding/register';
                var tempStr = JSON.stringify(registrationData);
                var newData = JSON.parse(tempStr);


                for(var i=0; i<newData.bankDetails.length; i++) {
                    delete newData.bankDetails[i].accountNumberValidation;
                    delete newData.bankDetails[i].isValid;
                }
                for(var j=0; j<newData.upiDetails.length; j++) {
                    delete newData.upiDetails[j].accountNumberValidation;
                    delete newData.upiDetails[j].isValid;
                }

                delete newData.activeBankDetails;
                delete newData.activeUpiDetail;

                return $http({
                    method: "POST",
                    url: url,
                    data:newData,
                    headers: {
                        "Authorization": token,
                        "Content-Type": "application/json",
                        "Accept": "application/json"
                    }
                }).then(function (result) {
                    return result.data;
                });
            },
            getPamentsModes:function(token,merchantId,establishmentId,paymentMode){
                var url=CONSTANTS.SERVER_PATH+"payment/paymentModes?";
                return $http({
                    method: "GET",
                    url: url,
                    params: {entityId:merchantId,establishmentId:establishmentId,paymentMode:paymentMode},
                    headers: {
                        "Authorization": token
                    }
                }).then(function (result) {
                    return result.data;
                });
            },
            createPayment:function(token,data){
                var url=CONSTANTS.SERVER_PATH+'payment/create';
                return $http({
                    method: "POST",
                    url: url,
                    data:data,
                    headers: {
                        "Authorization": token,
                        "Content-Type": "application/json",
                        "Accept": "application/json"
                    }
                }).then(function (result) {
                    return result.data;
                });
            },
            //invoice maker and checker
            getcheckerInvoice:function(token,data){
                var url=CONSTANTS.SERVER_PATH+'invoice/getInvoiceForMakerChecker';
                return $http({
                    method: "POST",
                    url: url,
                    data:data,
                    headers: {
                        "Authorization": token,
                        "Content-Type": "application/json",
                        "Accept": "application/json"
                    }
                }).then(function (result) {
                    return result.data;
                });
            },
            //payment checker section
            updatecheckerInvoice: function (datas,token) {
                var url=CONSTANTS.SERVER_PATH+'invoice/updateInvoiceForMakerChecker';
                return $http({
                    method: 'POST',
                    url: url,
                    headers: {
                        "Authorization": token,
                        "Content-Type": "application/json",
                        "Accept": "application/json"
                    },
                    data:datas
                }).then(function (result) {
                    return result.data;
                });
            },
            //invoice maker and checker
            getmakerInvoice:function(token,data){
                var url=CONSTANTS.SERVER_PATH+'invoice/getInvoiceForMakerChecker';
                return $http({
                    method: "POST",
                    url: url,
                    data:data,
                    headers: {
                        "Authorization": token,
                        "Content-Type": "application/json",
                        "Accept": "application/json"
                    }
                }).then(function (result) {
                    return result.data;
                });
            },
            //payment checker section
            updatemakerInvoice: function (datas,token) {
                var url=CONSTANTS.SERVER_PATH+'invoice/updateInvoiceForMakerChecker';
                return $http({
                    method: 'POST',
                    url: url,
                    headers: {
                        "Authorization": token,
                        "Content-Type": "application/json",
                        "Accept": "application/json"
                    },
                    data:datas
                }).then(function (result) {
                    return result.data;
                });
            }
        };
    });
})();
