(function () {
    var Adbapp = angular.module('abpib');
    Adbapp.service('APIInterceptor', ["$q",'$location','$state','userStroage','$window','growl', function ($q,$location,$state,userStroage,$window,growl) {
       /* var access_Token;*/
        // defining a method, which should will be called before sending the request.
        this.request = function (config) {

            // If you want to do some async work before sending the request then use $q service and return promise.
            var deferred = $q.defer();
            setTimeout(function () {
              /*  access_Token=userStroage.get('access_token');
             if(access_Token ==undefined){
                 console.log('called');
             }*/
                deferred.resolve(config);
            }, 100);
            return deferred.promise;
        };
        // defining a method, which will be called after response received.
        this.response = function (response) {
           // console.log("Executing after response of APIInterceptor");
            // If you want to do some async work after response received then use $q service and return promise.
            var deferred = $q.defer();
            setTimeout(function () {
                //console.log("Executed after response of APIInterceptor");
                deferred.resolve(response);
            }, 100);
            return deferred.promise;
        };
        this.responseError=function (error) {
            if (error.status === 401) {
                growl.error('Invalid Access Token');
               $state.go('login');
              //  window.location.href = 'https://localhost:8070/#/';
                //$location.path('/login')
            }else if(error.status === 500){
                growl.warning('Internal Error Please Contact  Administrator');
            }
        }
    }]);


})();