(function () {
    'use strict';
    var Adbapp = angular.module('abpib');
    Adbapp.controller('invoiceMakerpendingCtrl', ['$scope', '$state', '$filter', 'userStroage', 'adithyaServices', '$timeout', 'growl', '$window','adtiyasharedService','DTOptionsBuilder',
        function ($scope, $state, $filter, userStroage, adithyaServices, $timeout, growl, $window,adtiyasharedService,DTOptionsBuilder) {
            var va = this;
            va.hoverIn = function(index){
                va.editIndex=index;
                va.hoverEdit = true;
            };
            va.hoverOut = function(index){
                va.editIndex=index;
                va.hoverEdit = false;
            };
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('bLengthChange', true)
                .withOption('aLengthMenu', [5, 10, 15, 20]);


            $scope.currentNavItem = $state.current.name;
            var token = userStroage.get('access_token');
            var access_token = "bearer" + " " + token;

            va.todates = new Date();
            va.minDate = new Date(
                va.todates.getFullYear(),
                va.todates.getMonth() - 2,
                va.todates.getDate()
            );

            va.maxDate = new Date(
                va.todates.getFullYear(),
                va.todates.getMonth(),
                va.todates.getDate()
            );
            var obj = {};
            obj.merchantId = $window.sessionStorage.getItem('merchantId');
            obj.action = "INVOICE_MAKER_DISPLAY";
            obj.reportType ='PENDING';
            obj.fromDate='';
            obj.toDate='';
            va.changeDate=function (fdate,tdate) {
                if(fdate !=null && fdate !== undefined){
                    obj.fromDate= moment(fdate).format('DD-MMM-YYYY');
                }
                if(tdate !=null && tdate !==undefined){
                    obj.toDate= moment(tdate).format('DD-MMM-YYYY');
                }
                if((obj.fromDate !==undefined && obj.fromDate !=='')&&(obj.toDate !==undefined && obj.toDate !=='')){
                    if(new Date(obj.fromDate) <= new Date(obj.toDate)){
                        getMakerInvoicePendingDetails();
                    }
                    else{
                        alert('please Select  From Date and To Date Valid');
                    }

                }
            };
            function  getMakerInvoicePendingDetails() {
                $timeout(function () {
                    adithyaServices.getmakerInvoice(access_token, obj).then(function (obje) {
                        va.mandateTransactions = obje.pendingInvoices;
                        if (va.mandateTransactions.length > 0) {
                            _.each(va.mandateTransactions, function (obj) {
                                obj.flag = 'N';
                                obj.dueDate=moment(obj.dueDate).format('DD-MM-YYYY');
                                obj.remarks = '';
                            })
                        } else {
                            va.gridOptions.data = [];
                            va.getMessage = 'No Data Found';
                        }
                        va.transacTions=va.mandateTransactions;
                        //   va.gridOptions.data = va.mandateTransactions;
                        va.visibleProgress = false;
                    }).catch(function (reason) {
                        growl.warning('No Data Found');
                    });
                })

            }
            getMakerInvoicePendingDetails();

            va.viewDownloads = function (encodedata) {
                var viewPath = '/downloadImage';
                var currentPath = window.location.href.substr(0, window.location.href.indexOf('#') + 1);
                var fullPath = currentPath + viewPath;
                console.log(fullPath);
                // $window.open(fullPath, '_blank');
                $window.open(fullPath, '_blank').var1 = encodedata;

            };

            va.updateChecker = function (griddata) {
                for (var i = 0; i < griddata.data.length; i++) {
                    delete (griddata.data[i].$$hashKey);
                }
                var obj = {
                    "mkrckrFlag": "M",
                    "invoiceupdatedData": griddata.data
                };
                obj.merchantId = $window.sessionStorage.getItem('merchantId');
                va.visibleProgress = true;
                adithyaServices.updatemakerInvoice(obj, access_token).then(function (d) {
                    if(d.status === 'SUCCESS' || d.status === 'Success' || d.status === 'success'){
                        growl.success('Sent Successfully');
                        var obj = {};
                        obj.merchantId = $window.sessionStorage.getItem('merchantId');
                        obj.action = "INVOICE_MAKER_DISPLAY";
                        adithyaServices.getmakerInvoice(access_token, obj).then(function (d) {
                            va.mandateTransactions = d.invoiceList;
                            /* va.utilityCode = d.utilityCode;
                             va.sponoserCode = d.sponoserCode;
                             va.sysDate = d.sysDate;*/
                            if (va.mandateTransactions.length > 0) {
                                _.each(va.mandateTransactions, function (obj) {
                                    obj.flag = 'N';
                                    obj.remarks = '';

                                })
                            }
                            va.gridOptions.data = va.mandateTransactions;
                            va.visibleProgress = false;
                        }).catch(function (reason) {
                            growl.warning('No Data Found');
                        });
                    }else{
                        growl.warning('Sending faild');
                    }

                }).catch(function (reason) {
                    growl.error('Internal Error Please Contact  Administrator');
                });


            };

        }
    ]);
    var ModalInstanceCtrl = function ($scope, $modalInstance, data) {
        $scope.data = data;
        if ($scope.data.status == 'SUCCESS' && $scope.data.status != undefined) {
            $scope.data.message = "Mandates Uploaded Successfully";
            $scope.data.mode = 'success';
        } else {
            $scope.data.mode = 'danger';
            $scope.data.message = "Mandates Upload Failed";
        }

        $scope.close = function (/*result*/) {
            $modalInstance.close($scope.data);
        };
    };
})();