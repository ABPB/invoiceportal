(function () {
    'use strict';
    var Adbapp = angular.module('abpib');
    Adbapp.controller('invoiceApprovedMakerCtrl', ['$scope', '$state', '$filter', 'userStroage', 'adithyaServices', '$timeout', 'growl', '$window','$location','adtiyasharedService','DTOptionsBuilder',
        function ($scope, $state, $filter, userStroage, adithyaServices, $timeout, growl, $window,$location,adtiyasharedService,DTOptionsBuilder) {
            var vactrl = this;
            vactrl.hoverIn = function(index){
                vactrl.editIndex=index;
                vactrl.hoverEdit = true;
            };
            vactrl.hoverOut = function(index){
                vactrl.editIndex=index;
                vactrl.hoverEdit = false;
            };
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('bLengthChange', true)
                .withOption('aLengthMenu', [5, 10, 15, 20]);


            $scope.currentNavItem = $state.current.name;
            var token = userStroage.get('access_token');
            var access_token = "bearer" + " " + token;

            vactrl.todates = new Date();
            vactrl.minDate = new Date(
                vactrl.todates.getFullYear(),
                vactrl.todates.getMonth() - 2,
                vactrl.todates.getDate()
            );

            vactrl.maxDate = new Date(
                vactrl.todates.getFullYear(),
                vactrl.todates.getMonth(),
                vactrl.todates.getDate()
            );

            var obj = {};
            obj.merchantId = $window.sessionStorage.getItem('merchantId');
            obj.action = "INVOICE_MAKER_DISPLAY";
            obj.reportType ='APPROVED';
            obj.fromDate='';
            obj.toDate='';
            vactrl.changeDate=function (fdate,tdate) {
                if(fdate !=null && fdate !== undefined){
                    obj.fromDate= moment(fdate).format('DD-MMM-YYYY');
                }
                if(tdate !=null && tdate !==undefined){
                    obj.toDate= moment(tdate).format('DD-MMM-YYYY');
                }
                if((obj.fromDate !==undefined && obj.fromDate !=='')&&(obj.toDate !==undefined && obj.toDate !=='')){
                    if(new Date(obj.fromDate) <= new Date(obj.toDate)){
                        getMakerInvoiceApprovedDetails();
                    }
                    else{
                        alert('please Select  From Date and To Date Valid');
                    }

                }
            };
            function getMakerInvoiceApprovedDetails() {
                $timeout(function () {
                    adithyaServices.getmakerInvoice(access_token, obj).then(function (obje) {
                        vactrl.mandateTransactions = obje.approvedInvoices;
                        if (vactrl.mandateTransactions.length > 0) {
                            _.each(vactrl.mandateTransactions, function (obj) {
                                obj.flag = 'N';
                                obj.remarks = '';
                                obj.dueDate=moment(obj.dueDate).format('DD-MM-YYYY');
                            })
                        } else {
                            vactrl.gridOptions.data = [];
                            vactrl.getMessage = 'No Data Found';
                        }
                        vactrl.transacTions = vactrl.mandateTransactions;
                        /* vactrl.gridOptions.data = vactrl.mandateTransactions;
                         adtiyasharedService.setDataservice(vactrl.mandateTransactions);*/
                        vactrl.visibleProgress = false;
                    }).catch(function (reason) {
                        growl.warning('No Data Found');
                    });
                })

            }
            getMakerInvoiceApprovedDetails();


            vactrl.viewDownloads = function (encodedata) {
                var viewPath = '/downloadImage';
                var currentPath = window.location.href.substr(0, window.location.href.indexOf('#') + 1);
                var fullPath = currentPath + viewPath;
                console.log(fullPath);
               // $window.open(fullPath, '_blank');
                $window.open(fullPath, '_blank').var1 = encodedata;

            };

          //  vactrl.gridOptions.data = sample;
            vactrl.updateChecker = function (griddata) {
                for (var i = 0; i < griddata.data.length; i++) {
                    delete (griddata.data[i].$$hashKey);
                }
                var obj = {
                    "mkrckrFlag": "M",
                    "invoiceupdatedData": griddata.data
                };
                obj.merchantId = $window.sessionStorage.getItem('merchantId');
                vactrl.visibleProgress = true;
                adithyaServices.updatemakerInvoice(obj, access_token).then(function (d) {
                    if(d.status === 'SUCCESS' || d.status === 'Success' || d.status === 'success'){
                        growl.success('Sent Successfully');
                        var obj = {};
                        obj.merchantId = $window.sessionStorage.getItem('merchantId');
                        obj.action = "INVOICE_MAKER_DISPLAY";
                        adithyaServices.getmakerInvoice(access_token, obj).then(function (d) {
                            vactrl.mandateTransactions = d.approvedInvoices;
                            if (vactrl.mandateTransactions.length > 0) {
                                _.each(vactrl.mandateTransactions, function (obj) {
                                    obj.flag = 'N';
                                    obj.remarks = '';

                                })
                            }
                            vactrl.gridOptions.data = vactrl.mandateTransactions;
                            vactrl.visibleProgress = false;
                        }).catch(function (reason) {
                            growl.warning('No Data Found');
                        });
                    }else{
                        growl.warning('Sending faild');
                    }

                }).catch(function (reason) {
                    growl.error('Internal Error Please Contact  Administrator');
                });


            };

        }
    ]);
    var ModalInstanceCtrl = function ($scope, $modalInstance, data) {
        $scope.data = data;
        if ($scope.data.status == 'SUCCESS' && $scope.data.status != undefined) {
            $scope.data.message = "Mandates Uploaded Successfully";
            $scope.data.mode = 'success';
        } else {
            $scope.data.mode = 'danger';
            $scope.data.message = "Mandates Upload Failed";
        }

        $scope.close = function (/*result*/) {
            $modalInstance.close($scope.data);
        };
    };
})();