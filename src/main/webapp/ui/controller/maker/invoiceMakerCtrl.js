(function () {
    'use strict';
    var Adbapp = angular.module('abpib');
    Adbapp.controller('invoiceMakerCtrl', ['$scope', '$state', '$filter', 'userStroage', 'adithyaServices', '$timeout', 'growl', '$window','adtiyasharedService','DTOptionsBuilder',
        function ($scope, $state, $filter, userStroage, adithyaServices, $timeout, growl, $window,adtiyasharedService,DTOptionsBuilder) {
            var vi = this;
            vi.transacTions=[];
            vi.hoverIn = function(index){
                vi.editIndex=index;
                vi.hoverEdit = true;
            };
            vi.hoverOut = function(index){
                vi.editIndex=index;
                vi.hoverEdit = false;
            };
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('bLengthChange', true)
                .withOption('aLengthMenu', [5, 10, 15, 20]);
            vi.todates = new Date();
            vi.minDate = new Date(
                vi.todates.getFullYear(),
                vi.todates.getMonth() - 2,
                vi.todates.getDate()
            );

            vi.maxDate = new Date(
                vi.todates.getFullYear(),
                vi.todates.getMonth(),
                vi.todates.getDate()
            );
            $scope.currentNavItem = $state.current.name;
            vi.authentication=function(){
                var encoded = btoa("middleware:secret");
                adithyaServices.loginNec(encoded).then(function (d) {
                    if (d.access_token != null) {
                        userStroage.set('access_token', d.access_token);
                    }
                });
            };

            vi.authentication();
            var token = userStroage.get('access_token');
            var access_token = "bearer" + " " + token;
            var obj = {};
            obj.merchantId = $window.sessionStorage.getItem('merchantId');
            obj.action = "INVOICE_MAKER_DISPLAY";
            obj.reportType ='VIEW';
            obj.fromDate='';
            obj.toDate='';
            vi.changeDate=function (fdate,tdate) {
                if(fdate !=null && fdate !== undefined){
                    obj.fromDate= moment(fdate).format('DD-MMM-YYYY');
                }
                if(tdate !=null && tdate !==undefined){
                    obj.toDate= moment(tdate).format('DD-MMM-YYYY');
                }
                if((obj.fromDate !==undefined && obj.fromDate !=='')&&(obj.toDate !==undefined && obj.toDate !=='')){
                    if(new Date(obj.fromDate) <= new Date(obj.toDate)){
                        getMakerInvoiceDetails();
                    }
                    else{
                        alert('please Select  From Date and To Date Valid');
                    }

                }
            };

           function  getMakerInvoiceDetails() {
               $scope.loaderVisibles=true;
               $timeout(function () {
                   adithyaServices.getmakerInvoice(access_token, obj).then(function (deObj) {
                       vi.mandateTransactions = deObj.invoiceList;
                       if (vi.mandateTransactions.length > 0) {
                           _.each(vi.mandateTransactions, function (obj) {
                               obj.flag = 'N';
                               obj.dueDate=moment(obj.dueDate).format('DD-MM-YYYY');

                           })
                       } else {
                           vi.gridOptions.data = [];
                           vi.getMessage = 'No Data Found';
                       }
                       $scope.loaderVisibles=false;
                       vi.transacTions = vi.mandateTransactions;
                   }).catch(function (reason) {
                       $timeout(function () {
                           $scope.loaderVisibles=false;
                       })
                       growl.warning('No Data Found');
                   });
               })

           }
            getMakerInvoiceDetails();
            vi.gridOptions = {};
            vi.gridOptions.columnDefs = [
                {
                    name: 'Invoice Number',
                    field: 'invoiceNumber',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.invoiceNumber.length> 0 ? row.entity.invoiceNumber :"N/A"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/maker/girdHeaderTemplate.html'
                },
                {
                    name: 'payer Id',
                    field: 'fromEstId',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.fromEstId.length> 0 ? row.entity.fromEstId :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/maker/girdHeaderTemplate.html'
                },
                {
                    name: 'Payer Name',
                    field: 'fromCustName',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.fromCustName.length> 0 ? row.entity.fromCustName :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/maker/girdHeaderTemplate.html'
                },
                {
                    name: 'Payer EstablishName',
                    field: 'fromEstName',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.fromEstName.length> 0 ? row.entity.fromEstName :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/maker/girdHeaderTemplate.html'
                },

                {
                    name: 'Payer Mobile',
                    field: 'fromCustMob',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.fromCustMob.length> 0 ? row.entity.fromCustMob :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/maker/girdHeaderTemplate.html'
                },
                {
                    name: 'payee Id',
                    field: 'toEstId',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.toEstId.length> 0 ? row.entity.toEstId :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/maker/girdHeaderTemplate.html'
                },

                {
                    name: 'Payee Name',
                    field: 'toCustName',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.toCustName.length> 0 ? row.entity.toCustName :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/maker/girdHeaderTemplate.html'
                },


                {
                    name: 'Payee EstablishName',
                    field: 'toEstName',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.toEstName.length> 0 ? row.entity.toEstName :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/maker/girdHeaderTemplate.html'
                },
                {
                    name: 'payee Moile No',
                    field: 'toEstMob      ',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.toEstMob.length> 0 ? row.entity.toEstMob :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/maker/girdHeaderTemplate.html'
                },
                {
                    name: 'Amount',
                    field: 'amount',
                    cellTemplate: '<p class="grid-text-font"><i class="fa fa-rupee"></i>{{row.entity.amount.length> 0 ? row.entity.amount :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/maker/girdHeaderTemplate.html'
                },
                {
                    name: 'Due date',
                    field: 'dueDate',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.dueDate.length> 0 ? row.entity.dueDate :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/maker/girdHeaderTemplate.html'
                }, {
                    name: 'Remarks',
                    field: 'remarks',
                    cellTemplate: '<p class="grid-text-font"><i class="glyphicon glyphicon-pencil" style="font-size: 8px;float:right"></i>{{row.entity.remarks.length> 0 ? row.entity.remarks :"-"}}</p>',
                    enableCellEdit: true,
                    headerCellTemplate: 'ui/views/maker/girdHeaderTemplate.html'
                },
                {
                    name: 'Select flag',
                    field: 'flag',
                    cellTemplate: '<md-checkbox  aria-label="Follow" class="md-secondary" ng-init="row.entity.flag ==\'Y\'"\n' +
                    'ng-model="row.entity.flag" ng-true-value="\'Y\'"\n'+ 'ng-false-value="\'N\'" style="\n' + 'margin-left: 35px;\n' + 'margin-right: 35px;\n' + '"></md-checkbox>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/checker/girdHeaderTemplate.html'

                },
                {
                    name: 'View/Download',
                    field: 'imageData',
                    cellTemplate:'<a ng-if="row.entity.imageData != null" class="glyphicon glyphicon-download-alt"  href="" ng-click="grid.appScope.viewDownloads(row.entity.imageData)"></a>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/maker/girdHeaderTemplate.html'

                }
            ];

            vi.viewDownloads = function (encodedata) {
                var viewPath = '/downloadImage';
                var currentPath = window.location.href.substr(0, window.location.href.indexOf('#') + 1);
                var fullPath = currentPath + viewPath;
                console.log(fullPath);
                // $window.open(fullPath, '_blank');
                $window.open(fullPath, '_blank').var1 = encodedata;

            };


            var sample = [
                {
                    "invoiceNumber": "syed",
                    "fromEstId": "456123",
                    "toEstId": "101",
                    "fromCustName": "byresds",
                    "fromEstName": "Personal",
                    "Amount": "25632",
                    "dueDate": "11/15/2017",
                    "fromCustMob": "9.30AM",
                    "toCustName": "Aproved",
                    "toEstName": "901",
                    "toEstMob": "905",
                    "remarks": '',
                    "flag":'Y',
                    "viewDownload": 'Y'
                },
                {
                    "invoiceNumber": "syed",
                    "fromEstId": "456123",
                    "toEstId": "101",
                    "fromCustName": "byresds",
                    "fromEstName": "Personal",
                    "Amount": "25632",
                    "dueDate": "11/15/2017",
                    "fromCustMob": "9.30AM",
                    "toCustName": "Aproved",
                    "toEstName": "901",
                    "toEstMob": "905",
                    "remarks": '',
                    "flag":'Y',
                    "viewDownload": 'Y'
                },
                {
                    "invoiceNumber": "syed",
                    "fromEstId": "456123",
                    "toEstId": "101",
                    "fromCustName": "byresds",
                    "fromEstName": "Personal",
                    "Amount": "25632",
                    "dueDate": "11/15/2017",
                    "fromCustMob": "9.30AM",
                    "toCustName": "Aproved",
                    "toEstName": "901",
                    "toEstMob": "905",
                    "remarks": '',
                    "flag":'Y',
                    "viewDownload": 'Y'
                }, {
                    "invoiceNumber": "syed",
                    "fromEstId": "456123",
                    "toEstId": "101",
                    "fromCustName": "byresds",
                    "fromEstName": "Personal",
                    "Amount": "25632",
                    "dueDate": "11/15/2017",
                    "fromCustMob": "9.30AM",
                    "toCustName": "Aproved",
                    "toEstName": "901",
                    "toEstMob": "905",
                    "remarks": '',
                    "flag":'Y',
                    "viewDownload": 'Y'
                }
            ];
              // vi.gridOptions.data = sample;
               vi.updateMakerInvoice = function (data) {
                  var validationRules=[];
                   angular.forEach(data, function (value, key) {
                       delete(data[key].$$hashKey);
                       delete (data[key].imageData);
                       if (data[key].flag =='Y') {
                           validationRules.push(data[key]);
                       }
                   });
                var obj = {
                    "mkrckrFlag": "M",
                    "invoiceupdatedData": data
                };
                obj.merchantId = $window.sessionStorage.getItem('merchantId');
                if(validationRules.length>0){
                    adithyaServices.updatemakerInvoice(obj, access_token).then(function (d) {
                        if(d.status === 'SUCCESS' || d.status === 'Success' || d.status === 'success'){
                            growl.success('Sent Successfully');
                            var obj = {};
                            obj.merchantId = $window.sessionStorage.getItem('merchantId');
                            obj.action = "INVOICE_MAKER_DISPLAY";
                            obj.reportType ='VIEW';
                            obj.fromDate=obj.fromDate;
                            obj.toDate=obj.toDate;
                            adithyaServices.getmakerInvoice(access_token, obj).then(function (d) {
                                vi.mandateTransactions = d.invoiceList;
                                /* vi.utilityCode = d.utilityCode;
                                 vi.sponoserCode = d.sponoserCode;
                                 vi.sysDate = d.sysDate;*/
                                if (vi.mandateTransactions.length > 0) {
                                    _.each(vi.mandateTransactions, function (obj) {
                                        obj.flag = 'N';

                                    })
                                }
                                vi.transacTions = vi.mandateTransactions;
                                vi.visibleProgress = false;
                            }).catch(function (reason) {
                                growl.warning('No Data Found');
                            });
                        }else{
                            growl.warning('Sending faild');
                        }

                    }).catch(function (reason) {
                        growl.error('Internal Error Please Contact  Administrator');
                    });
                }else{
                    growl.warning('Please Select at least One Invoice');
                }



            };

        }

    ]);
    var ModalInstanceCtrl = function ($scope, $modalInstance, data) {
        $scope.data = data;
        if ($scope.data.status == 'SUCCESS' && $scope.data.status != undefined) {
            $scope.data.message = "Mandates Uploaded Successfully";
            $scope.data.mode = 'success';
        } else {
            $scope.data.mode = 'danger';
            $scope.data.message = "Mandates Upload Failed";
        }

        $scope.close = function (/*result*/) {
            $modalInstance.close($scope.data);
        };
    };
})();