
(function () {
    'use strict';
    var Adbapp = angular.module('abpib');
    Adbapp.controller('invoiceCheckerCtrl', ['$scope', '$state', 'userStroage', 'adithyaServices','$timeout','growl','$window','DTOptionsBuilder',
        function ($scope, $state, userStroage, adithyaServices,$timeout,growl,$window,DTOptionsBuilder) {
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('bLengthChange', true)
                .withOption('aLengthMenu', [5, 10, 15, 20]);
            var ppi = this;

            ppi.hoverIn = function(index){
                ppi.editIndex=index;
                ppi.hoverEdit = true;
            };
            ppi.hoverOut = function(index){
                ppi.editIndex=index;
                ppi.hoverEdit = false;
            };
            $scope.dataTableOpt = {
                //custom datatable options
                // or load data through ajax call also
                "aLengthMenu": [[5,10, 50, 100,-1], [5,10, 50, 100,'All']],
            };
            $scope.currentNavItem = $state.current.name;

            ppi.todates = new Date();
            ppi.minDate = new Date(
                ppi.todates.getFullYear(),
                ppi.todates.getMonth() - 2,
                ppi.todates.getDate()
            );

            ppi.maxDate = new Date(
                ppi.todates.getFullYear(),
                ppi.todates.getMonth(),
                ppi.todates.getDate()
            );



            ppi.authentication=function(){
                var encoded = btoa("middleware:secret");
                adithyaServices.loginNec(encoded).then(function (d) {
                    if (d.access_token != null) {
                        userStroage.set('access_token', d.access_token);
                    }
                });
            };

            ppi.authentication();
            var token=userStroage.get('access_token');
            var access_token= "bearer" + " " + token;
            var obj={};
            obj.merchantId= $window.sessionStorage.getItem('merchantId');
            obj.action="INVOICE_CHECKER_DISPLAY";
            obj.reportType ='VIEW';
            obj.fromDate='';
            obj.toDate='';
            ppi.changeDate=function (fdate,tdate) {
                if(fdate !=null && fdate !== undefined){
                    obj.fromDate= moment(fdate).format('DD-MMM-YYYY');
                }
                if(tdate !=null && tdate !==undefined){
                    obj.toDate= moment(tdate).format('DD-MMM-YYYY');
                }
                if((obj.fromDate !==undefined && obj.fromDate !=='')&&(obj.toDate !==undefined && obj.toDate !=='')){
                    if(new Date(obj.fromDate) <= new Date(obj.toDate)){
                        getcheckerInvoiceDetails();
                    }
                    else{
                        alert('please Select  From Date and To Date Valid');
                    }

                }
            };

            function getcheckerInvoiceDetails() {
                adithyaServices.getcheckerInvoice(access_token,obj).then(function (d) {
                    ppi.mandateTransactions = d.invoiceList;
                    if (ppi.mandateTransactions.length > 0) {
                        _.each(ppi.mandateTransactions, function (obj) {
                            obj.flag = 'N';
                            obj.dueDate=moment(obj.dueDate).format('DD-MM-YYYY');
                        })
                    }
                    ppi.transacTions = ppi.mandateTransactions;
                }).catch(function (reason) {
                    growl.warning('No Data Found');
                });
            }
            getcheckerInvoiceDetails();

            ppi.viewDownloads = function (encodedata) {
                var viewPath = '/downloadImage';
                var currentPath = window.location.href.substr(0, window.location.href.indexOf('#') + 1);
                var fullPath = currentPath + viewPath;
                $window.open(fullPath, '_blank').var1 = encodedata;

            };

            ppi.RejectCheckerInvoice = function (griddata) {
                for(var i=0;i< griddata.length ;i++){
                    if(griddata[i].flag =='Y'){
                        griddata[i].flag="R";
                    }
                    delete (griddata[i].$$hashKey);
                    delete (griddata[i].imageData);
                }
                var obj = {
                    "mkrckrFlag":"C",
                    "invoiceupdatedData":griddata};
                obj.merchantId= $window.sessionStorage.getItem('merchantId');

                adithyaServices.updatecheckerInvoice(obj, access_token).then(function (d) {
                    if(d.status === 'SUCCESS' || d.status === 'Success' || d.status === 'success'){
                        growl.success('Reject Successfully');
                        var obj={};
                        obj.merchantId= $window.sessionStorage.getItem('merchantId');
                        obj.action="INVOICE_CHECKER_DISPLAY";
                        obj.reportType ='VIEW';
                        obj.fromDate=obj.fromDate;
                        obj.toDate=obj.toDate;
                        adithyaServices.getcheckerInvoice(access_token,obj).then(function (de) {
                            ppi.mandateTransactions = de.invoiceList;
                            if (ppi.mandateTransactions.length > 0) {
                                _.each(ppi.mandateTransactions, function (obj) {
                                    obj.flag = 'N';
                                    obj.dueDate=moment(obj.dueDate).format('DD-MM-YYYY');
                                })
                            }
                            ppi.transacTions = ppi.mandateTransactions;

                        }).catch(function (reason) {
                            growl.warning('No Data Found');
                        });
                    }else{
                        growl.warning('Reject faild');
                    }

                }).catch(function (reason) {
                    growl.error('Internal Error Please Contact  Administrator');
                });

            };
            ppi.updateCheckerInvoice = function (data) {
                var validationRules=[];
                angular.forEach(data, function (value, key) {
                    delete(data[key].$$hashKey);
                    delete (data[key].imageData);
                    if (data[key].flag =='Y') {
                        validationRules.push(data[key]);
                    }
                });
                var obj = {
                    "mkrckrFlag":"C",
                    "invoiceupdatedData":data};
                obj.merchantId= $window.sessionStorage.getItem('merchantId');
                if(validationRules.length>0){
                    adithyaServices.updatecheckerInvoice(obj, access_token).then(function (d) {
                        if(d.status === 'SUCCESS' || d.status === 'Success' || d.status === 'success'){
                            growl.success('Sent Successfully');
                            var obj={};
                            obj.merchantId= $window.sessionStorage.getItem('merchantId');
                            obj.action="INVOICE_CHECKER_DISPLAY";
                            obj.reportType ='VIEW';
                            obj.fromDate=obj.fromDate;
                            obj.toDate=obj.toDate;
                            adithyaServices.getcheckerInvoice(access_token,obj).then(function (d) {
                                ppi.mandateTransactions = d.invoiceList;
                                /*  ppi.utilityCode = d.utilityCode;
                                  ppi.sponoserCode = d.sponoserCode;
                                  ppi.sysDate = d.sysDate;*/

                                if (ppi.mandateTransactions.length > 0) {
                                    _.each(ppi.mandateTransactions, function (obj) {
                                        obj.flag = 'N';
                                        obj.dueDate=moment(obj.dueDate).format('DD-MM-YYYY');
                                    })
                                }
                                ppi.transacTions = ppi.mandateTransactions;

                            }).catch(function (reason) {
                                growl.warning('No Data Found');
                            });
                        }else{
                            growl.warning('Sending faild');
                        }

                    }).catch(function (reason) {
                        growl.error('Internal Error Please Contact  Administrator');
                    });
                }else{
                    growl.warning('Please Select at least One Invoice');
                }


            };

        }
    ]);

})();