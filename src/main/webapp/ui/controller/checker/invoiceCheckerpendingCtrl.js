
(function () {
    'use strict';
    var Adbapp = angular.module('abpib');
    Adbapp.controller('invoiceCheckerpendingCtrl', ['$scope', '$state', 'userStroage', 'adithyaServices','$timeout','growl','$window','adtiyasharedService','DTOptionsBuilder',
        function ($scope, $state, userStroage, adithyaServices,$timeout,growl,$window,adtiyasharedService,DTOptionsBuilder) {
            var pi = this;

            pi.hoverIn = function(index){
                pi.editIndex=index;
                pi.hoverEdit = true;
            };
            pi.hoverOut = function(index){
                pi.editIndex=index;
                pi.hoverEdit = false;
            };
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('bLengthChange', true)
                .withOption('aLengthMenu', [5, 10, 15, 20]);

            $scope.currentNavItem = $state.current.name;
            var token=userStroage.get('access_token');
            var access_token= "bearer" + " " + token;
            pi.todates = new Date();
            pi.minDate = new Date(
                pi.todates.getFullYear(),
                pi.todates.getMonth() - 2,
                pi.todates.getDate()
            );

            pi.maxDate = new Date(
                pi.todates.getFullYear(),
                pi.todates.getMonth(),
                pi.todates.getDate()
            );



            var obj={};
            obj.merchantId= $window.sessionStorage.getItem('merchantId');
            obj.action="INVOICE_CHECKER_DISPLAY";
            obj.reportType ='PENDING';
            obj.fromDate='';
            obj.toDate='';
            pi.changeDate=function (fdate,tdate) {
                if(fdate !=null && fdate !== undefined){
                    obj.fromDate= moment(fdate).format('DD-MMM-YYYY');
                }
                if(tdate !=null && tdate !==undefined){
                    obj.toDate= moment(tdate).format('DD-MMM-YYYY');
                }
                if((obj.fromDate !==undefined && obj.fromDate !=='')&&(obj.toDate !==undefined && obj.toDate !=='')){
                    if(new Date(obj.fromDate) <= new Date(obj.toDate)){
                        getCheckerInvoicePendingDetails();
                    }
                    else{
                        alert('please Select  From Date and To Date Valid');
                    }

                }
            };
            function  getCheckerInvoicePendingDetails() {
                adithyaServices.getcheckerInvoice(access_token,obj).then(function (d) {
                    if(d.status === 'SUCCESS' || d.status === 'Success' || d.status === 'success'){
                        pi.mandateTransactions = d.approvedInvoices;
                        if (pi.mandateTransactions.length > 0) {
                            _.each(pi.mandateTransactions, function (obj) {
                                obj.flag = 'N';
                                obj.dueDate=moment(obj.dueDate).format('DD-MM-YYYY');
                                obj.remarks='';
                            })
                        }
                        pi.transacTions=pi.mandateTransactions;
                        pi.visibleProgress=false;
                    }else{
                        growl.warning('No Data Found');
                    }

                }).catch(function (reason) {
                    growl.error('Internal Error Please Contact  Administrator');
                });
            }
                getCheckerInvoicePendingDetails();
            pi.viewDownloads = function (encodedata) {
                var viewPath = '/downloadImage';
                var currentPath = window.location.href.substr(0, window.location.href.indexOf('#') + 1);
                var fullPath = currentPath + viewPath;
                console.log(fullPath);
                // $window.open(fullPath, '_blank');
                $window.open(fullPath, '_blank').var1 = encodedata;

            };

            pi.updateChecker = function (griddata) {
                for(var i=0;i< griddata.data.length ;i++){
                    delete (griddata.data[i].$$hashKey);
                }
                var obj = {
                    "mkrckrFlag":"C",
                    "invoiceupdatedData":griddata.data};
                obj.merchantId= $window.sessionStorage.getItem('merchantId');
                pi.visibleProgress=true;
                adithyaServices.updatecheckerInvoice(obj, access_token).then(function (d) {
                    if(d.status === 'SUCCESS' || d.status === 'Success' || d.status === 'success'){
                        growl.success('Sent Successfully');
                        var obj={};
                        obj.merchantId= $window.sessionStorage.getItem('merchantId');
                        obj.action="INVOICE_CHECKER_DISPLAY";
                        obj.reportType ='PENDING';
                        obj.fromDate = obj.fromDate;
                        obj.toDate = obj.toDate;
                        adithyaServices.getcheckerInvoice(access_token,obj).then(function (d) {
                            pi.mandateTransactions = d.invoiceList;
                            /*  pi.utilityCode = d.utilityCode;
                              pi.sponoserCode = d.sponoserCode;
                              pi.sysDate = d.sysDate;*/

                            if (pi.mandateTransactions.length > 0) {
                                _.each(pi.mandateTransactions, function (obj) {
                                    obj.flag = 'N';
                                    obj.remarks='';
                                })
                            }
                            pi.gridOptions.data = pi.mandateTransactions;
                            pi.visibleProgress=false;
                        }).catch(function (reason) {
                            growl.warning('No Data Found');
                        });
                    }else{
                        growl.warning('Sending faild');
                    }

                }).catch(function (reason) {
                    growl.error('Internal Error Please Contact  Administrator');
                });

            };

        }
    ]);

})();