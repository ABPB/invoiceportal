
(function () {
    'use strict';
    var Adbapp = angular.module('abpib');
    Adbapp.controller('paymentCheckerCtrl', ['$scope', '$state', 'userStroage', 'adithyaServices','$timeout','growl','$window',
        function ($scope, $state, userStroage, adithyaServices,$timeout,growl,$window) {
            var pc = this;
            $scope.currentNavItem = $state.current.name;
             var token=userStroage.get('access_token');
            var access_token= "bearer" + " " + token;
            pc.visibleProgress=true;
            var obj={};
            obj.merchantId= $window.sessionStorage.getItem('merchantId');
            obj.action="INVOICE_CHECKER_DISPLAY";
            adithyaServices.getcheckerInvoice(access_token,obj).then(function (d) {
                pc.mandateTransactions = d.invoiceList;
                pc.utilityCode = d.utilityCode;
                pc.sponoserCode = d.sponoserCode;
                pc.sysDate = d.sysDate;
                if (pc.mandateTransactions.length > 0) {
                    _.each(pc.mandateTransactions, function (obj) {
                        obj.flag = 'N';
                        obj.remarks='';
                    })
                }
                pc.gridOptions.data = pc.mandateTransactions;
                pc.visibleProgress=false;
            }).catch(function (reason) {
                growl.error('Internal Error Please Contact  Administrator');
            });
            pc.gridOptions = {};

            pc.gridOptions.columnDefs = [
                {
                    name: 'Invoice Number',
                    field: 'invoiceNumber',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.invoiceNumber.length> 0 ? row.entity.invoiceNumber :"N/A"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/client/girdHeaderTemplate.html'
                },
                {
                    name: 'payer Id',
                    field: 'fromEstId',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.fromEstId.length> 0 ? row.entity.fromEstId :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/client/girdHeaderTemplate.html'
                },
                {
                    name: 'Payer Name',
                    field: 'fromCustName',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.fromCustName.length> 0 ? row.entity.fromCustName :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/client/girdHeaderTemplate.html'
                },
                {
                    name: 'Payer EstablishName',
                    field: 'fromEstName',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.fromEstName.length> 0 ? row.entity.fromEstName :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/client/girdHeaderTemplate.html'
                },

                {
                    name: 'Payer Mobile',
                    field: 'fromCustMob',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.fromCustMob.length> 0 ? row.entity.fromCustMob :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/client/girdHeaderTemplate.html'
                },
                {
                    name: 'payee Id',
                    field: 'toEstId',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.toEstId.length> 0 ? row.entity.toEstId :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/client/girdHeaderTemplate.html'
                },

                {
                    name: 'Payee Name',
                    field: 'toCustName',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.toCustName.length> 0 ? row.entity.toCustName :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/client/girdHeaderTemplate.html'
                },


                {
                    name: 'Payee EstablishName',
                    field: 'toEstName',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.toEstName.length> 0 ? row.entity.toEstName :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/client/girdHeaderTemplate.html'
                },
                {
                    name: 'payee Moile No',
                    field: 'toEstMob      ',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.toEstMob.length> 0 ? row.entity.toEstMob :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/client/girdHeaderTemplate.html'
                },
                {
                    name: 'Amount',
                    field: 'amount',
                    cellTemplate: '<p class="grid-text-font"><i class="fa fa-rupee"></i>{{row.entity.amount.length> 0 ? row.entity.amount :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/client/girdHeaderTemplate.html'
                },
                {
                    name: 'Due date',
                    field: 'dueDate',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.dueDate.length> 0 ? row.entity.dueDate :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/client/girdHeaderTemplate.html'
                },{
                    name: 'Remarks',
                    field: 'remarks',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.remarks.length> 0 ? row.entity.remarks :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/client/girdHeaderTemplate.html'
                },
                {
                    name: 'Select flag',
                    field: 'flag',
                    enableCellEdit: false,
                    cellTemplate: '<md-checkbox  aria-label="Follow" class="md-secondary" ng-init="row.entity.flag ==\'Y\'"\n' +
                    'ng-model="row.entity.flag" ng-true-value="\'Y\'"\n'+ 'ng-false-value="\'N\'" style="\n' + 'margin-left: 35px;\n' + 'margin-right: 35px;\n' + '"></md-checkbox>',
                    headerCellTemplate: 'ui/views/client/girdHeaderTemplate.html'

                }


            ];
            var sample = [
                {
                    "invoiceNumber": "syed",
                    "fromEstId": "456123",
                    "toEstId": "101",
                    "fromCustName": "byresds",
                    "fromEstName": "Personal",
                    "Amount": "25632",
                    "dueDate": "11/15/2017",
                    "fromCustMob": "9.30AM",
                    "toCustName": "Aproved",
                    "toEstName": "901",
                    "toEstMob": "905",
                    "selectFlag":'Y'
                },
                {
                    "invoiceNumber": "syed",
                    "fromEstId": "456123",
                    "toEstId": "101",
                    "fromCustName": "byresds",
                    "fromEstName": "Personal",
                    "Amount": "25632",
                    "dueDate": "11/15/2017",
                    "fromCustMob": "9.30AM",
                    "toCustName": "Aproved",
                    "toEstName": "901",
                    "toEstMob": "905",
                    "selectFlag":'Y'
                },
                {
                    "invoiceNumber": "syed",
                    "fromEstId": "456123",
                    "toEstId": "101",
                    "fromCustName": "byresds",
                    "fromEstName": "Personal",
                    "Amount": "25632",
                    "dueDate": "11/15/2017",
                    "fromCustMob": "9.30AM",
                    "toCustName": "Aproved",
                    "toEstName": "901",
                    "toEstMob": "905",
                    "selectFlag":'Y'
                }, {
                    "invoiceNumber": "syed",
                    "fromEstId": "456123",
                    "toEstId": "101",
                    "fromCustName": "byresds",
                    "fromEstName": "Personal",
                    "Amount": "25632",
                    "dueDate": "11/15/2017",
                    "fromCustMob": "9.30AM",
                    "toCustName": "Aproved",
                    "toEstName": "901",
                    "toEstMob": "905",
                    "selectFlag":'Y'
                }
            ];
            //  pc.gridOptions.data = sample;
            pc.updateChecker = function (griddata) {
                for(var i=0;i< griddata.data.length ;i++){
                    delete (griddata.data[i].$$hashKey);
                }
                var obj = {
                    "mkrckrFlag":"C",
                    "invoiceupdatedData":griddata.data};
                obj.merchantId= $window.sessionStorage.getItem('merchantId');
                pc.visibleProgress=true;
                adithyaServices.updatecheckerInvoice(obj, access_token).then(function (d) {
                    if(d.status === 'SUCCESS' || d.status === 'Success' || d.status === 'success'){
                        growl.success('Sent Successfully');
                        var obj={};
                        obj.merchantId= $window.sessionStorage.getItem('merchantId');
                        obj.action="INVOICE_CHECKER_DISPLAY";
                        adithyaServices.getcheckerInvoice(access_token,obj).then(function (d) {
                            pc.mandateTransactions = d.invoiceList;
                            /*  pc.utilityCode = d.utilityCode;
                              pc.sponoserCode = d.sponoserCode;
                              pc.sysDate = d.sysDate;*/

                            if (pc.mandateTransactions.length > 0) {
                                _.each(pc.mandateTransactions, function (obj) {
                                    obj.flag = 'N';
                                    obj.remarks='';
                                })
                            }
                            pc.gridOptions.data = pc.mandateTransactions;
                            pc.visibleProgress=false;
                        }).catch(function (reason) {
                            growl.error('Internal Error Please Contact  Administrator');
                        });
                    }else{
                        growl.warning('Sending faild');
                    }

                }).catch(function (reason) {
                    growl.error('Internal Error Please Contact  Administrator');
                });

            };

        }
    ]);

})();