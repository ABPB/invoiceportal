
(function () {
    'use strict';
    var Adbapp = angular.module('abpib');
    Adbapp.controller('invoiceApprovedcheckerCtrl', ['$scope', '$state', 'userStroage', 'adithyaServices','$timeout','growl','$window','adtiyasharedService','DTOptionsBuilder',
        function ($scope, $state, userStroage, adithyaServices,$timeout,growl,$window,adtiyasharedService,DTOptionsBuilder) {
            var ai = this;
            ai.hoverIn = function(index){
                ai.editIndex=index;
                ai.hoverEdit = true;
            };
            ai.hoverOut = function(index){
                ai.editIndex=index;
                ai.hoverEdit = false;
            };
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('bLengthChange', true)
                .withOption('aLengthMenu', [5, 10, 15, 20]);


            $scope.currentNavItem = $state.current.name;
            var token=userStroage.get('access_token');
            var access_token= "bearer" + " " + token;

            var obj={};
            obj.merchantId= $window.sessionStorage.getItem('merchantId');
            obj.action="INVOICE_CHECKER_DISPLAY";
            obj.reportType ='APPROVED';
            obj.fromDate='';
            obj.toDate='';
            ai.changeDate=function (fdate,tdate) {
                if(fdate !=null && fdate !== undefined){
                    obj.fromDate= moment(fdate).format('DD-MMM-YYYY');
                }
                if(tdate !=null && tdate !==undefined){
                    obj.toDate= moment(tdate).format('DD-MMM-YYYY');
                }
                if((obj.fromDate !==undefined && obj.fromDate !=='')&&(obj.toDate !==undefined && obj.toDate !=='')){
                    if(new Date(obj.fromDate) <= new Date(obj.toDate)){
                        getCheckerInvoiceApprovedDetails();
                    }
                    else{
                        alert('please Select  From Date and To Date Valid');
                    }

                }
            };
            function getCheckerInvoiceApprovedDetails() {
                adithyaServices.getcheckerInvoice(access_token,obj).then(function (d) {
                    ai.mandateTransactions = d.pendingInvoices;
                    if (ai.mandateTransactions.length > 0) {
                        _.each(ai.mandateTransactions, function (obj) {
                            obj.flag = 'N';
                            obj.dueDate=moment(obj.dueDate).format('DD-MM-YYYY');
                            obj.remarks='';
                        })
                    }
                    ai.transacTions=ai.mandateTransactions;
                    ai.visibleProgress=false;
                }).catch(function (reason) {
                    growl.warning('No Data Found');
                });
            }
            getCheckerInvoiceApprovedDetails();

            ai.viewDownloads = function (encodedata) {
                var viewPath = '/downloadImage';
                var currentPath = window.location.href.substr(0, window.location.href.indexOf('#') + 1);
                var fullPath = currentPath + viewPath;
                console.log(fullPath);
                // $window.open(fullPath, '_blank');
                $window.open(fullPath, '_blank').var1 = encodedata;

            };


            ai.updateChecker = function (griddata) {
                for(var i=0;i< griddata.data.length ;i++){
                    delete (griddata.data[i].$$hashKey);
                }
                var obj = {
                    "mkrckrFlag":"C",
                    "invoiceupdatedData":griddata.data};
                obj.merchantId= $window.sessionStorage.getItem('merchantId');
                ai.visibleProgress=true;
                adithyaServices.updatecheckerInvoice(obj, access_token).then(function (d) {
                    if(d.status === 'SUCCESS' || d.status === 'Success' || d.status === 'success'){
                        growl.success('Sent Successfully');
                        var obj={};
                        obj.merchantId= $window.sessionStorage.getItem('merchantId');
                        obj.action="INVOICE_CHECKER_DISPLAY";
                        obj.reportType ='APPROVED';
                        obj.fromDate=obj.fromDate;
                        obj.toDate=obj.toDate;
                        adithyaServices.getcheckerInvoice(access_token,obj).then(function (d) {
                            ai.mandateTransactions = d.invoiceList;
                            /*  ai.utilityCode = d.utilityCode;
                              ai.sponoserCode = d.sponoserCode;
                              ai.sysDate = d.sysDate;*/

                            if (ai.mandateTransactions.length > 0) {
                                _.each(ai.mandateTransactions, function (obj) {
                                    obj.flag = 'N';
                                    obj.remarks='';
                                })
                            }
                            ai.gridOptions.data = ai.mandateTransactions;
                            ai.visibleProgress=false;
                        }).catch(function (reason) {
                            growl.warning('No Data Found');
                        });
                    }else{
                        growl.warning('Sending faild');
                    }

                }).catch(function (reason) {
                    growl.error('Internal Error Please Contact  Administrator');
                });

            };

        }
    ]);

})();