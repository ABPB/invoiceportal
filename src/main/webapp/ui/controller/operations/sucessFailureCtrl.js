/**
 * Created by Mahesh on 1/9/2018.
 */
(function() {
    'use strict';
    var Adbapp = angular.module('abpib');

    Adbapp.controller('sucessFailureCtrl', ['$scope','$state','userStroage','adithyaServices','$timeout','$stateParams','$filter',
        function($scope,$state,userStroage,adithyaServices,$timeout,$stateParams,$filter) {
            var sf = this;
            $scope.currentNavItem = $state.current.name;
            sf.status=$stateParams.status;
            sf.reportType= $stateParams.reportType;
            sf.suceessFailure=$stateParams.totalval;
          var params={
              collectionFilter:$stateParams.collectionFilter,
              transactinFilter:$stateParams.transactinFilte,
              formDate:$stateParams.formDate,
              toDate:$stateParams.toDate,
              reportType:$stateParams.reportType
          };

            sf.visibleProgress=true;
            var encoded = btoa("middleware:secret");
            if(sf.status =='success' && sf.status != undefined){
                adithyaServices.loginNec(encoded).then(function (d) {
                    if (d.access_token != null) {
                        adithyaServices.successFailure(d.access_token,params).then(function (d) {
                            if(d.successList.length >0){
                                var i=1;
                                _.each(d.successList, function (obj) {
                                    obj.Sno = i;
                                    i++;
                                });
                                sf.list = d.successList;
                                sf.gridOptions.data= sf.list;
                            }
                            sf.visibleProgress=false;
                        });
                    }
                });
            }else{
                if(sf.status =='failure' && sf.status != undefined){
                    adithyaServices.loginNec(encoded).then(function (d) {
                        if (d.access_token != null) {
                            adithyaServices.successFailure(d.access_token,params).then(function (d) {
                                if(d.failedList.length >0){
                                    var i=1;
                                    _.each(d.failedList, function (obj) {
                                        obj.Sno = i;
                                        i++;
                                    });
                                    sf.list = d.failedList;
                                    sf.gridOptions.data= sf.list;
                                }
                                sf.visibleProgress=false;
                            });
                        }
                    });
                }
            }

            $timeout(function () {
                sf.visibleProgress=false;
            },3000);
            sf.searchGrid=function (data) {
                sf.filterdata = $filter('filter')(sf.list,{customerId:data});
                if(sf.filterdata.length>0){
                    sf.gridOptions.data=sf.filterdata;
                }else{
                    sf.gridOptions.data=sf.list;
                }

            };

            sf.gridOptions = {};

            sf.gridOptions.columnDefs = [
                {
                    name: 'Sno',
                    field: 'Sno',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.Sno}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'views/operations/girdHeaderTemplate.html',
                    width:50

                },
                {
                    name: 'Customer ID',
                    field: 'customerId',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.customerId.length> 0 ? row.entity.customerId :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'views/operations/girdHeaderTemplate.html',
                    width:180
                },
                {
                    name: 'Destination Bank',
                    field: 'bankName',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.bankName.length> 0 ? row.entity.bankName :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'views/operations/girdHeaderTemplate.html',
                    width:180
                },
                {
                    name: 'Mandate Value',
                    field: 'mandateAmount',
                    cellTemplate: '<p class="grid-text-font"><i class="fa fa-inr"></i>{{row.entity.mandateAmount.length> 0 ? row.entity.mandateAmount :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'views/operations/girdHeaderTemplate.html',
                    width:180
                },
                {
                    name: 'Reason',
                    field: 'reason',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.reason.length> 0 ? row.entity.reason :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'views/operations/girdHeaderTemplate.html'
                }

            ];
           /* var sample = [
                {
                    "Sno": "1",
                    "CustomerID": "ABH1234",
                    "DestinationBank": "HDFC",
                    "MandateValue": "60000",
                    "Reason": "Personal Personal Personal Personal Personal Personal Personal Personal Personal Personal Personal Personal",

                },
                {
                    "Sno": "2",
                    "CustomerID": "ABH1234",
                    "Destination Bank": "HDFC",
                    "MandateValue": "60000",
                    "Reason": "Personal Personal Personal Personal Personal Personal Personal Personal Personal Personal Personal Personal",

                },
                {
                    "Sno": "3",
                    "CustomerID": "ABH1234",
                    "DestinationBank": "HDFC",
                    "MandateValue": "60000",
                    "Reason": "Personal Personal Personal Personal Personal Personal Personal Personal Personal Personal Personal Personal",

                },
            ];
            sf.gridOptions.data = sample;*/



        }
    ]);
})();