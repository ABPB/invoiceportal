/**
 * Created by Mahesh on 1/28/2018.
 */
(function () {
    var Adbapp = angular.module('abpib');

    Adbapp.controller('paymentCtrl', ['$scope', '$state', 'adithyaServices', 'userStroage',
        function ($scope, $state, adithyaServices, userStroage) {
            var encoded = btoa("middleware:secret");
            adithyaServices.loginNec(encoded).then(function (d) {
                userStroage.set('access_token', d.access_token);
            });
                 }]
    )})();