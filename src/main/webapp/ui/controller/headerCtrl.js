/**
 * Created by Mahesh on 1/9/2018.
 */
(function() {
    var Adbapp = angular.module('abpib');

    Adbapp.controller('headerCtrl', ['$scope','$state','userStroage','$window',
        function($scope,$state,userStroage,$window) {
            $scope.currentNavItem = $state.current.name;
           $scope.clientIds= userStroage.get('client');
          $scope.userType=$window.sessionStorage.getItem('userType');
          $scope.userName=$window.sessionStorage.getItem('userName');
          $scope.roleId=$window.sessionStorage.getItem('roleId');

          $scope.callingRoute=function (data) {
              $state.go(data);
          }
        }
    ]);
})();