/**
 * Created by Mahesh on 1/8/2018.
 */
(function() {
    var Adbapp = angular.module('abpib');

    Adbapp.controller('dashboardCtrl', ['$scope','$state','$stateParams','userStroage','adithyaServices','CONSTANTS','$timeout',
        function($scope,$state,$stateParams,userStroage,adithyaServices,CONSTANTS,$timeout) {
           // var token=userStroage.get('access_token');
            var db=this;
            $scope.currentNavItem = "Dashboard";
            db.manDateval=[];
            db.collection=[];
          /*  db.manDateval=[40,50];
            db.collection=[50,40];*/
            db.options = {
                cutoutPercentage: 70,
                legend: {
                    display: true,
                    position: 'bottom'
                }
            };
          //  db.options = {cutoutPercentage: 75};
            db.userId=$stateParams.userId;
            userStroage.set('userId',  db.userId);
            db.userTypeId= $stateParams.userTypeId;
            userStroage.set('userTypeId',db.userTypeId);

            db.mandtlabels = ["Response Received","Response Pending"];
            db.collectionlab=["Response Collection Received","Response Collection Pending"];

            var encoded = btoa("middleware:secret");
            db.visibleProgress=true;
            adithyaServices.loginNec(encoded).then(function (d) {
                if (d.access_token != null) {
                    adithyaServices.dashBoard(d.access_token).then(function (d) {
                        db.totalResponseReceived=d.totalResponseReceived;
                        db.totalResponsePending=d.totalResponsePending;
                        db.responseReceivedSuccess=d.responseReceivedSuccess;
                        db.responseReceivedFailure=d.responseReceivedFailure;
                        db.responsePendingLessthan10days=d.responsePendingLessthan10days;
                        db.responsePendingmorethan10days=d.responsePendingmorethan10days;
                        db.manDateval.push(d.totalResponseReceived,d.totalResponsePending);
                        db.totalMandatesCreated=d.totalMandatesCreated;

                        db.collection.push(d.totalCollectionRequestSent,d.totalCollectionResponseReceived);
                        db.totalCollectionResponseReceived=d.totalCollectionResponseReceived;
                        db.failedCollectionsAmount=d.failedCollectionsAmount;
                        db.successCollectionsAmount=d.successCollectionsAmount;
                        db.collectionResponseReceivedSuccess=d.collectionResponseReceivedSuccess;
                        db.collectionResponseReceivedFailure=d.collectionResponseReceivedFailure;
                        db.visibleProgress=false;
                    })
                }


            });
            $timeout(function () {
                db.visibleProgress=false;
            },3000);

            db.successfailures=function (report,sta,totalval) {
                /* row.entity.remarks.length> 0 ? row.entity.remarks :"-"*/
                $state.go('successfailure',
                    {'collectionFilter': db.collectionFilter == undefined ? '':db.collectionFilter ,
                        'transactinFilter': db.transactinFilter == undefined ?'':db.transactinFilter,
                        'formDate':db.fromDate == undefined ?'':db.fromDate,
                        'toDate':db.toDate == undefined ?'':db.toDate,'reportType':report,'status':sta,totalval:totalval});

            };

            db.filterDate=function(formats){
                db.collection=[];
                db.manDateval=[];
                db.fromDate='';
                db.toDate='';

                if(formats =='Week'){
                    db.toDate='';
                    db.fromDate='';
                    db.fromDate=moment(moment().subtract(7, 'days').calendar()).format('DD-MMM-YY').toUpperCase();
                    db.toDate=moment().format('DD-MMM-YY').toUpperCase();
                }else if(formats == 'Month'){
                    db.toDate='';
                    db.fromDate='';
                    db.fromDate=moment(moment().subtract((moment().daysInMonth()), 'days').calendar()).format('DD-MMM-YY').toUpperCase();
                    db.toDate=moment().format('DD-MMM-YY').toUpperCase();
                }
                else if(formats == 'Year')
                {
                    db.toDate='';
                    db.fromDate='';
                    var formDate=moment(moment()).subtract(365,'days').calendar();
                    db.fromDate= moment(formDate).format('DD-MMM-YY').toUpperCase();
                    db.toDate=moment().format('DD-MMM-YY').toUpperCase();

                }
                var encoded = btoa("middleware:secret");
                db.visibleProgress=true;
                adithyaServices.loginNec(encoded).then(function (d) {
                    if (d.access_token != null) {
                        adithyaServices.dashBoard(d.access_token,db.mfilter,db.cfilter,db.fromDate,db.toDate).then(function (d) {
                            db.totalResponseReceived=d.totalResponseReceived;
                            db.totalResponsePending=d.totalResponsePending;
                            db.responseReceivedSuccess=d.responseReceivedSuccess;
                            db.responseReceivedFailure=d.responseReceivedFailure;
                            db.responsePendingLessthan10days=d.responsePendingLessthan10days;
                            db.responsePendingmorethan10days=d.responsePendingmorethan10days;

                            db.manDateval.push(d.totalResponseReceived,d.totalResponsePending);
                            db.totalMandatesCreated=d.totalMandatesCreated;

                            db.collection.push(d.totalCollectionRequestSent,d.totalCollectionResponseReceived);
                            db.totalCollectionResponseReceived=d.totalCollectionResponseReceived;
                            db.failedCollectionsAmount=d.failedCollectionsAmount;
                            db.successCollectionsAmount=d.successCollectionsAmount;
                            db.collectionResponseReceivedSuccess=d.collectionResponseReceivedSuccess;
                            db.collectionResponseReceivedFailure=d.collectionResponseReceivedFailure;
                            db.visibleProgress=false;
                        });
                    }


                });
                $timeout(function () {
                    db.visibleProgress=false;
                },3000);
            };
            function getDayOfYear(date) {
                var month = date.getMonth();
                var year = date.getFullYear();
                var days = date.getDate();
                for (var i = 0; i < month; i++) {
                    days += new Date(year, i+1, 0).getDate();
                }
                return days;
            }
        }

    ]);
})();