'use strict';
(function () {
    var Adbapp = angular.module('abpib');
    Adbapp.controller('initiatePaymentCtrl', ['$scope', '$state', '$filter', 'userStroage', 'adithyaServices','$window',
        function ($scope, $state, $filter, userStroage, adithyaServices,$window) {
            var pm = this;
            $scope.currentNavItem = $state.current.name;
            var encoded = btoa("middleware:secret");
            pm.visibleProgress=true;
            adithyaServices.loginNec(encoded).then(function (d) {
                if (d.access_token != null) {
                    adithyaServices.getInvoiceDetails(d.access_token).then(function(d) {
                      pm.merchantId=d.merchantId;
                      pm.establishmentId=d.establishmentId;
                      pm.payment=d.invoiceDtls[0].pay;
                      pm.collect=d.invoiceDtls[0].collect;
                        pm.gridOptions1.data= pm.payment;
                        pm.gridOptions2.data=pm.collect;
                    });
                }}).catch(function(reason) {
                    console.log(reason);
                //growl.error('Internal Error Please Contact  Administrator');
            });

            pm.gridOptions1 = {};
            pm.gridOptions2 = {};

            pm.gridOptions1.columnDefs = [
                {
                    name: 'Invoice Id',
                    field: 'invoiceId',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.invoiceId.length> 0 ? row.entity.invoiceId :"N/A"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/initiate/girdHeaderTemplate.html'
                },
                {
                    name: 'Invoice Amtount',
                    field: 'invoiceAmt',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.invoiceAmt.length> 0 ? row.entity.invoiceAmt :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/initiate/girdHeaderTemplate.html'
                },
                {
                    name: 'Invoice DueDate',
                    field: 'invoiceDueDate',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.invoiceDueDate.length> 0 ? row.entity.invoiceDueDate :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/initiate/girdHeaderTemplate.html'
                },
                {
                    name: 'Establishment Id',
                    field: 'toEstablishmentId',
                    cellTemplate: '<p class="grid-text-font"><i class="fa fa-inr"></i>{{row.entity.toEstablishmentId.length> 0 ? row.entity.toEstablishmentId :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/initiate/girdHeaderTemplate.html'
                },
                {
                    name: 'payee Name',
                    field: 'payeeName',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.payeeName.length> 0 ? row.entity.payeeName :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/initiate/girdHeaderTemplate.html'
                },
                {
                    name: 'Select flag',
                    field: 'selectFlag',
                    headerCellTemplate: 'ui/views/initiate/girdHeaderTemplate.html',
                    cellTemplate: '<md-checkbox  aria-label="Follow" class="md-secondary" ng-init="row.entity.selectFlag ==\'Y\'"\n' +
                    '                                    ng-model="row.entity.selectFlag" ng-true-value="\'Y\'"\n'
                    + 'ng-false-value="\'N\'" style="\n' + 'margin-left: 35px;\n' + 'margin-right: 35px;\n' + '"></md-checkbox>'
                }

            ];
            pm.gridOptions2.columnDefs = [
                {
                    name: 'Invoice Id',
                    field: 'invoiceId',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.invoiceId.length> 0 ? row.entity.invoiceId :"N/A"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/initiate/girdHeaderTemplate.html'
                },
                {
                    name: 'Invoice Amtount',
                    field: 'invoiceAmt',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.invoiceAmt.length> 0 ? row.entity.invoiceAmt :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/initiate/girdHeaderTemplate.html'
                },
                {
                    name: 'Invoice DueDate',
                    field: 'invoiceDueDate',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.invoiceDueDate.length> 0 ? row.entity.invoiceDueDate :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/initiate/girdHeaderTemplate.html'
                },
                {
                    name: 'Establishment Id',
                    field: 'toEstablishmentId',
                    cellTemplate: '<p class="grid-text-font"><i class="fa fa-inr"></i>{{row.entity.toEstablishmentId.length> 0 ? row.entity.toEstablishmentId :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/initiate/girdHeaderTemplate.html'
                },
                {
                    name: 'payee Name',
                    field: 'payeeName',
                    cellTemplate: '<p class="grid-text-font">{{row.entity.payeeName.length> 0 ? row.entity.payeeName :"-"}}</p>',
                    enableCellEdit: false,
                    headerCellTemplate: 'ui/views/initiate/girdHeaderTemplate.html'
                },
                {
                    name: 'Select flag',
                    field: 'selectFlag',
                    headerCellTemplate: 'ui/views/initiate/girdHeaderTemplate.html',
                    cellTemplate: '<md-checkbox  aria-label="Follow" class="md-secondary" ng-init="row.entity.selectFlag ==\'Y\'"\n' +
                    '                                    ng-model="row.entity.selectFlag" ng-true-value="\'Y\'"\n'
                    + 'ng-false-value="\'N\'" style="\n' + 'margin-left: 35px;\n' + 'margin-right: 35px;\n' + '"></md-checkbox>'
                }

            ];
        }
    ]);

})();