/**
 /**
 * Created by Mahesh on 1/8/2018.
 */
'use strict';
// Declare app level module which depends on views, and components

var Adbapp = angular.module('abpib', ['ngMaterial', 'ui.router', 'ui.bootstrap', 'ui.grid','ui.grid.edit','angular-growl','ngFileSaver','ui.utils','datatables']);


// Declare the providers only handle the config method

Adbapp.config(['$stateProvider', '$httpProvider', '$urlRouterProvider','growlProvider','$windowProvider','$mdDateLocaleProvider', function ($stateProvider, $httpProvider, $urlRouterProvider,growlProvider,$windowProvider,$mdDateLocaleProvider) {
    growlProvider.globalTimeToLive(1500);
    growlProvider.globalDisableCountDown(true);
    growlProvider.globalPosition('top-center');
    //called the token passed the header section
    $mdDateLocaleProvider.formatDate = function(date) {
        return date ? moment(date).format('DD-MM-YYYY') : '';
    };

    $mdDateLocaleProvider.parseDate = function(dateString) {
        var m = moment(dateString, 'DD-MM-YYYY', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };

    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
     $httpProvider.interceptors.push('APIInterceptor');
    // var PRODUCTION_SERVER_PATH = 'http://dev.healthwiz.in/';
    $urlRouterProvider.otherwise(function($injector, $location,$window){
        var state = $injector.get('$state');
        var provier=$injector.get('$window');
        if(provier.sessionStorage.getItem('roleId') == 2){
            state.go('viewinvoicechecker');
           // $urlRouterProvider.otherwise('/viewinvoicechecker'); //checker
        }else if(provier.sessionStorage.getItem('roleId') == 3){
            state.go('invoicemaker');
           // $urlRouterProvider.otherwise('/invoicemaker');// maker
        }else if(provier.sessionStorage.getItem('roleId') == 1){
            state.go('initiatePayment');

        }


    });
    $stateProvider
        .state('login', {
            url: '/login',
            views: {
                'template': {
                    templateUrl: "jsp/login.jsp",
                    controller: 'loginCtrl'
                },
                'header': {
                    "visible": false

                },
                'footer': {
                    'footer': {
                        "visible": false
                    }

                }

            }
        })
        .state('batchupload', {
            url: '/batchupload',
            views:
                {
                    'template': {
                        templateUrl: "ui/views/fileupload/batchupload.html",
                        controller: 'batchuploadCtrl'
                    },
                    'header': {
                        templateUrl: "ui/views/header.html",
                        controller: "headerCtrl"

                    },
                    'footer': {
                        templateUrl: "ui/views/footer.html",
                        controller: "footerCtrl"

                    }


                }
        })
        //checker
        .state('onboardentity', {
            url: '/distributorDetails',
            views: {
                'template': {
                    templateUrl: "distributor-details/distributorDetails.html",
                    controller: 'DistributorDetailsCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }
        })
        .state('topnavview', {
            url: '/topnavview',

            views: {
                'template': {
                    templateUrl: 'top-nav-view/topNavView.html',
                    controller: 'TopNavViewCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }

        })
        .state('topnavview.merchantonboarding', {
            url: '/merchantonboarding',
            abstract: true,
            views: {
                'template': {
                    templateUrl: 'merchant-onboarding/merchantOnboarding.html',
                    controller: 'MerchantOnboardingCtrl',
                    onEnter: function ($rootScope, $timeout) {
                        $timeout(function () {
                            $rootScope.$broadcast('ON_ENTER_STATE_MERCHANT_ONBOARDING', "");
                        })
                    }
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }

        })
        .state('topnavview.transactions', {
            url: '/transactions',
            views: {
                'template': {
                    templateUrl: 'transactions/transactions.html'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }


        })
        .state('topnavview.merchantonboarding.bankdetails', {
            url: '/bankdetails',
            views: {
                'template': {
                    templateUrl: 'bank-details/bankDetails.html',
                    controller: 'BankDetailsCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }

        })
        .state('topnavview.merchantonboarding.upidetails', {
            url: '/upidetails',
            views: {
                'template': {
                    templateUrl: 'upi-details/upiDetails.html',
                    controller: 'UpiDetailsCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }

        })
        .state('topnavview.confirmation', {
            url: '/confirmation',
            views: {
                'template': {
                    templateUrl: 'confirmation/confirmation.html',
                    controller: 'ConfirmationCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }

        })
        .state('topnavview.success', {
            url: '/success',
            views: {
                'template': {
                    templateUrl: 'success/success.html',
                    controller: 'SuccessCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }

        })

        // new routing apps-----------
        .state('topnavview.payment', {
            url: '/payment',
            abstract: true,
            views: {
                'template': {
                    templateUrl: 'payments/pages/payment/payment.html',
                    controller: 'PaymentCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }


        })



        .state('topnavview.payment.collect', {
            url: '/collect',
            views: {
                'template': {
                    templateUrl: 'payments/pages/collect/collect.html',
                    controller: 'CollectCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }


        })

        .state('topnavview.choosepayment', {
            url: '/choosepayment',
            abstract: true,
            views: {
                'template': {
                    templateUrl: 'payments/pages/choosePayment/choosePayment.html',
                    controller: 'ChoosePaymentCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }


        })

        .state('fromaccount', {
            url: '/fromaccount',
            views: {
                'template': {
                    templateUrl: 'payments/fromAccount/fromAccount.html',
                    controller: 'FromAccountCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }

        })



        .state('paymentselection', {
            url: '/paymentselection',

            views: {
                'template': {
                    templateUrl: 'payments/pages/selectPayment/selectionRoot/paymentSelection.html',
                    controller: 'PaymentSelectionCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }


        })

        .state('paymentselection.quickpay', {
            url: '/quickpay',
            views: {
                'template': {
                    templateUrl: 'payments/pages/selectPayment/quickPay/quickPay.html',
                    controller: 'QuickPayCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }


        })

        .state('paymentselection.internetbanking', {
            url: '/internetbanking',
            views: {
                'template': {
                    templateUrl: 'payments/pages/selectPayment/internetBanking/internetBanking.html',
                    controller: 'InternetBankingCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }


        })

        .state('paymentselection.cards', {
            url: '/cards',
            views: {
                'template': {
                    templateUrl: 'payments/pages/selectPayment/cards/cards.html',
                    controller: 'CardsCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }


        })

        .state('paymentselection.vpatransfer', {
            url: '/vpatransfer',
            views: {
                'template': {
                    templateUrl: 'payments/pages/selectPayment/vpaTransfer/vpaTransfer.html',
                    controller: 'VpaTransferCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }


        })

        //end of the resigion-------------


        //mandate Section crate and view and update
        .state('createmandate', {
            url: '/createmandate',
            views: {
                'template': {
                    templateUrl: "ui/views/mandate/createmandate.html",
                    controller: 'createMandateCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }
        })
        .state('verifymandate', {
            url: '/verifymandate',
            views: {
                'template': {
                    templateUrl: "ui/views/mandate/verifymandate.html",
                    controller: 'verifyMandateCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }
        })
        //operation Section
        .state('operations', {
            url: '/operations',
            views:
                {
                    'template': {
                        templateUrl: "ui/views/operations/operations.html",
                        controller: 'operationCtrl'
                    },
                    'header': {
                        templateUrl: "ui/views/header.html",
                        controller: "headerCtrl"

                    },
                    'footer': {
                        templateUrl: "ui/views/footer.html",
                        controller: "footerCtrl"

                    }


                },
            redirectTo: 'operations.mandates'

        })
        .state('operations.payment', {
            url: '/paymnet',
            views: {
                'template': {
                    templateUrl: "ui/views/operations/payment.html",
                    controller: 'operationCtrl'
                }
            }
        })
        .state('operations.mandates', {
            url: '/mandates',
            views: {
                'template': {
                    templateUrl: "ui/views/operations/mandates.html",
                    controller: 'mandatesCtrl'
                }
            }
        })
        //dashBoard section

        .state('initiatePayment', {
            url:'/initiatePayment',
            abstruct:true,
            views: {
                'template': {
                    templateUrl: 'payments/pages/payment/payment.html',
                    controller: 'PaymentCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            },

            redirectTo: 'initiatePayment.pay'

        })

        .state('initiatePayment.pay', {
            url: '/pay',
            views: {
                'template': {
                    templateUrl: 'payments/pages/pay/pay.html',
                    controller: 'PayCtrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }


        })
        .state('successfailure', {
            url: '/successfailure/:collectionFilter/:transactinFilter/:formDate/:toDate/:reportType/:status?totalval',
            views:
                {
                    'template': {
                        templateUrl: "ui/views/operations/successFailure.html",
                        controller: 'sucessFailureCtrl'
                    },
                    'header': {
                        templateUrl: "ui/views/header.html",
                        controller: "headerCtrl"

                    },
                    'footer': {
                        templateUrl: "ui/views/footer.html",
                        controller: "footerCtrl"

                    }

                }
        })
        .state('invoiceapprovedchecker', {
            url: '/invoiceapprovedchecker',
            views: {
                'template': {
                    templateUrl: "ui/views/checker/approvedinvoiceChecker.html",
                    controller: 'invoiceApprovedcheckerCtrl as ai'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }
        })
        .state('invoicependingchecker', {
            url: '/invoicependingchecker',
            views: {
                'template': {
                    templateUrl: "ui/views/checker/invoicependingChecker.html",
                    controller: 'invoiceCheckerpendingCtrl as pi'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }
        })
        .state('viewinvoicechecker', {
            url: '/viewinvoicechecker',
            views: {
                'template': {
                    templateUrl: "ui/views/checker/paymentprocessChecker.html",
                    controller: 'invoiceCheckerCtrl as ppi'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }
        })

        .state('invoicemaker', {
            url: '/invoicemaker',
            views: {
                'template': {
                    templateUrl: "ui/views/maker/viewInvoice.html",
                    controller: 'invoiceMakerCtrl as vi',
                    animation: 'second'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }
        })

        .state('invoicemakerpending', {
                url: '/invoicemakerpending',
                views: {
                    'template': {
                        templateUrl: "ui/views/maker/viewpendingInvoice.html",
                        controller: 'invoiceMakerpendingCtrl as va',
                        animation: 'first'
                    },
                    'header': {
                        templateUrl: "ui/views/header.html",
                        controller: "headerCtrl"

                    },
                    'footer': {
                        templateUrl: "ui/views/footer.html",
                        controller: "footerCtrl"

                    }

                }
            })
        .state('invoiceapprovedmaker', {
            url: '/invoiceapprovedmaker',
            views: {
                'template': {
                    templateUrl: "ui/views/maker/viewinvoiceApproved.html",
                    controller: 'invoiceApprovedMakerCtrl as vactrl'
                },
                'header': {
                    templateUrl: "ui/views/header.html",
                    controller: "headerCtrl"

                },
                'footer': {
                    templateUrl: "ui/views/footer.html",
                    controller: "footerCtrl"

                }

            }
        }) .state('downloadImage', {
            url: '/downloadImage',
            views: {
                'template': {
                    templateUrl: "ui/views/initiate/imageViewer.html",
                    controller: 'imageViewerCtrl'
                }


            }
        })

}]);


// Aws Server Path;

var PRODUCTION_PATH = 'https://13.126.208.204:8441/middleware/';

//Local Server Path
/*var PRODUCTION_PATH='http://lcocal.host:9000';*/

Adbapp.constant('CONSTANTS', {
    SERVER_PATH: PRODUCTION_PATH
});

Adbapp.run(['$rootScope', function ($rootScope) {
    $rootScope.ngPopupConfig = {
        width: 200,
        height: 200,
        template: '<div class="loader"></div>',
        resizable: false,
        draggable: false,
        hasTitleBar: false,
        isShow: false,
        position: {top: window.innerHeight / 2 - 100, left: window.innerWidth / 2 - 150},
        onOpen: function () {
            /*Some Logic...*/
        }
    }
}]);