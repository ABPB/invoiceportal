'use strict';

// Declare app level module which depends on views, and components
angular.module('B2B', ['ui.router', 'ngPopup'])

    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'login/login.html',
                controller: 'loginCtrl'
            })

            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'dashboard/dashboard.html',
                controller: 'dashCtrl'
            })

            .state('topnavview',{
                url: '/topnavview',
                abstract: true,
                templateUrl: 'top-nav-view/topNavView.html',
                controller: 'TopNavViewCtrl'
            })

            .state('topnavview.merchantonboarding', {
                url: '/merchantonboarding',
                abstract: true,
                templateUrl: 'merchant-onboarding/merchantOnboarding.html',
                controller: 'MerchantOnboardingCtrl',
                onEnter: function($rootScope, $timeout) {
                    $timeout(function() {
                        $rootScope.$broadcast('ON_ENTER_STATE_MERCHANT_ONBOARDING', "");
                    })
                },
            })

            .state('topnavview.transactions', {
                url: '/transactions',
                templateUrl: 'transactions/transactions.html'
            })

            .state('topnavview.merchantonboarding.bankdetails', {
                url: '/bankdetails',
                controller:'BankDetailsCtrl',
                templateUrl: 'bank-details/bankDetails.html'
            })

            .state('topnavview.merchantonboarding.upidetails', {
                url: '/upidetails',
                controller: 'UpiDetailsCtrl',
                templateUrl: 'upi-details/upiDetails.html'
            })

            .state('distributordetails', {
                url: '/distributordetails',
                templateUrl: 'distributor-details/distributorDetails.html',
                controller:'DistributorDetailsCtrl',
            })

            .state('topnavview.confirmation', {
                url: '/confirmation',
                templateUrl: 'confirmation/confirmation.html',
                controller:'ConfirmationCtrl',
            })

            .state('topnavview.success', {
                url: '/success',
                templateUrl: 'success/success.html',
                controller:'SuccessCtrl',
            });

        $urlRouterProvider.otherwise('distributordetails');


    }])

    .run(['$rootScope', '$state', 'B2bSharedServices', '$transitions', function($rootScope, $state, B2bSharedServices, $transitions) {

        $transitions.onStart( {  }, function($transitions) {
            if ($transitions.$to().name != "distributordetails" && B2bSharedServices.isDistributorAdded == false ) {
                return $state.target('distributordetails');
            }
        });
        console.log(B2bSharedServices.isDistributorAdded);
        $rootScope.$on( "$stateChangeStart", function(event, toState, toParams, fromState, fromParams, options) {
            console.log(B2bSharedServices.isDistributorAdded);

        });

        $rootScope.ngPopupConfig = {
            width: 200,
            height:200,
            template:'<div class="loader"></div>',
            resizable:false,
            draggable:false,
            hasTitleBar: false,
            isShow: false,
            position: { top : window.innerHeight/2 - 100, left : window.innerWidth/2 - 150},
            onOpen: function(){
                /*Some Logic...*/
            }
        }
    }])

    .controller('AppCtrl', function($scope, $location) {

        $scope.goto = function(pageName)
        {
            $location.path(pageName);
        }
    });