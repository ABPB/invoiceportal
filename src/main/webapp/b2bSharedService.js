'use strict';

(function(){
    angular.module('abpib').service('B2bSharedServices', [B2bSharedServices]);

    function B2bSharedServices() {
        
        var createNewBankDetails = function() {
            var newBankDetails = {};
            newBankDetails.abd_account_no;
            newBankDetails.abd_account_holdername = "";
            newBankDetails.abd_bank_name = "";
            newBankDetails.abd_branch_name = "";
            newBankDetails.abd_account_type = "Current";
            newBankDetails.abd_ifsc_code = "";
            newBankDetails.abd_linked_mobile_no = 0;
            newBankDetails.abd_linked_email_id = "";
            newBankDetails.abd_active_flg = "";
            newBankDetails.abd_preferred_bank_flg = "";
            newBankDetails.abd_crd_dbt_flg = "B";
            
            newBankDetails.accountNumberValidation = "";
            return newBankDetails;
        }

        var createNewUPIDetails = function() {
            var newUPIDetails = {};
            newUPIDetails.aud_upa_handler = "";
            newUPIDetails.aud_upa_crddbt_flg = "C";
            newUPIDetails.aud_upa_dft_flg = "N";
            newUPIDetails.aud_upa_active_flg = "N";

            newUPIDetails.accountType = "Credit"; //To remove
            newUPIDetails.isVerified = false;
            return newUPIDetails;
        }

        var distributorDetails;
        distributorDetails = {};
        distributorDetails.p_mer_mst_id = 101;
        distributorDetails.p_title = "mr";
        distributorDetails.p_first_name = "Ganesh"; 
        distributorDetails.p_middle_name = "";
        distributorDetails.p_last_name = "Babu";
        distributorDetails.p_phone_no_work = "9444172830";
        distributorDetails.p_phone_no_home = "9444172830";
        distributorDetails.p_gender =   "M";
        distributorDetails.p_email_id = "chris@ddd";
        distributorDetails.p_alt_email_id = "";
        distributorDetails.p_nationality = "Indian";
        distributorDetails.p_dob =   "1989-06-14";
        distributorDetails.p_entity_level = 1;
        distributorDetails.p_establishment_name = "Establishment Name";
        distributorDetails.p_establishment_id = 2;
        distributorDetails.p_poi_type = "PAN";
        distributorDetails.p_poi_id = "AKJPG4667L";
        distributorDetails.p_poa_type = "Adhar";
        distributorDetails.p_poa_id = "123456654321";

        distributorDetails.p_doorno =  "12f";
        distributorDetails.p_street = "Mailing Address";
        distributorDetails.p_city_village_town =  "";
        distributorDetails.p_district =   "chennai";
        distributorDetails.p_state = "";
        distributorDetails.p_pincode =   "600052";
        distributorDetails.p_country =   "India";
        distributorDetails.p_landmark =   "sun office";
        
        distributorDetails.p_m_doorno = "M Door No";
        distributorDetails.p_m_street = "M Street";
        distributorDetails.p_m_city_village_town = "M Village/Town";
        distributorDetails.p_m_district = "M District";
        distributorDetails.p_m_state =  "M State";
        distributorDetails.p_m_pincode =  "602001"; 
        distributorDetails.p_m_country =  "M Country";
        distributorDetails.p_m_landmark =  "M Landmark";

        distributorDetails.p_s_doorno =   "S Door No";
        distributorDetails.p_s_street =   "S ";
        distributorDetails.p_s_city_village_town = "S Village/Town";
        distributorDetails.p_s_district =   "S District";
        distributorDetails.p_s_state =   "S State";
        distributorDetails.p_s_pincode =   "602001";
        distributorDetails.p_s_country =  "S Country";
        distributorDetails.p_s_landmark = "S Landmark";
        
        distributorDetails.bankDetails = [];
        distributorDetails.activeBankDetails = createNewBankDetails();
        distributorDetails.bankDetails.push(distributorDetails.activeBankDetails);
        distributorDetails.bankDetails[0].abd_preferred_bank_flg = 'Y';
        
        distributorDetails.upiDetails = [];
        distributorDetails.upiDetails.push(createNewUPIDetails());
        distributorDetails.upiDetails[0].aud_upa_dft_flg = "Y";

        var isDistributorAdded = false;

        function checkIsValidBankDetail(bankDetail) {
            
            if(bankDetail.abd_ifsc_code.length != 11) return {valid:false,  message:"Please enter the valid ifsc code"};

            else if(bankDetail.abd_account_holdername == "")
                return {valid:false,  message:"Please enter valid bank account holder name"};
            
            else if(!bankDetail.abd_account_no)
                return {valid:false,  message:"Please enter valid bank account number"};

            else if(bankDetail.abd_bank_name == "" || bankDetail.abd_branch_name == "" || bankDetail.abd_account_no == "")
                return {valid:false,  message:"Please enter the valid bank details"};
            
            else if(isNaN(bankDetail.abd_account_no)) {
                return {valid:false,  message:"Bank account number must be of only numbers"};
            }
            
            else if(bankDetail.abd_account_no != bankDetail.accountNumberValidation)
                return {valid:false,  message:"Please check the account number. A/c number not matched with re-entered value"};

            else return {valid:true,  message:""};
        }

        return {
            distributorDetails: distributorDetails,
            createNewBankDetails: createNewBankDetails,
            createNewUPIDetails: createNewUPIDetails,
            isDistributorAdded: isDistributorAdded,
            checkIsValidBankDetail: checkIsValidBankDetail
        }
    }
})();