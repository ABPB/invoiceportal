'use strict';

(function(){
    angular.module('abpib').controller('DistributorDetailsCtrl', ['$scope', '$state', 'B2bSharedServices',DistributorDetailsCtrl]);

    function DistributorDetailsCtrl($scope, $state, B2bSharedServices) {

        $scope.distributorDetails = B2bSharedServices.distributorDetails;

        $scope.onProceedBtnClicked = function(){
            console.log('test',B2bSharedServices.distributorDetails);
            B2bSharedServices.isDistributorAdded = true;
            $state.go('topnavview.merchantonboarding.bankdetails');
        }

        
    }



})();