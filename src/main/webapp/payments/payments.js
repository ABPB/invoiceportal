'use strict';

// Declare app level module which depends on views, and components
angular.module('Payment', ['ui.router', "ngPopup"])

	.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

		$stateProvider

			.state('topnavview', {
				url: '/topnavview',
				abstract: true,
				templateUrl: 'payments/pages/top-nav-view/topNavView.html',
				controller: 'PaymentsTopNavViewCtrl'
			})

			.state('topnavview.payment', {
				url: '/payment',
				abstract: true,
				templateUrl: 'payments/pages/payment/payment.html',
				controller: 'PaymentCtrl'
			})

			.state('topnavview.payment.pay', {
				url: '/pay',
				templateUrl: 'payments/pages/pay/pay.html',
				controller: 'PayCtrl'
			})

			.state('topnavview.payment.collect', {
				url: '/collect',
				templateUrl: 'payments/pages/collect/collect.html',
				controller: 'CollectCtrl'
			})

			.state('topnavview.choosepayment', {
				url: '/choosepayment',
				abstract: true,
				templateUrl: 'payments/pages/choosePayment/choosePayment.html',
				controller: 'ChoosePaymentCtrl'
			})

			.state('fromaccount', {
				url: '/fromaccount',
				templateUrl: 'payments/fromAccount/fromAccount.html',
				controller: 'FromAccountCtrl'
			})
			
			.state('paymentselection', {
				url: '/paymentselection',
				abstract: true,
				templateUrl: 'payments/pages/selectPayment/selectionRoot/paymentSelection.html',
				controller: 'PaymentSelectionCtrl'
			})
			
			.state('paymentselection.quickpay', {
				url: '/quickpay',
				templateUrl: 'payments/pages/selectPayment/quickPay/quickPay.html',
				controller: 'QuickPayCtrl'
			})

			.state('paymentselection.internetbanking', {
				url: '/internetbanking',
				templateUrl: 'payments/pages/selectPayment/internetBanking/internetBanking.html',
				controller: 'InternetBankingCtrl'
			})

			.state('paymentselection.cards', {
				url: '/cards',
				templateUrl: 'payments/pages/selectPayment/cards/cards.html',
				controller: 'CardsCtrl'
			})

			.state('paymentselection.vpatransfer', {
				url: '/vpatransfer',
				templateUrl: 'payments/pages/selectPayment/vpaTransfer/vpaTransfer.html',
				controller: 'VpaTransferCtrl'
			});

				$urlRouterProvider.otherwise('fromaccount');

	}])

	.run(['$rootScope', '$state', '$transitions', 'B2bPaymentsSharedServices', function ($rootScope, $state, $transitions, B2bPaymentsSharedServices) {

		$transitions.onStart( {  }, function($transitions) {

			if ($transitions.$to().name == "fromaccount") {
				return;
			}

			else if($transitions.$to().name != "fromaccount" && !B2bPaymentsSharedServices.customerInformation.retailerId) {
				return $state.target('fromaccount');
			}

			/* else if ($transitions.$to().name != "topnavview.pay" && B2bPaymentsSharedServices.selectedInvoices.length <= 0 ) {
				return $state.target('topnavview.pay');
			} */
		});
	
		$rootScope.ngPopupConfig = {
			width: 200,
			height:200,
			template:'<div class="loader"></div>',
			resizable:false, 
			draggable:false,
			hasTitleBar: false,
			isShow: false,
			position: { top : window.innerHeight/2 - 100, left : window.innerWidth/2 - 150},
			onOpen: function(){
				/*Some Logic...*/
			}
		}
		
	}]);