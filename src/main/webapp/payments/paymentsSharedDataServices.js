'use strict';

(function(){
    angular.module('abpib').service('B2bPaymentsSharedServices', ['$http', '$q', B2bPaymentsSharedServices]);

    function B2bPaymentsSharedServices($http, $q) {
        
        var selectedInvoices = [];
        var customerInformation = {};
        var invoiceDetails = {};
        var amountToBePayorCollect;
        var isPushorPull;
    
        return {
            selectedInvoices: selectedInvoices,
            customerInformation: customerInformation,
            invoiceDetails: invoiceDetails,
            amountToBePayorCollect: amountToBePayorCollect,
            isPushorPull: isPushorPull
        }
    }
})();