(function(){

    angular.module('abpib').controller('FromAccountCtrl', ['$scope', '$state','$rootScope','adithyaServices','userStroage', FromAccountCtrl]);

    function FromAccountCtrl($scope, $state,$rootScope,adithyaServices,userStroage) {

        var token=userStroage.get('access_token');
        var access_token= "bearer" + " " + token;
        adithyaServices.getInvoiceDetails(access_token).then(function(data){
            console.log(data);
            $rootScope.ngPopupConfig.isShow = false;
            $state.go('topnavview.payment.pay');
        }, function(error) {
            $rootScope.ngPopupConfig.isShow = false;
            alert("There is an error in service call. Please contact administrator");
            console.log(error);
        });

    }

})();