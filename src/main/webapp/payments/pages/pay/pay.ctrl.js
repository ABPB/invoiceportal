(function(){
    angular.module('abpib').controller('PayCtrl', ['$scope', '$state','B2bPaymentsSharedServices','adithyaServices','$window','userStroage', PayCtrl]);

    function PayCtrl($scope, $state,B2bPaymentsSharedServices,adithyaServices,$window,userStroage) {

        var encoded = btoa("middleware:secret");
        adithyaServices.loginNec(encoded).then(function (d) {
            userStroage.set('access_token', d.access_token);
        });
       $scope.numberOfPaidInvoices = 0;
        $scope.numberOfUnPaidInvoices = 0;
        var token=userStroage.get('access_token');
        var access_token= "bearer" + " " + token;
        var entity_id=$window.sessionStorage.getItem('entityId');
        var merchantId=$window.sessionStorage.getItem('merchantId');
        adithyaServices.getInvoiceDetails(access_token,merchantId,entity_id).then(function (invoiceObj) {
            B2bPaymentsSharedServices.invoiceDetails=invoiceObj;
            $scope.invoiceDetails = invoiceObj.invoiceDtls[0].pay;

        });
        var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
        var today = new Date();
        if($scope.invoiceDetails != undefined){
            for(var i=0; i<$scope.invoiceDetails.length; i++)
            {
                var dueDate = new Date($scope.invoiceDetails[i].invoiceDueDate);
                var diffDays = Math.round((dueDate.getTime() - today.getTime())/(oneDay));
                console.log(Math.abs(diffDays));
                (diffDays > 0)? $scope.invoiceDetails[i].isOverDue = false: $scope.invoiceDetails[i].isOverDue = true;
                $scope.invoiceDetails[i].diffDays = Math.abs(diffDays);
                if($scope.invoiceDetails[i].isPaid == true) ++$scope.numberOfPaidInvoices;
                else ++$scope.numberOfUnPaidInvoices;
            }
        }


        $scope.onselectBtnClicked = function(invoice) {
            if(invoice.isPaid) return;
            invoice.isSelected = !invoice.isSelected;
        }

        $scope.selectAll = function() {
            angular.forEach($scope.invoiceDetails, function(invoice) {
                if(!invoice.isPaid) invoice.isSelected = $scope.selectedAll;
            });
        };

        $scope.onPayBtnClicked = function() {

            var totalPayAmount = 0;
            var selectedInvoices = [];
            angular.forEach($scope.invoiceDetails, function(invoice) {
                if(invoice.isSelected) 
                {
                    selectedInvoices.push(invoice);
                    totalPayAmount += parseFloat(invoice.invoiceAmt);
                }
            });

            console.log(selectedInvoices);
            if(selectedInvoices.length == 0) {
                alert("Please select atleast one invoice");
                return;
            }

            B2bPaymentsSharedServices.amountToBePayorCollect = totalPayAmount;
            B2bPaymentsSharedServices.selectedInvoices = selectedInvoices;
            B2bPaymentsSharedServices.isPushorPull = "PUSH";

            $state.go('paymentselection.quickpay');
        }
    }

    

})();