(function(){
    angular.module('abpib').controller('PaymentSelectionCtrl', ['$scope', '$state', '$rootScope', 'B2bPaymentsSharedServices', 'B2bPaymentsAPIServices','adithyaServices','$window','userStroage',PaymentSelectionCtrl]);

    function PaymentSelectionCtrl($scope, $state, $rootScope, B2bPaymentsSharedServices, B2bPaymentsAPIServices,adithyaServices,$window,userStroage) {

        var token=userStroage.get('access_token');
        var access_token= "bearer" + " " + token;

        $scope.customerEmailId = B2bPaymentsSharedServices.customerInformation.emailId;
        $scope.customerPhoneNumber = B2bPaymentsSharedServices.customerInformation.phoneNumber;

        $scope.amountToBePayorCollect = B2bPaymentsSharedServices.amountToBePayorCollect;
        $scope.isDisabled = true;

        $rootScope.ngPopupConfig.isShow = true;
        B2bPaymentsSharedServices.customerInformation.retailerId =$window.sessionStorage.getItem('entityId');
        var merchantId=$window.sessionStorage.getItem('merchantId');
        adithyaServices.getPamentsModes(access_token,merchantId,B2bPaymentsSharedServices.customerInformation.retailerId, B2bPaymentsSharedServices.isPushorPull).then(function(data){
            console.log(data);
            if(!data.paymentModes)
            {
                alert("There is an issue in API. Please contact the administrator");
                $rootScope.ngPopupConfig.isShow = false;
                return;
            }

            var paymentsModeArray = data.paymentModes.mode;
            var rawUPIArray = [];
            var filteredUPIArray = [];
            var rawNetBankingArray = [];
            var filteredNetBankingArray = [];
            $scope.paymentMode = {};

            for(var i=0; i<paymentsModeArray.length; i++)
            {
                if(paymentsModeArray[i].mode == "NB") {
                    rawNetBankingArray = paymentsModeArray[i].bankDetails;
                }

                else if(paymentsModeArray[i].mode == "UPI")  {
                    rawUPIArray = paymentsModeArray[i].upiDetails;
                }
            }

            //$scope.netBankingArray = rawNetBankingArray;
            var isBankPreferredSelectecd = false;

            angular.forEach(rawNetBankingArray, function(bankDetail) {
                if(bankDetail.preferredFlag == 'Y') {
                    isBankPreferredSelectecd = true;
                    bankDetail.preferredradio = true;
                    $scope.paymentMode.name = "NB";
                    filteredNetBankingArray.push(bankDetail);
                }
            });

            angular.forEach(rawUPIArray, function(upiDetail) {
                if(upiDetail.preferredFlag == 'Y') {
                    if(isBankPreferredSelectecd == true) {
                        upiDetail.preferredFlag = 'N';
                        upiDetail.preferredradio = false;
                    }
                    else {
                        upiDetail.preferredradio = true;
                        $scope.paymentMode.name = "UPI_"+B2bPaymentsSharedServices.isPushorPull;
                    }
                    filteredUPIArray.push(upiDetail);
                }
            });

            console.log(filteredUPIArray);
            $scope.netBankingArray = filteredNetBankingArray;
            $scope.upiDetailArray = filteredUPIArray;

            // $scope.$broadcast('BANK_LIST', {filteredNetBankingArray, filteredUPIArray});

            $rootScope.ngPopupConfig.isShow = false;

        },function(error){
            $rootScope.ngPopupConfig.isShow = false;
            alert("There is an issue in API. Please contact the administrator");

        });

        $scope.callroot=function () {
            $state.go(paymentselection.internetbanking)
        }

        $scope.onInternetBankChoosed = function($event) {
            $event.preventDefault();
        };

        $scope.onCardsChoosed = function($event) {
            if(B2bPaymentsSharedServices.isPushorPull == "PULL") $event.preventDefault();
        }
    }
})();