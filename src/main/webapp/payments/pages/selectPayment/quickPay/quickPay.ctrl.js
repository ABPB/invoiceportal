(function(){
    angular.module('abpib').controller('QuickPayCtrl', ['$scope', '$state', '$rootScope', 'B2bPaymentsSharedServices', 'B2bPaymentsAPIServices','adithyaServices','$window','userStroage', QuickPayCtrl]);

    function QuickPayCtrl($scope, $state, $rootScope, B2bPaymentsSharedServices, B2bPaymentsAPIServices,adithyaServices,$window,userStroage) {
        var token=userStroage.get('access_token');
        var access_token= "bearer" + " " + token;

        $scope.upiFlagName = "UPI_"+B2bPaymentsSharedServices.isPushorPull;

        $scope.onBankChanged = function() {
            console.log($scope.paymentMode.name);
        }


        $scope.onPayBtnClicked = function() {

            
            var invoiceList = [];
            for(var i=0; i<B2bPaymentsSharedServices.selectedInvoices.length; i++)
            {
                var invoiceData = {};
                invoiceData.invoiceId = B2bPaymentsSharedServices.selectedInvoices[i].invoiceId;
                invoiceData.dueDate = B2bPaymentsSharedServices.selectedInvoices[i].invoiceDueDate;
                invoiceData.invoiceAmount = B2bPaymentsSharedServices.selectedInvoices[i].invoiceAmt;
                invoiceList.push(invoiceData);
            }
            B2bPaymentsSharedServices.customerInformation.retailerId=$window.sessionStorage.getItem('entityId');
           var  merchantId=$window.sessionStorage.getItem('merchantId');

            var requestObject = {
                "clientTxnRef":"34567",
                "merchantId" : merchantId,
                "fromEntityId": B2bPaymentsSharedServices.customerInformation.retailerId,
                "toEntityId":35, //B2bPaymentsSharedServices.customerInformation.distributorId,
                "paymentMode":$scope.paymentMode.name,
                "invoiceDtlsList":invoiceList
            };

            $rootScope.ngPopupConfig.isShow = true;
            adithyaServices.createPayment(access_token,requestObject).then(function(data){
                console.log(data);

                if(data.clientPaymentChannel == 'Direct') {
                    if(data.status == "Success") window.location.href = "upiSuccess.html";

                    else  window.location.href = "upiFailure.html";
                }

                else if(data.redirectURL) window.location.href = data.redirectURL;
                else alert("There is an issue in API. Please contact the administrator");
                $rootScope.ngPopupConfig.isShow = false;

            },function(error){
                $rootScope.ngPopupConfig.isShow = false;
                alert("There is an issue in API. Please contact the administrator");
                console.log(error);
            });


            console.log(B2bPaymentsSharedServices.selectedInvoices);
        }
    }
})();