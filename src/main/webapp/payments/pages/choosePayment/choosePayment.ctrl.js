(function(){
    angular.module('abpib').controller('ChoosePaymentCtrl', ['$scope', '$state', '$rootScope', 'B2bPaymentsSharedServices', 'B2bPaymentsAPIServices','userStroage','$window','adithyaServices', ChoosePaymentCtrl]);

    function ChoosePaymentCtrl($scope, $state, $rootScope, B2bPaymentsSharedServices, B2bPaymentsAPIServices,userStroage,$window,adithyaServices) {

        $rootScope.ngPopupConfig.isShow = true;
        var token=userStroage.get('access_token');
        var access_token= "bearer" + " " + token;


        B2bPaymentsSharedServices.customerInformation.retailerId=$window.sessionStorage.getItem('entityId');
        var  merchantId=$window.sessionStorage.getItem('merchantId');
        B2bPaymentsAPIServices.getPamentsModes(access_token,merchantId,B2bPaymentsSharedServices.customerInformation.retailerId,B2bPaymentsSharedServices.isPushorPull).then(function(data){
            
            if(!data.paymentModes)
            {
                alert("There is an issue in API. Please contact the administrator");
                $rootScope.ngPopupConfig.isShow = false;
                return;
            }

            var paymentsModeArray = data.paymentModes.mode;
            for(var i=0; i<paymentsModeArray.length; i++)
            {
                if(paymentsModeArray[i].mode == "NB") {
                    $scope.netBankingArray = paymentsModeArray[i].bankDetails;
                }
    
                else if(paymentsModeArray[i].mode == "UPI")  {
                    $scope.UPIArray = paymentsModeArray[i].upiDetails;
                }

                var filteredArray = [];
                var isBankPreferredSelectecd = false;
                angular.forEach($scope.netBankingArray, function(bankDetail) {
                    if(bankDetail.preferredFlag == 'Y') {
                        isBankPreferredSelectecd = true;
                        filteredArray.push()
                    }
                });
                $scope.netBankingArray = filteredArray;


                if(isBankPreferredSelectecd == true) {
                    angular.forEach($scope.UPIArray, function(upiDetail) {
                        upiDetail.preferredFlag = 'N';
                    });
                }
            }

            $rootScope.ngPopupConfig.isShow = false;
            
        },function(error){
            $rootScope.ngPopupConfig.isShow = false;
            alert("There is an issue in API. Please contact the administrator");
    
        });
        
        
        $scope.onPreferredBankChange = function(bankEntry) {
            
            if(bankEntry.preferredFlag === 'Y') return;

            bankEntry.preferredFlag = 'Y';

            angular.forEach($scope.UPIArray, function(upiDetail) {
                upiDetail.preferredFlag = 'N';
            });
            $scope.isCreditCardPreferred = false;
            $scope.isDebitCardPreferred = false;
        }

        $scope.onPreferredUPIHandlerChange = function(upiEntry) {
            
            if(upiEntry.preferredFlag === 'Y') return;

            upiEntry.preferredFlag = 'Y';
            angular.forEach($scope.netBankingArray, function(bankDetail) {
                bankDetail.preferredFlag = 'N';
            });
            $scope.isCreditCardPreferred = false;
            $scope.isDebitCardPreferred = false;
        }

        $scope.onDebitCardPreffered = function() {
            
            if($scope.isDebitCardPreferred == true) return;
            
            $scope.isDebitCardPreferred = !$scope.isDebitCardPreferred;
            $scope.isCreditCardPreferred = false;
            angular.forEach($scope.netBankingArray, function(bankDetail) {
                bankDetail.preferredFlag = 'N';
            });
            angular.forEach($scope.UPIArray, function(upiDetail) {
                upiDetail.preferredFlag = 'N';
            });
        }

        $scope.onCreditCardPreffered = function() {

            if($scope.isCreditCardPreferred == true) return;

            $scope.isCreditCardPreferred = !$scope.isCreditCardPreferred;
            $scope.isDebitCardPreferred = false;
            
            angular.forEach($scope.netBankingArray, function(bankDetail) {
                bankDetail.preferredFlag = 'N';
            });
            angular.forEach($scope.UPIArray, function(upiDetail) {
                upiDetail.preferredFlag = 'N';
            });
        }

        $scope.onProceedBtnClicked = function() {

            var paymentMode = "";

            angular.forEach($scope.netBankingArray, function(bankEntry) {
                if(bankEntry.preferredFlag === 'Y') paymentMode = "NB";
            });

            if(paymentMode == "")
            {
                angular.forEach($scope.UPIArray, function(upiEntry) {
                    if(upiEntry.preferredFlag === 'Y') paymentMode = "UPI_PULL";
                });
            }

            if(paymentMode == "" && $scope.isCreditCardPreferred) paymentMode = "CC";

            if(paymentMode == "" && $scope.isDebitCardPreferred) paymentMode = "DC";

            if(paymentMode == "")
            {
                alert("Please select payment detail");
                return;
            }

            var invoiceList = [];
            for(var i=0; i<B2bPaymentsSharedServices.selectedInvoices.length; i++)
            {
                var invoiceData = {};
                invoiceData.invoiceData = B2bPaymentsSharedServices.selectedInvoices[i].invoiceId;
                invoiceData.dueDate = B2bPaymentsSharedServices.selectedInvoices[i].dueDate
                invoiceData.invoiceAmount = B2bPaymentsSharedServices.selectedInvoices[i].invoiceAmount;
                invoiceList.push(invoiceData);
            }

            var requestObject = {
                "clientId":"INVOICE",
                "clientTxnRef":"34567",
                "merchantId" : "101",
                "fromEntityId":B2bPaymentsSharedServices.customerInformation.retailerId,
                "toEntityId":B2bPaymentsSharedServices.customerInformation.distributorId,
                "paymentMode":paymentMode,
                "invoiceDtlsList":invoiceList
            }

            $rootScope.ngPopupConfig.isShow = true;

            B2bPaymentsAPIServices.createPayment(requestObject).then(function(data){
                console.log(data);
                
                if(data.clientPaymentChannel == 'Direct') {
                    if(data.status == "Success") window.location.href = "upiSuccess.html";

                    else  window.location.href = "upiFailure.html";                    
                }

                else if(data.redirectURL) window.location.href = data.redirectURL;
                else alert("There is an issue in API. Please contact the administrator");
                $rootScope.ngPopupConfig.isShow = false;
                
            },function(error){
                alert("There is an issue in API. Please contact the administrator");
                console.log(error);
            });
            console.log(B2bPaymentsSharedServices.selectedInvoices);
        }

    }

})();