'use strict';

(function(){
    angular.module('abpib').service('B2bPaymentsAPIServices', ['$http', '$q', B2bPaymentsAPIServices]);

    function B2bPaymentsAPIServices($http, $q) {
        
        var serverPath = 'https://13.126.208.204:8441/middleware/';
        
        function callService(methodType, requestBody, path)
        {
            var deferred = $q.defer();
            var req = {
                method: methodType,
                url: path,
                data: requestBody,
                timeout: 150000,
                headers: {
                    'Content-Type': 'application/json'
                }
            };

            $http(req).success(function(data){
                deferred.resolve(data);
            })

            .error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function getPamentsModes(establishmentId, paymentMode) {
            return callService('GET', {}, serverPath+"payment/paymentModes?entityId=101&establishmentId=" +establishmentId+ "&type=" +paymentMode);
        }
    
        function createPayment(data) {    
            return callService('POST', data, serverPath+"payment/create");
        }

        function getBankDetailsForIFSC(ifscCode) {
            return callService('GET', {}, serverPath+"payment/bankIFSC?ifscCode="+ifscCode);
        }

        function verifyUPIHandler(handler) {
            return callService('GET', {}, serverPath+"payment/upiDetails?upiName="+handler);
        }

        function postRegistrationDetails(registrationData) {
            console.log(registrationData);
            return callService('POST', registrationData, serverPath+'payment/register')
        }

        function getCustomerDetails(customerId) {
            return callService('GET', {}, serverPath+'merchantOnboarding/customerDetails?customerId='+customerId);
        }

        function getInvoiceDetails(invoiceRequestObj) {
            console.log(invoiceRequestObj);
            return callService('POST', invoiceRequestObj, serverPath+'invoice/getInvoiceDetails');
        }
    
        return {
            getPamentsModes: getPamentsModes,
            getCustomerDetails: getCustomerDetails,
            createPayment: createPayment,
            getBankDetailsForIFSC: getBankDetailsForIFSC,
            postRegistrationDetails: postRegistrationDetails,
            verifyUPIHandler: verifyUPIHandler,
            getInvoiceDetails: getInvoiceDetails
        }
    }
})();