'use strict';

(function(){
    angular.module('abpib').controller('ConfirmationCtrl', ['$scope', '$state', '$rootScope', 'B2bSharedServices', 'B2bAPIServices','adithyaServices','userStroage', ConfirmationCtrl]);

    function ConfirmationCtrl($scope, $state, $rootScope, B2bSharedServices, B2bAPIServices,adithyaServices,userStroage) {
        var token=userStroage.get('access_token');
        var access_token= "bearer" + " " + token;

        $scope.distributorDetails = B2bSharedServices.distributorDetails;
        var bankDetailsArray = $scope.distributorDetails.bankDetails;
        var upiDetailsArray = $scope.distributorDetails.upiDetails;

        $scope.onConfirmBtnClicked = function(){

            console.log($scope.distributorDetails);

            for(var i=0; i<bankDetailsArray.length; i++) {
                if(!bankDetailsArray[i].isValid) bankDetailsArray.splice(i, 1);
            }

            for(var i=0; i<upiDetailsArray.length; i++) {
                if(!upiDetailsArray[i].isVerified) upiDetailsArray.splice(i, 1);
            }

            $rootScope.ngPopupConfig.isShow = true;
            adithyaServices.postRegistrationDetails(access_token,$scope.distributorDetails).then(function(data){
                $rootScope.ngPopupConfig.isShow = false;
                if(data.body == "Success")
                {
                    $state.go('topnavview.success');
                }

                else alert("Please check the values");

            },function(error){
                $rootScope.ngPopupConfig.isShow = false;
                alert("There is an issue in API. Please contact the administrator");
            });


        };
        
        $scope.onEditBankBtnClicked = function(bankDetail) {
            $state.go("topnavview.merchantonboarding.bankdetails");
        };

        $scope.onEditUPIBtnClicked = function(upiDetail){
            $state.go("topnavview.merchantonboarding.upidetails");
        };

        $scope.addAnotherAccountClicked = function() {
            $state.go("topnavview.merchantonboarding.bankdetails");
        } 
    }

})();