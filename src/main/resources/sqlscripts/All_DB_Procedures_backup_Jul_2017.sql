--------------------------------------------------------
--  File created - Tuesday-July-18-2017   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View CRS_BULK_PULL
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "RECON_USR"."CRS_BULK_PULL" ("FC_BATCH_ID", "FC_RUNG_SEQ", "FC_CUST_ID", "FC_CUST_NAME", "FC_CUST_VPA", "FC_BILL_NO", "FC_BILL_MONTH", "FC_BILL_AMOUNT", "FC_BILL_DUEDATE", "FC_CHANNEL", "CA_MERCHANT_CATEGORYCODE", "CA_CLIENT_VPA", "CA_SETTLEMENT_ACCOUNTNO", "CA_SETTLEMENT_IFSC", "CA_SETTLEMENT_BANK", "CA_CLIENT_MOBILENO", "CA_CLIENT_DEVICEID") AS 
  SELECT C.FC_BATCH_ID,
                                C.FC_RUNG_SEQ,
                                C.FC_CUST_ID,
                                C.FC_CUST_NAME,
                                C.FC_CUST_VPA,
                                C.FC_BILL_NO,
                                C.FC_BILL_MONTH,
                                C.FC_BILL_AMOUNT,
                                C.FC_BILL_DUEDATE,
                                C.FC_CHANNEL,
                                D.CA_MERCHANT_CATEGORYCODE,
                                D.CA_CLIENT_VPA,
                                D.CA_SETTLEMENT_ACCOUNTNO,
                                D.CA_SETTLEMENT_IFSC,
                                D.CA_SETTLEMENT_BANK,
                                D.CA_CLIENT_MOBILENO,
                                D.CA_CLIENT_DEVICEID
                         FROM recon_usr.crs_final_customer c,
                               recon_usr.crs_client_accountdetail d
                         WHERE c.fc_cust_id = d.ca_client_id
                              AND c.fc_status IN (
                                        SELECT cs_status
                                        FROM recon_usr.crs_status
                                        WHERE cs_status_id IN ( 2,3 ))
;
--------------------------------------------------------
--  DDL for View CUSTOMER_MASTER_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "RECON_USR"."CUSTOMER_MASTER_VIEW" ("USRID", "MSISDN", "CM_DESCRIPTION", "LAST_TRANSCTN_DATE", "CID", "CM_BANK_CIRCLE", "FRST_NME", "MDDLE_NME", "LST_NME", "DB", "GENDER", "MMN", "HSE_NMBR", "HSE_STRT", "HSE_LNDMRK", "HSE_STATE", "HSE_DSTRCT", "HSE_PNCDE", "CUNTRY", "TLPHNE", "HSE_CTY", "POI_VALUE_TYPE", "POI_NMBR", "POA_VALUE", "POA_NMBR", "CM_CHN_VALUE", "CRTD_ON", "UPDTD_ON", "IP_STTS", "CM_ACTVTN_TYPE", "RGSTRNG_AGNT_MSISDN", "CM_VALUE", "FORM_ACCPT_STTS", "FORM_ACCPT_DATE", "MN_ACTVTN_DATE", "APPLCTN_NMBR", "EMIL", "RGSTRNG_AGENT_USERID", "CM_BANK_CIRCLE_REG", "CM_CHN_VALUE_IP_MODE", "IP_DATE", "AF_STTS", "CM_CHN_VALUE_AF_MODE", "AF_DATE", "KYC_UPDATED_ON", "KYC_REMARKS", "PINCODE", "IMEI_NO", "LAST_IMEI", "IS_IMEI_WHITELISTED", "IS_ALL_NOTIFIACTION_ON", "IS_BILLPAY_NOTIFIACTION_ON", "IS_RQSTMNY_NOTIFICATION_ON", "CM_SRC_PRDCT_VAL", "CM_VALUE_RUL_BND_MAST", "CM_VALUE_CHG_BND_MAST", "CIF_ID", "ACCNT_NMBR", "CREATEDBY", "UPDATEDBY") AS 
  select a.USRID,
a.MSISDN,
nvl((select description from  lov_stts@dms2_link where value = STTS),'NA') as CM_description,
a.LAST_TRANSCTN_DATE,
a.CID,
nvl((select bank_circle from  lov_circle_sol@dms2_link where circle_id = a.CIRCLE_ID),'NA') as CM_bank_circle,
a.FRST_NME,
a.MDDLE_NME,
a.LST_NME,
a.DB,
a.GENDER,
a.MMN,
b.HSE_NMBR,
b.HSE_STRT,
b.HSE_LNDMRK,
b.HSE_STATE,
b.HSE_DSTRCT,
b.HSE_PNCDE,
b.CUNTRY,
b.TLPHNE,
B.HSE_CTY,
nvl((select src_poi_value from  lov_poi_type@dms2_link where src_poi_type = POI_TYPE),'NA') as POI_VALUE_TYPE,
a.POI_NMBR,
nvl((select src_poa_value from  lov_poa_type@dms2_link where src_poa_type = POA_TYPE),'NA') as poa_value,
a.POA_NMBR,
nvl((select chn_value from  lov_chn_id@dms2_link where CH_ID = RGSTRNG_CHNNL),'NA') as CM_chn_value,
a.CRTD_ON,
a.UPDTD_ON,
a.IP_STTS,
case when ACTVTN_TYPE = '0' then 'minKYC' when ACTVTN_TYPE = '2' then 'fullKYC' else 'NA' end as CM_ACTVTN_TYPE,
a.RGSTRNG_AGNT_MSISDN,
nvl((select value from  lov_mod_reg@dms2_link where ID = MODE_OF_RGSTRTN),'NA') as CM_value,
a.FORM_ACCPT_STTS,
a.FORM_ACCPT_DATE,
a.MN_ACTVTN_DATE,
a.APPLCTN_NMBR,
a.EMIL,
a.RGSTRNG_AGENT_USERID,
NVL((select BANK_CIRCLE from  LOV_CIRCLE_SOL@DMS2_LINK where CIRCLE_ID = RGSTRNG_CRCLE),'NA') as CM_BANK_CIRCLE_REG,
nvl((select chn_value from  lov_chn_id@dms2_link where CH_ID = IP_MODE),'NA') as CM_chn_value_IP_MODE,
a.IP_DATE,
a.AF_STTS,
nvl((select chn_value from  lov_chn_id@dms2_link where CH_ID = AF_MODE),'NA') as CM_chn_value_AF_MODE,
a.AF_DATE,
a.KYC_UPDATED_ON,
a.KYC_REMARKS,
a.PINCODE,
a.IMEI_NO,
a.LAST_IMEI,
a.IS_IMEI_WHITELISTED,
a.IS_ALL_NOTIFIACTION_ON,
a.IS_BILLPAY_NOTIFIACTION_ON,
a.IS_RQSTMNY_NOTIFICATION_ON,
NVL((select SRC_PRDCT_VAL from  LOV_PRODUCTS@DMS2_LINK where PRDCT_MSTR_ID = a.PRDCT_MSTR_ID),'NA') as CM_SRC_PRDCT_VAL,
NVL((select value from  LOV_RUL_BND_MAST@DMS2_LINK where id = RLE_BND_MSTR_ID),'NA') as CM_VALUE_RUL_BND_MAST, 
nvl((select value from  lov_chg_bnd_mast@dms2_link where ID = CHRGE_BND_MSTR_ID),'NA') as CM_value_CHG_bnd_masT, 
a.CIF_ID,
a.ACCNT_NMBR,
'admin' as CREATEDby,
'admin' as updatedby
from MIG_CUSTOMER_PROFILE_STG@dms2_link  a  left outer join MIG_CUSTOMER_CONTACT_STG@dms2_link b 
on a.CID = B.CID and B.ADDRSS_TYPE_MSTR_ID = '1001'
;
--------------------------------------------------------
--  DDL for View DISTRIBUTOR_MASTER_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "RECON_USR"."DISTRIBUTOR_MASTER_VIEW" ("USRID", "ENTITYTYPE", "MSISDN", "MPIN_ATTMPT_CNT", "STATUS", "LAST_TRANSCTN_DATE", "TRDID", "MD_MSISDN", "PN_NMBR", "CRTD_ON", "UPDTD_ON", "MD_NME", "DOB_DOI", "ZONETYPE", "BUSTYPE", "FRST_NME", "MDDLE_NME", "LST_NME", "MMN", "HSE_NMBR", "HSE_STRT", "HSE_LNDMRK", "HSE_STATE", "HSE_DSTRCT", "HSE_PNCDE", "CUNTRY", "TLPHNE", "EMIL", "HSE_CTY", "SHP_NME", "DOCTYPE1", "POI_NMBR", "DOCTYPE2", "POA_NMBR", "PRDCT_MSTR_ID", "CMMSSN_BND_ID", "CIF_ID", "ACCNT_NMBR", "BNK_ACCNT_NMBR", "IFSC_CDE", "ACCNT_HLDR_NME", "CIRCLE", "CREATEDBY", "UPDATEDBY") AS 
  select a.usrid, 
nvl((select src_val from lov_trade_entity_id@dms2_link where src_id = entity_mster_id),'NA') as entitytype,
a.MSISDN,
a.MPIN_ATTMPT_CNT,
nvl((select description from  lov_stts@dms2_link where value = STTS),'NA') as status,
a.LAST_TRANSCTN_DATE,
a.TRDID,
a.MD_MSISDN,
a.PN_NMBR,
a.CRTD_ON,
a.UPDTD_ON,
a.MD_NME,
a.DOB_DOI,
nvl((select value from  lov_MSTR_ID@dms2_link where id = LOC_MSTR_ID),'NA') as zonetype ,
nvl((select src_val from  lov_industry@dms2_link where src_id = INDSTRY_ID),'NA') as bustype,
a.FRST_NME,
a.MDDLE_NME,
a.LST_NME,
a.MMN,
b.HSE_NMBR,
b.HSE_STRT,
b.HSE_LNDMRK,
b.HSE_STATE,
b.HSE_DSTRCT,
b.HSE_PNCDE,
b.CUNTRY,
b.TLPHNE,
b.EMIL,
b.HSE_CTY,
b.SHP_NME,
nvl((select src_poi_value from  lov_poi_type@dms2_link where src_poi_type = POI_TYPE),'NA') as doctype1,
a.POI_NMBR,
nvl((select src_poa_value from  lov_poa_type@dms2_link where src_poa_type = POA_TYPE),'NA') as doctype2,
a.POA_NMBR,
a.PRDCT_MSTR_ID,
a.CMMSSN_BND_ID,
a.CIF_ID,
a.ACCNT_NMBR,
a.BNK_ACCNT_NMBR,
a.IFSC_CDE,
a.ACCNT_HLDR_NME,
nvl((select bank_circle from  lov_circle_sol@dms2_link where circle_id = CRCLE_ID),'NA') as circle,
'admin' as createdby,
'admin' as updatedby
from mig_trade_profile_stg@dms2_link a left outer join MIG_TRADE_CONTACT_STG@dms2_link b 
on a.trdid = b.trdid and addrss_type_mstr_id = '1001'
;
--------------------------------------------------------
--  DDL for View MERCHANT_MASTER_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "RECON_USR"."MERCHANT_MASTER_VIEW" ("USRID", "TRADE_ENTITY_TYPE", "MSISDN", "STTS_DESCRIPTION", "ENTERPRISE_ID", "CRTD_ON", "UPDTD_ON", "PN_NMBR", "DOB_DOI", "BANK_CIRCLE", "CATEGORY_TYPE", "BUS_CATEGORY_TYPE", "CIF_ID", "ACCNT_NMBR", "FRST_NME", "MDDLE_NME", "LST_NME", "MMN", "TLPHNE", "EMIL", "HSE_NMBR", "HSE_STRT", "HSE_LNDMRK", "HSE_STATE", "HSE_DSTRCT", "HSE_CTY", "HSE_PNCDE", "SHP_NME", "SRC_POI_VALUE", "POI_NMBR", "SRC_POA_VALUE", "POA_NMBR", "BNK_ACCNT_NMBR", "IFSC_CDE", "ACCNT_HLDR_NME", "CREATED_BY", "UPDATED_BY") AS 
  select a.USRID,
nvl((select src_val from  lov_trade_entity_id@dms2_link where src_id = entity_mster_id),'NA') as TRADE_ENTITY_TYPE,
a.MSISDN,
nvl((select description from  lov_stts@dms2_link where value = STTS),'NA') as STTS_DESCRIPTION,
a.ENTERPRISE_ID,
a.CRTD_ON,
a.UPDTD_ON,
a.PN_NMBR,
a.DOB_DOI,
NVL((select BANK_CIRCLE from  LOV_CIRCLE_SOL@DMS2_LINK where CIRCLE_ID = CRCLE_ID),'NA') as BANK_CIRCLE,
case when EN_CATEGORY = '1' then 'DSE incentive payout' when EN_CATEGORY = '2'  then 'Customer Cash back credit' else 'NA' end as CATEGORY_TYPE,
nvl((select category from  lov_deal_cde@dms2_link where value = CTGRY_MSTR_ID),'NA') as BUS_CATEGORY_TYPE,
a.CIF_ID,
a.ACCNT_NMBR,
a.FRST_NME,
a.MDDLE_NME,
a.LST_NME,
a.MMN,
b.TLPHNE,
b.EMIL,
b.HSE_NMBR,
b.HSE_STRT,
b.HSE_LNDMRK,
b.HSE_STATE,
b.HSE_DSTRCT,
b.HSE_CTY,
b.HSE_PNCDE,
B.SHP_NME,
nvl((select src_poi_value from  lov_poi_type@dms2_link where src_poi_type = POI_TYPE),'NA') as SRC_POI_VALUE,
a.POI_NMBR,
nvl((select src_poa_value from  lov_poa_type@dms2_link where src_poa_type = POA_TYPE),'NA') as SRC_POA_VALUE,
a.POA_NMBR,
a.BNK_ACCNT_NMBR,
a.IFSC_CDE,
a.ACCNT_HLDR_NME,
'admin' as CREATED_BY,
'admin' as UPDATED_BY
from MIG_ENTERPRISE_PROFILE_STG@DMS2_LINK a  left outer join MIG_ENTERPRISE_CONTACT_STG@DMS2_LINK B 
on a.ENTERPRISE_ID = B.ENTERPRISE_ID and ADDRSS_TYPE_MSTR_ID = '1001'
;
--------------------------------------------------------
--  DDL for View RETAILER_FILTER_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "RECON_USR"."RETAILER_FILTER_VIEW" ("USRID", "ENTITYTYPE", "MSISDN", "MPIN_ATTMPT_CNT", "STATUS", "LAST_TRANSCTN_DATE", "TRDID", "MD_MSISDN", "PN_NMBR", "CRTD_ON", "UPDTD_ON", "MD_NME", "DOB_DOI", "ZONETYPE", "BUSTYPE", "FRST_NME", "MDDLE_NME", "LST_NME", "MMN", "HSE_NMBR", "HSE_STRT", "HSE_LNDMRK", "HSE_STATE", "HSE_DSTRCT", "HSE_PNCDE", "CUNTRY", "TLPHNE", "EMIL", "HSE_CTY", "SHP_NME", "DOCTYPE1", "POI_NMBR", "DOCTYPE2", "POA_NMBR", "PRDCT_MSTR_ID", "CMMSSN_BND_ID", "CIF_ID", "ACCNT_NMBR", "BNK_ACCNT_NMBR", "IFSC_CDE", "ACCNT_HLDR_NME", "CIRCLENAME", "CREATEDBY", "UPDATEDBY") AS 
  select a.usrid, 
nvl((select src_val from lov_trade_entity_id@dms2_link where src_id =83),'NA') entitytype,
a.MSISDN,
a.MPIN_ATTMPT_CNT,
nvl((select description from  lov_stts@dms2_link where value = STTS),'NA') as status,
a.LAST_TRANSCTN_DATE,
a.TRDID,
a.MD_MSISDN,
a.PN_NMBR,
a.CRTD_ON,
a.UPDTD_ON,
a.MD_NME,
a.DOB_DOI,
nvl((select value from  lov_MSTR_ID@dms2_link where id = LOC_MSTR_ID),'NA') as zonetype,
nvl((select src_val from  lov_industry@dms2_link where src_id = INDSTRY_ID),'NA') as bustype,
a.FRST_NME,
a.MDDLE_NME,
a.LST_NME,
a.MMN,
b.HSE_NMBR,
b.HSE_STRT,
b.HSE_LNDMRK,
b.HSE_STATE,
b.HSE_DSTRCT,
b.HSE_PNCDE,
b.CUNTRY,
b.TLPHNE,
b.EMIL,
b.HSE_CTY,
b.SHP_NME,
nvl((select src_poi_value from  lov_poi_type@dms2_link where src_poi_type = POI_TYPE),'NA') as doctype1,
a.POI_NMBR,
nvl((select src_poa_value from  lov_poa_type@dms2_link where src_poa_type = POA_TYPE),'NA') as doctype2,
a.POA_NMBR,
a.PRDCT_MSTR_ID,
a.CMMSSN_BND_ID,
a.CIF_ID,
a.ACCNT_NMBR,
a.BNK_ACCNT_NMBR,
a.IFSC_CDE,
a.ACCNT_HLDR_NME,
nvl((select bank_circle from  lov_circle_sol@dms2_link where circle_id = CRCLE_ID),'NA') as circlename,
'admin' as createdby,
'admin' as updatedby
from mig_trade_profile_stg@dms2_link a left outer join MIG_TRADE_CONTACT_STG@dms2_link b 
on a.trdid = b.trdid and addrss_type_mstr_id = 1001
and entity_mster_id = ( select src_id from lov_trade_entity_id@dms2_link where src_val = 'Retailer')
;
--------------------------------------------------------
--  DDL for View RETAILER_MASTER_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "RECON_USR"."RETAILER_MASTER_VIEW" ("USRID", "TRADEENTITY", "MSISDN", "MPIN_ATTMPT_CNT", "STATUS", "LAST_TRANSCTN_DATE", "TRDID", "MD_MSISDN", "PN_NMBR", "CRTD_ON", "UPDTD_ON", "MD_NME", "DOB_DOI", "ZONENAME", "BUSTYPE", "FRST_NME", "MDDLE_NME", "LST_NME", "MMN", "HSE_NMBR", "HSE_STRT", "HSE_LNDMRK", "HSE_STATE", "HSE_DSTRCT", "HSE_PNCDE", "CUNTRY", "TLPHNE", "EMIL", "HSE_CTY", "SHP_NME", "DOCTYPE1", "POI_NMBR", "DOCTYPE2", "POA_NMBR", "PRDCT_MSTR_ID", "CMMSSN_BND_ID", "CIF_ID", "ACCNT_NMBR", "BNK_ACCNT_NMBR", "IFSC_CDE", "ACCNT_HLDR_NME", "BANKCIRCLE", "USERID1", "USERID2") AS 
  select a.usrid, 
nvl((select src_val from lov_trade_entity_id@dms2_link where src_id = 83),'NA') as tradeentity,
a.MSISDN,
a.MPIN_ATTMPT_CNT,
nvl((select description from  lov_stts@dms2_link where value = STTS),'NA') as status,
a.LAST_TRANSCTN_DATE,
a.TRDID,
a.MD_MSISDN,
a.PN_NMBR,
a.CRTD_ON,
a.UPDTD_ON,
a.MD_NME,
a.DOB_DOI,
nvl((select value from  lov_MSTR_ID@dms2_link where id = LOC_MSTR_ID),'NA') as zonename,
nvl((select src_val from  lov_industry@dms2_link where src_id = INDSTRY_ID),'NA') as bustype,
a.FRST_NME,
a.MDDLE_NME,
a.LST_NME,
a.MMN,
b.HSE_NMBR,
b.HSE_STRT,
b.HSE_LNDMRK,
b.HSE_STATE,
b.HSE_DSTRCT,
b.HSE_PNCDE,
b.CUNTRY,
b.TLPHNE,
b.EMIL,
b.HSE_CTY,
b.SHP_NME,
nvl((select src_poi_value from  lov_poi_type@dms2_link where src_poi_type = POI_TYPE),'NA') as doctype1,
a.POI_NMBR,
nvl((select src_poa_value from  lov_poa_type@dms2_link where src_poa_type = POA_TYPE),'NA') as doctype2,
a.POA_NMBR,
a.PRDCT_MSTR_ID,
a.CMMSSN_BND_ID,
a.CIF_ID,
a.ACCNT_NMBR,
a.BNK_ACCNT_NMBR,
a.IFSC_CDE,
a.ACCNT_HLDR_NME,
nvl((select bank_circle from  lov_circle_sol@dms2_link where circle_id = CRCLE_ID),'NA') as bankcircle,
'admin' as userid1,
'admin' as userid2
from mig_trade_profile_stg@dms2_link a left outer join MIG_TRADE_CONTACT_STG@dms2_link b 
on a.trdid = b.trdid and addrss_type_mstr_id = '1001'
;
--------------------------------------------------------
--  DDL for View V_CRS_BULK_PULL
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "RECON_USR"."V_CRS_BULK_PULL" ("FC_BATCH_ID", "FC_RUNG_SEQ", "FC_CUST_ID", "FC_CUST_NAME", "FC_CUST_VPA", "FC_BILL_NO", "FC_BILL_MONTH", "FC_BILL_AMOUNT", "FC_BILL_DUEDATE", "FC_CHANNEL", "FC_REMARK", "FC_BILL_STAGE", "FC_BILL_STAGE_DUEDATE", "CA_MERCHANT_CATEGORYCODE", "CA_CLIENT_VPA", "CA_SETTLEMENT_ACCOUNTNO", "CA_SETTLEMENT_IFSC", "CA_SETTLEMENT_BANK", "CA_CLIENT_MOBILENO", "CA_CLIENT_DEVICEID") AS 
  SELECT c.fc_batch_id, 
       c.fc_rung_seq,
       c.fc_cust_id,
       c.fc_cust_name,
       c.fc_cust_vpa,
       c.fc_bill_no,
       c.fc_bill_month,
       c.fc_bill_amount,
       c.fc_bill_duedate,
       c.fc_channel,
       c.fc_remark,
       c.fc_bill_stage,
       c.fc_bill_stage_duedate,       
       d.ca_merchant_categorycode,
       d.ca_client_vpa,
       d.ca_settlement_accountno,
       d.ca_settlement_ifsc,
       d.ca_settlement_bank,
       d.ca_client_mobileno,
       d.ca_client_deviceid
FROM crs_final_customer c,
      crs_client_accountdetail d
WHERE c.fc_cust_id = d.ca_client_id
     AND c.fc_status IN (
               SELECT cs_status
               FROM crs_status
               WHERE cs_status_id IN ( 2,3 ))
     AND fc_bill_no in (
'123456',
'298765',
'475829',
'475830')
;
--------------------------------------------------------
--  DDL for Procedure ARRAY_PROCEDURE_TEST
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."ARRAY_PROCEDURE_TEST" as
 CURSOR s_cur IS
 SELECT *
 FROM servers2;

 TYPE fetch_array IS TABLE OF s_cur%ROWTYPE;
 s_array fetch_array;
BEGIN
  OPEN s_cur;
  LOOP
    FETCH s_cur BULK COLLECT INTO s_array LIMIT 1000;

    FORALL i IN 1..s_array.COUNT
    INSERT INTO servers2 VALUES s_array(i);

    EXIT WHEN s_cur%NOTFOUND;
  END LOOP;
  CLOSE s_cur;
  COMMIT;
END;

/
--------------------------------------------------------
--  DDL for Procedure ARRAY_PROCEDURE_UPDATE_TEST
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."ARRAY_PROCEDURE_UPDATE_TEST" as
 CURSOR s_cur IS
 SELECT *
 FROM servers2;

 TYPE fetch_array IS TABLE OF s_cur%ROWTYPE;
 s_array fetch_array;
BEGIN
  OPEN s_cur;
  LOOP
    FETCH s_cur BULK COLLECT INTO s_array LIMIT 1000;

    FORALL i IN 1..s_array.COUNT
    --INSERT INTO servers2 VALUES s_array(i);
    update servers2
    set emp_id = 2
    where emp_id = s_array.emp_id(i);

    EXIT WHEN s_cur%NOTFOUND;
  END LOOP;
  CLOSE s_cur;
  COMMIT;
END;

/
--------------------------------------------------------
--  DDL for Procedure CITY_LAT_LONG
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."CITY_LAT_LONG" ( cityname  IN VARCHAR2 )as
      httpuri        HttpUriType;
      x              clob;
      address        VARCHAR2(32000):='New York';
      lat_long_value VARCHAR2(4000);
    begin

      UTL_HTTP.set_transfer_timeout(90); --set timeout to 60 seconds
      httpuri        := HttpUriType('https://www.google.co.in/search?q=find+latitude+and+longitude+of+' ||cityname||'&'||'ie=utf-'||'8&'||'oe=utf-'||'8&'||'aq=t'||'&'||'rls=org.mozilla'||':'||'en-US'||':'||'official'||'&'||'client=firefox-a');
      x              := httpuri.getclob();
      lat_long_value := regexp_substr(x, '<div class="_Xj">(.+)</div>');
      if lat_long_value is not null then
        select regexp_replace(lat_long_value,
                              '<div class="_Xj">(.+)</div>',
                              '\1')
          into lat_long_value
          FROM dual;
      end if;
      dbms_output.put_line(lat_long_value);
    end;

/
--------------------------------------------------------
--  DDL for Procedure CSV_PROC_HEADER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."CSV_PROC_HEADER" (p_msg out varchar2) AS 

l_rows NUMBER; 
BEGIN 
  p_msg := 'Success';
  l_rows := dump_csv( 'SELECT ''USERNAME'',''USER_ID'',''CREATED'',''COMMON'',''ORACLE_MAINTAINED'' FROM DUAL', ',', 'WES_DIR', 'test_headerall_gtg.csv' ); 
  l_rows := dump_csv( 'select * from all_users where rownum < 5', ',', 'WES_DIR', 'test_headerall_gtg.csv' ); 
 dbms_output.put_line( to_char(l_rows) ||  ' rows extracted to ascii file' ); 
 
 EXCEPTION
     WHEN OTHERS THEN
          p_msg := 'Failed';
          raise_application_error(-20001,'An error was encountered - ' || SQLCODE || ' -ERROR- ' || sqlerrm);
END;

/
--------------------------------------------------------
--  DDL for Procedure FORALLUPDATE_DEMO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."FORALLUPDATE_DEMO" AS 

    CURSOR rec_cur IS
    SELECT OBJECT_ID, DATA_OBJECT_ID, namespace, CREATED
    FROM t;
    

    TYPE num_tab_t IS TABLE OF NUMBER(38);
    TYPE vc2_tab_t IS TABLE OF VARCHAR2(4000);
    type v_date_t is table of date;

    pk_tab NUM_TAB_T;
    fk_tab NUM_TAB_T;
    fill_tab NUM_TAB_T;
    date_tab v_date_t;
BEGIN
    OPEN rec_cur;
    LOOP
        FETCH rec_cur BULK COLLECT INTO pk_tab, fk_tab, fill_tab, date_tab LIMIT 1000;
        EXIT WHEN pk_tab.COUNT() = 0;

        FORALL i IN pk_tab.FIRST .. pk_tab.LAST
           UPDATE t
            SET    object_id = fill_tab(i),
                   namespace = fill_tab(i); -- check constant data
           -- WHERE  data_object_id = fk_tab(i);
    END LOOP;
    commit;
    CLOSE rec_cur;
END;

/
--------------------------------------------------------
--  DDL for Procedure NROWS_AT_A_TIME
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."NROWS_AT_A_TIME" ( p_array_size   PLS_INTEGER ) IS

     l_owner            dbms_sql.varchar2_table;
     l_object_name      dbms_sql.varchar2_table;
     l_subobject_name   dbms_sql.varchar2_table;
     l_object_id        dbms_sql.number_table;
     l_data_object_id   dbms_sql.number_table;
     l_object_type      dbms_sql.varchar2_table;
     l_created          dbms_sql.date_table;
     l_last_ddl_time    dbms_sql.date_table;
     l_timestamp        dbms_sql.varchar2_table;
     l_status           dbms_sql.varchar2_table;
     l_temporary        dbms_sql.varchar2_table;
     l_generated        dbms_sql.varchar2_table;
     l_secondary        dbms_sql.varchar2_table;
     l_namespace        dbms_sql.number_table;
     TYPE employees_aat IS
          TABLE OF t%rowtype INDEX BY PLS_INTEGER;
     l_employees        employees_aat;
     CURSOR c IS
          SELECT owner,
                 object_name,
                 subobject_name,
                 object_id,
                 data_object_id,
                 object_type,
                 created,
                 last_ddl_time,
                 timestamp,
                 status,
                 temporary,
                 generated,
                 secondary,
                 namespace
          FROM all_objects;

BEGIN
     OPEN c;
     LOOP
          FETCH c BULK COLLECT INTO l_owner,l_object_name,l_subobject_name,l_object_id,l_data_object_id,l_object_type,l_created,l_last_ddl_time,
 l_timestamp,l_status,l_temporary,l_generated,l_secondary, l_namespace LIMIT p_array_size;

          FORALL i IN 1..l_owner.count
               INSERT INTO t (
                    owner,
                    object_name,
                    subobject_name,
                    object_id,
                    data_object_id,
                    object_type,
                    created,
                    last_ddl_time,
                    timestamp,
                    status,
                    temporary,
                    generated,
                    secondary,
                    namespace
               ) VALUES (
                    l_owner(i),
                    l_object_name(i),
                    l_subobject_name(i),
                    l_object_id(i),
                    l_data_object_id(i),
                    l_object_type(i),
                    l_created(i),
                    l_last_ddl_time(i),
                    l_timestamp(i),
                    l_status(i),
                    l_temporary(i),
                    l_generated(i),
                    l_secondary(i),
                    l_namespace(i)  --  for all l_status
               );
               
               for all update;
               
               
          EXIT WHEN c%notfound;
     END LOOP;

     COMMIT;
     CLOSE c;
END nrows_at_a_time;

/
--------------------------------------------------------
--  DDL for Procedure PRINT1
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."PRINT1" 
(
  PARAM1 IN VARCHAR2 
) AS 
BEGIN
  dbms_output.put_line(param1);
END PRINT1;

/
--------------------------------------------------------
--  DDL for Procedure PROC_CREATE_CLIENT_CONFIG
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."PROC_CREATE_CLIENT_CONFIG" (
     p_client_name                  IN VARCHAR2,
     p_client_id                    IN NUMBER,
     p_client_secretkey             IN VARCHAR2,
     p_partialpay                   IN VARCHAR2,
     p_payafter_duedate             IN VARCHAR2,
     p_daysincentive_valid          IN NUMBER,
     p_early_incentive_percnt       IN NUMBER,
     p_early_incentive_amt          IN NUMBER,
     p_daysafter_duedate            IN NUMBER,
     p_latefee_percnt               IN NUMBER,
     p_latefee_amt                  IN NUMBER,
     p_floor_payment                IN NUMBER,
     p_ceiling_payment              IN NUMBER,
     p_onlinepayment_confirmation   IN VARCHAR2,
     p_payment_confirmationurl      IN VARCHAR2,
     p_account_verification         IN VARCHAR2,
     p_msg_out                      OUT VARCHAR2
)
     AS
BEGIN
     INSERT INTO crs_client_config VALUES (
          p_client_name,
          p_client_id,
          p_client_secretkey,
          p_partialpay,
          p_payafter_duedate,
          p_daysincentive_valid,
          p_early_incentive_percnt,
          p_early_incentive_amt,
          p_daysafter_duedate,
          p_latefee_percnt,
          p_latefee_amt,
          p_floor_payment,
          p_ceiling_payment,
          p_onlinepayment_confirmation,
          p_payment_confirmationurl,
          p_account_verification,
          'admin',
          current_timestamp,
          'admin',
          current_timestamp
     );
     
     commit;
     p_msg_out := 'Success';
     

EXCEPTION
     WHEN OTHERS THEN
          p_msg_out := 'Failed';
          raise_application_error(-20001,'An error was encountered - ' || SQLCODE || ' -ERROR- ' || sqlerrm);
         -- dbms_output.put_line('Error in inserting client_config data');
END;

/
--------------------------------------------------------
--  DDL for Procedure PROC_UPDATE_CLIENT_CONFIG
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."PROC_UPDATE_CLIENT_CONFIG" (
     p_client_name                  IN VARCHAR2,
     p_client_id                    IN NUMBER,
     p_client_secretkey             IN VARCHAR2,
     p_partialpay                   IN VARCHAR2,
     p_payafter_duedate             IN VARCHAR2,
     p_daysincentive_valid          IN NUMBER,
     p_early_incentive_percnt       IN NUMBER,
     p_early_incentive_amt          IN NUMBER,
     p_daysafter_duedate            IN NUMBER,
     p_latefee_percnt               IN NUMBER,
     p_latefee_amt                  IN NUMBER,
     p_floor_payment                IN NUMBER,
     p_ceiling_payment              IN NUMBER,
     p_onlinepayment_confirmation   IN VARCHAR2,
     p_payment_confirmationurl      IN VARCHAR2,
     p_account_verification         IN VARCHAR2,
     p_msg_out                      OUT VARCHAR2
)
     AS
BEGIN
     UPDATE crs_client_config
          SET
               cc_client_name = p_client_name,
               cc_client_secretkey = p_client_secretkey,
               cc_partialpay = p_partialpay,
               cc_payafter_duedate = p_payafter_duedate,
               cc_daysincentive_valid = p_daysincentive_valid,
               cc_early_incentive_percnt = p_early_incentive_percnt,
               cc_early_incentive_amt = p_early_incentive_amt,
               cc_daysafter_duedate = p_daysafter_duedate,
               cc_latefee_percnt = p_latefee_percnt,
               cc_latefee_amt = p_latefee_amt,
               cc_floor_payment = p_floor_payment,
               cc_ceiling_payment = p_ceiling_payment,
               cc_onlinepayment_confirmation = p_onlinepayment_confirmation,
               cc_payment_confirmationurl = p_payment_confirmationurl,
               cc_account_verification = p_account_verification,
               cc_created_by = 'admin',
               cc_created_date = current_timestamp,
               cc_updated_by = 'admin',
               cc_updated_date = current_timestamp
     WHERE cc_client_id = p_client_id;
     
     

     p_msg_out := 'Success';
     
EXCEPTION
     WHEN OTHERS THEN
          p_msg_out := 'Failed';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
    --dbms_output.put_line('Error in updating client_config data');
END;

/
--------------------------------------------------------
--  DDL for Procedure PR_GET_PULL_FROM_CRYSTAL
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."PR_GET_PULL_FROM_CRYSTAL" ( p_msg   OUT VARCHAR2 ) AS

     x_batch_id                 VARCHAR(200);
     x_data_batch_id            VARCHAR(200);
     x_rung_seq                 NUMBER;
     x_cust_id                  VARCHAR(200);
     x_cust_name                VARCHAR(100);
     x_cust_vpa                 VARCHAR(100);
     x_bill_no                  NUMBER;
     x_bill_month               VARCHAR(100);
     x_bill_amount              VARCHAR(100);
     x_bill_due_date           date;
     x_channel                  VARCHAR(50);
     x_remarks                  VARCHAR(200);
     x_bill_stage               VARCHAR(100);
     x_bill_stage_duedate       date;
     x_merchant_category_code   VARCHAR(100);
     x_merchant_vpa             VARCHAR(100);
     x_settlement_account_no    VARCHAR(100);
     x_settlement_ifsc          VARCHAR(100);
     x_settlement_bank          VARCHAR(100);
     x_merchant_mobile_no       NUMBER;
     x_merchant_device_id       VARCHAR(200);
     x_record_id                NUMBER;
     p_count                    NUMBER DEFAULT 0;
     p_data_refcursor           SYS_REFCURSOR;
     total_count                NUMBER DEFAULT 0;
    
BEGIN
     p_msg := 'success';
     p_batch_process_crystal(p_data_refcursor);
     LOOP
          FETCH p_data_refcursor INTO x_batch_id,x_rung_seq,x_cust_id,x_cust_name,x_cust_vpa,x_bill_no,x_bill_month,x_bill_amount,x_bill_due_date
,x_channel,x_remarks,x_bill_stage,x_bill_stage_duedate,x_merchant_category_code,x_merchant_vpa,x_settlement_account_no,x_settlement_ifsc
,x_settlement_bank,x_merchant_mobile_no,x_merchant_device_id;

          EXIT WHEN p_data_refcursor%notfound;
          dbms_output.put_line('Batch id ' || x_batch_id);
     END LOOP;

     CLOSE p_data_refcursor;
END pr_get_pull_from_crystal;

/
--------------------------------------------------------
--  DDL for Procedure P_BATCH_CRS_CUSTPUSH
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_BATCH_CRS_CUSTPUSH" ( p_msg OUT varchar ) AS
     PRAGMA autonomous_transaction;
     
v_query varchar2(2000);     
BEGIN

         p_msg := 'Success';
         
         v_query := 'truncate table crs_cust_push_data';
         
         execute immediate v_query;

    -- Code checks UPI data for status and updates the customerpush table in crystal processed records
     UPDATE recon_usr.crs_customer_push
          SET
               cc_status = ( SELECT cs_status FROM crs_status WHERE cs_status_id = 3 )
     WHERE cc_batch_id IN ( SELECT batch_id FROM ENTERPRISE_PUSH_SOLUTION@recon_link )
          AND
               cc_rung_seq IN ( SELECT record_id FROM ENTERPRISE_PUSH_SOLUTION@recon_link )
          AND
               CC_CUSTOMER_VPA IN ( SELECT CUSTOMER_VPA FROM ENTERPRISE_PUSH_SOLUTION@recon_link );

     COMMIT;   
     
     -- Insert the batch to table
    
     INSERT INTO crs_cust_push_data (
                              CC_BATCH_ID,
                              CC_RUNG_SEQ,
                              CC_CUSTID,
                              CC_CUSTOMER_VPA,
                              CC_CUSTOMER_NAME,
                              CC_CUSTOMER_ACC,
                              CC_CUSTOMER_IFSC,
                              CC_TXN_MODE,
                              CC_AMOUNT,
                              CC_MERCHT_NAME,
                              CC_MERCHT_VPA,
                              CC_MERCHT_CATEGRYCODE,
                              CC_SETTLMNT_ACCTNO,
                              CC_SETTLMT_IFSC,
                              CC_MERCHT_MOBNO,
                              CC_MERCHT_DEVICEID,
                              CC_CREATED_DATE
                              ) select CC_BATCH_ID,
                                   CC_RUNG_SEQ,
                                   CC_CUSTID,
                                   CC_CUSTOMER_VPA,
                                   CC_CUSTOMER_NAME,
                                   CC_CUSTOMER_ACC,
                                   CC_CUSTOMER_IFSC,
                                   CC_TXN_MODE,
                                   CC_AMOUNT,
                                   CC_MERCHT_NAME,
                                   CC_MERCHT_VPA,
                                   CC_MERCHT_CATEGRYCODE,
                                   CC_SETTLMNT_ACCTNO,
                                   CC_SETTLMT_IFSC,
                                   CC_MERCHT_MOBNO,
                                   CC_MERCHT_DEVICEID,
                                   current_timestamp
                                   from crs_customer_push c, crs_client_config_push d
                                   where C.CC_CUSTID = D.CC_MERCHT_ID
                                   and  c.cc_status IN ( SELECT cs_status FROM recon_usr.crs_status WHERE cs_status_id IN (
                                                       2,5
                                                  ) )
                                             AND
                                        ROWNUM BETWEEN 1 AND 4 order by 1,2;    commit;
               
 
EXCEPTION
     WHEN OTHERS THEN
      p_msg := 'Failed';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_BATCH_PROCESS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_BATCH_PROCESS" (p_batchId VARCHAR2, p_data_refcur OUT SYS_REFCURSOR)
AS 
BEGIN
  
  OPEN p_data_refcur FOR select cb_custId, cb_custVPA, 
                                cb_billMonth, cb_billAmount, 
                                cb_billDueDate, cb_disconnectionDate
  from crystal_batchid 
  where cb_batchId = p_batchId;

  EXCEPTION
  WHEN OTHERS THEN
  raise_application_error(-20001,'An error was encountered - ' || SQLCODE || ' -ERROR- ' || sqlerrm);
      
END p_batch_process;

/
--------------------------------------------------------
--  DDL for Procedure P_BATCH_PROCESS_CRYSTAL
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_BATCH_PROCESS_CRYSTAL" ( p_msg OUT varchar ) AS
     PRAGMA autonomous_transaction;
     
v_query varchar2(2000);     
BEGIN

         p_msg := 'success';
         
         v_query := 'truncate table crs_cust_pull_data';
         
         execute immediate v_query;

    -- Code checks UPI data for status and updates the customer table in crystal
     UPDATE recon_usr.crs_final_customer
          SET
               fc_status = ( SELECT cs_status FROM crs_status WHERE cs_status_id = 3 )
     WHERE fc_batch_id IN ( SELECT batch_id FROM get_pull_solution@recon_link )
          AND
               fc_rung_seq IN ( SELECT rung_seq FROM get_pull_solution@recon_link )
          AND
               fc_cust_id IN ( SELECT custid FROM get_pull_solution@recon_link );

     COMMIT;
     
     -- Insert the batch to table
    
     INSERT INTO crs_cust_pull_data (
          fc_batch_id,
          fc_rung_seq,
          fc_cust_id,
          fc_cust_name,
          fc_cust_vpa,
          fc_bill_no,
          fc_bill_month,
          fc_bill_amount,
          fc_bill_duedate,
          fc_channel,
          fc_remark,
          fc_bill_stage,
          fc_bill_stage_duedate,
          ca_merchant_categorycode,
          CA_CLIENT_NAME,
          ca_client_vpa,
          ca_settlement_accountno,
          ca_settlement_ifsc,
          ca_settlement_bank,
          ca_client_mobileno,
          ca_client_deviceid,
          CA_CREATED_DATE
     ) SELECT c.fc_batch_id,
            c.fc_rung_seq,
            c.fc_cust_id,
            c.fc_cust_name,
            c.fc_cust_vpa,
            c.fc_bill_no,
            c.fc_bill_month,
            c.fc_bill_amount,
            c.fc_bill_duedate,
            c.fc_channel,
            c.fc_remark,
            c.fc_bill_stage,
            c.fc_bill_stage_duedate,
            d.ca_merchant_categorycode,
            d.CA_CLIENT_NAME,
            d.ca_client_vpa,
            d.ca_settlement_accountno,
            d.ca_settlement_ifsc,
            d.ca_settlement_bank,
            d.ca_client_mobileno,
            d.ca_client_deviceid,
            current_timestamp
     FROM recon_usr.crs_final_customer c,
           recon_usr.crs_client_accountdetail d
     WHERE c.fc_cust_id = d.ca_client_id
           and c.FC_DUEDATE_FLG = 'Y'  -- Invalid due date is lesser than sysdate removed data validations 
          AND
               c.fc_status IN ( SELECT cs_status FROM recon_usr.crs_status WHERE cs_status_id IN (
                    2,5
               ) )
          AND
               ROWNUM BETWEEN 1 AND 4 order by 1;   commit;
               
 
EXCEPTION
     WHEN OTHERS THEN
      p_msg := 'failed';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_BATCH_PROCESS_CRYSTAL545
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_BATCH_PROCESS_CRYSTAL545" (p_data_refcur OUT SYS_REFCURSOR)
                                                    
AS pragma autonomous_transaction;

BEGIN

    -- Code checks UPI data for status and updates the customer table in crystal
     update recon_usr.crs_final_customer
     set fc_status =      ( SELECT cs_status
                                   FROM crs_status
                                   WHERE cs_status_id = 3)
     where fc_batch_id in ( select batch_id from get_pull_solution@recon_link)
     and fc_rung_seq in ( select rung_seq from get_pull_solution@recon_link)
     and fc_cust_id in ( select custid from get_pull_solution@recon_link);
     commit;

  
  OPEN p_data_refcur FOR SELECT c.fc_batch_id,
                      c.fc_rung_seq--,
                    /*  c.fc_cust_id,
                      c.fc_cust_name,
                      c.fc_cust_vpa,
                      c.fc_bill_no,
                      c.fc_bill_month,
                      c.fc_bill_amount,
                      c.fc_bill_duedate,
                      c.fc_channel,
                      c.fc_remark,
                      c.fc_bill_stage,
                      c.fc_bill_stage_duedate,       
                      d.ca_merchant_categorycode,
                      d.ca_client_vpa,
                      d.ca_settlement_accountno,
                      d.ca_settlement_ifsc,
                      d.ca_settlement_bank,
                      d.ca_client_mobileno,
                      d.ca_client_deviceid  */
               FROM recon_usr.crs_final_customer c,
                     recon_usr.crs_client_accountdetail d
               WHERE c.fc_cust_id = d.ca_client_id
                      AND c.fc_status IN (
                              SELECT cs_status
                              FROM recon_usr.crs_status
                              WHERE cs_status_id IN ( 2,5 ))
                     AND rownum between 1 and 5000;
                   
      /*
         -- Updating records as processed Status 
       update crs_final_customer
       set fc_status = (select cs_status from crs_status where cs_status_id=3)
       WHERE fc_batch_id = v_batch_id
       AND fc_rung_seq between v_start and v_end;     
       commit; 
       */
                                
  
  EXCEPTION
  WHEN OTHERS THEN
  raise_application_error(-20001,'An error was encountered - ' || SQLCODE || ' -ERROR- ' || sqlerrm);
      
END;

/
--------------------------------------------------------
--  DDL for Procedure P_BATCH_PROCESS_CRYSTAL_NEW
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_BATCH_PROCESS_CRYSTAL_NEW" (p_batch_id in varchar2,
                                                    p_rung_seq in number,
                                                    p_data_refcur OUT SYS_REFCURSOR)
AS 
v_rung_seq2  number:=0;
BEGIN

  IF p_batch_id IS NULL AND p_rung_seq IS NULL THEN
  
  OPEN p_data_refcur FOR SELECT c.fc_batch_id,
                      c.fc_rung_seq,
                      c.fc_cust_id,
                      c.fc_cust_name,
                      c.fc_cust_vpa,
                      c.fc_bill_no,
                      c.fc_bill_month,
                      c.fc_bill_amount,
                      c.fc_bill_duedate,
                      c.fc_channel,
                      c.fc_remark,
                      c.fc_bill_stage,
                      c.fc_bill_stage_duedate,       
                      d.ca_merchant_categorycode,
                      d.ca_client_vpa,
                      d.ca_settlement_accountno,
                      d.ca_settlement_ifsc,
                      d.ca_settlement_bank,
                      d.ca_client_mobileno,
                      d.ca_client_deviceid
               FROM crs_final_customer c,
                     crs_client_accountdetail d
               WHERE c.fc_cust_id = d.ca_client_id
                 AND c.fc_batch_id = 'BATCH_15062017_1'
                     AND c.fc_status IN (
                              SELECT cs_status
                              FROM crs_status
                              WHERE cs_status_id IN ( 2,5 ))
                              and c.fc_rung_seq between 1 and 5000;
                     
         ELSE

    v_rung_seq2 := p_rung_seq +5000;
                     
   OPEN p_data_refcur FOR SELECT c.fc_batch_id,
                      c.fc_rung_seq,
                      c.fc_cust_id,
                      c.fc_cust_name,
                      c.fc_cust_vpa,
                      c.fc_bill_no,
                      c.fc_bill_month,
                      c.fc_bill_amount,
                      c.fc_bill_duedate,
                      c.fc_channel,
                      c.fc_remark,
                      c.fc_bill_stage,
                      c.fc_bill_stage_duedate,       
                      d.ca_merchant_categorycode,
                      d.ca_client_vpa,
                      d.ca_settlement_accountno,
                      d.ca_settlement_ifsc,
                      d.ca_settlement_bank,
                      d.ca_client_mobileno,
                      d.ca_client_deviceid
                FROM crs_final_customer c,
                     crs_client_accountdetail d
               WHERE c.fc_cust_id = d.ca_client_id
                    AND c.fc_batch_id = p_batch_id
                     AND c.fc_status IN (
                              SELECT cs_status
                              FROM crs_status
                              WHERE cs_status_id IN ( 2,5 ))
                     AND c.fc_rung_seq between p_rung_seq and v_rung_seq2; 
 
      END IF;                                
  
  EXCEPTION
  WHEN OTHERS THEN
  raise_application_error(-20001,'An error was encountered - ' || SQLCODE || ' -ERROR- ' || sqlerrm);
      
END;

/
--------------------------------------------------------
--  DDL for Procedure P_BATCH_PROCESS_CRYSTAL_O
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_BATCH_PROCESS_CRYSTAL_O" ( p_msg OUT varchar ) AS
     PRAGMA autonomous_transaction;
     
v_query varchar2(2000);     
BEGIN

         p_msg := 'success';
         
         v_query := 'truncate table crs_cust_pull_data';
         
         execute immediate v_query;

    -- Code checks UPI data for status and updates the customer table in crystal
     UPDATE recon_usr.crs_final_customer
          SET
               fc_status = ( SELECT cs_status FROM crs_status WHERE cs_status_id = 3 )
     WHERE fc_batch_id IN ( SELECT batch_id FROM get_pull_solution@recon_link )
          AND
               fc_rung_seq IN ( SELECT rung_seq FROM get_pull_solution@recon_link )
          AND
               fc_cust_id IN ( SELECT custid FROM get_pull_solution@recon_link );

     COMMIT;
     
     -- Insert the batch to table
    
     INSERT INTO crs_cust_pull_data (
          fc_batch_id,
          fc_rung_seq,
          fc_cust_id,
          fc_cust_name,
          fc_cust_vpa,
          fc_bill_no,
          fc_bill_month,
          fc_bill_amount,
          fc_bill_duedate,
          fc_channel,
          fc_remark,
          fc_bill_stage,
          fc_bill_stage_duedate,
          ca_merchant_categorycode,
          ca_client_vpa,
          ca_settlement_accountno,
          ca_settlement_ifsc,
          ca_settlement_bank,
          ca_client_mobileno,
          ca_client_deviceid,
          CA_CREATED_DATE
     ) SELECT c.fc_batch_id,
            c.fc_rung_seq,
            c.fc_cust_id,
            c.fc_cust_name,
            c.fc_cust_vpa,
            c.fc_bill_no,
            c.fc_bill_month,
            c.fc_bill_amount,
            c.fc_bill_duedate,
            c.fc_channel,
            c.fc_remark,
            c.fc_bill_stage,
            c.fc_bill_stage_duedate,
            d.ca_merchant_categorycode,
            d.ca_client_vpa,
            d.ca_settlement_accountno,
            d.ca_settlement_ifsc,
            d.ca_settlement_bank,
            d.ca_client_mobileno,
            d.ca_client_deviceid,
            current_timestamp
     FROM recon_usr.crs_final_customer c,
           recon_usr.crs_client_accountdetail d
     WHERE c.fc_cust_id = d.ca_client_id
          AND
               c.fc_status IN ( SELECT cs_status FROM recon_usr.crs_status WHERE cs_status_id IN (
                    2,5
               ) )
          AND
               ROWNUM BETWEEN 1 AND 5000;   commit;
               
 
EXCEPTION
     WHEN OTHERS THEN
      p_msg := 'failed';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_BATCH_PROCESS_CRYSTAL_OLD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_BATCH_PROCESS_CRYSTAL_OLD" (p_data_refcur OUT SYS_REFCURSOR)
                                                    
AS pragma autonomous_transaction;

BEGIN

    -- Code checks UPI data for status and updates the customer table in crystal
     update recon_usr.crs_final_customer
     set fc_status =      ( SELECT cs_status
                                   FROM crs_status
                                   WHERE cs_status_id = 3)
     where fc_batch_id in ( select batch_id from get_pull_solution@recon_link)
     and fc_rung_seq in ( select rung_seq from get_pull_solution@recon_link)
     and fc_cust_id in ( select custid from get_pull_solution@recon_link);
     commit;

  
  OPEN p_data_refcur FOR SELECT c.fc_batch_id,
                      c.fc_rung_seq,
                      c.fc_cust_id,
                      c.fc_cust_name,
                      c.fc_cust_vpa,
                      c.fc_bill_no,
                      c.fc_bill_month,
                      c.fc_bill_amount,
                      c.fc_bill_duedate,
                      c.fc_channel,
                      c.fc_remark,
                      c.fc_bill_stage,
                      c.fc_bill_stage_duedate,       
                      d.ca_merchant_categorycode,
                      d.ca_client_vpa,
                      d.ca_settlement_accountno,
                      d.ca_settlement_ifsc,
                      d.ca_settlement_bank,
                      d.ca_client_mobileno,
                      d.ca_client_deviceid
               FROM recon_usr.crs_final_customer c,
                     recon_usr.crs_client_accountdetail d
               WHERE c.fc_cust_id = d.ca_client_id
                      AND c.fc_status IN (
                              SELECT cs_status
                              FROM recon_usr.crs_status
                              WHERE cs_status_id IN ( 2,5 ))
                     AND rownum between 1 and 5000;
                   
      /*
         -- Updating records as processed Status 
       update crs_final_customer
       set fc_status = (select cs_status from crs_status where cs_status_id=3)
       WHERE fc_batch_id = v_batch_id
       AND fc_rung_seq between v_start and v_end;     
       commit; 
       */
                                
  
  EXCEPTION
  WHEN OTHERS THEN
  raise_application_error(-20001,'An error was encountered - ' || SQLCODE || ' -ERROR- ' || sqlerrm);
      
END;

/
--------------------------------------------------------
--  DDL for Procedure P_BATCH_PROCESS_CRYSTAL_TAB
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_BATCH_PROCESS_CRYSTAL_TAB" ( p_msg OUT varchar ) AS
     PRAGMA autonomous_transaction;
     
v_query varchar2(2000);     
BEGIN

         p_msg := 'success';
         
         v_query := 'truncate table crs_cust_pull_data';
         
         execute immediate v_query;

    -- Code checks UPI data for status and updates the customer table in crystal
     UPDATE recon_usr.crs_final_customer
          SET
               fc_status = ( SELECT cs_status FROM crs_status WHERE cs_status_id = 3 )
     WHERE fc_batch_id IN ( SELECT batch_id FROM get_pull_solution@recon_link )
          AND
               fc_rung_seq IN ( SELECT rung_seq FROM get_pull_solution@recon_link )
          AND
               fc_cust_id IN ( SELECT custid FROM get_pull_solution@recon_link );

     COMMIT;
     
     -- Insert the batch to table
    
     INSERT INTO crs_cust_pull_data (
          fc_batch_id,
          fc_rung_seq,
          fc_cust_id,
          fc_cust_name,
          fc_cust_vpa,
          fc_bill_no,
          fc_bill_month,
          fc_bill_amount,
          fc_bill_duedate,
          fc_channel,
          fc_remark,
          fc_bill_stage,
          fc_bill_stage_duedate,
          ca_merchant_categorycode,
          ca_client_vpa,
          ca_settlement_accountno,
          ca_settlement_ifsc,
          ca_settlement_bank,
          ca_client_mobileno,
          ca_client_deviceid
     ) SELECT c.fc_batch_id,
            c.fc_rung_seq,
            c.fc_cust_id,
            c.fc_cust_name,
            c.fc_cust_vpa,
            c.fc_bill_no,
            c.fc_bill_month,
            c.fc_bill_amount,
            c.fc_bill_duedate,
            c.fc_channel,
            c.fc_remark,
            c.fc_bill_stage,
            c.fc_bill_stage_duedate,
            d.ca_merchant_categorycode,
            d.ca_client_vpa,
            d.ca_settlement_accountno,
            d.ca_settlement_ifsc,
            d.ca_settlement_bank,
            d.ca_client_mobileno,
            d.ca_client_deviceid
     FROM recon_usr.crs_final_customer c,
           recon_usr.crs_client_accountdetail d
     WHERE c.fc_cust_id = d.ca_client_id
          AND
               c.fc_status IN ( SELECT cs_status FROM recon_usr.crs_status WHERE cs_status_id IN (
                    2,5
               ) )
          AND
               ROWNUM BETWEEN 1 AND 5000;   commit;
               
 
EXCEPTION
     WHEN OTHERS THEN
      p_msg := 'failed';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_BRS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_BRS" (
p_wes OUT varchar2 )
AS
BEGIN  

-- truncating the base tables

	execute immediate 'truncate table brs_escrow_load';
	execute immediate 'truncate table brs_escrow';
	execute immediate 'truncate table brs_revenue';
	execute immediate 'truncate table brs_merchant';
	execute immediate 'truncate table brs_od';
	execute immediate 'truncate table brs_single_pool';
	execute immediate 'truncate table neft_returns_temp';
	execute immediate 'truncate table t_escrow_tcs';
	execute immediate 'truncate table brs_ctr';
	execute immediate 'truncate table brs_kotak_neft';
/*
-- brs escrow
BULK INSERT dbo.brs_escrow_load 
FROM 'C:uploadbrs3611409323_DAY-1.txt' 
WITH ( FIELDTERMINATOR ='|', FIRSTROW = 3 );

-- brs revenue
BULK INSERT dbo.brs_revenue 
FROM 'C:uploadbrs

-- brs od
BULK INSERT dbo.brs_od
FROM 'C:\upload\brs\0411614506_DAY-1.txt' 
WITH ( FIELDTERMINATOR ='|', FIRSTROW = 2 );

-- brs single pool
BULK INSERT dbo.brs_single_pool
FROM 'C:\upload\brs\0411405197_DAY-1.txt' 
WITH ( FIELDTERMINATOR ='|', FIRSTROW = 2 );

-- brs merchant
BULK INSERT dbo.brs_merchant
FROM 'C:\upload\brs\0411405210_DAY-1.txt' 
WITH ( FIELDTERMINATOR ='|', FIRSTROW = 2 );

-- neft return temp
BULK INSERT dbo.neft_returns_temp
FROM 'C:\upload\brs\neft.csv' 
WITH ( FIELDTERMINATOR ='|', FIRSTROW = 2 );

-- escrow tcs
BULK INSERT dbo.t_escrow_tcs
FROM 'C:\upload\brs\er.csv' 
WITH ( FIELDTERMINATOR =',', FIRSTROW = 2 );


-- kotak neft
BULK INSERT dbo.brs_kotak_neft
FROM 'C:\upload\brs\kotak_neft.csv' 
WITH ( FIELDTERMINATOR ='|', FIRSTROW = 2 );

-- ctr
BULK INSERT dbo.brs_ctr 
FROM 'C:\upload\brs\ctr.csv' 
WITH ( FIELDTERMINATOR ='|', FIRSTROW = 2 )

*/

-- loading main table

insert into brs_escrow(acc_no,acc_name,post_dt,value_dt,narration,ref_no,chq_no,dr_cr,curr_code,txn_amnt,run_bal)
select * from brs_escrow_load;

/*Execute immediate  dbo.p_brs_fileload

Execute immediate  dbo.p_brs_process
*/

p_wes := 'Successfully Completed';

RETURN;
END;

/
--------------------------------------------------------
--  DDL for Procedure P_BRS_FILELOAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_BRS_FILELOAD" 

AS
BEGIN 

delete from fileload_log where fl_id in ('F01','F02','F03','F04','F05','F06','F07','F08','F09');

INSERT INTO fileload_log(FL_ID,FL_DESC,FL_DATE,FL_COUNT,FL_CRT_DT)
select 'F02','CTR - DUMP',txn_dt, count(1) ,systimestamp  from brs_ctr group by txn_dt;

INSERT INTO fileload_log(FL_ID,FL_DESC,FL_DATE,FL_COUNT,FL_CRT_DT)
SELECT 'F03','ESCROW - TCS',SUBSTR(TXN_TIME,1,9), COUNT(1),systimestamp   FROM T_EsCROW_TCS GROUP BY SUBSTR(TXN_TIME,1,9);

INSERT INTO fileload_log(FL_ID,FL_DESC,FL_DATE,FL_COUNT,FL_CRT_DT)
select 'F04','NEFT - RETURNS', SUBSTR(NTR_POST_DATE,1,8), COUNT(1),systimestamp from neft_returns_temp GROUP BY SUBSTR(NTR_POST_DATE,1,8);

INSERT INTO fileload_log(FL_ID,FL_DESC,FL_DATE,FL_COUNT,FL_CRT_DT)
SELECT 'F05','BANK - ESCROW',SUBSTR(POST_DT,1,8), COUNT(1) ,systimestamp FROM BRS_ESCROW GROUP BY SUBSTR(POST_DT,1,8);

INSERT INTO fileload_log(FL_ID,FL_DESC,FL_COUNT,FL_CRT_DT)
SELECT 'F06','BANK - MERCHANT',COUNT(1),systimestamp FROM BRS_MERCHANT;

INSERT INTO fileload_log(FL_ID,FL_DESC,FL_COUNT,FL_CRT_DT)
SELECT 'F07','BANK - OD',COUNT(1),systimestamp FROM BRS_OD;

INSERT INTO fileload_log(FL_ID,FL_DESC,FL_COUNT,FL_CRT_DT)
SELECT 'F08','BANK - REVENUE',COUNT(1),systimestamp FROM BRS_REVENUE;

INSERT INTO fileload_log(FL_ID,FL_DESC,FL_COUNT,FL_CRT_DT)
SELECT 'F09','BANK - SINGLE-POOL',COUNT(1),systimestamp FROM BRS_SINGLE_POOL;


RETURN;
END;

/
--------------------------------------------------------
--  DDL for Procedure P_BRS_PROCESS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_BRS_PROCESS" /*(cur OUT SYS_REFCURSOR) (cur OUT SYS_REFCURSOR)*/

AS
BEGIN  

	--  start brs recon temp block
	execute immediate 'truncate table brs_recon_temp';
	execute immediate 'truncate table BRS_RECON_WES';
	execute immediate 'truncate table brs_recon_details';
	execute immediate 'truncate table brs_recon_summary_temp';


	insert into brs_recon_temp(brt_desc,brt_narration,brt_ref_no,brt_crt_dt,brt_crt_by)
select 'P2B / IMPS Comm',br_narration,br_reference_no,systimestamp,'DBA'  FROM brs_revenue where br_dr_cr = 'D'  AND BR_REFERENCE_NO IN (
select REF_NO  from brs_escrow where dr_cr = 'C' )
union all
-- marchant
select DISTINCT  'MERCHANT REVENUE',bm_narration,bm_reference_no,systimestamp,'DBA'  FROM brs_merchant where bm_dr_cr = 'D'  AND Bm_REFERENCE_NO IN (
select REF_NO  from brs_escrow where dr_cr = 'C' )
union all
-- od
select 'WEB AGENT',bo_narration,bo_reference_no,systimestamp,'DBA'  FROM brs_od where bo_dr_cr = 'D'  AND Bo_REFERENCE_NO IN (
select REF_NO  from brs_escrow where dr_cr = 'C' )
union all
-- single pool
select 'AGENT CASHIN', bsp_narration, bsp_reference_no,systimestamp,'DBA'  FROM brs_single_pool where bsp_dr_cr = 'D'  AND Bsp_REFERENCE_NO IN (
select REF_NO  from brs_escrow where dr_cr = 'C' )
union all
-- ADDED to include kotak neft txns
select 'Merchant Settlement',txn_id,utr_rrn,systimestamp,'DBA' from brs_kotak_neft
where txn_id in (
select narration from brs_escrow
where dr_cr = 'D') and ser_name in ('Merchant NEFT Settlement','-')
union all

select 'P2B',txn_id,utr_rrn,systimestamp,'DBA' from brs_kotak_neft
where txn_id in (
select narration from brs_escrow
where dr_cr = 'D') and ser_name in ('P2B','Assisted P2B','RAM NEFT','DMR NEFT');


-- end of brs recon temp

-- start of updation block  added by maghesh to record the type along with flag

update brs_escrow  set brs_flag = wes.aa, brs_desc = wes.cc , ctr_batch_no = wes.bb from brs_escrow
inner join (
SELECT 'RECON' aa ,BRT_REF_NO  bb,brt_desc cc  FROM BRS_RECON_TEMP  ) wes
on  REF_NO = wes.bb
 and  dr_cr = 'C'
 AND LENGTH(RTRIM(REF_NO)) = '12';

 update brs_escrow  set brs_flag = wes.aa, brs_desc = wes.bb , ctr_batch_no = wes.bb from brs_escrow
inner join (
SELECT BRT_DESC aa ,BRT_NARRATION  bb ,brt_desc cc FROM BRS_RECON_TEMP  ) wes
on  NARRATION = wes.bb
 and  dr_cr = 'D';
 


UPDATE BRS_ESCROW set brs_flag = 'NEFT' WHERE BRS_FLAG IS NULL AND NARRATION like 'NEFT%';



UPDATE BRS_ESCROW set brs_flag = 'IMPS' WHERE BRS_FLAG IS NULL AND NARRATION like 'IMPS%';

-- end of updation block


-- start of brs recon wes


-- IMPS & NEFT CREDIT 
INSERT INTO BRS_RECON_WES(BRW_DESC,BRW_D_C,BRW_CNT,BRW_SUM,BRW_CRT_TIME)
SELECT BRS_FLAG, DR_CR, COUNT(1),SUM(TXN_AMNT)  ,systimestamp FROM BRS_ESCROW WHERE BRS_FLAG = 'IMPS'  --    BRS_FLAG IN ('IMPS','NEFT')
GROUP BY DR_CR ,BRS_FLAG
union all
-- AGENT CASHIN,WEB AGENT, P2B/IMPS COMM, MERCHANT REVENUE CREDIT 
SELECT BRT_DESC,'C', COUNT(BRT_REF_NO), SUM(TXN_AMNT),systimestamp  from brs_escrow , BRS_RECON_TEMP where brs_flag = 'RECON'
and ref_no = brt_ref_no GROUP BY BRT_DESC
union all
-- ASSISTED P2B, DMR NEFT, P2B,RAM NEFT DEBIT
select ser_name, 'D', count(txn_id), sum(txn_val), systimestamp from brs_kotak_neft
where txn_id in (
select narration from brs_escrow
where dr_cr = 'D') and ser_name in ('P2B','Assisted P2B','RAM NEFT','DMR NEFT')
group by ser_name
union all
-- P2B COMM

select  'P2B / IMPS Comm', 'D', count(txn_AMNT), sum(txn_AMNT), systimestamp from brs_escrow
where narration like 'IDEADMTC%'
union all
-- MERCHANT REVENUE 
select  'MERCHANT REVENUE', 'D', count(txn_AMNT), sum(txn_AMNT), systimestamp from brs_escrow
where narration like 'IDEAMERC%'
union all
--CHARGES ADDED by Maghesh
select  'CHARGES', 'D', count(txn_AMNT), sum(txn_AMNT), systimestamp from brs_escrow
where narration like 'IDEA CHAR%'  having count(txn_amnt) > 0 
union all
-- MERCHANT SETTLEMENT DEBIT
select 'Merchant NEFT Settlement','D',count(txn_id), sum(txn_val), systimestamp from brs_kotak_neft
where txn_id in (
select narration from brs_escrow
where dr_cr = 'D') and ser_name in ('Merchant NEFT Settlement')
union all
select 'Physical Merchant','D',count(txn_id), sum(txn_val), systimestamp from brs_kotak_neft
where txn_id in (
select narration from brs_escrow
where dr_cr = 'D') and ser_name in ('-')
union all
-- MISSED CREDIT ENTRIES ASS P2B, DMR NEFT, MER NEFT P2B
select case ser_name when '-' then 'Physical Merchant'
else ser_name end,'C', count(txn_val), sum(txn_val) , systimestamp from brs_kotak_neft   where txn_id In
( (select case length(rtrim(ntr_tran_id)) when '13' then  concat('0',substr(ntr_tran_id,1,14))
  when '14' then substr(ntr_tran_id,1,14)
  else substr(ntr_tran_id,1,14) end
 from neft_returns_temp where rtrim(ltrim(NTR_CHQ_NO)) In (
select rtrim(ltrim(chq_no)) from brs_escrow where dr_cr = 'C')))
group by ser_name;


-- end of brs recon wes


-- start of brs recon details


-- TRANSACTION
--  DEBIT P2B,ASSISTED P2B, RAM NEFT , DMR NEFT
INSERT INTO brs_recon_details(BRD_ACC_NO,BRD_ACC_NAME,BRD_POST_DT,BRD_VALUE_DT,BRD_NARRATION,BRD_REF_NO,BRD_CHQ_NO,
BRD_DR_CR,BRD_CURR_CODE,BRD_TXN_AMT,BRD_RUN_BAL,BRD_TXN_ID,BRD_SER_NAME)
SELECT a.ACC_NO,a.ACC_NAME,a.POST_DT,a.VALUE_DT,a.NARRATION,a.REF_NO,a.CHQ_NO,a.DR_CR,
a.CURR_CODE,a.TXN_AMNT,a.RUN_BAL,b.TXN_ID,b.SER_NAME
FROM brs_escrow a, brs_kotak_neft b
where a.narration = b.txn_id  and a.DR_CR = 'D' 
and b.SER_NAME in ('P2B','Assisted P2B','RAM NEFT','DMR NEFT')
union all
-- P2B COMM
select  a.ACC_NO,a.ACC_NAME,a.POST_DT,a.VALUE_DT,a.NARRATION,a.REF_NO,a.CHQ_NO,a.DR_CR,
a.CURR_CODE,a.TXN_AMNT,a.RUN_BAL,NULL,'P2B / IMPS Comm' from brs_escrow a
where narration like 'IDEADMTC%'
union all
-- MERCANT REVE
select  a.ACC_NO,a.ACC_NAME,a.POST_DT,a.VALUE_DT,a.NARRATION,a.REF_NO,a.CHQ_NO,a.DR_CR,
a.CURR_CODE,a.TXN_AMNT,a.RUN_BAL,NULL,'Merchant Revenue' from brs_escrow a
where narration like 'IDEAMERC%'
union all
-- merchant settlement
SELECT a.ACC_NO,a.ACC_NAME,a.POST_DT,a.VALUE_DT,a.NARRATION,a.REF_NO,a.CHQ_NO,a.DR_CR,
a.CURR_CODE,a.TXN_AMNT,a.RUN_BAL,b.TXN_ID,case b.SER_NAME
when 'Merchant NEFT Settlement' then 'Merchant NEFT Settlement'
when '-' then 'Physical Merchant'
else '-' end 
FROM brs_escrow a, brs_kotak_neft b
where a.narration = b.txn_id  and a.DR_CR = 'D' 
and b.SER_NAME in ('Merchant NEFT Settlement','-')
union all 

-- IMPS BOTH CREDIT & DEBIT
select  a.ACC_NO,a.ACC_NAME,a.POST_DT,a.VALUE_DT,a.NARRATION,a.REF_NO,a.CHQ_NO,a.DR_CR,
a.CURR_CODE,a.TXN_AMNT,a.RUN_BAL,NULL,brs_flag from brs_escrow a
where BRS_FLAG IN ('IMPS')
union all
-- transaction credit 

SELECT a.ACC_NO,a.ACC_NAME,a.POST_DT,a.VALUE_DT,a.NARRATION,a.REF_NO,a.CHQ_NO,a.DR_CR,
a.CURR_CODE,a.TXN_AMNT,a.RUN_BAL,b.TXN_ID,b.SER_NAME
FROM brs_escrow a, brs_kotak_neft b, neft_returns_temp c
where a.DR_CR = 'C' and  LENGTH(RTRIM(rtrim(ltrim(chq_no)))) > 0 and a.chq_no = c.ntr_dr_cr and  substr(c.ntr_tran_id,1,14) = b.txn_id 

union all 

SELECT a.ACC_NO,a.ACC_NAME,a.POST_DT,a.VALUE_DT,a.NARRATION,a.REF_NO,a.CHQ_NO,a.DR_CR,
a.CURR_CODE,a.TXN_AMNT,a.RUN_BAL,BRT_REF_NO,BRT_DESC  from brs_escrow a , BRS_RECON_TEMP where brs_flag = 'RECON'
and a.ref_no = brt_ref_no;



-- end of brs recon details


-- start of brs recon summary temp

insert into brs_recon_summary_temp(BRST_TXN_TYP,brst_d_cnt,brst_c_cnt,brst_d_sum,brst_c_sum)
SELECT D.BRW_DESC,D.BRW_CNT, C.BRW_CNT ,D.BRW_SUM,C.BRW_SUM  FROM 
(SELECT BRW_DESC, BRW_CNT,BRW_SUM FROM BRS_RECON_WES WHERE BRW_D_C = 'D') as D  join
(SELECT BRW_DESC, BRW_CNT,BRW_SUM FROM BRS_RECON_WES WHERE BRW_D_C = 'C') as C
on (D.BRW_DESC = C.BRW_DESC) 
union all
-- only debit
OPEN cur FOR SELECT BRW_DESC, BRW_CNT,'0', BRW_SUM ,'0'  FROM BRS_RECON_WES WHERE BRW_D_C = 'D' and brw_desc not in 
(SELECT BRW_DESC FROM BRS_RECON_WES WHERE BRW_D_C = 'C')
union all
-- only credit 
SELECT BRW_DESC,'0', BRW_CNT,'0',BRW_SUM FROM BRS_RECON_WES WHERE BRW_D_C = 'C' and brw_desc not in 
(SELECT BRW_DESC FROM BRS_RECON_WES WHERE BRW_D_C = 'D');

/*
-- end of brs recon summary temp


-- Added Code for Escrow output download file -samuel 7/4/2017


-- Download File escrow
/*

-- matching ctr

select t.BATCH_ID, b.* from brs_ctr b, t_escrow_tcs t
where b.TXN_TYP = SUBSTRING(t.txn_id,1,len(t.txn_id) - 1)

*/

-- Escrow select * from brs_escrow  Escrow Match Report1

-- Downloading header for ESCROW file 
/*
 v_sqlheaderescrow1 varchar2(8000);

 v_sqlheaderescrow1 := 'bcp "SELECT ''ACC_NO'',''ACC_NAME'',''POST_DT'',''VALUE_DT'',''NARRATION'',''REF_NO'',''CHQ_NO'',''DR_CR'',''CURR_CODE'',''TXN_AMNT'',''RUN_BAL'',''CRT_DT'',''BRS_FLAG'',''BRS_DESC'',''CTR_BATCH_NO'' " queryout "C:downloadbrsheadercolumn_escrow.csv" /c /t"|" -T -S' from dual;
execute immediate  master..xp_cmdshell; v_sqlheaderescrow1

 v_sqldata varchar2(8000);
--  Downloading Code - moving to CSV file into download folder c:downloadbrs
 v_sqldata := 'bcp "select * from [abipb].[dbo].[brs_escrow]" queryout "C:downloadbrsescrowdata1.csv" /c /t"|" -T -S' from dual;
execute immediate  master..xp_cmdshell; v_sqldata

execute immediate  master..xp_cmdshell; 'copy /b "C:downloadbrsheadercolumn_escrow.csv"+"C:downloadbrsescrowdata1.csv" "C:downloadbrsEscrow_Main_File.csv"'

--***Deleting not required files after downloading

execute immediate  master..xp_cmdshell; 'del "C:downloadbrsheadercolumn_escrow.csv"'
execute immediate  master..xp_cmdshell; 'del "C:downloadbrsescrowdata1.csv"'

--  End of Escrow download brs_escrow report1

-- summary output
-- select * from brs_recon_summary_temp

 v_sqlsummary varchar2(8000);

 v_sqlsummary := 'bcp "SELECT ''BRST_TXN_TYP'', ''BRST_D_CNT'', ''BRST_D_SUM'', ''BRST_C_SUM'',''BRST_CRT_DT'',''BRST_C_CNT'' " queryout "C:downloadbrsheadercolumn_summary.csv" /c /t"|" -T -S' from dual;
execute immediate  master..xp_cmdshell; v_sqlsummary

 v_sqlsummarydata varchar2(8000);
--  Downloading Code - moving to CSV file into download folder c:downloadbrs
 v_sqlsummarydata := 'bcp "select * from [abipb].[dbo].[brs_recon_summary_temp]" queryout "C:downloadbrssummarydata.csv" /c /t"|" -T -S' from dual;
execute immediate  master..xp_cmdshell; v_sqlsummarydata

execute immediate  master..xp_cmdshell; 'copy /b "C:downloadbrsheadercolumn_summary.csv"+"C:downloadbrssummarydata.csv" "C:downloadbrsEscrow_Main_SummaryFile.csv"'

execute immediate  master..xp_cmdshell; 'del "C:downloadbrsheadercolumn_summary.csv"'
execute immediate  master..xp_cmdshell; 'del "C:downloadbrssummarydata.csv"'


-- Matching -- matching ctr

 v_sqlmatch varchar2(8000);

 v_sqlmatch := 'bcp " SELECT ''BATCH_ID'',''TXN_ID'',''TXN_TYP'',''TXN_DT'',''TXN_TIME'',''CHANNEL'',''CUST_MOB'',''CUST_FST_NAME'',''CUST_LST_NAME'',''CUST_CLE'',''CUST_PRDT'',''DOR'',''TOR'',''CUST_KYC_STAT'',''CUST_ENT_ID'',''CUST_ACC'',''CUST_TXN_AMNT'',''CHRG'',''SRC_TAX'',''EDU_TAX'',''EDU_CESS'',''COMISION'',''TDS'',''NET_COMISION'',''IFSCCODE'',''BEN_ACC'',''BILL_PAY'',''BILLERCODE'',''BILLERDESC'',''BILLERFILED'',''TXN_STAT'',''ORG_TXN_ID'',''ORG_TXN_DT'',''ORG_TXN_TIME'',''BPARTY_NUMBER'',''BPARTYCIRCLE'',''UTR_RRN_NUMBER'',''FINAL_STAT'',''TELES_ID'',''TRANS_FLAG''" queryout "C:downloadbrsheadercolumn_match.csv" /c /t"|" -T -S' from dual;
execute immediate  master..xp_cmdshell; v_sqlmatch

 v_sqlmatchdata varchar2(8000);
--  Downloading Code - moving to CSV file into download folder c:downloadbrs
 v_sqlmatchdata := 'bcp "select t.BATCH_ID,b.TXN_ID,b.[TXN_TYP],b.[TXN_DT],b.[TXN_TIME],b.[CHANNEL],b.[CUST_MOB],b.[CUST_FST_NAME],b.[CUST_LST_NAME],b.[CUST_CLE],b.[CUST_PRDT],b.[DOR],b.[TOR],b.[CUST_KYC_STAT],b.[CUST_ENT_ID] ,b.[CUST_ACC] ,b.[CUST_TXN_AMNT], b.[CHRG], b.[SRC_TAX],b.[EDU_TAX] ,b.[EDU_CESS]  ,b.[COMISION]   ,b.[TDS] ,b.[NET_COMISION]   ,b.[IFSCCODE]   ,b.[BEN_ACC]  ,b.[BILL_PAY]   ,b.[BILLERCODE] ,b.[BILLERDESC]  ,b.[BILLERFILED]    ,b.[TXN_STAT]      ,b.[ORG_TXN_ID]    ,b.[ORG_TXN_DT]     ,b.[ORG_TXN_TIME]   ,b.[BPARTY_NUMBER]   ,b.[BPARTYCIRCLE]    ,b.[UTR_RRN_NUMBER]     ,b.[FINAL_STAT]   ,b.[TELES_ID]  ,b.[TRANS_FLAG] from [abipb].[dbo].[brs_ctr] b, [abipb].[dbo].[t_escrow_tcs] t where b.TXN_TYP = SUBSTRING(t.txn_id,1,len(t.txn_id) - 1) " queryout "C:downloadbrssummarydata.csv" /c /t"|" -T -S' from dual;
execute immediate  master..xp_cmdshell; v_sqlmatchdata


execute immediate  master..xp_cmdshell; 'copy /b "C:downloadbrsheadercolumn_match.csv"+"C:downloadbrssummarydata.csv" "C:downloadbrsEscrow_Main_MatchFile.csv"'

execute immediate  master..xp_cmdshell; 'del "C:downloadbrsheadercolumn_match.csv"'
execute immediate  master..xp_cmdshell; 'del "C:downloadbrssummarydata.csv"'

--End of Downloading Escrow file
*/

END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_CTR_LOAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_CTR_LOAD" ( msg   OUT VARCHAR2 ) AS
    v_query    VARCHAR2(10000);
    v_query2   VARCHAR2(4000);
    v_query3   VARCHAR2(10000);
BEGIN
    v_query := 'create table brs_ctr_load_ext(
                TXN_TYPE	VARCHAR2(200 BYTE),
                TXN_ID	VARCHAR2(200 BYTE),
                TXN_DT	VARCHAR2(200 BYTE),
                TXN_TIME	VARCHAR2(200 BYTE),
                CHANNEL	VARCHAR2(200 BYTE),
                CUST_MOB	VARCHAR2(200 BYTE), 
                CUST_FST_NAME	VARCHAR2(200 BYTE),
                CUST_LST_NAME	VARCHAR2(200 BYTE),
                CUST_CLE VARCHAR2(200 BYTE),
                CUST_PRDT	VARCHAR2(200 BYTE),
                DOR	VARCHAR2(200 BYTE),
                TOR	VARCHAR2(200 BYTE),
                CUST_KYC_STAT	VARCHAR2(200 BYTE),
                CUST_ENT_ID	VARCHAR2(200 BYTE),
                CUST_ACC	VARCHAR2(200 BYTE),
                CUST_TXN_AMNT	VARCHAR2(200 BYTE),
                CHRG VARCHAR2(200 BYTE),
                SRC_TAX	VARCHAR2(200 BYTE),
                EDU_CESS	VARCHAR2(200 BYTE),
                SHEDUCESS VARCHAR2(200 BYTE),
                COMISION	VARCHAR2(200 BYTE),
                TDS	VARCHAR2(200 BYTE),
                NET_COMISION	VARCHAR2(200 BYTE),
                IFSCCODE	VARCHAR2(200 BYTE),
                BEN_ACC	VARCHAR2(200 BYTE),
                BILL_PAY	VARCHAR2(200 BYTE),
                BILLERCODE	VARCHAR2(200 BYTE),
                BILLERDESC	VARCHAR2(200 BYTE),
                BILLERFIELD	VARCHAR2(200 BYTE),
                TXN_STAT	VARCHAR2(200 BYTE),
                ORG_TXN_ID	VARCHAR2(200 BYTE),
                ORG_TXN_DT	VARCHAR2(200 BYTE),
                ORG_TXN_TIME	VARCHAR2(200 BYTE),
                BPARTY_NUMBER	VARCHAR2(200 BYTE),
                BPARTYCIRCLE	VARCHAR2(200 BYTE),
                UTR_RRN_NUMBER	VARCHAR2(200 BYTE),
                FINAL_STAT	VARCHAR2(200 BYTE),
                TELES_ID	VARCHAR2(200 BYTE),
               TRANS_FLAG	VARCHAR2(200 BYTE)
        )
        organization external (
          type oracle_loader
          default directory WES_DIR  
          access parameters (
          RECORDS DELIMITED BY NEWLINE 
          skip 1
          fields terminated by ''|'' )
          location (''ctr.csv'')
        )
        parallel
        reject limit unlimited';

    EXECUTE IMMEDIATE v_query;
   v_query3 := 'INSERT INTO brs_load_ctr_main_int(txn_type,   
                                              txn_id,
                                              txn_dt,
                                              txn_time,
                                              channel,
                                              cust_mob,
                                              cust_fst_name,    
                                              cust_lst_name,
                                              cust_cle,
                                              cust_prdt,
                                              dor,
                                              tor,
                                              cust_kyc_stat,
                                              cust_ent_id,
                                              cust_acc,    
                                              cust_txn_amnt,
                                              chrg,src_tax,
                                              edu_cess,
                                              SHEDUCESS,
                                              comision,
                                              tds,
                                              net_comision,
                                              ifsccode,
                                              ben_acc,    
                                              bill_pay,
                                              bill_code,
                                              billerdesc,
                                              billerfield,
                                              txn_stat,
                                              org_txn_id,
                                              org_txn_dt,
                                              org_txn_time,
                                              bparty_number,
                                              BPARTYCIRCLE,
                                              UTR_RRN_NUMBER,
                                              FINAL_STAT,
                                              TELES_ID,
                                              TRANS_FLAG                                    
                                             ) SELECT
                                             txn_type,txn_id,
                                             to_date(txn_dt,''DD/MM/YYYY''),
                                             txn_time,
                                            channel,
                                            cust_mob,
                                            cust_fst_name,    
                                            cust_lst_name,
                                            cust_cle,
                                            cust_prdt,
                                            to_date(dor,''DD/MM/YYYY''),
                                            tor,
                                            cust_kyc_stat,
                                            cust_ent_id,
                                            cust_acc,    
                                            cust_txn_amnt,
                                            decode(chrg,''-'', ''0'',chrg),
                                            decode(src_tax,''-'', ''0'',src_tax),
                                            edu_cess,
                                            SHEDUCESS,
                                            comision,
                                            tds,
                                            net_comision,
                                            ifsccode,
                                            ben_acc,    
                                            bill_pay,
                                            billercode,
                                            billerdesc,
                                            billerfield,
                                            txn_stat,
                                            org_txn_id,
                                            org_txn_dt,
                                            org_txn_time,
                                            bparty_number,
                                            BPARTYCIRCLE,
                                            UTR_RRN_NUMBER,
                                            FINAL_STAT,
                                            TELES_ID,
                                            TRANS_FLAG 
                                            FROM brs_ctr_load_ext';
        
    EXECUTE IMMEDIATE v_query3;
    COMMIT;
    v_query2 := 'DROP TABLE brs_ctr_load_ext';
    
    EXECUTE IMMEDIATE v_query2;
    msg := 'File Loaded Successfully';

    EXCEPTION 
    WHEN OTHERS THEN
    msg := 'Failed to Load CTR_File';
    raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||sqlerrm);  
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_CTR_LOAD_OLD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_CTR_LOAD_OLD" ( msg   OUT VARCHAR2 ) AS
    v_query    VARCHAR2(10000);
    v_query2   VARCHAR2(4000);
    v_query3   VARCHAR2(10000);
BEGIN
    v_query := 'create table brs_ctr_load_ext(
                TXN_TYPE	VARCHAR2(200 BYTE),
                TXN_ID	VARCHAR2(200 BYTE),
                TXN_DT	VARCHAR2(200 BYTE),
                TXN_TIME	VARCHAR2(200 BYTE),
                CHANNEL	VARCHAR2(200 BYTE),
                CUST_MOB	VARCHAR2(200 BYTE), 
                CUST_FST_NAME	VARCHAR2(200 BYTE),
                CUST_LST_NAME	VARCHAR2(200 BYTE),
                CUST_CLE VARCHAR2(200 BYTE),
                CUST_PRDT	VARCHAR2(200 BYTE),
                DOR	VARCHAR2(200 BYTE),
                TOR	VARCHAR2(200 BYTE),
                CUST_KYC_STAT	VARCHAR2(200 BYTE),
                CUST_ENT_ID	VARCHAR2(200 BYTE),
                CUST_ACC	VARCHAR2(200 BYTE),
                CUST_TXN_AMNT	VARCHAR2(200 BYTE),
                CHRG VARCHAR2(200 BYTE),
                SRC_TAX	VARCHAR2(200 BYTE),
                EDU_CESS	VARCHAR2(200 BYTE),
                COMISION	VARCHAR2(200 BYTE),
                TDS	VARCHAR2(200 BYTE),
                NET_COMISION	VARCHAR2(200 BYTE),
                IFSCCODE	VARCHAR2(200 BYTE),
                BEN_ACC	VARCHAR2(200 BYTE),
                BILL_PAY	VARCHAR2(200 BYTE),
                BILLERCODE	VARCHAR2(200 BYTE),
                BILLERDESC	VARCHAR2(200 BYTE),
                BILLERFIELD	VARCHAR2(200 BYTE),
                TXN_STAT	VARCHAR2(200 BYTE),
                ORG_TXN_ID	VARCHAR2(200 BYTE),
                ORG_TXN_DT	VARCHAR2(200 BYTE),
                ORG_TXN_TIME	VARCHAR2(200 BYTE),
                BPARTY_NUMBER	VARCHAR2(200 BYTE),
                BPARTYCIRCLE	VARCHAR2(200 BYTE),
                UTR_RRN_NUMBER	VARCHAR2(200 BYTE),
                FINAL_STAT	VARCHAR2(200 BYTE),
                TELES_ID	VARCHAR2(200 BYTE),
               TRANS_FLAG	VARCHAR2(200 BYTE)
        )
        organization external (
          type oracle_loader
          default directory WES_DIR  
          access parameters (
          RECORDS DELIMITED BY NEWLINE
          skip 1
          fields terminated by ''|'' )
          location (''ctr.csv'')
        )
        parallel
        reject limit unlimited';

    EXECUTE IMMEDIATE v_query;
   v_query3 := 'INSERT INTO brs_load_ctr_main_int(txn_type,
                                              txn_id,
                                              txn_dt,
                                              txn_time,
                                              channel,
                                              cust_mob,
                                              cust_fst_name,    
                                              cust_lst_name,
                                              cust_cle,
                                              cust_prdt,
                                              dor,
                                              tor,
                                              cust_kyc_stat,
                                              cust_ent_id,
                                              cust_acc,    
                                              cust_txn_amnt,
                                              chrg,src_tax,
                                              edu_cess,
                                              comision,
                                              tds,
                                              net_comision,
                                              ifsccode,
                                              ben_acc,    
                                              bill_pay,
                                              bill_code,
                                              billerdesc,
                                              billerfield,
                                              txn_stat,
                                              org_txn_id,
                                              org_txn_dt,
                                              org_txn_time,
                                              bparty_number    
                                             ) SELECT
                                             txn_type,txn_id,
                                             txn_dt,
                                             txn_time,
                                            channel,
                                            cust_mob,
                                            cust_fst_name,    
                                            cust_lst_name,
                                            cust_cle,
                                            cust_prdt,
                                            dor,
                                            tor,
                                            cust_kyc_stat,
                                            cust_ent_id,
                                            cust_acc,    
                                            cust_txn_amnt,
                                            chrg,
                                            src_tax,
                                            edu_cess,
                                            comision,
                                            tds,
                                            net_comision,
                                            ifsccode,
                                            ben_acc,    
                                            bill_pay,
                                            billercode,
                                            billerdesc,
                                            billerfield,
                                            txn_stat,
                                            org_txn_id,
                                            org_txn_dt,
                                            org_txn_time,
                                            bparty_number 
                                            FROM brs_ctr_load_ext';
        
    EXECUTE IMMEDIATE v_query3;
    COMMIT;
    v_query2 := 'DROP TABLE brs_ctr_load_ext';
    
    EXECUTE IMMEDIATE v_query2;
    msg := 'File Loaded Successfully';

    EXCEPTION 
    WHEN OTHERS THEN
    msg := 'Failed to Load CTR_File';
    raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||sqlerrm);  
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_DTR_LOAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_DTR_LOAD" ( msg   OUT VARCHAR2 ) AS
    v_query    VARCHAR2(10000);
    v_query2   VARCHAR2(4000);
    v_query3   VARCHAR2(30000);
    file_count   NUMBER := 0;
BEGIN
   
     SELECT COUNT(1) INTO file_count
     FROM daily_log
     WHERE dl_file_id = '004';

     IF file_count > 0 THEN
          msg := 'File Already Loaded';
     ELSE

   
    v_query := 'create table brs_dtr_ext (                 
                TXN_TYPE VARCHAR2(4000 BYTE),               
                TXN_ID VARCHAR2(1000 BYTE),               
                TXN_DATE VARCHAR2(1000 BYTE),               
                TXN_TIME VARCHAR2(1000 BYTE),               
                TXN_CHANNEL VARCHAR2(1000 BYTE),               
                RTR_MOB_NO VARCHAR2(1000 BYTE),               
                RTR_FST_NAME VARCHAR2(1000 BYTE),               
                RTR_LST_NAME VARCHAR2(1000 BYTE),               
                RTR_CIRCLE VARCHAR2(1000 BYTE),               
                RTR_POA_TYP VARCHAR2(1000 BYTE),               
                RTR_POA_NO VARCHAR2(1000 BYTE),               
                RTR_CMMSN_BAND VARCHAR2(1000 BYTE),               
                DTR_MOB_NO VARCHAR2(1000 BYTE),               
                DTR_NAME VARCHAR2(1000 BYTE),               
                DTR_REG_DATE VARCHAR2(1000 BYTE),               
                TXN_ENTITY_ID VARCHAR2(1000 BYTE),               
                RTR_ACCT_NO VARCHAR2(1000 BYTE),               
                TXN_AMT VARCHAR2(1000 BYTE),              
                TXN_CHRG VARCHAR2(1000 BYTE),               
                TXN_SER_TAX VARCHAR2(1000 BYTE),               
                TXN_EDU_TAX VARCHAR2(1000 BYTE),               
                TXN_HEDU_TAX VARCHAR2(1000 BYTE),               
                RTR_COMISN VARCHAR2(1000 BYTE),               
                RTR_TDS VARCHAR2(1000 BYTE),               
                DTR_COMISN VARCHAR2(1000 BYTE),               
                DTR_TDS VARCHAR2(1000 BYTE),               
                TOTAL_COMISN VARCHAR2(1000 BYTE),               
                IFSS_CODE VARCHAR2(1000 BYTE),               
                BENI_AC_NO VARCHAR2(1000 BYTE),               
                BILLPAY_REF VARCHAR2(1000 BYTE),               
                TXN_STATUS VARCHAR2(1000 BYTE),               
                ORIG_TXN_ID VARCHAR2(1000 BYTE),               
                ORIG_TXN_DATE VARCHAR2(1000 BYTE),               
                TXN_COMISN_ID VARCHAR2(1000 BYTE),               
                COMISN_TXN_TYPE VARCHAR2(1000 BYTE),               
                SHOP_NAME VARCHAR2(1000 BYTE),               
                FIRM_NAME VARCHAR2(1000 BYTE),             
                CUST_MOB_NO VARCHAR2(1000 BYTE)              
                )        
                organization external (          
                type oracle_loader          
                default directory WES_DIR            
                access parameters (
                 RECORDS DELIMITED BY NEWLINE 
                 SKIP 1
                fields terminated by ''|''
                MISSING FIELD VALUES ARE NULL
                )  
                location (''dtr4321.csv'')        
                ) parallel     
                reject limit unlimited'
                ;
    EXECUTE IMMEDIATE v_query;
    v_query3 := 'INSERT INTO brs_dtr_wes_main_int (
                    txn_type, txn_id,
                    txn_date,
                    txn_time,
                    txn_channel,
                    rtr_mob_no,
                    rtr_fst_name,
                    rtr_lst_name,
                    rtr_circle,
                    rtr_poa_typ,
                    rtr_poa_no,
                    rtr_cmmsn_band,
                    dtr_mob_no,
                    dtr_name,
                    dtr_reg_date,
                    txn_entity_id,
                    rtr_acct_no,
                    txn_amt,
                    txn_chrg,
                    txn_ser_tax,
                    txn_edu_tax,
                    txn_hedu_tax,
                    rtr_comisn,
                    rtr_tds,
                    dtr_comisn,
                    dtr_tds,
                    total_comisn,
                    ifss_code,
                    beni_ac_no,
                    billpay_ref,
                    txn_status,
                    orig_txn_id,
                    orig_txn_date,
                    txn_comisn_id,
                    comisn_txn_type,
                    shop_name,
                    firm_name,
                    cust_mob_no
                    ) SELECT
                    txn_type,
                    txn_id,
                    txn_date,
                    txn_time,
                    txn_channel,
                    rtr_mob_no,
                    rtr_fst_name,
                    rtr_lst_name,
                    rtr_circle,
                    rtr_poa_typ,
                    rtr_poa_no,
                    rtr_cmmsn_band,
                    dtr_mob_no,
                    dtr_name,
                    dtr_reg_date,
                    txn_entity_id,
                    rtr_acct_no,
                    txn_amt,
                    txn_chrg,
                    txn_ser_tax,
                    txn_edu_tax,
                    txn_hedu_tax,
                    rtr_comisn,
                    rtr_tds,
                    dtr_comisn,
                    dtr_tds,
                    total_comisn,
                    ifss_code,
                    beni_ac_no,
                    billpay_ref,
                    txn_status,
                    orig_txn_id,
                    orig_txn_date,
                    txn_comisn_id,
                    comisn_txn_type,
                    shop_name,
                    firm_name,
                    cust_mob_no
                    FROM
                    brs_dtr_ext'
                    ;
    EXECUTE IMMEDIATE v_query3;
    COMMIT;
      v_query2 := 'DROP TABLE brs_dtr_ext';
       INSERT INTO daily_log (
                    dl_file_id,
                    dl_file_name,
                    dl_transaction_dt,
                    dl_upld_dt_time,
                    dl_upld_count,
                    dl_rej_count,
                    dl_status,
                    dl_remarks,
                    dl_created_by,
                    dl_created_dt,
                    dl_updated_by,
                    dl_updated_dt
               ) SELECT '004',
                      'DTR',
                      SYSDATE,
                      current_timestamp,
                      COUNT(1),
                      '0',
                      'Loaded',
                      'NA',
                      'Admin',
                      current_timestamp,
                      'Admin',
                      current_timestamp
               FROM brs_dtr_wes_main_int; commit;
       
      
      
      EXECUTE IMMEDIATE v_query2;
    msg := 'File Loaded Successfully';
    END IF;
EXCEPTION
    WHEN OTHERS THEN
        msg := 'Failed to Load dtr_File';
        raise_application_error(
            -20001,
            'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
        );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_DTR_LOAD_NEW
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_DTR_LOAD_NEW" ( msg   OUT VARCHAR2 ) AS
    v_query    VARCHAR2(10000);
    v_query2   VARCHAR2(4000);
    v_query3   VARCHAR2(30000);
    file_count   NUMBER := 0;
BEGIN
   
   SELECT COUNT(1) INTO file_count
     FROM daily_log
     WHERE dl_file_id = '00400';

     IF file_count > 0 THEN
          msg := 'File Already Loaded';
     ELSE 

   
    v_query := 'create table brs_dtr_ext (                 
                TXN_TYPE	VARCHAR2(200 BYTE),
                TXN_ID	VARCHAR2(200 BYTE),
                TXN_DATE	VARCHAR2(200 BYTE),
                TXN_TIME	VARCHAR2(200 BYTE),
                TXN_CHANNEL	VARCHAR2(200 BYTE),
                DTR_MOB_NO	VARCHAR2(200 BYTE), 
                DTR_FST_NAME	VARCHAR2(200 BYTE),
                DTR_LST_NAME	VARCHAR2(200 BYTE),
                DTR_SHOPNAME VARCHAR2(200 BYTE),
                DTR_CIRCLE	VARCHAR2(200 BYTE),
                DTR_POI_TYPE	VARCHAR2(200 BYTE),
                DTR_POI_NUMBER	VARCHAR2(200 BYTE),
                DTR_REGDATE	VARCHAR2(200 BYTE),
                DTR_ENTITYID	VARCHAR2(200 BYTE),
                DTR_ACC_NO	VARCHAR2(200 BYTE),
                DTR_TXN_AMNT	VARCHAR2(200 BYTE),
                DTR_CHARGES VARCHAR2(200 BYTE),
                DTR_SERTAX	VARCHAR2(200 BYTE),
                DTR_EDUCESS	VARCHAR2(200 BYTE),
                DTR_SHEDU VARCHAR2(200 BYTE),
                DTR_RET_COMM	VARCHAR2(200 BYTE),
                DTR_RET_TDS	VARCHAR2(200 BYTE),
                DTR_COMM	VARCHAR2(200 BYTE),
                DTR_TDS	VARCHAR2(200 BYTE),
                TOTAL_COMM	VARCHAR2(200 BYTE),
                IFSC_CODE	VARCHAR2(200 BYTE),
                BENI_ACC_NO	VARCHAR2(200 BYTE),
                BILLPAY_REF	VARCHAR2(200 BYTE),
                TXN_STATUS	VARCHAR2(200 BYTE),
                ORIG_TXN_ID	VARCHAR2(200 BYTE),
                ORIG_TXN_DATE	VARCHAR2(200 BYTE),
                ORIG_TXN_TIME	VARCHAR2(200 BYTE),
                COMISN_TXN_ID	VARCHAR2(200 BYTE),
                COMM_TXN_TYPE	VARCHAR2(200 BYTE),
                COMM_BAND_NAM	VARCHAR2(200 BYTE)        
                )        
                organization external (          
                type oracle_loader          
                default directory WES_DIR            
                access parameters (
                RECORDS DELIMITED BY NEWLINE 
                skip 1
                fields terminated by ''|''
                MISSING FIELD VALUES ARE NULL
                )  
                location (''dtr.csv'')        
                ) 
                parallel     
                reject limit unlimited';
                
    EXECUTE IMMEDIATE v_query;
    v_query3 := 'INSERT INTO brs_dtr_wes_main_int_june (
                   TXN_TYPE,
                   TXN_ID,
                  TXN_DATE,
                  TXN_TIME,
                  TXN_CHANNEL,
                  DTR_MOB_NO,
                  DTR_FST_NAME,
                  DTR_LST_NAME,
                  DTR_SHOPNAME,
                  DTR_CIRCLE,
                  DTR_POI_TYPE,
                  DTR_POI_NUMBER,
                  DTR_REGDATE,
                  DTR_ENTITYID,
                  DTR_ACC_NO,
                  DTR_TXN_AMNT,
                  DTR_CHARGES,
                  DTR_SERTAX,
                  DTR_EDUCESS,
                  DTR_SHEDU,
                  DTR_RET_COMM,
                  DTR_RET_TDS,
                  DTR_COMM,
                  DTR_TDS,
                  TOTAL_COMM,
                  IFSC_CODE,
                  BENI_ACC_NO,
                  BILLPAY_REF,
                  TXN_STATUS,
                  ORIG_TXN_ID,
                  ORIG_TXN_DATE,
                  ORIG_TXN_TIME,
                  COMISN_TXN_ID,
                  COMM_TXN_TYPE,
                  COMM_BAND_NAM
                    ) SELECT
                    TXN_TYPE,
                   TXN_ID,
                  to_date(txn_date,''DD/MM/YYYY''),
                  TXN_TIME,
                  TXN_CHANNEL,
                  DTR_MOB_NO,
                  DTR_FST_NAME,
                  DTR_LST_NAME,
                  DTR_SHOPNAME,
                  DTR_CIRCLE,
                  DTR_POI_TYPE,
                  DTR_POI_NUMBER,
                  DTR_REGDATE,
                  DTR_ENTITYID,
                  DTR_ACC_NO,
                  DTR_TXN_AMNT,
                  decode(DTR_CHARGES,''-'', ''0'',DTR_CHARGES),
                  decode(DTR_SERTAX,''-'', ''0'',DTR_SERTAX),
                 DTR_EDUCESS,
                  DTR_SHEDU,
                  DTR_RET_COMM,
                  DTR_RET_TDS,
                  DTR_COMM,
                  DTR_TDS,
                  TOTAL_COMM,
                  IFSC_CODE,
                  BENI_ACC_NO,
                  BILLPAY_REF,
                  TXN_STATUS,
                  ORIG_TXN_ID,
                  ORIG_TXN_DATE,
                  ORIG_TXN_TIME,
                  COMISN_TXN_ID,
                  COMM_TXN_TYPE,
                  COMM_BAND_NAM
                    FROM
                    brs_dtr_ext';
                    
    EXECUTE IMMEDIATE v_query3;
    COMMIT;
      v_query2 := 'DROP TABLE brs_dtr_ext';
     INSERT INTO daily_log (
                    dl_file_id,
                    dl_file_name,
                    dl_transaction_dt,
                    dl_upld_dt_time,
                    dl_upld_count,
                    dl_rej_count,
                    dl_status,
                    dl_remarks,
                    dl_created_by,
                    dl_created_dt,
                    dl_updated_by,
                    dl_updated_dt
               ) SELECT '004',
                      'DTR',
                      SYSDATE,
                      current_timestamp,
                      COUNT(1),
                      '0',
                      'Loaded',
                      'NA',
                      'Admin',
                      current_timestamp,
                      'Admin',
                      current_timestamp
               FROM brs_dtr_wes_main_int_june; 
               
               commit; 
           
      EXECUTE IMMEDIATE v_query2; 
    msg := 'File Loaded Successfully';
    END IF;
EXCEPTION
    WHEN OTHERS THEN
        msg := 'Failed to Load dtr_File';
        raise_application_error(-20001,'An error was encountered - '|| SQLCODE ||' -ERROR- ' || sqlerrm);
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_EMONEY_LOAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_EMONEY_LOAD" ( msg   OUT VARCHAR2 ) AS

     v_query      VARCHAR2(10000);
     v_query2     VARCHAR2(4000);
     v_query3     VARCHAR2(10000);
     file_count   NUMBER := 0;
BEGIN
     SELECT COUNT(1) INTO file_count
     FROM daily_log
     WHERE dl_file_id = '008';

     IF file_count > 0 THEN
          msg := 'File Already Loaded';
     ELSE
     
         v_query := 'create table brs_Emoney_ext (
                    BEE_Circle Varchar2(200) NULL,
                    BEE_TransactionID Number(18, 0) NULL,
                    BEE_TransactionAmount Number(18, 4) NULL,
                    BEE_TransactionType Varchar2(200) NULL,
                    BEE_TransactionCompletedDate Varchar2(200) NULL,
                    BEE_TransactionCompletedTime Varchar2(200) NULL,
                    BEE_TransactionStatus Varchar2(200) NULL,
                    BEE_EntityType Varchar2(200) NULL,
                    BEE_DistributorMobilenumber Varchar2(200) NULL,
                    BEE_DistributorFirstName Varchar2(200) NULL,
                    BEE_DistributorLastName Varchar2(200) NULL,
                    BEE_EntityCode Varchar2(200) NULL,
                    BEE_UTRNo Varchar2(200) NULL,
                    BEE_DateDepositedinBank Varchar2(200) NULL,
                    BEE_TimeDepositedinBank Varchar2(200) NULL,
                    BEE_AccountNumber Varchar2(200) NULL
            )
            organization external (
              type oracle_loader
              default directory WES_DIR  
              access parameters (
              RECORDS DELIMITED BY NEWLINE 
              SKIP 1
              fields terminated by ''|'' )
              location (''Emoney.csv'')
            )
            parallel
            reject limit unlimited';
            
          EXECUTE IMMEDIATE v_query;
          
          v_query2 := 'INSERT INTO brs_emoney_main(be_circle,
                                   be_transactionid,
                                   be_transactionamount,
                                   be_transactiontype,
                                   be_transactioncompleteddate,
                                   be_transactioncompletedtime,
                                   be_transactionstatus,
                                   be_entitytype,
                                   be_distributormobilenumber,
                                   be_distributorfirstname,
                                   be_distributorlastname,
                                   be_entitycode,
                                   be_utrno,
                                   be_datedepositedinbank,
                                   be_timedepositedinbank,
                                   be_accountnumber)
                                   SELECT bee_circle,
                                   bee_transactionid,
                                   bee_transactionamount,
                                   bee_transactiontype,
                                   bee_transactioncompleteddate,
                                   bee_transactioncompletedtime,
                                   bee_transactionstatus,
                                   bee_entitytype,
                                   bee_distributormobilenumber,
                                   bee_distributorfirstname,
                                   bee_distributorlastname,
                                   bee_entitycode,
                                   bee_utrno,
                                   bee_datedepositedinbank,
                                   bee_timedepositedinbank,
                                   bee_accountnumber
                                   FROM brs_emoney_ext';
                              
          EXECUTE IMMEDIATE v_query2;
          COMMIT;
          v_query3 := 'DROP TABLE brs_emoney_ext';
          
        INSERT INTO daily_log (
                    dl_file_id,
                    dl_file_name,
                    dl_transaction_dt,
                    dl_upld_dt_time,
                    dl_upld_count,
                    dl_rej_count,
                    dl_status,
                    dl_remarks,
                    dl_created_by,
                    dl_created_dt,
                    dl_updated_by,
                    dl_updated_dt
               ) SELECT '008',
                      'EMoney',
                      SYSDATE,
                      current_timestamp,
                      COUNT(1),
                      '0',
                      'Loaded',
                      'NA',
                      'Admin',
                      current_timestamp,
                      'Admin',
                      current_timestamp
               FROM brs_emoney_main; commit;
               
          
          EXECUTE IMMEDIATE v_query3;
          msg := 'Emoney File Loaded Successfully';
     END IF;

EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to Load Emoney File';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_ESCROW_LOAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_ESCROW_LOAD" ( msg   OUT VARCHAR2 ) AS
    v_query    VARCHAR2(10000);
    v_query2   VARCHAR2(4000);
    v_query3   VARCHAR2(10000);
    file_count   NUMBER := 0;
BEGIN
     SELECT COUNT(1) INTO file_count
     FROM daily_log
     WHERE dl_file_id = '002';

     IF file_count > 0 THEN
          msg := 'File Already Loaded';
     ELSE


    v_query := 'create table brs_escrow_load_ext (
                ACC_NO	VARCHAR2(30 BYTE),
                ACC_NAME	VARCHAR2(100 BYTE),
                POST_DT	VARCHAR2(50 BYTE),
                VALUE_DT	VARCHAR2(50 BYTE),
                NARRATION	VARCHAR2(100 BYTE),
                REF_NO	VARCHAR2(100 BYTE),
                CHQ_NO	VARCHAR2(50 BYTE),
                DR_CR	VARCHAR2(50 BYTE),
                CURR_CODE	VARCHAR2(50 BYTE),
                TXN_AMNT	VARCHAR2(50 BYTE),
                RUN_BAL	VARCHAR2(50 BYTE)
        )
        organization external (
          type oracle_loader
          default directory WES_DIR  
          access parameters (
          RECORDS DELIMITED BY NEWLINE 
          SKIP 1
          fields terminated by ''|'' )
          location (''3611409323_DAY-1.txt'')
        )
        parallel
        reject limit unlimited';

    EXECUTE IMMEDIATE v_query;
    v_query3 := 'INSERT INTO brs_escrow_main_int(acc_no,acc_name,post_dt,
        value_dt,
        narration,
        ref_no,
        chq_no,
        dr_cr,
        curr_code,
        txn_amnt,
        run_bal
    ) SELECT
        acc_no,
        acc_name,
        TO_timestamp(POST_DT,''mm/dd/yyyy HH:MI:SS AM''),
        TO_timestamp(value_dt,''mm/dd/yyyy HH:MI:SS AM''),
        narration,
        ref_no,
        chq_no,
        dr_cr,
        curr_code,
        txn_amnt,
        run_bal
    FROM
        brs_escrow_load_ext where acc_name not in ''ACCT_NAME''';
        
    EXECUTE IMMEDIATE v_query3;
    COMMIT;
    v_query2 := 'DROP TABLE brs_escrow_load_ext';
    
     INSERT INTO daily_log (
                    dl_file_id,
                    dl_file_name,
                    dl_transaction_dt,
                    dl_upld_dt_time,
                    dl_upld_count,
                    dl_rej_count,
                    dl_status,
                    dl_remarks,
                    dl_created_by,
                    dl_created_dt,
                    dl_updated_by,
                    dl_updated_dt
               ) SELECT '002',
                      'ESCROW',
                      SYSDATE,
                      current_timestamp,
                      COUNT(1),
                      '0',
                      'Loaded',
                      'NA',
                      'Admin',
                      current_timestamp,
                      'Admin',
                      current_timestamp
               FROM brs_escrow_main_int; commit;
    
    EXECUTE IMMEDIATE v_query2;
    msg := 'File Loaded Successfully';
     END IF;

    EXCEPTION 
    WHEN OTHERS THEN
    msg := 'Failed to Load Escrow_File';
    raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||sqlerrm);  
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_IMPS_LOAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_IMPS_LOAD" ( msg   OUT VARCHAR2 ) AS

     v_query      VARCHAR2(10000);
     v_query2     VARCHAR2(4000);
     v_query3     VARCHAR2(10000);
     file_count   NUMBER := 0;
BEGIN
     SELECT COUNT(1) INTO file_count
     FROM daily_log
     WHERE dl_file_id = '003';

     IF file_count > 0 THEN
          msg := 'File Already Loaded';
     ELSE
     
         v_query := 'create table brs_imps_load_ext (
                    TXN_TYPE	VARCHAR2(200 BYTE),
                    TXN_ID	VARCHAR2(200 BYTE),
                    TXN_DT	VARCHAR2(200 BYTE),
                    TXN_TIME	VARCHAR2(200 BYTE),
                    CHANNEL	VARCHAR2(200 BYTE),
                    CUST_MOB	VARCHAR2(200 BYTE),
                    CUST_FST_NAME	VARCHAR2(200 BYTE),
                    CUST_LST_NAME	VARCHAR2(200 BYTE),
                    CUST_CLE	VARCHAR2(200 BYTE),
                    CUST_PRDT	VARCHAR2(200 BYTE),
                    DOR	VARCHAR2(200 BYTE),
                    TOR	VARCHAR2(200 BYTE),
                    CUST_KYC_STAT	VARCHAR2(200 BYTE),
                    CUST_ENT_ID	VARCHAR2(200 BYTE),
                    CUST_ACC	VARCHAR2(200 BYTE),
                    CUST_TXN_AMNT	NUMBER(18,4),
                    CHRG	VARCHAR2(200 BYTE),
                    SRC_TAX	VARCHAR2(200 BYTE),
                    EDU_CESS	VARCHAR2(200 BYTE),
                    SHEDUCESS	VARCHAR2(200 BYTE),
                    COMISION	VARCHAR2(200 BYTE),
                    TDS	VARCHAR2(200 BYTE),
                    NET_COMISION	VARCHAR2(200 BYTE),
                    IFSCCODE	VARCHAR2(200 BYTE),
                    BEN_ACC	VARCHAR2(200 BYTE),
                    BILL_PAY	VARCHAR2(200 BYTE),
                    BILL_CODE	VARCHAR2(200 BYTE),
                    BILLERDESC	VARCHAR2(200 BYTE),
                    BILLERFILED	VARCHAR2(200 BYTE),
                    TXN_STAT	VARCHAR2(200 BYTE),
                    ORG_TXN_ID	VARCHAR2(200 BYTE),
                    ORG_TXN_DT	VARCHAR2(200 BYTE),
                    ORG_TXN_TIME	VARCHAR2(200 BYTE),
                    BPARTY_NUMBER	VARCHAR2(200 BYTE),
                    BPARTYCIRCLE	VARCHAR2(200 BYTE),
                    UTR_RRN_NUMBER	VARCHAR2(200 BYTE),
                    FINAL_STAT	VARCHAR2(200 BYTE),
                    TELES_ID	VARCHAR2(200 BYTE),
                    TRANS_FLAG	VARCHAR2(200 BYTE)
            )
            organization external (
              type oracle_loader
              default directory WES_DIR  
              access parameters (
              RECORDS DELIMITED BY NEWLINE 
              SKIP 1
              fields terminated by ''|'' )
              location (''ctr.csv'')
            )
            parallel
            reject limit unlimited';
            
          EXECUTE IMMEDIATE v_query;
          
          v_query3 := 'INSERT INTO brs_imps(TXN_TYPE
                    ,TXN_ID
                    ,TXN_DT
                    ,TXN_TIME
                    ,Channel
                    ,CUST_MOB
                    ,CUST_FST_NAME
                    ,CUST_LST_NAME
                    ,CUST_CLE
                    ,CUST_PRDT
                    ,DOR
                    ,TOR
                    ,CUST_KYC_STAT
                    ,CUST_ENT_ID
                    ,CUST_ACC
                    ,CUST_TXN_AMNT
                    ,CHRG
                    ,SRC_TAX
                    ,EDU_CESS
                    ,SHEduCess
                    ,COMISION
                    ,TDS
                    ,Net_Comision
                    ,IFSCCode
                    ,BEN_ACC
                    ,BILL_PAY
                    ,BILL_CODE
                    ,BillerDesc
                    ,BillerFiled
                    ,TXN_STAT
                    ,ORG_TXN_ID
                    ,ORG_TXN_DT
                    ,ORG_TXN_TIME
                    ,BParty_Number
                    ,BPartyCircle
                    ,UTR_RRN_Number
                    ,FINAL_Stat
                    ,Teles_ID
                    ,TRANS_Flag
              ) select TXN_TYPE,
                              TXN_ID,
                              TXN_DT,
                              TXN_TIME,
                              CHANNEL,
                              CUST_MOB,
                              CUST_FST_NAME,
                              CUST_LST_NAME,
                              CUST_CLE,
                              CUST_PRDT,
                              DOR,
                              TOR,
                              CUST_KYC_STAT,
                              CUST_ENT_ID,
                              CUST_ACC,
                              CUST_TXN_AMNT,
                              CHRG,
                              SRC_TAX,
                              EDU_CESS,
                              SHEDUCESS,
                              COMISION,
                              TDS,
                              NET_COMISION,
                              IFSCCODE,
                              BEN_ACC,
                              BILL_PAY,
                              BILL_CODE,
                              BILLERDESC,
                              BILLERFILED,
                              TXN_STAT,
                              ORG_TXN_ID,
                              ORG_TXN_DT,
                              ORG_TXN_TIME,
                              BPARTY_NUMBER,
                              BPARTYCIRCLE,
                              UTR_RRN_NUMBER,
                              FINAL_STAT,
                              TELES_ID,
                              TRANS_FLAG FROM brs_imps_load_ext';
                              
          EXECUTE IMMEDIATE v_query3;
          COMMIT;
          v_query2 := 'DROP TABLE brs_imps_load_ext';
          
        INSERT INTO daily_log (
                    dl_file_id,
                    dl_file_name,
                    dl_transaction_dt,
                    dl_upld_dt_time,
                    dl_upld_count,
                    dl_rej_count,
                    dl_status,
                    dl_remarks,
                    dl_created_by,
                    dl_created_dt,
                    dl_updated_by,
                    dl_updated_dt
               ) SELECT '003',
                      'IMPS',
                      SYSDATE,
                      current_timestamp,
                      COUNT(1),
                      '0',
                      'Loaded',
                      'NA',
                      'Admin',
                      current_timestamp,
                      'Admin',
                      current_timestamp
               FROM brs_imps; commit;
               
          
          EXECUTE IMMEDIATE v_query2;
          msg := 'Imps File Loaded Successfully';
     END IF;

EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to Load Imps_File';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_KOTAK_NEFT_LOAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_KOTAK_NEFT_LOAD" ( msg   OUT VARCHAR2 ) AS

     v_query      VARCHAR2(10000);
     v_query2     VARCHAR2(4000);
     v_query3     VARCHAR2(10000);
     file_count   NUMBER := 0;
BEGIN
     SELECT COUNT(1) INTO file_count
     FROM daily_log
     WHERE dl_file_id = '007';

     IF file_count > 0 THEN
          msg := 'File Already Loaded';
     ELSE
     
         v_query := 'create table brs_kotak_neft_ext (
                    TXN_ID Varchar2(100) NULL,
                    TXN_DATE_TIME Varchar2(100) NULL,
                    SER_NAME Varchar2(100) NULL,
                    MODE_typ Varchar2(100) NULL,
                    ENT_TYP Varchar2(100) NULL,
                    MOB Varchar2(100) NULL,
                    ENT_NAME Varchar2(100) NULL,
                    CIRCLE_NAME Varchar2(100) NULL,
                    BEN_NAME Varchar2(100) NULL,
                    BEN_MOB Varchar2(100) NULL,
                    BEN_ACC_NO Varchar2(100) NULL,
                    BEN_IFSC Varchar2(100) NULL,
                    UTR_RRN Varchar2(100) NULL,
                    TXN_VAL Number(18, 4) NULL,
                    TXN_STATUS Varchar2(100) NULL,
                    TXN_FAIL_REASON Varchar2(100) NULL,
                    TXN_FAIL_CODE Varchar2(100) NULL,
                    TXN_FAIL_BANK_REASON Varchar2(100) NULL,
                    TXN_FAIL_BANK_CODE Varchar2(100) NULL,
                    CHANNEL Varchar2(100) NULL,
                    REV_TXN_ID Varchar2(100) NULL,
                    TEL_ID Varchar2(100) NULL
            )
            organization external (
              type oracle_loader
              default directory WES_DIR  
              access parameters (
              RECORDS DELIMITED BY NEWLINE 
              SKIP 1
              fields terminated by ''|'' )
              location (''kotak_neft.csv'')
            )
            parallel
            reject limit unlimited';
            
          EXECUTE IMMEDIATE v_query;
          
          v_query2 := 'INSERT INTO brs_kotak_neft_main(TXN_ID,
                                   TXN_DATE_TIME,
                                   SER_NAME,
                                   MODE_TYP,
                                   ENT_TYP,
                                   MOB,
                                   ENT_NAME,
                                   CIRCLE_NAME,
                                   BEN_NAME,
                                   BEN_MOB,
                                   BEN_ACC_NO,
                                   BEN_IFSC,
                                   UTR_RRN,
                                   TXN_VAL,
                                   TXN_STATUS,
                                   TXN_FAIL_REASON,
                                   TXN_FAIL_CODE,
                                   TXN_FAIL_BANK_REASON,
                                   TXN_FAIL_BANK_CODE,
                                   CHANNEL,
                                   REV_TXN_ID,
                                   TEL_ID)
                                   SELECT TXN_ID,
                                   TXN_DATE_TIME,
                                   SER_NAME,
                                   MODE_TYP,
                                   ENT_TYP,
                                   MOB,
                                   ENT_NAME,
                                   CIRCLE_NAME,
                                   BEN_NAME,
                                   BEN_MOB,
                                   BEN_ACC_NO,
                                   BEN_IFSC,
                                   UTR_RRN,
                                   TXN_VAL,
                                   TXN_STATUS,
                                   TXN_FAIL_REASON,
                                   TXN_FAIL_CODE,
                                   TXN_FAIL_BANK_REASON,
                                   TXN_FAIL_BANK_CODE,
                                   CHANNEL,
                                   REV_TXN_ID,
                                   TEL_ID
                                   FROM brs_kotak_neft_ext';
                              
          EXECUTE IMMEDIATE v_query2;
          COMMIT;
          v_query3 := 'DROP TABLE brs_kotak_neft_ext';
          
        INSERT INTO daily_log (
                    dl_file_id,
                    dl_file_name,
                    dl_transaction_dt,
                    dl_upld_dt_time,
                    dl_upld_count,
                    dl_rej_count,
                    dl_status,
                    dl_remarks,
                    dl_created_by,
                    dl_created_dt,
                    dl_updated_by,
                    dl_updated_dt
               ) SELECT '007',
                      'KOTAKNEFT',
                      SYSDATE,
                      current_timestamp,
                      COUNT(1),
                      '0',
                      'Loaded',
                      'NA',
                      'Admin',
                      current_timestamp,
                      'Admin',
                      current_timestamp
               FROM brs_kotak_neft_main; commit;
               
               
          
          EXECUTE IMMEDIATE v_query3;
          msg := 'Kotakneft File Loaded Successfully';
     END IF;

EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to Load Kotakneft File';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_LOAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_LOAD" ( msg   OUT VARCHAR2 ) AS
    v_query    VARCHAR2(10000);
    v_query2   VARCHAR2(4000);
    v_query3   VARCHAR2(10000);
BEGIN
    v_query := 'create table brs_escrow_load_ext (
                ACC_NO	VARCHAR2(30 BYTE),
                ACC_NAME	VARCHAR2(100 BYTE),
                POST_DT	VARCHAR2(50 BYTE),
                VALUE_DT	VARCHAR2(50 BYTE),
                NARRATION	VARCHAR2(100 BYTE),
                REF_NO	VARCHAR2(100 BYTE),
                CHQ_NO	VARCHAR2(50 BYTE),
                DR_CR	VARCHAR2(50 BYTE),
                CURR_CODE	VARCHAR2(50 BYTE),
                TXN_AMNT	VARCHAR2(50 BYTE),
                RUN_BAL	VARCHAR2(50 BYTE)
        )
        organization external (
          type oracle_loader
          default directory WES_DIR  
          access parameters (fields terminated by ''|'' )
          location (''3611409323_DAY-1.txt'')
        )
        reject limit unlimited';

    EXECUTE IMMEDIATE v_query;
    v_query3 := 'INSERT INTO brs_escrow(acc_no,acc_name,post_dt,
        value_dt,
        narration,
        ref_no,
        chq_no,
        dr_cr,
        curr_code,
        txn_amnt,
        run_bal
    ) SELECT
        acc_no,
        acc_name,
        post_dt,
        value_dt,
        narration,
        ref_no,
        chq_no,
        dr_cr,
        curr_code,
        txn_amnt,
        run_bal
    FROM
        brs_escrow_load_ext';
        
    EXECUTE IMMEDIATE v_query3;
    COMMIT;
    v_query2 := 'DROP TABLE brs_escrow_load_ext';
    EXECUTE IMMEDIATE v_query2;
    msg := 'File Loaded Successfully';

    EXCEPTION 
    WHEN OTHERS THEN
    msg := 'Failed to Load File';
    raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);  
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_MERCHANT_LOAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_MERCHANT_LOAD" ( msg   OUT VARCHAR2 ) AS

     v_query      VARCHAR2(10000);
     v_query2     VARCHAR2(4000);
     v_query3     VARCHAR2(10000);
     file_count   NUMBER := 0;
BEGIN
     SELECT COUNT(1) INTO file_count
     FROM daily_log
     WHERE dl_file_id = '009';

     IF file_count > 0 THEN
          msg := 'File Already Loaded';
     ELSE
     
         v_query := 'create table brs_merchant_ext (
                    BM_ACCOUNT_NO Varchar2(100) NULL,
                    BM_ACCT_NAME Varchar2(100) NULL,
                    BM_POST_DATE varchar2(100) NULL,
                    BM_VALUE_DATE varchar2(100) NULL,
                    BM_NARRATION Varchar2(100) NULL,
                    BM_REFERENCE_NO Varchar2(100) NULL,
                    BM_CHEQUE_NO Varchar2(100) NULL,
                    BM_DR_CR Varchar2(100) NULL,
                    BM_CURRENCY_CODE Varchar2(100) NULL,
                    BM_TXN_AMOUNT Varchar2(100) null,
                    BM_RUNNING_BALANCE Varchar2(100) NULL
            )
            organization external (
              type oracle_loader
              default directory WES_DIR  
              access parameters (
               RECORDS DELIMITED BY NEWLINE 
               SKIP 1
              fields terminated by ''|''
               )
              location (''0411405210_DAY-1.txt'')
            )
            reject limit unlimited
            parallel';
            
          EXECUTE IMMEDIATE v_query;
          
          v_query2 := 'INSERT INTO brs_merchant_main_int(
                                   BM_ACCOUNT_NO,
                                   BM_ACCT_NAME,
                                   BM_POST_DATE,
                                   BM_VALUE_DATE,
                                   BM_NARRATION,
                                   BM_REFERENCE_NO,
                                   BM_CHEQUE_NO,
                                   BM_DR_CR,
                                   BM_CURRENCY_CODE,
                                   BM_TXN_AMOUNT,
                                   BM_RUNNING_BALANCE          
                                   )
                                   SELECT BM_ACCOUNT_NO,
                                   BM_ACCT_NAME,
                                   BM_POST_DATE,
                                   BM_VALUE_DATE,
                                   BM_NARRATION,
                                   BM_REFERENCE_NO,
                                   BM_CHEQUE_NO,
                                   BM_DR_CR,
                                   BM_CURRENCY_CODE,
                                   nvl(BM_TXN_AMOUNT,0),
                                   nvl(BM_RUNNING_BALANCE,0)
                                   FROM brs_merchant_ext';
                              
          EXECUTE IMMEDIATE v_query2;
          COMMIT;
          v_query3 := 'DROP TABLE brs_merchant_ext';
          
        INSERT INTO daily_log (
                    dl_file_id,
                    dl_file_name,
                    dl_transaction_dt,
                    dl_upld_dt_time,
                    dl_upld_count,
                    dl_rej_count,
                    dl_status,
                    dl_remarks,
                    dl_created_by,
                    dl_created_dt,
                    dl_updated_by,
                    dl_updated_dt
               ) SELECT '009',
                      'Merchant',
                      SYSDATE,
                      current_timestamp,
                      COUNT(1),
                      '0',
                      'Loaded',
                      'NA',
                      'Admin',
                      current_timestamp,
                      'Admin',
                      current_timestamp
               FROM brs_merchant_main_int; commit;
               
               
          
          EXECUTE IMMEDIATE v_query3;
          msg := 'Merchant File Loaded Successfully';
     END IF;

EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to Load Merchant File';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_MTR_LOAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_MTR_LOAD" (msg OUT VARCHAR2 )
AS
  v_query  VARCHAR2(10000);
  v_query2 VARCHAR2(4000);
  v_query3 VARCHAR2(10000);
  file_count   NUMBER := 0;
BEGIN
  SELECT COUNT(1) INTO file_count FROM daily_log WHERE dl_file_id = '006';
  IF file_count > 0 THEN
    msg        := 'File Already Loaded';
  ELSE
    v_query :=    'create table brs_mtr_imp_ext(                
                                MT_CUSTOMER_CIRCLE VARCHAR2(200 BYTE),  
                              	MT_MERCHANT_CIRCLE VARCHAR2(200 BYTE), 
                                MT_TXN_TYPE VARCHAR2(200 BYTE), 
                                MT_TXN_ID VARCHAR2(200 BYTE), 
                                MT_TXN_INITIATED_DATE VARCHAR2(200 BYTE), 
                                MT_TXN_INITIATED_TIME VARCHAR2(200 BYTE), 
                                MT_TXN_COMPLETED_DATE VARCHAR2(200 BYTE), 
                                MT_TXN_COMPLETED_TIME VARCHAR2(200 BYTE), 
                                MT_TXN_AMOUNT VARCHAR2(200 BYTE), 
                                MT_DEBIT_ENTITY_MSISDN VARCHAR2(200 BYTE), 
                                MT_DEBIT_ENTITY_NAME VARCHAR2(200 BYTE), 
                                MT_CREDIT_ENTITY_MSISDN VARCHAR2(200 BYTE), 
                                MT_CREDIT_ENTITY_NAME VARCHAR2(200 BYTE), 
                                MT_TXN_STATUS VARCHAR2(200 BYTE), 
                                MT_AGENT_MSISDN VARCHAR2(200 BYTE), 
                                MT_TXN_FAILURE_REASON VARCHAR2(200 BYTE), 
                                MT_SETTELEMENT_DUE_DATE VARCHAR2(200 BYTE), 
                                MT_SETTELEMENT_DUE_TIME VARCHAR2(200 BYTE), 
                                MT_MERCHANT_FEE VARCHAR2(200 BYTE), 
                                MT_SETTELEMENT_DATE VARCHAR2(200 BYTE), 
                                MT_SETTELEMENT_TIME VARCHAR2(200 BYTE), 
                                MT_SETTELEMENT_TYPE VARCHAR2(200 BYTE), 
                                MT_FILLER_1 VARCHAR2(200 BYTE), 
                                MT_FILLER_2 VARCHAR2(200 BYTE), 
                                MT_FILLER_3 VARCHAR2(200 BYTE), 
                                MT_FILLER_4 VARCHAR2(200 BYTE), 
                                MT_FILLER_5 VARCHAR2(200 BYTE), 
                                MT_NARRATION VARCHAR2(200 BYTE), 
                                MT_MERCHANT_TXN_REF_NUMBER VARCHAR2(200 BYTE), 
                                MT_REVERSAL_FLAG VARCHAR2(200 BYTE), 
                                MT_KYC_TYPE VARCHAR2(200 BYTE), 
                                MT_SETTELEMENT_MODE VARCHAR2(200 BYTE)        
)        
organization external (          
type oracle_loader          
default directory WES_DIR            
access parameters (          
RECORDS DELIMITED BY NEWLINE           
fields terminated by ''|'' ) 
location (''mtr.csv'')        
)        
parallel        
reject limit unlimited';

    EXECUTE IMMEDIATE v_query;
    v_query2 :='INSERT INTO brs_load_mtr_main_int(MT_CUSTOMER_CIRCLE,                                              
MT_MERCHANT_CIRCLE,                                              
MT_TXN_TYPE,                                              
MT_TXN_ID,                                              
MT_TXN_INITIATED_DATE, 
MT_TXN_INITIATED_TIME,
MT_TXN_COMPLETED_DATE,
MT_TXN_COMPLETED_TIME,
MT_TXN_AMOUNT,
MT_DEBIT_ENTITY_MSISDN,
MT_DEBIT_ENTITY_NAME,
MT_CREDIT_ENTITY_MSISDN,
MT_CREDIT_ENTITY_NAME,
MT_TXN_STATUS,
MT_AGENT_MSISDN,
MT_TXN_FAILURE_REASON,
MT_SETTELEMENT_DUE_DATE,
MT_SETTELEMENT_DUE_TIME,
MT_MERCHANT_FEE,
MT_SETTELEMENT_DATE,
MT_SETTELEMENT_TIME,
MT_SETTELEMENT_TYPE,
MT_FILLER_1,
MT_FILLER_2,
MT_FILLER_3,
MT_FILLER_4,
MT_FILLER_5,
MT_NARRATION,
MT_MERCHANT_TXN_REF_NUMBER,
MT_REVERSAL_FLAG,
MT_KYC_TYPE,
MT_SETTELEMENT_MODE
) SELECT                                             
MT_CUSTOMER_CIRCLE,                                              
MT_MERCHANT_CIRCLE,                                              
MT_TXN_TYPE,                                              
MT_TXN_ID,                                              
MT_TXN_INITIATED_DATE, 
MT_TXN_INITIATED_TIME,
MT_TXN_COMPLETED_DATE,
MT_TXN_COMPLETED_TIME,
MT_TXN_AMOUNT,
MT_DEBIT_ENTITY_MSISDN,
MT_DEBIT_ENTITY_NAME,
MT_CREDIT_ENTITY_MSISDN,
MT_CREDIT_ENTITY_NAME,
MT_TXN_STATUS,
MT_AGENT_MSISDN,
MT_TXN_FAILURE_REASON,
MT_SETTELEMENT_DUE_DATE,
MT_SETTELEMENT_DUE_TIME,
MT_MERCHANT_FEE,
MT_SETTELEMENT_DATE,
MT_SETTELEMENT_TIME,
MT_SETTELEMENT_TYPE,
MT_FILLER_1,
MT_FILLER_2,
MT_FILLER_3,
MT_FILLER_4,
MT_FILLER_5,
MT_NARRATION,
MT_MERCHANT_TXN_REF_NUMBER,
MT_REVERSAL_FLAG,
MT_KYC_TYPE,
MT_SETTELEMENT_MODE FROM brs_mtr_imp_ext';
    EXECUTE IMMEDIATE v_query2;
    COMMIT;
    v_query3 := 'DROP TABLE brs_mtr_imp_ext';
    
    INSERT INTO daily_log (
                    dl_file_id,
                    dl_file_name,
                    dl_transaction_dt,
                    dl_upld_dt_time,
                    dl_upld_count,
                    dl_rej_count,
                    dl_status,
                    dl_remarks,
                    dl_created_by,
                    dl_created_dt,
                    dl_updated_by,
                    dl_updated_dt
               ) SELECT '006',
                      'MTR',
                      SYSDATE,
                      current_timestamp,
                      COUNT(1),
                      '0',
                      'Loaded',
                      'NA',
                      'Admin',
                      current_timestamp,
                      'Admin',
                      current_timestamp
               FROM brs_load_mtr_main_int;
               
               commit;
                       
        EXECUTE IMMEDIATE v_query3;
    msg := 'File Loaded Successfully';
  END IF;
EXCEPTION
WHEN OTHERS THEN
  msg := 'Failed to Load MTR_File';
  raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||sqlerrm);
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_NEFTRETURN_LOAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_NEFTRETURN_LOAD" ( msg   OUT VARCHAR2 ) AS

     v_query      VARCHAR2(10000);
     v_query2     VARCHAR2(4000);
     v_query3     VARCHAR2(10000);
     file_count   NUMBER := 0;
BEGIN
     SELECT COUNT(1) INTO file_count
     FROM daily_log
     WHERE dl_file_id = '010';

     IF file_count > 0 THEN
          msg := 'File Already Loaded';
     ELSE
     
         v_query := 'create table neft_returns_temp_ext(
                    NTR_ACCNO Varchar2(100) NULL,
                    NTR_ACC_NAME Varchar2(500) NULL,
                    NTR_POST_DATE Varchar2(100) NULL,
                    NTR_VALUE_DATE Varchar2(100) NULL,
                    NTR_NARRATION Varchar2(500) NULL,
                    NTR_CHQ_NO Varchar2(100) NULL,
                    NTR_DR_CR Varchar2(100) NULL,
                    NTR_CUR Varchar2(100) NULL,
                    NTR_TXN_AMT Varchar2(100) NULL,
                    NTR_UTR Varchar2(100) NULL,
                    NTR_TRAN_ID Varchar2(100) NULL
            )
            organization external (
              type oracle_loader
              default directory WES_DIR  
              access parameters (
               RECORDS DELIMITED BY NEWLINE 
               SKIP 1
              fields terminated by ''|''
               )
              location (''neft.csv'')
            )
            reject limit unlimited
            parallel';
            
          EXECUTE IMMEDIATE v_query;
          
          v_query2 := 'INSERT INTO neft_returns_Main_int(
                                   NTR_ACCNO,
                                   NTR_ACC_NAME,
                                   NTR_POST_DATE,
                                   NTR_VALUE_DATE,
                                   NTR_NARRATION,
                                   NTR_CHQ_NO,
                                   NTR_DR_CR,
                                   NTR_CUR,
                                   NTR_TXN_AMT,
                                   NTR_UTR,
                                   NTR_TRAN_ID       
                                   )
                                   SELECT  NTR_ACCNO,
                                   NTR_ACC_NAME,
                                   NTR_POST_DATE,
                                   NTR_VALUE_DATE,
                                   NTR_NARRATION,
                                   NTR_CHQ_NO,
                                   NTR_DR_CR,
                                   NTR_CUR,
                                   NTR_TXN_AMT,
                                   NTR_UTR,
                                   NTR_TRAN_ID  
                                   FROM neft_returns_temp_ext';
                              
          EXECUTE IMMEDIATE v_query2;
          COMMIT;
          v_query3 := 'DROP TABLE neft_returns_temp_ext';
          
        INSERT INTO daily_log (
                    dl_file_id,
                    dl_file_name,
                    dl_transaction_dt,
                    dl_upld_dt_time,
                    dl_upld_count,
                    dl_rej_count,
                    dl_status,
                    dl_remarks,
                    dl_created_by,
                    dl_created_dt,
                    dl_updated_by,
                    dl_updated_dt
               ) SELECT '010',
                      'NeftReturn',
                      SYSDATE,
                      current_timestamp,
                      COUNT(1),
                      '0',
                      'Loaded',
                      'NA',
                      'Admin',
                      current_timestamp,
                      'Admin',
                      current_timestamp
               FROM neft_returns_Main_int; commit;
               
          
          EXECUTE IMMEDIATE v_query3;
          msg := 'File Loaded Successfully';
     END IF;

EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to NeftReturn File';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_OD_LOAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_OD_LOAD" ( msg OUT VARCHAR2 ) AS

     v_query      VARCHAR2(10000);
     v_query2     VARCHAR2(4000);
     v_query3     VARCHAR2(10000);
     file_count   NUMBER := 0;
     
BEGIN
     SELECT COUNT(1) INTO file_count
     FROM daily_log
     WHERE dl_file_id = '011';

     IF file_count > 0 THEN
          msg := 'File Already Loaded';
     ELSE
     
         v_query := 'create table brs_od_ext(
                         BO_ACCOUNT_NO Varchar2(100) NULL,
                         BO_ACCT_NAME Varchar2(100) NULL,
                         BO_POST_DATE Varchar2(100) NULL,
                         BO_VALUE_DATE Varchar2(100) NULL,
                         BO_NARRATION Varchar2(100) NULL,
                         BO_REFERENCE_NO Varchar2(100) NULL,
                         BO_CHEQUE_NO Varchar2(100) NULL,
                         BO_DR_CR Varchar2(100) NULL,
                         BO_CURRENCY_CODE Varchar2(100) NULL,
                         BO_TXN_AMOUNT Varchar2(100) NULL,
                         BO_RUNNING_BALANCE Varchar2(100) NULL
            )
            organization external (
              type oracle_loader
              default directory WES_DIR  
              access parameters (
               RECORDS DELIMITED BY NEWLINE 
               SKIP 1
              fields terminated by ''|''
               )
              location (''0411614506_DAY-1.txt'')  
            )
            reject limit unlimited
            parallel';
            
          EXECUTE IMMEDIATE v_query;
          
          v_query2 := 'INSERT INTO brs_od_main_int(
                                        BO_ACCOUNT_NO,
                                        BO_ACCT_NAME,
                                        BO_POST_DATE,
                                        BO_VALUE_DATE,
                                        BO_NARRATION,
                                        BO_REFERENCE_NO,
                                        BO_CHEQUE_NO,
                                        BO_DR_CR,
                                        BO_CURRENCY_CODE,
                                        BO_TXN_AMOUNT,
                                        BO_RUNNING_BALANCE  
                                       )
                                   SELECT BO_ACCOUNT_NO,
                                        BO_ACCT_NAME,
                                        BO_POST_DATE,
                                        BO_VALUE_DATE,
                                        BO_NARRATION,
                                        BO_REFERENCE_NO,
                                        BO_CHEQUE_NO,
                                        BO_DR_CR,
                                        BO_CURRENCY_CODE,
                                        BO_TXN_AMOUNT,
                                        BO_RUNNING_BALANCE  
                                   FROM brs_od_ext';
                              
          EXECUTE IMMEDIATE v_query2;
          COMMIT;
          v_query3 := 'DROP TABLE brs_od_ext';
          
        INSERT INTO daily_log (
                    dl_file_id,
                    dl_file_name,
                    dl_transaction_dt,
                    dl_upld_dt_time,
                    dl_upld_count,
                    dl_rej_count,
                    dl_status,
                    dl_remarks,
                    dl_created_by,
                    dl_created_dt,
                    dl_updated_by,
                    dl_updated_dt
               ) SELECT '011',
                      'OD',
                      SYSDATE,
                      current_timestamp,
                      COUNT(1),
                      '0',
                      'Loaded',
                      'NA',
                      'Admin',
                      current_timestamp,
                      'Admin',
                      current_timestamp
               FROM brs_od_main_int; commit;
               
          
          EXECUTE IMMEDIATE v_query3;
          msg := 'File Loaded Successfully';
     END IF;

EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to load OD File';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_RTR_LOAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_RTR_LOAD" ( msg   OUT VARCHAR2 ) AS

     v_query      VARCHAR2(10000);
     v_query2     VARCHAR2(4000);
     v_query3     VARCHAR2(10000);
     file_count   NUMBER := 0;
BEGIN
     SELECT COUNT(1)
     INTO
          file_count
     FROM daily_log
     WHERE dl_file_id = '00500';

     IF
          file_count > 0
     THEN
          msg := 'File Already Loaded';
     ELSE
          v_query := 'create table recon_rtr_imp_ext(
               TXN_TYPE varchar2(200),
               TXN_ID varchar2(200),
               TXN_DATE varchar2(200),
               TXN_TIME varchar2(200),
               TXN_CHANNEL varchar2(200),
               RTR_MOB_NO varchar2(200),
               RTR_FST_NAME varchar2(200),
               RTR_LST_NAME varchar2(200),
               RTR_CIRCLE varchar2(200),
               RTR_POA_TYP varchar2(200),
               RTR_POA_NO varchar2(200),
               RTR_CMMSN_BAND varchar2(200),
               DTR_MOB_NO varchar2(200),
               DTR_NAME varchar2(200),
               DTR_REG_DATE varchar2(200),
               TXN_ENTITY_ID varchar2(200),
               RTR_ACCT_NO varchar2(200),
               TXN_AMT varchar2(200),
               TXN_CHRG varchar2(200),
               TXN_SER_TAX varchar2(200),
               TXN_EDU_TAX varchar2(200),
               TXN_HEDU_TAX varchar2(200),
               RTR_COMISN varchar2(200),
               RTR_TDS varchar2(200),
               DTR_COMISN varchar2(200),
               DTR_TDS varchar2(200),
               TOTAL_COMISN varchar2(200),
               IFSS_CODE varchar2(200),
               BENI_AC_NO varchar2(200),
               BILLPAY_REF varchar2(200),
               TXN_STATUS varchar2(200),
               ORIG_TXN_ID varchar2(200),
               ORIG_TXN_DATE varchar2(200),
               ORIG_TXN_TIME varchar2(200),
               TXN_COMISN_ID varchar2(200),
               COMISN_TXN_TYPE varchar2(200),
               SHOP_NAME varchar2(200),
               FIRM_NAME varchar2(200),
               CUST_NAME varchar2(200),
               CUST_MOB_NO varchar2(200)               
                )
             organization external (
               type oracle_loader
               default directory WES_DIR  
               access parameters (
               RECORDS DELIMITED BY NEWLINE 
               SKIP 1
               fields terminated by ''|'' )
               location (''rtr.csv'')
             )
             parallel
             reject limit unlimited';
             
          EXECUTE IMMEDIATE v_query;
          v_query2 := 'INSERT INTO recon_rtr_main (
                              txn_type,
                              txn_id,
                              txn_date,
                              txn_time,
                              txn_channel,
                              rtr_mob_no,
                              rtr_fst_name,
                              rtr_lst_name,
                              rtr_circle,
                              rtr_poa_typ,
                              rtr_poa_no,
                              rtr_cmmsn_band,
                              dtr_mob_no,
                              dtr_name,
                              dtr_reg_date,
                              txn_entity_id,
                              rtr_acct_no,
                              txn_amt,
                              txn_chrg,
                              txn_ser_tax,
                              txn_edu_tax,
                              txn_hedu_tax,
                              rtr_comisn,
                              rtr_tds,
                              dtr_comisn,
                              dtr_tds,
                              total_comisn,
                              ifss_code,
                              beni_ac_no,
                              billpay_ref,
                              txn_status,
                              orig_txn_id,
                              orig_txn_date,
                              orig_txn_time,
                              txn_comisn_id,
                              comisn_txn_type,
                              shop_name,
                              firm_name,
                              cust_name,
                              cust_mob_no
                              ) SELECT txn_type,
                                txn_id,
                                to_date(txn_date,''DD/MM/YYYY''),
                                txn_time,
                                txn_channel,
                                rtr_mob_no,
                                rtr_fst_name,
                                rtr_lst_name,
                                rtr_circle,
                                rtr_poa_typ,
                                rtr_poa_no,
                                rtr_cmmsn_band,
                                dtr_mob_no,
                                dtr_name,
                                to_date(dtr_reg_date,''DD/MM/YYYY''),
                                txn_entity_id,
                                rtr_acct_no,
                                txn_amt,
                                decode(txn_chrg,''-'', ''0'',txn_chrg),
                                decode(txn_ser_tax,''-'', ''0'',txn_ser_tax),
                                txn_edu_tax,
                                txn_hedu_tax,
                                rtr_comisn,
                                rtr_tds,
                                dtr_comisn,
                                dtr_tds,
                                total_comisn,
                                ifss_code,
                                beni_ac_no,
                                billpay_ref,
                                txn_status,
                                orig_txn_id,
                                orig_txn_date,
                                orig_txn_time,
                                txn_comisn_id,
                                comisn_txn_type,
                                shop_name,
                                firm_name,
                                cust_name,
                                cust_mob_no
                              FROM recon_rtr_imp_ext';
          EXECUTE IMMEDIATE v_query2;
          COMMIT;
          v_query3 := 'DROP TABLE recon_rtr_imp_ext';
          INSERT INTO daily_log (
               dl_file_id,
               dl_file_name,
               dl_transaction_dt,
               dl_upld_dt_time,
               dl_upld_count,
               dl_rej_count,
               dl_status,
               dl_remarks,
               dl_created_by,
               dl_created_dt,
               dl_updated_by,
               dl_updated_dt
          ) SELECT '005',
                 'RTR',
                 SYSDATE,
                 current_timestamp,
                 COUNT(1),
                 '0',
                 'Loaded',
                 'NA',
                 'Admin',
                 current_timestamp,
                 'Admin',
                 current_timestamp
          FROM recon_load_rtr_main_int;

          COMMIT;
          EXECUTE IMMEDIATE v_query3;
          msg := 'File Loaded Successfully';
     END IF;

EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to Load RTR_File';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_RTR_LOAD_OLD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_RTR_LOAD_OLD" ( msg   OUT VARCHAR2 ) AS

     v_query      VARCHAR2(10000);
     v_query2     VARCHAR2(4000);
     v_query3     VARCHAR2(10000);
     file_count   NUMBER := 0;
BEGIN
     SELECT COUNT(1)
     INTO
          file_count
     FROM daily_log
     WHERE dl_file_id = '005';

     IF
          file_count > 0
     THEN
          msg := 'File Already Loaded';
     ELSE
          v_query := 'create table recon_rtr_imp_ext(
               TXN_TYPE varchar2(200),
               TXN_ID varchar2(200),
               TXN_DATE varchar2(200),
               TXN_TIME varchar2(200),
               TXN_CHANNEL varchar2(200),
               RTR_MOB_NO varchar2(200),
               RTR_FST_NAME varchar2(200),
               RTR_LST_NAME varchar2(200),
               RTR_CIRCLE varchar2(200),
               RTR_POA_TYP varchar2(200),
               RTR_POA_NO varchar2(200),
               RTR_CMMSN_BAND varchar2(200),
               DTR_MOB_NO varchar2(200),
               DTR_NAME varchar2(200),
               DTR_REG_DATE varchar2(200),
               TXN_ENTITY_ID varchar2(200),
               RTR_ACCT_NO varchar2(200),
               TXN_AMT varchar2(200),
               TXN_CHRG varchar2(200),
               TXN_SER_TAX varchar2(200),
               TXN_EDU_TAX varchar2(200),
               TXN_HEDU_TAX varchar2(200),
               RTR_COMISN varchar2(200),
               RTR_TDS varchar2(200),
               DTR_COMISN varchar2(200),
               DTR_TDS varchar2(200),
               TOTAL_COMISN varchar2(200),
               IFSS_CODE varchar2(200),
               BENI_AC_NO varchar2(200),
               BILLPAY_REF varchar2(200),
               TXN_STATUS varchar2(200),
               ORIG_TXN_ID varchar2(200),
               ORIG_TXN_DATE varchar2(200),
               ORIG_TXN_TIME varchar2(200),
               TXN_COMISN_ID varchar2(200),
               COMISN_TXN_TYPE varchar2(200),
               SHOP_NAME varchar2(200),
               FIRM_NAME varchar2(200),
               CUST_NAME varchar2(200),
               CUST_MOB_NO varchar2(200)               
                )
             organization external (
               type oracle_loader
               default directory WES_DIR  
               access parameters (
               RECORDS DELIMITED BY NEWLINE 
               SKIP 1
               fields terminated by ''|'' )
               location (''rtr.csv'')
             )
             parallel
             reject limit unlimited';
             
          EXECUTE IMMEDIATE v_query;
          v_query2 := 'INSERT INTO recon_rtr_main (
                              txn_type,
                              txn_id,
                              txn_date,
                              txn_time,
                              txn_channel,
                              rtr_mob_no,
                              rtr_fst_name,
                              rtr_lst_name,
                              rtr_circle,
                              rtr_poa_typ,
                              rtr_poa_no,
                              rtr_cmmsn_band,
                              dtr_mob_no,
                              dtr_name,
                              dtr_reg_date,
                              txn_entity_id,
                              rtr_acct_no,
                              txn_amt,
                              txn_chrg,
                              txn_ser_tax,
                              txn_edu_tax,
                              txn_hedu_tax,
                              rtr_comisn,
                              rtr_tds,
                              dtr_comisn,
                              dtr_tds,
                              total_comisn,
                              ifss_code,
                              beni_ac_no,
                              billpay_ref,
                              txn_status,
                              orig_txn_id,
                              orig_txn_date,
                              orig_txn_time,
                              txn_comisn_id,
                              comisn_txn_type,
                              shop_name,
                              firm_name,
                              cust_name,
                              cust_mob_no
                              ) SELECT txn_type,
                                txn_id,
                                txn_date,
                                txn_time,
                                txn_channel,
                                rtr_mob_no,
                                rtr_fst_name,
                                rtr_lst_name,
                                rtr_circle,
                                rtr_poa_typ,
                                rtr_poa_no,
                                rtr_cmmsn_band,
                                dtr_mob_no,
                                dtr_name,
                                dtr_reg_date,
                                txn_entity_id,
                                rtr_acct_no,
                                txn_amt,
                                txn_chrg,
                                txn_ser_tax,
                                txn_edu_tax,
                                txn_hedu_tax,
                                rtr_comisn,
                                rtr_tds,
                                dtr_comisn,
                                dtr_tds,
                                total_comisn,
                                ifss_code,
                                beni_ac_no,
                                billpay_ref,
                                txn_status,
                                orig_txn_id,
                                orig_txn_date,
                                orig_txn_time,
                                txn_comisn_id,
                                comisn_txn_type,
                                shop_name,
                                firm_name,
                                cust_name,
                                cust_mob_no
                              FROM recon_rtr_imp_ext';
          EXECUTE IMMEDIATE v_query2;
          COMMIT;
          v_query3 := 'DROP TABLE recon_rtr_imp_ext';
          INSERT INTO daily_log (
               dl_file_id,
               dl_file_name,
               dl_transaction_dt,
               dl_upld_dt_time,
               dl_upld_count,
               dl_rej_count,
               dl_status,
               dl_remarks,
               dl_created_by,
               dl_created_dt,
               dl_updated_by,
               dl_updated_dt
          ) SELECT '005',
                 'RTR',
                 SYSDATE,
                 current_timestamp,
                 COUNT(1),
                 '0',
                 'Loaded',
                 'NA',
                 'Admin',
                 current_timestamp,
                 'Admin',
                 current_timestamp
          FROM recon_load_rtr_main_int;

          COMMIT;
          EXECUTE IMMEDIATE v_query3;
          msg := 'File Loaded Successfully';
     END IF;

EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to Load RTR_File';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_RTR_LOAD_TEST
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_RTR_LOAD_TEST" ( msg   OUT VARCHAR2 ) AS

     v_query      VARCHAR2(10000);
     v_query2     VARCHAR2(4000);
     v_query3     VARCHAR2(10000);
     file_count   NUMBER := 0;
BEGIN
     SELECT COUNT(1)
     INTO
          file_count
     FROM daily_log
     WHERE dl_file_id = '005';

     IF
          file_count > 0
     THEN
          msg := 'File Already Loaded';
     ELSE
          v_query := 'create table recon_rtr_imp_ext(
               TXN_TYPE varchar2(200),
               TXN_ID varchar2(200),
               TXN_DATE varchar2(200),
               TXN_TIME varchar2(200),
               TXN_CHANNEL varchar2(200),
               RTR_MOB_NO varchar2(200),
               RTR_FST_NAME varchar2(200),
               RTR_LST_NAME varchar2(200),
               RTR_CIRCLE varchar2(200),
               RTR_POA_TYP varchar2(200),
               RTR_POA_NO varchar2(200),
               RTR_CMMSN_BAND varchar2(200),
               DTR_MOB_NO varchar2(200),
               DTR_NAME varchar2(200),
               DTR_REG_DATE varchar2(200),
               TXN_ENTITY_ID varchar2(200),
               RTR_ACCT_NO varchar2(200),
               TXN_AMT varchar2(200),
               TXN_CHRG varchar2(200),
               TXN_SER_TAX varchar2(200),
               TXN_EDU_TAX varchar2(200),
               TXN_HEDU_TAX varchar2(200),
               RTR_COMISN varchar2(200),
               RTR_TDS varchar2(200),
               DTR_COMISN varchar2(200),
               DTR_TDS varchar2(200),
               TOTAL_COMISN varchar2(200),
               IFSS_CODE varchar2(200),
               BENI_AC_NO varchar2(200),
               BILLPAY_REF varchar2(200),
               TXN_STATUS varchar2(200),
               ORIG_TXN_ID varchar2(200),
               ORIG_TXN_DATE varchar2(200),
               ORIG_TXN_TIME varchar2(200),
               TXN_COMISN_ID varchar2(200),
               COMISN_TXN_TYPE varchar2(200),
               SHOP_NAME varchar2(200),
               FIRM_NAME varchar2(200),
               CUST_NAME varchar2(200),
               CUST_MOB_NO varchar2(200)               
                )
             organization external (
               type oracle_loader
               default directory WES_DIR  
               access parameters (
               RECORDS DELIMITED BY NEWLINE 
               SKIP 1
               fields terminated by ''|'' )
               location (''rtr.csv'')
             )
             parallel
             reject limit unlimited';
             
          EXECUTE IMMEDIATE v_query;
          v_query2 := 'INSERT INTO recon_rtr_main (
                              txn_type,
                              txn_id,
                              txn_date,
                              txn_time,
                              txn_channel,
                              rtr_mob_no,
                              rtr_fst_name,
                              rtr_lst_name,
                              rtr_circle,
                              rtr_poa_typ,
                              rtr_poa_no,
                              rtr_cmmsn_band,
                              dtr_mob_no,
                              dtr_name,
                              dtr_reg_date,
                              txn_entity_id,
                              rtr_acct_no,
                              txn_amt,
                              txn_chrg,
                              txn_ser_tax,
                              txn_edu_tax,
                              txn_hedu_tax,
                              rtr_comisn,
                              rtr_tds,
                              dtr_comisn,
                              dtr_tds,
                              total_comisn,
                              ifss_code,
                              beni_ac_no,
                              billpay_ref,
                              txn_status,
                              orig_txn_id,
                              orig_txn_date,
                              orig_txn_time,
                              txn_comisn_id,
                              comisn_txn_type,
                              shop_name,
                              firm_name,
                              cust_name,
                              cust_mob_no
                              ) SELECT txn_type,
                                txn_id,
                                txn_date,
                                txn_time,
                                txn_channel,
                                rtr_mob_no,
                                rtr_fst_name,
                                rtr_lst_name,
                                rtr_circle,
                                rtr_poa_typ,
                                rtr_poa_no,
                                rtr_cmmsn_band,
                                dtr_mob_no,
                                dtr_name,
                                dtr_reg_date,
                                txn_entity_id,
                                rtr_acct_no,
                                txn_amt,
                                txn_chrg,
                                txn_ser_tax,
                                txn_edu_tax,
                                txn_hedu_tax,
                                rtr_comisn,
                                rtr_tds,
                                dtr_comisn,
                                dtr_tds,
                                total_comisn,
                                ifss_code,
                                beni_ac_no,
                                billpay_ref,
                                txn_status,
                                orig_txn_id,
                                orig_txn_date,
                                orig_txn_time,
                                txn_comisn_id,
                                comisn_txn_type,
                                shop_name,
                                firm_name,
                                cust_name,
                                cust_mob_no
                              FROM recon_rtr_imp_ext';
          EXECUTE IMMEDIATE v_query2;
          COMMIT;
          v_query3 := 'DROP TABLE recon_rtr_imp_ext';
          INSERT INTO daily_log (
               dl_file_id,
               dl_file_name,
               dl_transaction_dt,
               dl_upld_dt_time,
               dl_upld_count,
               dl_rej_count,
               dl_status,
               dl_remarks,
               dl_created_by,
               dl_created_dt,
               dl_updated_by,
               dl_updated_dt
          ) SELECT '005',
                 'RTR',
                 SYSDATE,
                 current_timestamp,
                 COUNT(1),
                 '0',
                 'Loaded',
                 'NA',
                 'Admin',
                 current_timestamp,
                 'Admin',
                 current_timestamp
          FROM recon_load_rtr_main_int;

          COMMIT;
          EXECUTE IMMEDIATE v_query3;
          msg := 'File Loaded Successfully';
     END IF;

EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to Load RTR_File';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CLEMANTIS_SINGLEPOOL_LOAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CLEMANTIS_SINGLEPOOL_LOAD" ( msg OUT VARCHAR2 ) AS

     v_query      VARCHAR2(10000);
     v_query2     VARCHAR2(4000);
     v_query3     VARCHAR2(10000);
     file_count   NUMBER := 0;
     
BEGIN
     SELECT COUNT(1) INTO file_count
     FROM daily_log
     WHERE dl_file_id = '012';

     IF file_count > 0 THEN
          msg := 'File Already Loaded';
     ELSE
     
         v_query := 'create table brs_single_pool_ext(
                         BSP_ACCOUNT_NO Varchar2(100) NULL,
                         BSP_ACCT_NAME Varchar2(100) NULL,
                         BSP_POST_DATE Varchar2(100)  NULL,
                         BSP_VALUE_DATE Varchar2(100)  NULL,
                         BSP_NARRATION Varchar2(100) NULL,
                         BSP_REFERENCE_NO Varchar2(100) NULL,
                         BSP_CHEQUE_NO Varchar2(100) NULL,
                         BSP_DR_CR Varchar2(100) NULL,
                         BSP_CURRENCY_CODE Varchar2(100) NULL,
                         BSP_TXN_AMOUNT Varchar2(100) NULL,
                         BSP_RUNNING_BALANCE Varchar2(100) NULL
            )
            organization external (
              type oracle_loader
              default directory WES_DIR  
              access parameters (
               RECORDS DELIMITED BY NEWLINE 
               SKIP 1
              fields terminated by ''|''
               )
              location (''0411405197_DAY-1.txt'')  
            )
            reject limit unlimited
            parallel';
            
          EXECUTE IMMEDIATE v_query;
          
          v_query2 := 'INSERT INTO brs_single_pool_main_int(
                                   BSP_ACCOUNT_NO,
                                   BSP_ACCT_NAME,
                                   BSP_POST_DATE,
                                   BSP_VALUE_DATE,
                                   BSP_NARRATION,
                                   BSP_REFERENCE_NO,
                                   BSP_CHEQUE_NO,
                                   BSP_DR_CR,
                                   BSP_CURRENCY_CODE,
                                   BSP_TXN_AMOUNT,
                                   BSP_RUNNING_BALANCE   
                                   )
                                   SELECT   BSP_ACCOUNT_NO,
                                   BSP_ACCT_NAME,
                                   BSP_POST_DATE,
                                   BSP_VALUE_DATE,
                                   BSP_NARRATION,
                                   BSP_REFERENCE_NO,
                                   BSP_CHEQUE_NO,
                                   BSP_DR_CR,
                                   BSP_CURRENCY_CODE,
                                   BSP_TXN_AMOUNT,
                                   BSP_RUNNING_BALANCE 
                                   FROM brs_single_pool_ext';
                              
          EXECUTE IMMEDIATE v_query2;
          COMMIT;
          v_query3 := 'DROP TABLE brs_single_pool_ext';
          
        INSERT INTO daily_log (
                    dl_file_id,
                    dl_file_name,
                    dl_transaction_dt,
                    dl_upld_dt_time,
                    dl_upld_count,
                    dl_rej_count,
                    dl_status,
                    dl_remarks,
                    dl_created_by,
                    dl_created_dt,
                    dl_updated_by,
                    dl_updated_dt
               ) SELECT '012',
                      'SinglePool',
                      SYSDATE,
                      current_timestamp,
                      COUNT(1),
                      '0',
                      'Loaded',
                      'NA',
                      'Admin',
                      current_timestamp,
                      'Admin',
                      current_timestamp
               FROM brs_single_pool_main_int; commit;
               
          
          EXECUTE IMMEDIATE v_query3;
          msg := 'File Loaded Successfully';
     END IF;

EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to load SinglePool File';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CREATE_CLIENTCONFIG_PUSH
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CREATE_CLIENTCONFIG_PUSH" (
                                   p_mercht_id	VARCHAR2,
                                   p_mercht_acctno	VARCHAR2,
                                   p_mercht_vpa	VARCHAR2,
                                   p_mercht_name	VARCHAR2,
                                   p_mercht_mobno	VARCHAR2,
                                   p_mercht_categrycode	VARCHAR2,
                                   p_mercht_deviceid	VARCHAR2,
                                   p_mercht_ifsc	VARCHAR2,
                                   p_settlmnt_acctno	VARCHAR2,
                                   p_settlmt_ifsc	VARCHAR2,
                                   p_msg_out OUT VARCHAR2
) 
     AS pragma autonomous_transaction;
BEGIN
     INSERT INTO crs_client_config_push VALUES (
          p_mercht_id,     
          p_mercht_acctno,
          p_mercht_vpa,	    
          p_mercht_name,	
          p_mercht_mobno,		
          p_mercht_categrycode,
          p_mercht_deviceid,	
          p_mercht_ifsc,		
          p_settlmnt_acctno,	
          p_settlmt_ifsc,
          'admin',
          current_timestamp,
          'admin',
          current_timestamp
     );

     COMMIT;
     p_msg_out := 'Success';
EXCEPTION
     WHEN OTHERS THEN
          p_msg_out := 'Failed';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
         -- dbms_output.put_line('Error in inserting..');
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CREATE_MERCHANT_FILE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CREATE_MERCHANT_FILE" (
     P_Mercht_name IN	VARCHAR2,
     P_Mercht_id	IN VARCHAR2,
     P_Mercht_logo	 IN VARCHAR2,
     P_Mercht_tagline IN	VARCHAR2,
     P_Mercht_seckey IN	VARCHAR2,
     P_Redirect_URL IN	VARCHAR2,
     P_Mercht_acctno IN	VARCHAR2,
     P_Mercht_IFSC	IN VARCHAR2,
     P_Mercht_Bank	IN VARCHAR2,
     P_Mercht_Categrycode IN	VARCHAR2,
     P_Mercht_VPA IN	VARCHAR2,
     P_Settlmnt_acctno IN	VARCHAR2,
     P_Settlmt_IFSC IN	VARCHAR2,
     P_Settlmt_Bankd IN	VARCHAR2,
     P_Mercht_Mobno IN	VARCHAR2,
     P_Mercht_DeviceID IN	VARCHAR2,
     p_msg_out       OUT VARCHAR2
) 
     AS pragma autonomous_transaction;
BEGIN
     INSERT INTO crs_merchant_file VALUES (
          p_Mercht_name,
          p_Mercht_id,
          p_Mercht_logo,
          p_Mercht_tagline,
          p_Mercht_seckey,
          p_Redirect_URL,
          p_Mercht_acctno,
          p_Mercht_IFSC,
          p_Mercht_Bank,
          p_Mercht_Categrycode,
          p_Mercht_VPA,
          p_Settlmnt_acctno,
          p_Settlmt_IFSC,
          p_Settlmt_Bankd,
          p_Mercht_Mobno,
          p_Mercht_DeviceID,
          'admin',
          current_timestamp,
          'admin',
          current_timestamp
     );

     COMMIT;
     p_msg_out := 'Success';
EXCEPTION
     WHEN OTHERS THEN
          p_msg_out := 'Failed';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
         -- dbms_output.put_line('Error in inserting..');
END;

/
--------------------------------------------------------
--  DDL for Procedure P_CREATE_PROCESS_FILE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_CREATE_PROCESS_FILE" (
     p_cust_id                 IN VARCHAR2,
     p_cust_name               IN VARCHAR2,
     p_cust_vpa                IN VARCHAR2,
     p_bill_no                 IN VARCHAR2,
     p_cart_amt                IN NUMBER,
     p_expiry_after           IN VARCHAR2,
     p_vercust_name            IN VARCHAR2,
     p_remark                  IN VARCHAR2,
     p_mercht_id               IN VARCHAR2,
     p_channel                 IN VARCHAR2,
     p_timestamp_pullrequest   IN timestamp,
     p_timestamp_paid          IN timestamp,
     p_upi_txnid               IN VARCHAR2,
     p_paymt_status            IN VARCHAR2,
     p_status_comments         IN VARCHAR2,
     p_msg_out                 OUT VARCHAR2
) 
     AS pragma autonomous_transaction;
BEGIN
     INSERT INTO crs_processed_file VALUES (
          p_cust_id,
          p_cust_name,
          p_cust_vpa,
          p_bill_no,
          p_cart_amt,
          p_expiry_after,
          p_vercust_name,
          p_remark,
          p_mercht_id,
          p_channel,
          p_timestamp_pullrequest,
          p_timestamp_paid,
          p_upi_txnid,
          p_paymt_status,
          p_status_comments,
          'admin',
          current_timestamp,
          'admin',
          current_timestamp
     );

     COMMIT;
     p_msg_out := 'Success';
EXCEPTION
     WHEN OTHERS THEN
          p_msg_out := 'Failed';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
         -- dbms_output.put_line('Error in inserting..');
END;

/
--------------------------------------------------------
--  DDL for Procedure P_GUJ_CASHBACK
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_GUJ_CASHBACK" (WESS OUT VARCHAR2) AS 



cursor cust is
select distinct guj_cust_mobno cc from guj_cust_txn_dtl_new; --  where guj_cust_mobno = '8140160809';

cursor ddtt(emp in varchar2) is
select distinct guj_txn_dt dt  from guj_cust_txn_dtl_new where guj_cust_mobno = emp
order by 1;

cursor cust_seq(emp in varchar, empdt in date) is
select guj_cust_mobno aa, guj_txn_dt ab,guj_txn_id ac,guj_txn_time ad from guj_cust_txn_dtl_new
where guj_cust_mobno = emp and guj_txn_dt = empdt
order by 1,2,3,4;


cursor c is 
select guj_cust_mobno,guj_txn_type,guj_txn_dt,guj_txn_time,guj_txn_id,guj_txn_amt,guj_txn_seqno
from guj_cust_txn_dtl_new where guj_cust_city = 'anand' and guj_first_txn_flag is null and guj_txn_seqno is not null and guj_merctxn_flg is null
order by guj_txn_dt,guj_txn_time;

cursor cc is
select  a.grid_slab aa ,a.grid_amt bb from guj_grid a, guj_grid_txn b
where a.grid_slab = b.aa and b.grid_slab is null;

wes_seq number;
wes number;

BEGIN


--TXN SEQUNCE BLOCK
BEGIN
for r_cust in cust loop

for rdt in ddtt(r_cust.cc) loop

wes_seq:= 0;
for r_cust_seq in cust_seq(r_cust.cc,rdt.dt) loop



update guj_cust_txn_dtl_new set guj_txn_seqno = wes_seq where guj_cust_mobno = r_cust_seq.aa
and guj_txn_dt = r_cust_seq.ab and guj_txn_id = r_cust_seq.ac;


wes_seq := wes_seq + 1;
exit when wes_seq > 2;

end loop;
end loop;

end loop;
commit;

END;
-- END OF TRANSACTION SEQUENCE BLOCK

-- FIRST TRANSACTION BLOCK
BEGIN
UPDATE guj_cust_txn_dtl_new SET GUJ_FIRST_TXN_FLAG = 'Y' WHERE (guj_cust_mobno,guj_txn_dt) IN (
select guj_cust_mobno, min(guj_txn_dt) from guj_cust_txn_dtl_new where 
guj_txn_seqno = '0' group by guj_cust_mobno) AND guj_txn_seqno = '0'
AND guj_cust_mobno NOT IN (SELECT guj_cust_mobno FROM guj_cust_txn_dtl);
COMMIT;
END;
-- END OF FIRST TRANSACTION BLOCK

-- START OF GRID BLOCK
BEGIN
select max(aa) + 1 into wes from  guj_grid_txn;
for r in c loop
insert into guj_grid_txn(aa,guj_cust_mobno,guj_txn_type,guj_txn_dt,guj_txn_time,guj_txn_id,guj_txn_amt,guj_txn_seqno)
values (wes,r.guj_cust_mobno,r.guj_txn_type,r.guj_txn_dt,r.guj_txn_time,r.guj_txn_id,r.guj_txn_amt,r.guj_txn_seqno);
wes:= wes + 1;
end loop;
commit;



END;
-- END OF GRID BLOCK

-- start of grid amount
begin

for rr in cc loop

update guj_grid_txn set grid_slab = rr.aa,grid_amt = rr.bb 
where aa = rr.aa;
end loop;
commit;
end;


-- end of grid amount
-- 
wess := 'Sucessfully Completed';
END P_GUJ_CASHBACK;

/
--------------------------------------------------------
--  DDL for Procedure P_GUJ_CASHBACK2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_GUJ_CASHBACK2" (WESS OUT VARCHAR2) AS 



cursor cust is
select distinct guj_cust_mobno cc from guj_cust_txn_dtl_new_wes; --  where guj_cust_mobno = '8140160809';

cursor ddtt(emp in varchar2) is
select distinct guj_txn_dt dt  from guj_cust_txn_dtl_new_wes where guj_cust_mobno = emp
order by 1;

cursor cust_seq(emp in varchar, empdt in date) is
select guj_cust_mobno aa, guj_txn_dt ab,guj_txn_id ac,guj_txn_time ad from guj_cust_txn_dtl_new_wes
where guj_cust_mobno = emp and guj_txn_dt = empdt
order by 1,2,3,4;


cursor c is 
select guj_cust_mobno,guj_txn_type,guj_txn_dt,guj_txn_time,guj_txn_id,guj_txn_amt,guj_txn_seqno
from guj_cust_txn_dtl_new_wes where guj_cust_city = 'anand' and guj_first_txn_flag is null and guj_txn_seqno is not null and guj_merctxn_flg is null
order by guj_txn_dt,guj_txn_time;

cursor cc is
select  a.grid_slab aa ,a.grid_amt bb from guj_grid a, guj_grid_txn b
where a.grid_slab = b.aa and b.grid_slab is null;

wes_seq number;
wes number;

BEGIN


--TXN SEQUNCE BLOCK
BEGIN
for r_cust in cust loop

for rdt in ddtt(r_cust.cc) loop

wes_seq:= 0;
for r_cust_seq in cust_seq(r_cust.cc,rdt.dt) loop



update guj_cust_txn_dtl_new_wes set guj_txn_seqno = wes_seq where guj_cust_mobno = r_cust_seq.aa
and guj_txn_dt = r_cust_seq.ab and guj_txn_id = r_cust_seq.ac;


wes_seq := wes_seq + 1;
exit when wes_seq > 2;

end loop;
end loop;

end loop;
commit;

END;
-- END OF TRANSACTION SEQUENCE BLOCK

-- FIRST TRANSACTION BLOCK
BEGIN
UPDATE guj_cust_txn_dtl_new_wes SET GUJ_FIRST_TXN_FLAG = 'Y' WHERE (guj_cust_mobno,guj_txn_dt) IN (
select guj_cust_mobno, min(guj_txn_dt) from guj_cust_txn_dtl_new where 
guj_txn_seqno = '0' group by guj_cust_mobno) AND guj_txn_seqno = '0'
AND guj_cust_mobno NOT IN (SELECT guj_cust_mobno FROM guj_cust_txn_dtl_wes2test);
COMMIT;
END;
-- END OF FIRST TRANSACTION BLOCK

-- START OF GRID BLOCK
BEGIN
select max(aa) + 1 into wes from  guj_grid_txn_wes;
for r in c loop
insert into guj_grid_txn_wes(aa,guj_cust_mobno,guj_txn_type,guj_txn_dt,guj_txn_time,guj_txn_id,guj_txn_amt,guj_txn_seqno)
values (wes,r.guj_cust_mobno,r.guj_txn_type,r.guj_txn_dt,r.guj_txn_time,r.guj_txn_id,r.guj_txn_amt,r.guj_txn_seqno);
wes:= wes + 1;
end loop;
commit;



END;
-- END OF GRID BLOCK

-- start of grid amount
begin

for rr in cc loop

update guj_grid_txn_wes set grid_slab = rr.aa,grid_amt = rr.bb 
where aa = rr.aa;
end loop;
commit;
end;


-- end of grid amount
-- 
wess := 'Sucessfully Completed';
END P_GUJ_CASHBACK2;

/
--------------------------------------------------------
--  DDL for Procedure P_GUJ_PROCESS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_GUJ_PROCESS" (p_date IN varchar, p_msg out varchar2) AS 
 -- Always for testing give p_date as 'dd/mm/yyyy' eg:- '02/07/2017'
 
WESS VARCHAR2(200);
sql_query1 varchar2(1000);
v_date date;
l_rows number;
l_rows2 number;
filename1 varchar2(500);
filename2 varchar2(500);
v_count number;
v_file_seq number;
BEGIN 
  p_msg := 'Success';
  v_date:= to_date(p_date,'dd-mm-yy');
  
     select count(1) into v_count 
     from guj_daily_log
     where GD_TRANSACTION_DT = v_date;
     
     IF v_count > 0 then 
     p_msg := 'AlreadyRun';
     ELSE
  
  -- Initial Data Check
/*
select max(crd_date) from guj_cust_mast_full;
select max(txn_dt) from brs_load_ctr_main_int;
select max(txn_date) from recon_rtr_imp_wes where substr(txn_date,4,7) = '07/2017';
select max(mt_txn_initiated_date) from brs_mtr_wes where substr(mt_txn_initiated_date,4,7) = '07/2017';

*/


--Insert and update query EOD process

    
-- Guj Customer Insert
--start1 

    insert into guj_cust_master
     select distinct cust_mob, txn_dt, 0, 'A', 'anand' from brs_load_ctr_main_int where txn_dt = v_date and  txn_id in 
     (select txn_comisn_id from recon_rtr_imp_wes where txn_date = p_date and rtr_mob_no in
     (select MOB_NO from guj_retl_mast where mast_type = 'RETL' and city = 'anand')) 
     and cust_mob not in (select gj_mob_no from guj_cust_master)
     and txn_type = 'Agent Cash In' and cust_mob in (select cust_mob from brs_load_ctr_main_int where txn_type = 'Ekyc Upgradation')
     union all
     select distinct cust_mob, txn_dt, 0, 'A', 'thamna' from brs_load_ctr_main_int where txn_dt = v_date and  txn_id in 
     (select txn_comisn_id from recon_rtr_imp_wes where txn_date = p_date and rtr_mob_no in
     (select MOB_NO from guj_retl_mast where mast_type = 'RETL' and city = 'thamna'))
     and cust_mob not in (select gj_mob_no from guj_cust_master)
     and txn_type = 'Agent Cash In' and cust_mob in (select cust_mob from brs_load_ctr_main_int where txn_type = 'Ekyc Upgradation');
      
     commit;
     
     insert into guj_cust_master
     select distinct cust_mob, txn_dt, 1, 'A', 'anand' from brs_load_ctr_main_int where  txn_dt = v_date and txn_id in 
     (select txn_id from recon_rtr_imp_wes where txn_date = p_date and rtr_mob_no in
     (select MOB_NO from guj_retl_mast where mast_type = 'RETL' and city = 'anand')) 
     and cust_mob not in (select gj_mob_no from guj_cust_master) and txn_type = 'IP Collected' 
     and cust_mob in (select cust_mob from brs_load_ctr_main_int where txn_type = 'Ekyc Upgradation')
     union all
     select distinct cust_mob, txn_dt, 1, 'A', 'thamna' from brs_load_ctr_main_int where  txn_dt = v_date and txn_id in 
     (select txn_id from recon_rtr_imp_wes where txn_date = p_date and rtr_mob_no in
     (select MOB_NO from guj_retl_mast where mast_type = 'RETL' and city = 'thamna')) 
     and cust_mob not in (select gj_mob_no from guj_cust_master) and txn_type = 'IP Collected' 
     and cust_mob in (select cust_mob from brs_load_ctr_main_int where txn_type = 'Ekyc Upgradation');
     
     commit;

-- Txn Import query

     insert into guj_cust_txn_dtl_new
     
     select txn_type, txn_id, txn_dt, txn_time, channel, cust_mob, cust_fst_name, cust_lst_name, cust_txn_amnt, null, null,  bill_code, billerdesc, 'anand',
     null, null, null, null
     from brs_load_ctr_main_int where txn_dt = v_date and cust_txn_amnt < 0 and 
     cust_mob in (select gj_mob_no from guj_cust_master where gj_city = 'anand' and GJ_ACTIVE_DT <= v_date ) 
     and txn_stat = 'SUCCESS'  and txn_type in (select distinct guj_txn_type from guj_txn_dtl where guj_txn_flg = 'Y')
     
     union all
     
     select txn_type, txn_id, txn_dt, txn_time, channel, cust_mob, cust_fst_name, cust_lst_name, cust_txn_amnt, null, null,  bill_code, billerdesc, 'thamna', 
     null, null, null, null
     from brs_load_ctr_main_int where txn_dt = v_date and cust_txn_amnt < 0 and 
     cust_mob in (select gj_mob_no from guj_cust_master where gj_city = 'thamna' and GJ_ACTIVE_DT <= v_date ) 
     and txn_stat = 'SUCCESS'  and txn_type in (select distinct guj_txn_type from guj_txn_dtl where guj_txn_flg = 'Y')
     
     union all
     
     select txn_type, txn_id, txn_dt, txn_time, channel, cust_mob, cust_fst_name, cust_lst_name, cust_txn_amnt, null, null,  bill_code, billerdesc, 'anand', 
     null, null, null, null
     from brs_load_ctr_main_int where txn_dt = v_date and cust_txn_amnt < 0 and txn_stat = 'SUCCESS' and
     cust_mob not in (select gj_mob_no from guj_cust_master) and cust_mob in (select cust_mob_no from guj_cust_mast_full where crd_date <= v_date) 
     and txn_type = 'Merchant Payment - Pay Now'
     and txn_id in (select mt_txn_id from brs_mtr_wes where mt_txn_initiated_date = v_date and 
     mt_credit_entity_msisdn in (select MOB_NO from guj_retl_mast where mast_type = 'MERC'))
     
     union all
     
     select txn_type, txn_id, txn_dt, txn_time, channel, cust_mob, cust_fst_name, cust_lst_name, cust_txn_amnt, null, null,  bill_code, billerdesc, 'anand', 
     null, null, null, null
     from brs_load_ctr_main_int where txn_dt = v_date and cust_txn_amnt < 0 and txn_stat = 'SUCCESS' and
     cust_mob not in (select gj_mob_no from guj_cust_master) and cust_mob in (select cust_mob_no from guj_cust_mast_full where crd_date <= v_date) 
     and txn_id in (select TXN_COMISN_ID from recon_rtr_imp_wes where txn_date = p_date and 
     rtr_mob_no in (select MOB_NO from guj_retl_mast where mast_type = 'RETL' and city = 'anand') and TXN_COMISN_ID != '-')
     
     union all
     
     select txn_type, txn_id, txn_dt, txn_time, channel, cust_mob, cust_fst_name, cust_lst_name, cust_txn_amnt, null, null,  bill_code, billerdesc, 'thamna', 
     null, null, null, null
     from brs_load_ctr_main_int where txn_dt = v_date and cust_txn_amnt < 0 and txn_stat = 'SUCCESS' and
     cust_mob not in (select gj_mob_no from guj_cust_master) and cust_mob in (select cust_mob_no from guj_cust_mast_full where crd_date <= v_date) 
     and txn_id in (select TXN_COMISN_ID from recon_rtr_imp_wes where txn_date = p_date and 
     rtr_mob_no in (select MOB_NO from guj_retl_mast where mast_type = 'RETL' and city = 'thamna') and TXN_COMISN_ID != '-');

     commit;

     update guj_cust_txn_dtl_new  set GUJ_MERC_MOBNO = (select MT_CREDIT_ENTITY_MSISDN from brs_mtr_wes 
     where MT_TXN_ID = GUJ_TXN_ID and GUJ_CUST_MOBNO = MT_DEBIT_ENTITY_MSISDN and MT_TXN_TYPE = GUJ_TXN_TYPE) 
     where GUJ_TXN_TYPE = 'Merchant Payment - Pay Now';
     
     commit;

                    /*
                    select CONCAT(GUJ_MERC_MOBNO,GUJ_CUST_MOBNO), count(CONCAT(GUJ_MERC_MOBNO,GUJ_CUST_MOBNO)) from guj_cust_txn_dtl_new where GUJ_TXN_TYPE = 'Merchant Payment - Pay Now'
                    group by CONCAT(GUJ_MERC_MOBNO,GUJ_CUST_MOBNO) having count(CONCAT(GUJ_MERC_MOBNO,GUJ_CUST_MOBNO))  > 2;
                    */

--start1
 
  BEGIN
   P_GUJ_CASHBACK(WESS => WESS);
  END;  

--start1
  
  --Final Selection query Export data transaction and grid transaction

       filename1 := 'GUJ_GRID_Transaction_'||v_date||'.csv';
       
       filename2 := 'GUJ_Transaction_'||v_date||'.csv';

     
      l_rows := dump_csv( 'SELECT ''GUJ_CUST_MOBNO'', ''GUJ_TXN_TYPE'', ''GUJ_TXN_DT'', ''GUJ_TXN_TIME'', ''GUJ_TXN_ID'', ''GUJ_TXN_AMT'', ''GRID_SLAB'', ''CALC_AMT'' FROM DUAL', ',', 'WES_DIR', filename1 ); 
      l_rows := dump_csv( 'SELECT GUJ_CUST_MOBNO, GUJ_TXN_TYPE, GUJ_TXN_DT, GUJ_TXN_TIME, GUJ_TXN_ID, GUJ_TXN_AMT, GRID_SLAB, case when abs(guj_txn_amt) * grid_amt > 1000 then 1000 else abs(guj_txn_amt) * grid_amt end CALC_AMT 
                           FROM guj_grid_txn where guj_filegen_flg is null', ',', 'WES_DIR', filename1 ); 


      l_rows2 := dump_csv( 'SELECT ''GUJ_TXN_TYPE'', ''GUJ_TXN_ID'', ''GUJ_TXN_DT'', ''GUJ_TXN_TIME'', ''GUJ_CUST_MOBNO'', ''GUJ_TXN_AMT'', ''GUJ_CUST_CITY'', ''CALC_AMT'' FROM DUAL', ',', 'WES_DIR', filename2 ); 
      l_rows2 := dump_csv( 'SELECT guj_txn_type,guj_txn_id,guj_txn_dt,guj_txn_time,guj_cust_mobno,guj_txn_amt,guj_cust_city, 
                            case when abs(guj_txn_amt) * 0.15 > 30 then 30 else abs(guj_txn_amt) * 0.15 end CALC_AMT
                            from guj_cust_txn_dtl_new where guj_first_txn_flag = ''Y''
                              UNION ALL
                            SELECT guj_txn_type,guj_txn_id,guj_txn_dt,guj_txn_time,guj_cust_mobno,guj_txn_amt,guj_cust_city, 
                              case when abs(guj_txn_amt) * 0.15 > 30 then 30 else abs(guj_txn_amt) * 0.15 end CALC_AMT
                              from guj_cust_txn_dtl_new where guj_first_txn_flag is null and guj_txn_seqno is not null and guj_cust_city = ''thamna'' ', ',', 'WES_DIR', filename2);
 


       --select * from guj_cust_txn_dtl_new where guj_first_txn_flag is null and guj_txn_seqno is null;
  
--start1
  
   insert into guj_cust_txn_dtl (GUJ_TXN_TYPE, GUJ_TXN_ID, GUJ_TXN_DT, GUJ_TXN_TIME, GUJ_TXN_CHNL, GUJ_CUST_MOBNO, GUJ_CUST_FNAME, GUJ_CUST_LNAME,
     GUJ_TXN_AMT, GUJ_CB_AMT, GUJ_CBMAX_AMT, GUJ_TXN_BILLCODE, GUJ_TXN_BILLDESC, GUJ_CUST_CITY, GUJ_TXN_SEQNO, GUJ_FILEGEN_FLG,
     GUJ_FILEGEN_DT)
     select GUJ_TXN_TYPE, GUJ_TXN_ID, GUJ_TXN_DT, GUJ_TXN_TIME, GUJ_TXN_CHNL, GUJ_CUST_MOBNO, GUJ_CUST_FNAME, GUJ_CUST_LNAME,
     GUJ_TXN_AMT, GUJ_CB_AMT, GUJ_CBMAX_AMT, GUJ_TXN_BILLCODE, GUJ_TXN_BILLDESC, GUJ_CUST_CITY, GUJ_TXN_SEQNO, 'Y',
     v_date from guj_cust_txn_dtl_new;
     
     commit; --*/
     
         v_file_seq := file_id_seq.nextval;
     
          insert into GUJ_DAILY_LOG
          select v_file_seq, filename1, v_date, current_timestamp, count(GUJ_TXN_ID), 'Success', 'File Ran Successfully', 'admin', current_timestamp, 'admin', current_timestamp   
          FROM guj_cust_txn_dtl_new;
          
          commit;
     
     sql_query1 := 'truncate table guj_cust_txn_dtl_new';
     EXECUTE IMMEDIATE sql_query1;

     update guj_grid_txn 
     set GUJ_FILEGEN_FLG = 'Y', 
         GUJ_FILEGEN_DT = v_date--'03-JUL-17'
         where GUJ_FILEGEN_FLG is null;
         
     commit;
     
     
--start1

    END IF;
 
 EXCEPTION
     WHEN OTHERS THEN
          p_msg := 'Failed';
          raise_application_error(-20001,'An error was encountered - ' || SQLCODE || ' -ERROR- ' || sqlerrm);
END;

/
--------------------------------------------------------
--  DDL for Procedure P_GUJ_PROCESS2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_GUJ_PROCESS2" (p_date IN varchar, p_msg out varchar2) AS 
 -- Always for testing give p_date as 'dd/mm/yyyy' eg:- '02/07/2017'
 
WESS VARCHAR2(200);
sql_query1 varchar2(1000);
v_date date;
l_rows number;
l_rows2 number;
filename1 varchar2(500);
filename2 varchar2(500);
v_count number;
v_file_seq number;
BEGIN 
  p_msg := 'Success';
  v_date:= to_date(p_date,'dd-mm-yy');
  
     select count(1) into v_count 
     from guj_daily_log
     where GD_TRANSACTION_DT = v_date;
     
     IF v_count > 0 then 
     p_msg := 'AlreadyRun';
     ELSE
  
  -- Initial Data Check
/*
select max(crd_date) from guj_cust_mast_full;
select max(txn_dt) from brs_load_ctr_main_int;
select max(txn_date) from recon_rtr_imp_wes where substr(txn_date,4,7) = '07/2017';
select max(mt_txn_initiated_date) from brs_mtr_wes where substr(mt_txn_initiated_date,4,7) = '07/2017';

*/


--Insert and update query EOD process

    
-- Guj Customer Insert
--start1 

    insert into guj_cust_master_wes
     select distinct cust_mob, txn_dt, 0, 'A', 'anand' from brs_load_ctr_main_int where txn_dt = v_date and  txn_id in 
     (select txn_comisn_id from recon_rtr_imp_wes where txn_date = p_date and rtr_mob_no in
     (select MOB_NO from guj_retl_mast where mast_type = 'RETL' and city = 'anand')) 
     and cust_mob not in (select gj_mob_no from guj_cust_master_wes)
     and txn_type = 'Agent Cash In' and cust_mob in (select cust_mob from brs_load_ctr_main_int where txn_type = 'Ekyc Upgradation')
     union all
     select distinct cust_mob, txn_dt, 0, 'A', 'thamna' from brs_load_ctr_main_int where txn_dt = v_date and  txn_id in 
     (select txn_comisn_id from recon_rtr_imp_wes where txn_date = p_date and rtr_mob_no in
     (select MOB_NO from guj_retl_mast where mast_type = 'RETL' and city = 'thamna'))
     and cust_mob not in (select gj_mob_no from guj_cust_master_wes)
     and txn_type = 'Agent Cash In' and cust_mob in (select cust_mob from brs_load_ctr_main_int where txn_type = 'Ekyc Upgradation');
      
     commit;
     
     insert into guj_cust_master_wes
     select distinct cust_mob, txn_dt, 1, 'A', 'anand' from brs_load_ctr_main_int where  txn_dt = v_date and txn_id in 
     (select txn_id from recon_rtr_imp_wes where txn_date = p_date and rtr_mob_no in
     (select MOB_NO from guj_retl_mast where mast_type = 'RETL' and city = 'anand')) 
     and cust_mob not in (select gj_mob_no from guj_cust_master_wes) and txn_type = 'IP Collected' 
     and cust_mob in (select cust_mob from brs_load_ctr_main_int where txn_type = 'Ekyc Upgradation')
     union all
     select distinct cust_mob, txn_dt, 1, 'A', 'thamna' from brs_load_ctr_main_int where  txn_dt = v_date and txn_id in 
     (select txn_id from recon_rtr_imp_wes where txn_date = p_date and rtr_mob_no in
     (select MOB_NO from guj_retl_mast where mast_type = 'RETL' and city = 'thamna')) 
     and cust_mob not in (select gj_mob_no from guj_cust_master_wes) and txn_type = 'IP Collected' 
     and cust_mob in (select cust_mob from brs_load_ctr_main_int where txn_type = 'Ekyc Upgradation');
     
     commit;

-- Txn Import query

     insert into guj_cust_txn_dtl_new_wes
     
     select txn_type, txn_id, txn_dt, txn_time, channel, cust_mob, cust_fst_name, cust_lst_name, cust_txn_amnt, null, null,  bill_code, billerdesc, 'anand',
     null, null, null, null
     from brs_load_ctr_main_int where txn_dt = v_date and cust_txn_amnt < 0 and 
     cust_mob in (select gj_mob_no from guj_cust_master_wes where gj_city = 'anand' and GJ_ACTIVE_DT <= v_date ) 
     and txn_stat = 'SUCCESS'  and txn_type in (select distinct guj_txn_type from guj_txn_dtl where guj_txn_flg = 'Y')
     
     union all
     
     select txn_type, txn_id, txn_dt, txn_time, channel, cust_mob, cust_fst_name, cust_lst_name, cust_txn_amnt, null, null,  bill_code, billerdesc, 'thamna', 
     null, null, null, null
     from brs_load_ctr_main_int where txn_dt = v_date and cust_txn_amnt < 0 and 
     cust_mob in (select gj_mob_no from guj_cust_master_wes where gj_city = 'thamna' and GJ_ACTIVE_DT <= v_date ) 
     and txn_stat = 'SUCCESS'  and txn_type in (select distinct guj_txn_type from guj_txn_dtl where guj_txn_flg = 'Y')
     
     union all
     
     select txn_type, txn_id, txn_dt, txn_time, channel, cust_mob, cust_fst_name, cust_lst_name, cust_txn_amnt, null, null,  bill_code, billerdesc, 'anand', 
     null, null, null, null
     from brs_load_ctr_main_int where txn_dt = v_date and cust_txn_amnt < 0 and txn_stat = 'SUCCESS' and
     cust_mob not in (select gj_mob_no from guj_cust_master_wes) and cust_mob in (select cust_mob_no from guj_cust_mast_full where crd_date <= v_date) 
     and txn_type = 'Merchant Payment - Pay Now'
     and txn_id in (select mt_txn_id from brs_mtr_wes where mt_txn_initiated_date = p_date and 
     mt_credit_entity_msisdn in (select MOB_NO from guj_retl_mast where mast_type = 'MERC'))
     
     union all
     
     select txn_type, txn_id, txn_dt, txn_time, channel, cust_mob, cust_fst_name, cust_lst_name, cust_txn_amnt, null, null,  bill_code, billerdesc, 'anand', 
     null, null, null, null
     from brs_load_ctr_main_int where txn_dt = v_date and cust_txn_amnt < 0 and txn_stat = 'SUCCESS' and
     cust_mob not in (select gj_mob_no from guj_cust_master_wes) and cust_mob in (select cust_mob_no from guj_cust_mast_full where crd_date <= v_date) 
     and txn_id in (select TXN_COMISN_ID from recon_rtr_imp_wes where txn_date = p_date and 
     rtr_mob_no in (select MOB_NO from guj_retl_mast where mast_type = 'RETL' and city = 'anand') and TXN_COMISN_ID != '-')
     
     union all
     
     select txn_type, txn_id, txn_dt, txn_time, channel, cust_mob, cust_fst_name, cust_lst_name, cust_txn_amnt, null, null,  bill_code, billerdesc, 'thamna', 
     null, null, null, null
     from brs_load_ctr_main_int where txn_dt = v_date and cust_txn_amnt < 0 and txn_stat = 'SUCCESS' and
     cust_mob not in (select gj_mob_no from guj_cust_master_wes) and cust_mob in (select cust_mob_no from guj_cust_mast_full where crd_date <= v_date) 
     and txn_id in (select TXN_COMISN_ID from recon_rtr_imp_wes where txn_date = p_date and 
     rtr_mob_no in (select MOB_NO from guj_retl_mast where mast_type = 'RETL' and city = 'thamna') and TXN_COMISN_ID != '-');

     commit;

     update guj_cust_txn_dtl_new_wes  set GUJ_MERC_MOBNO = (select MT_CREDIT_ENTITY_MSISDN from brs_mtr_wes 
     where MT_TXN_ID = GUJ_TXN_ID and GUJ_CUST_MOBNO = MT_DEBIT_ENTITY_MSISDN and MT_TXN_TYPE = GUJ_TXN_TYPE) 
     where GUJ_TXN_TYPE = 'Merchant Payment - Pay Now';
     
     commit;

                    /*
                    select CONCAT(GUJ_MERC_MOBNO,GUJ_CUST_MOBNO), count(CONCAT(GUJ_MERC_MOBNO,GUJ_CUST_MOBNO)) from guj_cust_txn_dtl_new where GUJ_TXN_TYPE = 'Merchant Payment - Pay Now'
                    group by CONCAT(GUJ_MERC_MOBNO,GUJ_CUST_MOBNO) having count(CONCAT(GUJ_MERC_MOBNO,GUJ_CUST_MOBNO))  > 2;
                    */

--start1
 
 BEGIN
   P_GUJ_CASHBACK2(WESS => WESS); 
 END;  

--start1
  
  --Final Selection query Export data transaction and grid transaction

       filename1 := 'GUJ_GRID_Transaction_'||v_date||'.csv';
       
       filename2 := 'GUJ_Transaction_'||v_date||'.csv';

     
      l_rows := dump_csv( 'SELECT ''GUJ_CUST_MOBNO'', ''GUJ_TXN_TYPE'', ''GUJ_TXN_DT'', ''GUJ_TXN_TIME'', ''GUJ_TXN_ID'', ''GUJ_TXN_AMT'', ''GRID_SLAB'', ''CALC_AMT'' FROM DUAL', ',', 'WES_DIR', filename1 ); 
      l_rows := dump_csv( 'SELECT GUJ_CUST_MOBNO, GUJ_TXN_TYPE, GUJ_TXN_DT, GUJ_TXN_TIME, GUJ_TXN_ID, GUJ_TXN_AMT, GRID_SLAB, case when abs(guj_txn_amt) * grid_amt > 1000 then 1000 else abs(guj_txn_amt) * grid_amt end CALC_AMT 
                           FROM guj_grid_txn where guj_filegen_flg is null', ',', 'WES_DIR', filename1 ); 


      l_rows2 := dump_csv( 'SELECT ''GUJ_TXN_TYPE'', ''GUJ_TXN_ID'', ''GUJ_TXN_DT'', ''GUJ_TXN_TIME'', ''GUJ_CUST_MOBNO'', ''GUJ_TXN_AMT'', ''GUJ_CUST_CITY'', ''CALC_AMT'' FROM DUAL', ',', 'WES_DIR', filename2 ); 
      l_rows2 := dump_csv( 'SELECT guj_txn_type,guj_txn_id,guj_txn_dt,guj_txn_time,guj_cust_mobno,guj_txn_amt,guj_cust_city, 
                            case when abs(guj_txn_amt) * 0.15 > 30 then 30 else abs(guj_txn_amt) * 0.15 end CALC_AMT
                            from guj_cust_txn_dtl_new where guj_first_txn_flag = ''Y''
                              UNION ALL
                            SELECT guj_txn_type,guj_txn_id,guj_txn_dt,guj_txn_time,guj_cust_mobno,guj_txn_amt,guj_cust_city, 
                              case when abs(guj_txn_amt) * 0.15 > 30 then 30 else abs(guj_txn_amt) * 0.15 end CALC_AMT
                              from guj_cust_txn_dtl_new where guj_first_txn_flag is null and guj_txn_seqno is not null and guj_cust_city = ''thamna'' ', ',', 'WES_DIR', filename2);
 


       --select * from guj_cust_txn_dtl_new where guj_first_txn_flag is null and guj_txn_seqno is null;
  
--start1

-- new code:
        update guj_cust_txn_dtl_new_wes a set a.guj_cb_amt = 
     (select case when abs(b.guj_txn_amt) * 0.05 > 20 then 20 else abs(b.guj_txn_amt) * 0.05 end
     from guj_cust_txn_dtl b where b.guj_first_txn_flag is null and b.guj_txn_seqno is not null and b.guj_cust_city = 'thamna' and
     a.GUJ_TXN_ID = b.GUJ_TXN_ID)
     where a.guj_first_txn_flag is null and a.guj_txn_seqno is not null and a.guj_cust_city = 'thamna' 
     and a.guj_cb_amt is null; 
      commit;


     
     update guj_cust_txn_dtl_new_wes a set a.guj_cb_amt = 
     (select case when abs(b.guj_txn_amt) * 0.15 > 30 then 30 else abs(b.guj_txn_amt) * 0.15 end
     from guj_cust_txn_dtl b where a.guj_first_txn_flag = 'Y' and
     a.GUJ_TXN_ID = b.GUJ_TXN_ID)
     where a.guj_first_txn_flag = 'Y' 
     and a.guj_cb_amt is null; 
     commit;


  
   insert into guj_cust_txn_dtl_wes2test (GUJ_TXN_TYPE, GUJ_TXN_ID, GUJ_TXN_DT, GUJ_TXN_TIME, GUJ_TXN_CHNL, GUJ_CUST_MOBNO, GUJ_CUST_FNAME, GUJ_CUST_LNAME,
     GUJ_TXN_AMT, GUJ_CB_AMT, GUJ_CBMAX_AMT, GUJ_TXN_BILLCODE, GUJ_TXN_BILLDESC, GUJ_CUST_CITY, GUJ_TXN_SEQNO, GUJ_FILEGEN_FLG,
     GUJ_FILEGEN_DT)
     select GUJ_TXN_TYPE, GUJ_TXN_ID, GUJ_TXN_DT, GUJ_TXN_TIME, GUJ_TXN_CHNL, GUJ_CUST_MOBNO, GUJ_CUST_FNAME, GUJ_CUST_LNAME,
     GUJ_TXN_AMT, GUJ_CB_AMT, GUJ_CBMAX_AMT, GUJ_TXN_BILLCODE, GUJ_TXN_BILLDESC, GUJ_CUST_CITY, GUJ_TXN_SEQNO, 'Y',
     sysdate from guj_cust_txn_dtl_new_wes;
     
     commit; --*/
     
         v_file_seq := file_id_seq.nextval;
     
          insert into GUJ_DAILY_LOG
          select v_file_seq, filename1, v_date, current_timestamp, count(GUJ_TXN_ID), 'Success', 'File Ran Successfully', 'admin', current_timestamp, 'admin', current_timestamp   
          FROM guj_cust_txn_dtl_new_wes;
          
          commit;
     
     sql_query1 := 'truncate table guj_cust_txn_dtl_new_wes';
     EXECUTE IMMEDIATE sql_query1;

     update guj_grid_txn_wes 
     set GUJ_FILEGEN_FLG = 'Y', 
         GUJ_FILEGEN_DT = sysdate--'03-JUL-17'
         where GUJ_FILEGEN_FLG is null; 
         commit;
         
     insert into guj_summary_report  
     
      select guj_cust_mobno, min(guj_txn_dt), max(guj_txn_dt), 
          count( distinct(guj_txn_dt)), count(guj_txn_id),
          nvl(sum(abs(guj_txn_amt)), 0), nvl(sum(abs(guj_cb_amt)),0),
          'admin', current_timestamp 
          from guj_cust_txn_dtl_wes2test 
          group by guj_cust_mobno
          
     union all    
     
     select guj_cust_mobno, min(guj_txn_dt), max(guj_txn_dt), count( distinct(guj_txn_dt)), count(guj_txn_id),
           nvl(sum(abs(guj_txn_amt)),0),
           nvl(sum((abs(guj_txn_amt) * (grid_amt))), 0),
          'admin', current_timestamp
          from guj_grid_txn_wes 
          group by guj_cust_mobno;
         
     commit;
     
     
--start1

    END IF;
 
 EXCEPTION
     WHEN OTHERS THEN
          p_msg := 'Failed';
          raise_application_error(-20001,'An error was encountered - ' || SQLCODE || ' -ERROR- ' || sqlerrm);
END;

/
--------------------------------------------------------
--  DDL for Procedure P_INSERT_CARTCHECKOUT_CUSTPUSH
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_INSERT_CARTCHECKOUT_CUSTPUSH" ( P_CUSTOMER_ID in varchar2,
                                          P_CUSTOMER_NAME in varchar2,
                                          P_CUSTOMER_VPA in varchar2,
                                          P_BILL_ID in varchar2,
                                          P_BILL_AMOUNT in number,
                                          P_EXPIRY_AFTER in varchar2,
                                          P_TXN_REMARKS in varchar2,
                                          P_MERCHANT_ID in varchar2,
                                          P_CHANNEL_TYPE in varchar2,
                                          P_TXN_ID in varchar2,
                                          P_UPI_TXN_STATUS in varchar2,
                                          P_UPI_TXN_REMARKS in varchar2,
                                          P_PULL_TIMESTAMP in timestamp,
                                          P_PAID_TO_UPI_TIMESTAMP in timestamp,
                                          P_STATUS_SENT_TO_CRYSTAL in varchar2,
                                          P_REQUEST_TIMESTAMP in varchar2,
                                          MSG   OUT varchar2)as
                                          PRAGMA AUTONOMOUS_TRANSACTION;
                                          
begin

  MSG := 'Success';

insert into CRS_CARTCHECKOUT_CUSTOMERPUSH(CC_CUSTOMER_ID,
                                      CC_CUSTOMER_NAME,
                                      CC_CUSTOMER_VPA,
                                      CC_BILL_ID,
                                      CC_BILL_AMOUNT,
                                      CC_EXPIRY_AFTER,
                                      CC_TXN_REMARKS,
                                      CC_MERCHANT_ID,
                                      CC_CHANNEL_TYPE,
                                      CC_TXN_ID,
                                      CC_UPI_TXN_STATUS,
                                      CC_UPI_TXN_REMARKS,
                                      CC_PULL_TIMESTAMP,
                                      CC_PAID_TO_UPI_TIMESTAMP,
                                      CC_STATUS_SENT_TO_CRYSTAL,
                                      CC_REQUEST_TIMESTAMP,
                                      CC_CREATED_BY,
                                      CC_CREATED_DT,
                                      CC_UPDATED_BY,
                                      CC_UPDATED_DT) values(P_CUSTOMER_ID,
                                      P_CUSTOMER_NAME,
                                      P_CUSTOMER_VPA,
                                      P_BILL_ID,
                                      P_BILL_AMOUNT,
                                      P_EXPIRY_AFTER,
                                      P_TXN_REMARKS,
                                      P_MERCHANT_ID,
                                      P_CHANNEL_TYPE,
                                      P_TXN_ID,
                                      P_UPI_TXN_STATUS,
                                      P_UPI_TXN_REMARKS,
                                      P_PULL_TIMESTAMP,
                                      P_PAID_TO_UPI_TIMESTAMP,
                                      P_STATUS_SENT_TO_CRYSTAL,
                                      P_REQUEST_TIMESTAMP,
                                      'admin',
                                      current_timestamp,
                                      'admin',
                                      current_timestamp);
                                    
                                      
 commit;
   
    EXCEPTION 
    when OTHERS then
    MSG := 'Failed';
    RAISE_APPLICATION_ERROR(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);  
end;

/
--------------------------------------------------------
--  DDL for Procedure P_LOAD_CRYSTAL_CUSTOMER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_LOAD_CRYSTAL_CUSTOMER" ( msg   OUT VARCHAR2 ) 
AS pragma autonomous_transaction;
     v_query    VARCHAR2(10000);
     v_query2   VARCHAR2(4000);
     v_query3   VARCHAR2(10000);
     v_batch_date  varchar2(200);
     v_inc_amt number(38,2);
     v_client_id number;
     v_batch_no number;
     var1 number := 0;
     var2 number := 0;
     v_lotid number :=1;
     v_batchcount number := 0;
BEGIN
     v_query := 'create table load_crystal_customer_ext(
                          RC_CUST_ID	NUMBER,
                          RC_CUST_NAME	VARCHAR2(200 BYTE),
                          RC_CUST_VPA	VARCHAR2(200 BYTE),
                          RC_CUST_MOBNO	NUMBER,
                          RC_BILL_NO	VARCHAR2(200 BYTE),
                          RC_BILL_MONTH	VARCHAR2(200 BYTE),
                          RC_BILL_AMOUNT	NUMBER(38,2),
                          RC_BILL_DUEDATE	varchar2(200)
                          )
                            organization external (
                              type oracle_loader
                              default directory WES_DIR  
                              access parameters (
                              RECORDS DELIMITED BY NEWLINE 
                              fields terminated by '','' )
                              location (''crystal.csv'')
                            )
                            parallel
                            reject limit unlimited';
        
     EXECUTE IMMEDIATE v_query;
     
     SELECT TO_CHAR(SYSDATE, 'ddmmyyyy') INTO v_batch_date
     FROM dual;
     
     v_batch_no := batch_seq.nextval;
     
     v_batch_date := 'BATCH_'||v_batch_date||'_'||v_batch_no;
     
     v_query3 := 'INSERT INTO crs_raw_customer(RC_BATCH_ID,
                                          RC_CUST_ID,
                                          RC_CUST_NAME,
                                          RC_CUST_VPA,
                                          RC_CUST_MOBNO,
                                          RC_BILL_NO,
                                          RC_BILL_MONTH,
                                          RC_BILL_AMOUNT,
                                          RC_BILL_DUEDATE,
                                          RC_STATUS,
                                          RC_CHANNEL,
                                          RC_CREATED_BY,
                                          RC_CREATED_DATE,
                                          RC_UPDATED_BY,
                                          RC_UPDATED_DATE    
                                             ) SELECT ''' || v_batch_date ||''',
                                                        RC_CUST_ID,
                                                        RC_CUST_NAME,
                                                        RC_CUST_VPA,
                                                        RC_CUST_MOBNO,
                                                        RC_BILL_NO,
                                                        RC_BILL_MONTH,
                                                        RC_BILL_AMOUNT,
                                                        to_date( RC_BILL_DUEDATE, ''mm/dd/yyyy''),
                                                        ''READYTOGO'',
                                                        ''01'',
                                                        ''admin'',
                                                        current_timestamp,
                                                        ''admin'',
                                                        current_timestamp                                           
                                                        FROM load_crystal_customer_ext';
     EXECUTE IMMEDIATE v_query3;
     COMMIT;
     
     -- Inserting data into crs_summary table
     
     insert into crs_summary
     select v_batch_date, 'crystal', count(1), 'Fileloaded',
            'admin', current_timestamp,
            'admin', current_timestamp, ( ceil(count(1)/5000))
     from crs_raw_customer;
     
     UPDATE crs_raw_customer
     SET rc_rung_seq = crs_seq_cust.NEXTVAL
     where RC_BATCH_ID = v_batch_date;          
     COMMIT;
     
     select CS_RECORD_CNT into v_batchcount
     from crs_summary
     where cs_batch_id = v_batch_date;
          
     BEGIN
       reset_sequence('crs_seq_cust',1);
     END;
     
     /*
     
     BEGIN
          for r IN( select * from crs_summary)
          loop
          for i in 1..r.CS_LOT_NUMBER
          loop
          var2:= var2 + 5000;
          insert into crs_lot_info
          values(v_lotid,var1, var2,r.cs_batch_id,null);
          var1:= var2+1;
          v_lotid := v_lotid +1;
          exit when var2>= r.CS_RECORD_CNT;
          
          end loop;
          var1:=0;
          var2:=0;
            
          end loop;
          commit;      
     end;
     */
     
     INSERT INTO CRS_Final_CUSTOMER(FC_BATCH_ID,
                                          FC_RUNG_SEQ,
                                          FC_CUST_ID,
                                          FC_CUST_NAME,
                                          FC_CUST_VPA,
                                          FC_CUST_MOBNO,
                                          FC_BILL_NO,
                                          FC_BILL_MONTH,
                                          FC_BILL_AMOUNT,
                                          FC_BILL_DUEDATE,
                                          FC_STATUS,
                                          FC_CHANNEL,
                                          FC_CREATED_BY,
                                          FC_CREATED_DATE,
                                          FC_UPDATED_BY,
                                          FC_UPDATED_DATE,
                                          FC_DUEDATE_FLG
                                   )
                                   SELECT RC_BATCH_ID,
                                          RC_RUNG_SEQ, 
                                          RC_CUST_ID,
                                          RC_CUST_NAME,
                                          RC_CUST_VPA,
                                          RC_CUST_MOBNO,
                                          RC_BILL_NO,
                                          RC_BILL_MONTH,
                                          abs(RC_BILL_AMOUNT),
                                          RC_BILL_DUEDATE,
                                          RC_STATUS,
                                          RC_CHANNEL,
                                          RC_CREATED_BY,
                                          RC_CREATED_DATE,
                                          RC_UPDATED_BY,
                                          RC_UPDATED_DATE,
                                          'Y'
                                          FROM crs_raw_customer where rc_batch_id = v_batch_date order by RC_RUNG_SEQ;
                                          
     
     COMMIT;
     
     UPDATE CRS_Final_CUSTOMER
     set  FC_DUEDATE_FLG = 'N'
     WHERE FC_BILL_DUEDATE  <= sysdate;
     
     --select * from crs_final_customer where sign(fc_bill_amount) = -1;
     
     commit;
     
     v_query2 := 'DROP TABLE load_crystal_customer_ext';
     
     EXECUTE IMMEDIATE v_query2;
     
     msg := 'File Loaded Successfully';
EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to Load Crystal_customer_File';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_LOAD_CRYSTAL_CUSTOMER_PUSH
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_LOAD_CRYSTAL_CUSTOMER_PUSH" ( msg   OUT VARCHAR2 ) 
AS pragma autonomous_transaction;
     v_query    VARCHAR2(10000);
     v_query2   VARCHAR2(4000);
     v_query3   VARCHAR2(10000);
     v_batch_date  varchar2(200);
     v_inc_amt number(38,2);
     v_client_id number;
     v_batch_no number;
     var1 number := 0;
     var2 number := 0;
     v_lotid number :=1;
     v_batchcount number := 0;
BEGIN
     v_query := 'create table load_customer_push_ext(
                         CC_CUSTID	VARCHAR2(200),
                         CC_CUSTOMER_VPA	VARCHAR2(200),
                         CC_CUSTOMER_NAME	VARCHAR2(200),
                         CC_CUSTOMER_ACC	VARCHAR2(200),
                         CC_CUSTOMER_IFSC	VARCHAR2(200),
                         CC_TXN_MODE	VARCHAR2(200),
                         CC_AMOUNT	NUMBER(38,2)
                          )
                            organization external (
                              type oracle_loader
                              default directory WES_DIR  
                              access parameters (
                              RECORDS DELIMITED BY NEWLINE 
                              fields terminated by '','' )
                              location (''customerpush.csv'')
                            )
                            parallel
                            reject limit unlimited';
        
     EXECUTE IMMEDIATE v_query;
     
     SELECT TO_CHAR(SYSDATE, 'ddmmyyyy') INTO v_batch_date
     FROM dual;
     
     v_batch_no := customerpush_seq.nextval;
     
     v_batch_date := 'BATCH_'||v_batch_date||'_'||v_batch_no;
     
     v_query3 :=  'INSERT INTO crs_customer_push(CC_BATCH_ID,
                                                  CC_RUNG_SEQ,
                                                  CC_CUSTID,
                                                  CC_CUSTOMER_VPA,
                                                  CC_CUSTOMER_NAME,
                                                  CC_CUSTOMER_ACC,
                                                  CC_CUSTOMER_IFSC,
                                                  CC_TXN_MODE,
                                                  CC_AMOUNT,
                                                  CC_STATUS,
                                                  CC_REMARK,
                                                  CC_CREATED_BY,
                                                  CC_CREATED_DATE,
                                                  CC_UPDATED_BY,
                                                  CC_UPDATED_DATE   
                                                  ) SELECT ''' || v_batch_date ||''',
                                                            NULL,
                                                            CC_CUSTID,
                                                            CC_CUSTOMER_VPA,
                                                            CC_CUSTOMER_NAME,
                                                            CC_CUSTOMER_ACC,
                                                            CC_CUSTOMER_IFSC,
                                                            CC_TXN_MODE,
                                                            CC_AMOUNT,
                                                        ''READYTOGO'',
                                                        ''Remarks'',
                                                        ''admin'',
                                                        current_timestamp,
                                                        ''admin'',
                                                        current_timestamp                                           
                                                        FROM load_customer_push_ext';
     EXECUTE IMMEDIATE v_query3;
     COMMIT;
     
     -- Inserting data into crs_summary table
     
     insert into crs_summary_push
     select v_batch_date, 'crystal_customerPush', count(1), 'Fileloaded',
            'admin', current_timestamp,
            'admin', current_timestamp, ( ceil(count(1)/5000))
     from crs_customer_push;
     
     UPDATE crs_customer_push
     SET cc_rung_seq = crs_seq_custpush.NEXTVAL
     where CC_BATCH_ID = v_batch_date; 
     
     COMMIT;
     
            
     BEGIN
       reset_sequence('crs_seq_custpush',1);
     END;
    
         
     --select * from crs_summary_push where sign(fc_bill_amount) = -1;
     
     commit;
     
     v_query2 := 'DROP TABLE load_customer_push_ext';
     
     EXECUTE IMMEDIATE v_query2;
     
     msg := 'File Loaded Successfully';
EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to Load load_customer_push_ext';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_LOAD_CRYSTAL_CUSTOMER_RUNSEQ
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_LOAD_CRYSTAL_CUSTOMER_RUNSEQ" ( msg   OUT VARCHAR2 ) AS
     v_query    VARCHAR2(10000);
     v_query2   VARCHAR2(4000);
     v_query3   VARCHAR2(10000);
     v_batch_date  varchar2(200);
     v_inc_amt number(38,2);
     v_client_id number;
BEGIN
     v_query := 'create table load_crystal_customer_ext(
                RC_CUST_ID	NUMBER,
                RC_CUST_NAME	VARCHAR2(200 BYTE),
                RC_CUST_VPA	VARCHAR2(200 BYTE),
                RC_CUST_MOBNO	NUMBER,
                RC_BILL_NO	VARCHAR2(200 BYTE),
                RC_BILL_MONTH	VARCHAR2(200 BYTE),
                RC_BILL_AMOUNT	NUMBER(38,2),
                RC_BILL_DUEDATE	varchar2(200)
        )
        organization external (
          type oracle_loader
          default directory WES_DIR  
          access parameters (
          RECORDS DELIMITED BY NEWLINE 
          fields terminated by '','' )
          location (''crystal.csv'')
        )
        parallel
        reject limit unlimited';
        
     EXECUTE IMMEDIATE v_query;
     
     SELECT TO_CHAR(SYSDATE, 'ddmmyyyy') INTO v_batch_date
     FROM dual;
     
     v_batch_date := 'BATCH000_'||v_batch_date;
     
     v_query3 := 'INSERT INTO crs_raw_customer2(RC_BATCH_ID,
                                          RC_CUST_ID,
                                          RC_CUST_NAME,
                                          RC_CUST_VPA,
                                          RC_CUST_MOBNO,
                                          RC_BILL_NO,
                                          RC_BILL_MONTH,
                                          RC_BILL_AMOUNT,
                                          RC_BILL_DUEDATE,
                                          RC_STATUS,
                                          RC_CREATED_BY,
                                          RC_CREATED_DATE,
                                          RC_UPDATED_BY,
                                          RC_UPDATED_DATE    
                                             ) SELECT  ''' || v_batch_date ||''',                                                                                                    
                                                        RC_CUST_ID,
                                                        RC_CUST_NAME,
                                                        RC_CUST_VPA,
                                                        RC_CUST_MOBNO,
                                                        RC_BILL_NO,
                                                        RC_BILL_MONTH,
                                                        RC_BILL_AMOUNT,
                                                        to_date( RC_BILL_DUEDATE, ''mm/dd/yyyy''),
                                                        ''READYTOGO'',
                                                        ''admin'',
                                                        current_timestamp,
                                                        ''admin'',
                                                        current_timestamp                                           
                                                        FROM load_crystal_customer_ext';
     EXECUTE IMMEDIATE v_query3;
     COMMIT;
     
     
     UPDATE crs_raw_customer2
     SET rc_rung_seq = crs_seq_cust.NEXTVAL
     where RC_BATCH_ID = v_batch_date;          
     COMMIT;
          
     BEGIN
       reset_sequence('crs_seq_cust',1);
     END;
     
     -- Inserting data into crs_summary table
     /*
     insert into crs_summary
     select v_batch_date, 'crystal', count(1), 'Fileloaded',
            'admin', current_timestamp,
            'admin', current_timestamp
     from crs_raw_customer;
     
    /* SELECT DISTINCT(rc_cust_id) INTO v_client_id
     FROM crs_raw_customer
     WHERE rc_batch_id = v_batch_date AND ROWNUM=1; */
     /*
        
     SELECT cc_early_incentive_amt INTO v_inc_amt
     FROM client_config
     WHERE cc_client_id = 1;
     
     UPDATE crs_raw_customer
     SET rc_bill_amount = rc_bill_amount - v_inc_amt
     WHERE rc_batch_id = v_batch_date;
     
     INSERT INTO CRS_Final_CUSTOMER(FC_BATCH_ID,
                                          FC_CUST_ID,
                                          FC_CUST_NAME,
                                          FC_CUST_VPA,
                                          FC_CUST_MOBNO,
                                          FC_BILL_NO,
                                          FC_BILL_MONTH,
                                          FC_BILL_AMOUNT,
                                          FC_BILL_DUEDATE,
                                          FC_STATUS,
                                          FC_CREATED_BY,
                                          FC_CREATED_DATE,
                                          FC_UPDATED_BY,
                                          FC_UPDATED_DATE     
                                   )
                                   SELECT RC_BATCH_ID,
                                          RC_CUST_ID,
                                          RC_CUST_NAME,
                                          RC_CUST_VPA,
                                          RC_CUST_MOBNO,
                                          RC_BILL_NO,
                                          RC_BILL_MONTH,
                                          RC_BILL_AMOUNT,
                                          RC_BILL_DUEDATE,
                                          RC_STATUS,
                                          RC_CREATED_BY,
                                          RC_CREATED_DATE,
                                          RC_UPDATED_BY,
                                          RC_UPDATED_DATE
                                          FROM crs_raw_customer where rc_batch_id = v_batch_date;
                                          */
     
     COMMIT;
     
     v_query2 := 'DROP TABLE load_crystal_customer_ext';
     
     EXECUTE IMMEDIATE v_query2;
     
     msg := 'File Loaded Successfully';
EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to Load Crystal_customer_File';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_LOAD_CRYSTAL_CUSTOMER_TEST
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_LOAD_CRYSTAL_CUSTOMER_TEST" ( msg   OUT VARCHAR2 ) AS
     v_query    VARCHAR2(10000);
     v_query2   VARCHAR2(4000);
     v_query3   VARCHAR2(10000);
     v_filename1 varchar2(100);
     v_filename2 varchar2(100);
     v_filename3 varchar2(100);
     v_filename varchar2(100);
     v_count number :=1;
BEGIN

     LOOP
     SELECT TO_CHAR(SYSDATE, 'ddmmyyyy') INTO v_filename1
     FROM dual;   
     
     v_filename2 := '_'||v_count;
     
     v_filename3 := 'MER_'||v_filename1||v_filename2||'.csv';
     
     dbms_output.put_line(v_filename3);
     msg := 'Success';
     
      v_count := v_count +1;

   --  v_filename := 'crystal.csv';
     v_query := 'create table load_crystal_customer_ext2(
                RC_CUST_ID	NUMBER,
                RC_CUST_NAME	VARCHAR2(200 BYTE),
                RC_CUST_VPA	VARCHAR2(200 BYTE),
                RC_CUST_MOBNO	NUMBER,
                RC_BILL_NO	VARCHAR2(200 BYTE),
                RC_BILL_MONTH	VARCHAR2(200 BYTE),
                RC_BILL_AMOUNT	NUMBER(38,2),
                RC_BILL_DUEDATE	varchar2(200)
                )
                  organization external (
                    type oracle_loader
                    default directory WES_DIR  
                    access parameters (
                    RECORDS DELIMITED BY NEWLINE 
                    fields terminated by '','' )
                   location (''' || v_filename3 || ''')) parallel reject limit unlimited';     
     BEGIN  
        
     EXECUTE IMMEDIATE v_query;
     exception when others then
     continue;
     
     END;
     v_query3 := 'INSERT INTO raw_customer2(RC_BATCH_ID,
                                          RC_CUST_ID,
                                          RC_CUST_NAME,
                                          RC_CUST_VPA,
                                          RC_CUST_MOBNO,
                                          RC_BILL_NO,
                                          RC_BILL_MONTH,
                                          RC_BILL_AMOUNT,
                                          RC_BILL_DUEDATE,
                                          RC_STATUS,
                                          RC_CREATED_BY,
                                          RC_CREATED_DATE,
                                          RC_UPDATED_BY,
                                          RC_UPDATED_DATE    
                                             ) SELECT   1,
                                             RC_CUST_ID,
                                              RC_CUST_NAME,
                                              RC_CUST_VPA,
                                              RC_CUST_MOBNO,
                                              RC_BILL_NO,
                                              RC_BILL_MONTH,
                                              RC_BILL_AMOUNT,
                                              to_date( RC_BILL_DUEDATE, ''mm/dd/yyyy''),
                                              ''INITIAL'',
                                              ''admin'',
                                              current_timestamp,
                                              ''admin'',
                                              current_timestamp                                           
                                              FROM load_crystal_customer_ext2';
     EXECUTE IMMEDIATE v_query3;
     COMMIT;
     v_query2 := 'DROP TABLE load_crystal_customer_ext2';
     EXECUTE IMMEDIATE v_query2;
     msg := 'Success';  
     exit when v_count >3;
     END LOOP;   
EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_PROCESS_CRYSTAL_CUSTOMER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_PROCESS_CRYSTAL_CUSTOMER" ( p_batch_id IN VARCHAR2,
                                                         p_client_id  IN NUMBER,
                                                         p_mob_no in number,
                                                         p_rung_seq in number,
                                                         msg   OUT VARCHAR2 ) AS
                                                         pragma autonomous_transaction;

     v_inc_amt NUMBER(38,2) :=0;
     v_bill_amt NUMBER(38,2) :=0;
     v_daysinc_valid NUMBER :=0;
     v_daysafter_due NUMBER(38,2):=0;
     v_late_percnt NUMBER(38,2):=0;
     v_due_date date;
    
BEGIN
   
      -- dbms_output.put_line('line 21');
     
     SELECT early_incentive_amt, DAYSINCENTIVE_VALID, DAYSAFTER_DUEDATE, LATEFEE_PERCNT
     INTO v_inc_amt, v_daysinc_valid, v_daysafter_due, v_late_percnt
     FROM crs_client_config
     WHERE cid = p_client_id;
     
     --dbms_output.put_line('line 21');   
          
     select fc_bill_duedate, fc_bill_amount into v_due_date, v_bill_amt
     from crs_final_customer 
     where FC_CUST_ID = p_client_id
     and FC_BATCH_ID = p_batch_id
     and fc_cust_mobno = p_mob_no
     and FC_RUNG_SEQ = p_rung_seq
     and rownum =1;
     
      --dbms_output.put_line('line 34');
     
     
     --select LAST_DAY(trunc(sysdate)) into v_lastday from dual;
     
     IF trunc(sysdate) < (v_due_date- v_daysinc_valid) then
     
     --dbms_output.put_line('first if');
     
     UPDATE crs_final_customer
     SET FC_CALC_AMT = v_bill_amt - v_inc_amt,
         fc_bill_stage  = 'Early',
         fc_bill_stage_duedate  = (v_due_date- v_daysinc_valid),
         fc_status = 'READYTOGO',
         fc_updated_date = current_timestamp
     WHERE fc_batch_id = p_batch_id
           AND fc_cust_id = p_client_id
           AND fc_cust_mobno = p_mob_no
           and FC_RUNG_SEQ = p_rung_seq; commit;
     
     ELSIF trunc(sysdate ) > (v_due_date - v_daysinc_valid) and trunc(sysdate )  <= (v_due_date) then
     --dbms_output.put_line('2 if');
     
     UPDATE crs_final_customer
     SET FC_CALC_AMT = v_bill_amt,
         fc_bill_stage  = 'Normal',
         fc_bill_stage_duedate  = v_due_date,
         fc_status = 'READYTOGO',
         fc_updated_date = current_timestamp
     WHERE fc_batch_id = p_batch_id
           AND fc_cust_id = p_client_id
           AND fc_cust_mobno = p_mob_no
           and FC_RUNG_SEQ = p_rung_seq;
           --AND fc_status IN ('INITIAL', 'FAILED'); 
           commit;
           
           dbms_output.put_line('2.5 if');
     
     ELSIF trunc(sysdate) > (v_due_date) and trunc(sysdate) <= (v_due_date + v_daysafter_due) then
    -- dbms_output.put_line('3 if');
     
     UPDATE crs_final_customer
     SET FC_CALC_AMT = v_bill_amt + (v_late_percnt/100)*v_bill_amt,
         fc_bill_stage  = 'Late',
         fc_bill_stage_duedate  = (v_due_date+v_daysafter_due),
         fc_status = 'READYTOGO',
         fc_updated_date = current_timestamp
     WHERE fc_batch_id = p_batch_id
           AND fc_cust_id = p_client_id
           AND fc_cust_mobno = p_mob_no
           and FC_RUNG_SEQ = p_rung_seq;
           --AND fc_status IN ('INITIAL', 'FAILED');
           commit; 
           
           else
           null;
     
     END IF;     
     
     COMMIT;
     msg := 'Updated Successfully';

     
EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to Update';
          raise_application_error(
               -20001,
               'An error was encountered - ' || SQLCODE || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_REFCURSOR
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_REFCURSOR" (P_BANK_CODE VARCHAR2, P_DATA_REFCUR OUT SYS_REFCURSOR)
AS 
BEGIN
  
  OPEN P_DATA_REFCUR FOR SELECT BANK_NAME, STATE_NAME, BR_STATE_CODE, CITY_NAME, BR_CITY_CODE 
  FROM BANK_STATE_CITY_MASTER
  WHERE BANK_CODE = P_BANK_CODE;
  
  DBMS_OUTPUT.PUT_LINE( 'BANK_NAME'|| '          '|| 'STATE_NAME'|| '        '|| 'CITY_NAME' );
  DBMS_OUTPUT.PUT_LINE( '----------------------------------------------------------------'  );
  
  FOR R IN (SELECT * FROM BANK_STATE_CITY_MASTER WHERE BANK_CODE= P_BANK_CODE)
  
  LOOP
 
  DBMS_OUTPUT.PUT_LINE( R.BANK_NAME|| '  '|| R.STATE_NAME|| '   '|| R.CITY_NAME  );
  
  NULL;
  
  END LOOP;
  
  

  EXCEPTION
  WHEN OTHERS THEN
  RAISE_APPLICATION_ERROR(-20001,'An error was encountered - ' || SQLCODE || ' -ERROR- ' || SQLERRM);
  
END P_REFCURSOR;


--select * from bank_state_city_master;

/
--------------------------------------------------------
--  DDL for Procedure P_RET_COMM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_RET_COMM" (FDT IN VARCHAR2,TDT IN VARCHAR2,RES OUT VARCHAR2)
AS
BEGIN

EXECUTE IMMEDIATE 'truncate table com_ip_collected';

EXECUTE IMMEDIATE 'truncate table com_susp_summary';
-- LOADING BLOCK
BEGIN

INSERT INTO COM_IP_COLLECTED(CIC_RET_MOB,CIC_CUST_MOB)
select r.rtr_mob_no cic_ret_mob,  c.cust_mob  cic_cust_mob  from recon_rtr_imp_wes r, brs_ctr_wes c where  r.txn_date = '28/02/2017' and c.txn_dt = '28/02/2017'
and r.txn_id =  c.txn_typ and txn_type = 'IP Collected' and abs(txn_amt) >=100 and txn_stat = 'SUCCESS';

INSERT INTO COM_SUSP_SUMMARY
select amob css_mob, count(bmob) css_cnt from (
select distinct  a.txn_typ,a.cust_mob amob,b.cust_mob bmob from brs_ctr_wes a, brs_ctr_wes b  where a.txn_dt = '28/02/2017' and a.txn_id = 'P2P' and b.txn_dt = '28/02/2017'
and b.txn_id = 'P2P'  and a.txn_typ = b.txn_typ and a.cust_mob <> b.cust_mob and a.cust_txn_amnt > 0
order by a.txn_typ,a.cust_mob)
group by amob;



END;
-- END OF LOADING BLOCK





END;

/
--------------------------------------------------------
--  DDL for Procedure P_UPDATE_CRS_CUSTOMER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_UPDATE_CRS_CUSTOMER" ( p_batch_id IN varchar2,
                                                    p_rung_seq in number,
                                                    p_cust_vpa in varchar2,
                                                    p_status in varchar2,
                                                    p_remark in varchar2,
                                                    p_amt_received in number,
                                                    msg   OUT VARCHAR2 ) as
                                                    pragma autonomous_transaction;
                                                                                                       
                                                    
     
     v_batch_date varchar2(200);
     v_inc_amt number(38,2);
     v_client_id number;
BEGIN


     UPDATE CRS_Final_CUSTOMER
     SET FC_STATUS = p_status,
         FC_REMARK = p_remark,
         FC_RECEIVED_AMT = p_amt_received,
         fc_updated_date = current_timestamp
     WHERE FC_BATCH_ID = p_batch_id
     and FC_RUNG_SEQ = p_rung_seq
     and FC_CUST_VPA = p_cust_vpa;     
     
     COMMIT;
     
     msg := 'Updated Successfully';
EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to Update';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_UPDATE_CRS_CUSTOMER_PUSH
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_UPDATE_CRS_CUSTOMER_PUSH" ( p_batch_id IN varchar2,
                                                    p_rung_seq in number,
                                                    p_cust_vpa in varchar2,
                                                    p_status in varchar2,
                                                    p_remark in varchar2,
                                                   -- p_amt_received in number,
                                                    msg   OUT VARCHAR2 ) as
                                                    pragma autonomous_transaction;
                                                                                                       
                                                    
     
     v_batch_date varchar2(200);
     v_inc_amt number(38,2);
     v_client_id number;
BEGIN
    --- updates table crs_customer_push with new status from UPI

     UPDATE crs_customer_push
     SET CC_STATUS = p_status,
         CC_REMARK = p_remark,
         --CC_RECEIVED_AMT = p_amt_received,
         CC_UPDATED_DATE = current_timestamp
     WHERE CC_BATCH_ID = p_batch_id
     and CC_RUNG_SEQ = p_rung_seq
     and CC_CUSTOMER_VPA = p_cust_vpa;     
     
     COMMIT;
     
     msg := 'Success';
EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_UPDATE_CRS_ERR_CUSTOMER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_UPDATE_CRS_ERR_CUSTOMER" ( p_batch_id IN varchar2,
                                                    p_rung_seq in number,
                                                    p_cust_vpa in varchar2,
                                                    p_status in varchar2,
                                                    p_remark in varchar2,
                                                  --  p_amt_received in number,
                                                    msg   OUT VARCHAR2 ) AS
                                                       pragma autonomous_transaction;
     
     v_batch_date  varchar2(200);
     v_inc_amt number(38,2);
     v_client_id number;
BEGIN
-- Procedure update Error Records
     UPDATE CRS_Final_CUSTOMER
     SET FC_STATUS = p_status,
         FC_REMARK = p_remark,
    --     FC_RECEIVED_AMT = p_amt_received,
         fc_updated_date = current_timestamp
     WHERE FC_BATCH_ID = p_batch_id
     and FC_RUNG_SEQ = p_rung_seq
     and FC_CUST_VPA = p_cust_vpa;     
     
     COMMIT;
     
     msg := 'Updated Successfully';
EXCEPTION
     WHEN OTHERS THEN
          msg := 'Failed to Update';
          raise_application_error(
               -20001,
               'An error was encountered - ' || sqlcode || ' -ERROR- ' || sqlerrm
          );
END;

/
--------------------------------------------------------
--  DDL for Procedure P_WES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."P_WES" (ename varchar2)
as
wdate date;
wes varchar2(50);
begin
wdate:= to_char(sysdate,'ddmonyyyy');
wes:=wdate||'_'||w_seq.nextval;

insert into emp(emp_id,emp_name) values (wes,ename);
commit;

end;

/
--------------------------------------------------------
--  DDL for Procedure RESET_SEQUENCE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."RESET_SEQUENCE" (
seq_name IN VARCHAR2, startvalue IN PLS_INTEGER) AS
 cval   INTEGER;
 inc_by VARCHAR2(25);
BEGIN
  EXECUTE IMMEDIATE 'ALTER SEQUENCE ' ||seq_name||' MINVALUE 0';

  EXECUTE IMMEDIATE 'SELECT ' ||seq_name ||'.NEXTVAL FROM dual' 
  INTO cval;

  cval := cval - startvalue + 1;
  IF cval < 0 THEN 
    inc_by := ' INCREMENT BY ';
    cval:= ABS(cval); 
  ELSE
    inc_by := ' INCREMENT BY -';
  END IF; 

  EXECUTE IMMEDIATE 'ALTER SEQUENCE ' || seq_name || inc_by || cval;

  EXECUTE IMMEDIATE 'SELECT ' ||seq_name ||'.NEXTVAL FROM dual'
  INTO cval;

  EXECUTE IMMEDIATE 'ALTER SEQUENCE ' || seq_name || ' INCREMENT BY 1';
END reset_sequence;

/
--------------------------------------------------------
--  DDL for Procedure TEST_EXT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "RECON_USR"."TEST_EXT" (msg out varchar2) AS 

v_query varchar2(10000);
v_query2 varchar2(10000);

begin
v_query := 'create table emp_external3 (
      EMPID     NUMBER,
      EMPNAME      varchar2(50)
)
organization external (
  type oracle_loader
  default directory WES_DIR  
  access parameters (fields terminated by '','' )
  location (''emp.csv'')
)
reject limit unlimited';

execute immediate v_query;
msg:= 'Sucessfully loaded';


execute immediate 'insert into emp_internal1(empid, empname)( select empid, empname from emp_external3)';

commit;


V_Query2 := 'drop table emp_external3';

execute immediate v_query2;

EXCEPTION
WHEN OTHERS THEN
msg:='failed to load';
raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);  
END;

/
--------------------------------------------------------
--  DDL for Function CHECK_URL
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "RECON_USR"."CHECK_URL" (p_url IN VARCHAR2)
    RETURN VARCHAR2
IS
    v_request UTL_HTTP.req;
    v_respond UTL_HTTP.resp;
    v_text    VARCHAR2(1024);
BEGIN
    v_request := UTL_HTTP.begin_request(p_url);

    v_respond := UTL_HTTP.get_response(v_request);

    UTL_HTTP.end_response(v_respond);
    RETURN 'YES';
EXCEPTION
    WHEN OTHERS
    THEN
        DBMS_OUTPUT.put_line(SQLERRM);
        RETURN 'NO';
END;

/
--------------------------------------------------------
--  DDL for Function DUMP_CSV
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "RECON_USR"."DUMP_CSV" ( p_query IN VARCHAR2, 
 p_separator IN VARCHAR2 DEFAULT ',', 
 p_dir IN VARCHAR2 , 
 p_filename IN VARCHAR2 ) 
 RETURN NUMBER 
 IS 
 l_output utl_file.file_type; 
 l_thecursor INTEGER DEFAULT dbms_sql.open_cursor; 
 l_columnvalue VARCHAR2(12000); 
 l_status INTEGER; 
 l_colcnt NUMBER DEFAULT 0; 
 l_separator VARCHAR2(10) DEFAULT ''; 
 l_cnt NUMBER DEFAULT 0; 
 BEGIN 
 l_output := utl_file.fopen( p_dir, p_filename, 'A' ); 

 dbms_sql.parse( l_thecursor, p_query, 
 dbms_sql.NATIVE ); 

 FOR I IN 1 .. 255 LOOP 
 BEGIN 
 dbms_sql.define_column( l_thecursor, I, 
 l_columnvalue, 12000 ); 
 l_colcnt := I; 
 EXCEPTION 
 WHEN OTHERS THEN 
 IF ( SQLCODE = -1007 ) THEN EXIT; 
 ELSE 
 RAISE; 
 END IF; 
 END; 
 END LOOP; 

 dbms_sql.define_column( l_thecursor, 1, l_columnvalue, 12000 ); 

 l_status := dbms_sql.EXECUTE(l_thecursor); 

 LOOP 
 EXIT WHEN ( dbms_sql.fetch_rows(l_thecursor) <= 0 ); 
 l_separator := ''; 
 FOR I IN 1 .. l_colcnt LOOP 
 dbms_sql.COLUMN_VALUE( l_thecursor, I, 
 l_columnvalue ); 
 utl_file.put( l_output, 
 l_separator || l_columnvalue ); 
 l_separator := p_separator; 
 END LOOP; 
 utl_file.new_line( l_output ); 
 l_cnt := l_cnt+1; 
 END LOOP; 
 dbms_sql.close_cursor(l_thecursor); 

 utl_file.fclose( l_output ); 
 RETURN l_cnt; 
 END dump_csv; 

/
