package com.abipb.invoiceportal.util;

/**
 * Created by abhay.kumar-v on 11/8/2017.
 */
public enum ErrorCodeEnum {

   UPI_SCCESS_CODE("CMWUPI00"), UPI_NOT_INITIATED_CODE("CMWUPI01"),UPI_PENDING_CODE("CMWUPI02"),UPI_FAILURE_CODE("CMWUPI03");
   ErrorCodeEnum(String errorCode) { this.errorCode = errorCode;}

    private final String errorCode;

    public String getErrorCode() {
        return this.errorCode;
    }
}
