package com.abipb.invoiceportal.util;

import com.abipb.invoiceportal.dto.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;


/**
 * Created by abhay.kumar-v on 10/25/2017.
 */

@Component
public class PaymentBuilder {

    private static final Logger log= LoggerFactory.getLogger(PaymentBuilder.class);
    @Autowired
    private PropertiesValue propertiesValue;

    public  String buildPaymentQueryRequest(PaymentQueryRequest paymentQueryRequest){
        if(!StringUtils.isEmpty(paymentQueryRequest)){
            StringBuilder messageRequestBuilder=new StringBuilder();
            messageRequestBuilder.append(paymentQueryRequest.getRequestType())
                    .append(PGConstants.PG_MSG_SEPARATOR);//RequestType
            messageRequestBuilder.append(paymentQueryRequest.getMerchantId())
                    .append(PGConstants.PG_MSG_SEPARATOR);//Merchant ID
            messageRequestBuilder.append(paymentQueryRequest.getCustomerId()).
                    append(PGConstants.PG_MSG_SEPARATOR);//Customer Id
            messageRequestBuilder.append(paymentQueryRequest.getCurrentDateTimestamp());
            log.debug("Before checksum request msg" + messageRequestBuilder.toString());

//            Checksum
            String checksum = PGCheckSumConverter.HmacSHA256(messageRequestBuilder.toString(),propertiesValue.getPgChecksumKey());
            log.debug("Computed Checksum" + checksum);

            messageRequestBuilder.append(PGConstants.PG_MSG_SEPARATOR);
            messageRequestBuilder.append(checksum);

            log.debug("After checksum request msg" + messageRequestBuilder.toString());
                 return messageRequestBuilder.toString();
        }
        return PGConstants.BLANK_STRING;
    }

    public PaymentQueryResponse buildPaymentQueryResponse(String  messageResponse){
        if(!StringUtils.isEmpty(messageResponse)){
            messageResponse=messageResponse.trim();
            PaymentQueryResponse paymentQueryResponse=new PaymentQueryResponse();
            log.debug("messageResponse :: "+messageResponse);
            String [] data =messageResponse.split(PGConstants.PG_MSG_RESPONSE_SEPARATOR);
            if(data.length==33) {
                paymentQueryResponse.setRequestType(data[0]);//RequestType
                paymentQueryResponse.setMerchantId(data[1]);//Merchant ID
                paymentQueryResponse.setCustomerId(data[2]);//Customer Id
                paymentQueryResponse.setTxnReferenceNo(data[3]);//TxnReferenceNo
                paymentQueryResponse.setBankReferenceNo(data[4]);//BankReferenceNO
                paymentQueryResponse.setTxnAmount(data[5]);//TxnAmount
                paymentQueryResponse.setBankId(data[6]);//BankID
                paymentQueryResponse.setBankMarchantId(data[7]);//BankMerchantID
                paymentQueryResponse.setTxnType(data[8]);//TxnType
                paymentQueryResponse.setCurrencyName(data[9]);//CurrencyName
                paymentQueryResponse.setItemCode(data[10]);//ItemCode
                paymentQueryResponse.setSecurityType(data[11]);//SecurityType
                paymentQueryResponse.setSecurityId(data[12]);//SecurityID
                paymentQueryResponse.setSecurityPassword(data[13]);//SecurityPassword
                paymentQueryResponse.setTxnType(data[14]);//TxnDate check with venkat
                paymentQueryResponse.setAuthStatus(data[15]);//AuthStatus
                paymentQueryResponse.setSettlementType(data[16]);//SettlementType
                paymentQueryResponse.setAdditionalInfo1(data[17]);//AdditionalInfo1
                paymentQueryResponse.setAdditionalInfo1(data[18]);//AdditionalInfo2
                paymentQueryResponse.setAdditionalInfo1(data[19]);//AdditionalInfo3
                paymentQueryResponse.setAdditionalInfo1(data[20]);//AdditionalInfo4
                paymentQueryResponse.setAdditionalInfo1(data[21]);//AdditionalInfo5
                paymentQueryResponse.setAdditionalInfo1(data[22]);//AdditionalInfo6
                paymentQueryResponse.setAdditionalInfo1(data[23]);//AdditionalInfo7
                paymentQueryResponse.setErrorStatus(data[24]);//ErrorStatus
                paymentQueryResponse.setErrorDescription(data[25]);//ErrorDescription
                paymentQueryResponse.setFiller1(data[26]);//Filler1
                paymentQueryResponse.setRefeundStatus(data[27]);//RefundStatus
                paymentQueryResponse.setTotalRefundAmount(data[28]);//TotalRefundAmount
                paymentQueryResponse.setLastRefundDate(data[29]);//LastRefundDate
                paymentQueryResponse.setLastRefundRefNo(data[30]);// LastRefundRefNo
                paymentQueryResponse.setQueryStatus(data[31]);//QueryStatus
                paymentQueryResponse.setChecksum(data[32]);//Checksum

                paymentQueryResponse.setMsg(messageResponse);
                return paymentQueryResponse;
            }
        }
        return null;
    }

    public   String buildPaymentRefundRequest(PaymentRefundRequest paymentRefundRequest){
        if(!StringUtils.isEmpty(paymentRefundRequest)){
            StringBuilder messageRequestBuilder=new StringBuilder();
            messageRequestBuilder.append(paymentRefundRequest.getRequestType())
                    .append(PGConstants.PG_MSG_SEPARATOR);//RequestType
            messageRequestBuilder.append(paymentRefundRequest.getMerchantId())
                    .append(PGConstants.PG_MSG_SEPARATOR);//Merchant ID
            messageRequestBuilder.append(paymentRefundRequest.getTxnReferenceNo())
                    .append(PGConstants.PG_MSG_SEPARATOR);//TxnReferenceNo
            messageRequestBuilder.append(paymentRefundRequest.getTxnDate())
                    .append(PGConstants.PG_MSG_SEPARATOR);//TxnDate
            messageRequestBuilder.append(paymentRefundRequest.getCustomerId())
                    .append(PGConstants.PG_MSG_SEPARATOR);//Customer Id
            messageRequestBuilder.append(paymentRefundRequest.getTxnAmount())
                    .append(PGConstants.PG_MSG_SEPARATOR);//TxnAmount
            messageRequestBuilder.append(paymentRefundRequest.getRefAmount())
                    .append(PGConstants.PG_MSG_SEPARATOR);//RefAmount
            messageRequestBuilder.append(paymentRefundRequest.getRefDataTime())
                    .append(PGConstants.PG_MSG_SEPARATOR);//RefDateTime
            messageRequestBuilder.append(paymentRefundRequest.getMerchantRefNo())
                    .append(PGConstants.PG_MSG_SEPARATOR);//MerchantRefNo
            messageRequestBuilder.append(paymentRefundRequest.getFiller1())
                    .append(PGConstants.PG_MSG_SEPARATOR);//Filler1
            messageRequestBuilder.append(paymentRefundRequest.getFiller2())
                    .append(PGConstants.PG_MSG_SEPARATOR);//Filler2
            messageRequestBuilder.append(paymentRefundRequest.getFiller3());//Filler3
//for checksum
            String checkSum=PGCheckSumConverter.HmacSHA256(messageRequestBuilder.toString(),propertiesValue.getPgChecksumKey());
            paymentRefundRequest.setChecksum(checkSum);;
            messageRequestBuilder.append(PGConstants.PG_MSG_SEPARATOR);
            messageRequestBuilder.append(paymentRefundRequest.getChecksum());//Checksum
            System.out.println("Message Request : "+messageRequestBuilder.toString());
            return messageRequestBuilder.toString();
        }
        return PGConstants.BLANK_STRING;
    }

    public PaymentRefundResponse buildPaymentRefundResponse(String messageResponse){

        if(!StringUtils.isEmpty(messageResponse)){
            messageResponse=messageResponse.trim();
            PaymentRefundResponse paymentRefundResponse=new PaymentRefundResponse();
            log.debug("messageResponse :: "+messageResponse);
            String [] data =messageResponse.split(PGConstants.PG_MSG_RESPONSE_SEPARATOR);
            if(data.length==14) {
                paymentRefundResponse.setRequestType(data[0]);//RequestType
                paymentRefundResponse.setMerchantId(data[1]);//Merchant ID
                paymentRefundResponse.setTxnReferenceNo(data[2]);//TxnReferenceNo
                paymentRefundResponse.setTxnDate(data[3]);//TxnDate
                paymentRefundResponse.setCustomerId(data[4]);//Customer Id
                paymentRefundResponse.setTxnAmount(data[5]);//TxnAmount
                paymentRefundResponse.setRefAmount(data[6]);//RefAmount
                paymentRefundResponse.setRefDateTime(data[7]);//RefDateTime
                paymentRefundResponse.setRefStatus(data[8]);//RefStatus
                paymentRefundResponse.setRefundId(data[9]);//RefundId
                paymentRefundResponse.setErrorCode(data[10]);//ErrorCode
                paymentRefundResponse.setErrorReason(data[11]);//ErrorReason
                paymentRefundResponse.setProcessStatus(data[12]);//ProcessStatus
                paymentRefundResponse.setChecksum(data[13]);//Checksum
                return  paymentRefundResponse;
            }
        }
       return null;
    }

    public static Server2ServerDirectResponse bulidServer2ServerDirectResponse(String messageResponse){
        if(!StringUtils.isEmpty(messageResponse)){
            messageResponse=messageResponse.trim();
            Server2ServerDirectResponse server2ServerDirectResponse=new Server2ServerDirectResponse();
            log.debug("messageResponse :: "+messageResponse);
            String [] data =messageResponse.split(PGConstants.PG_MSG_RESPONSE_SEPARATOR);
            if(data.length==33) {
                 server2ServerDirectResponse.setMerchantId(data[1]);//Merchant ID
                server2ServerDirectResponse.setCustomerId(data[2]);//Customer Id
                server2ServerDirectResponse.setTxnReferenceNo(data[3]);//TxnReferenceNo
                server2ServerDirectResponse.setBankReferenceNo(data[4]);//BankReferenceNO
                server2ServerDirectResponse.setTxnAmount(data[5]);//TxnAmount
                server2ServerDirectResponse.setBankId(data[6]);//BankID
                server2ServerDirectResponse.setBankMerchantId(data[7]);//BankMerchantID
                server2ServerDirectResponse.setTxnType(data[8]);//TxnType
                server2ServerDirectResponse.setCurrencyName(data[9]);//CurrencyName
                server2ServerDirectResponse.setItemCode(data[10]);//ItemCode
                server2ServerDirectResponse.setSecurityType(data[11]);//SecurityType
                server2ServerDirectResponse.setSecurityId(data[12]);//SecurityID
                server2ServerDirectResponse.setSecurityPassword(data[13]);//SecurityPassword
                server2ServerDirectResponse.setTxnType(data[14]);//TxnDate check with venkat
                server2ServerDirectResponse.setAuthStatus(data[15]);//AuthStatus
                server2ServerDirectResponse.setSettlementType(data[16]);//SettlementType
                server2ServerDirectResponse.setAdditionalInfo1(data[17]);//AdditionalInfo1
                server2ServerDirectResponse.setAdditionalInfo1(data[18]);//AdditionalInfo2
                server2ServerDirectResponse.setAdditionalInfo1(data[19]);//AdditionalInfo3
                server2ServerDirectResponse.setAdditionalInfo1(data[20]);//AdditionalInfo4
                server2ServerDirectResponse.setAdditionalInfo1(data[21]);//AdditionalInfo5
                server2ServerDirectResponse.setAdditionalInfo1(data[22]);//AdditionalInfo6
                server2ServerDirectResponse.setAdditionalInfo1(data[23]);//AdditionalInfo7
                server2ServerDirectResponse.setErrorStatus(data[24]);//ErrorStatus
                server2ServerDirectResponse.setErrorDescription(data[25]);//ErrorDescription
                server2ServerDirectResponse.setChecksum(data[32]);//Checksum
                return server2ServerDirectResponse;
            }
        }
        return null;
    }
}

