package com.abipb.invoiceportal.util;


import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

//import com.abipb.middleware.binding.nach.mandate.amendment.Document;  //amendment ,create
//import com.abipb.middleware.binding.nach.mandate.cancellation.Document;  //cancellation ,create
//import com.abipb.middleware.binding.nach.mandate.acceptance.Document;  //Ack,response for (create,amendment,cancellation)

public class XmlToObject {
    public static Object convertXmlToObject(String packCategory,String xsdFilePath,File file) {
        Object document=null;
       try {
      /*  Schema schema= SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(new File("C:\\Users\\Penchalaiah.g-V\\IdeaProjects\\paymentmodes\\src\\main\\resources\\xsd\\nach\\mandate_initiation_pain.009.001.01.xsd"));*/
           Schema schema= SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(new File(xsdFilePath));
      //  JAXBContext jaxbContext=JAXBContext.newInstance("com.abipb.middleware.binding.nach.mandate.initiation");
           JAXBContext jaxbContext=JAXBContext.newInstance(packCategory);

        Unmarshaller unmarshaller=jaxbContext.createUnmarshaller();
        unmarshaller.setSchema(schema);
       // JAXBElement<Object> documentJAXBElement=(JAXBElement<Object> )unmarshaller.unmarshal(new File("D:/uploadFiles/MMS-CREATE-SBIN-SBIN1234-07062014-000003-INP.xml"));
           JAXBElement<Object> documentJAXBElement=(JAXBElement<Object> )unmarshaller.unmarshal(file);

           document=documentJAXBElement.getValue();


            }catch (JAXBException e){
        e.printStackTrace();
    }catch (SAXException s){
        s.printStackTrace();
    }

    return  document;
}




    }

