package com.abipb.invoiceportal.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Bookmarks {
    @Value("${middleware.application.server.url}")
    private String middlewareApplicationUrl;

    @Value("${upi.interface.endpoint}")
    private String upiInterfaceURL;

    @Value("${pg.payment.billdesk.query.endpoint}")
    private String billdeskQueryURL;

    @Value("${pg.payment.billdesk.refund.endpoint}")
    private String billdeskRefundURL;

    @Value("${cbs.fundtransfer.intrabank.endpoint}")
    private String intraBankFundTransferURL;

    @Value("${cbs.fundtransfer.interbank.endpoint}")
    private String interBankFundTransferURL;

    @Value("${cbs.otpgenerator.endpoint}")
    private String otpGeneratorURL;

    public String getIntraBankFundTransferURL() {
        return intraBankFundTransferURL;
    }

    public void setIntraBankFundTransferURL(String intraBankFundTransferURL) {
        this.intraBankFundTransferURL = intraBankFundTransferURL;
    }

    public String getInterBankFundTransferURL() {
        return interBankFundTransferURL;
    }

    public void setInterBankFundTransferURL(String interBankFundTransferURL) {
        this.interBankFundTransferURL = interBankFundTransferURL;
    }

    public String getOtpGeneratorURL() {
        return otpGeneratorURL;
    }

    public void setOtpGeneratorURL(String otpGeneratorURL) {
        this.otpGeneratorURL = otpGeneratorURL;
    }

    public String getUpiInterfaceURL() {
        return upiInterfaceURL;
    }

    public void setUpiInterfaceURL(String upiInterfaceURL) {
        this.upiInterfaceURL = upiInterfaceURL;
    }

    public String getMiddlewareApplicationUrl() {
        return middlewareApplicationUrl;
    }

    public void setMiddlewareApplicationUrl(String middlewareApplicationUrl) {
        this.middlewareApplicationUrl = middlewareApplicationUrl;
    }

    public String getBilldeskQueryURL() {
        return billdeskQueryURL;
    }

    public void setBilldeskQueryURL(String billdeskQueryURL) {
        this.billdeskQueryURL = billdeskQueryURL;
    }

    public String getBilldeskRefundURL() {
        return billdeskRefundURL;
    }

    public void setBilldeskRefundURL(String billdeskRefundURL) {
        this.billdeskRefundURL = billdeskRefundURL;
    }
}
