package com.abipb.invoiceportal.util;

/**
 * Created by Venkatesh.R on 10/15/2017.
 */
public enum PGResponseEnum {

        PRIMARY_RESPONSE("P"), SECONDRY_RESPONSE("S");

        PGResponseEnum(String responseType) { this.responseType = responseType;}

        private final String responseType;

        public String getResponseType() {
                return responseType;
        }
}
