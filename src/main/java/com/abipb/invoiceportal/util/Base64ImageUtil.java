package com.abipb.invoiceportal.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.Base64;

/**
 * Created by abhay.kumar-v on 12/14/2017.
 */
@Component
public class Base64ImageUtil {
    private static final Logger log= LoggerFactory.getLogger(Base64ImageUtil.class);


    public static String encodeImage(String filePathWithName){
        String base64ImageEncode="";
        if (StringUtils.isEmpty(filePathWithName)) {
            return base64ImageEncode;
        }
        File file= new File(filePathWithName);
        try (FileInputStream imageInFile=new FileInputStream(file)){
            byte[] imageData=new  byte[(int)file.length()];
            imageInFile.read(imageData);
            base64ImageEncode= Base64.getEncoder().encodeToString(imageData);
        }catch (FileNotFoundException fnfe){
            log.error("FileNotFoundException ",fnfe);
        }catch (IOException ioe){
            log.error("IOException ",ioe);

        }


        return base64ImageEncode;
    }

    public static  void decoderImage(String base64ImageEncode, String filePathWithName) {

        if (StringUtils.isEmpty(base64ImageEncode) && StringUtils.isEmpty(filePathWithName)){
            log.debug("Base64Image Encode : ",base64ImageEncode,"  File Path with Name ",filePathWithName);
            return;
        }
        try (FileOutputStream imageOutFile = new FileOutputStream(filePathWithName)) {
            byte[] imageByteArray = Base64.getDecoder().decode(base64ImageEncode);
            imageOutFile.write(imageByteArray);
        } catch (FileNotFoundException fnfe) {
            log.error("FileNotFoundException ",fnfe);
        } catch (IOException ioe) {
            log.error("IOException ",ioe);
        }
    }
    public static String encodeImage(byte[] bytes){
        String base64ImageEncode="";
        if (StringUtils.isEmpty(bytes)) {
            return base64ImageEncode;
        }
        base64ImageEncode= Base64.getEncoder().encodeToString(bytes);
        return base64ImageEncode;
    }
}
