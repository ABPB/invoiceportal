package com.abipb.invoiceportal.util;

import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by abhay.kumar-v on 12/6/2017.
 */
public class GetPropertyValue {
    private static Logger log = Logger.getLogger(GetPropertyValue.class);
    public String getPropertyValues(String key){

        return key;
    }
    public String getApplicationPropertyValues(String propName) throws IOException    //Reading File From Specified Location
    {
        Properties prop = new Properties();
        InputStream inputStream = null;
        inputStream=this.getClass().getResourceAsStream("/application.properties");
        prop.load(inputStream);

        if (inputStream != null)
        {
            prop.load(inputStream);                                                               // Loading All Input Stream
        } else {
            log.error("File Not Found.");
            throw new FileNotFoundException("property file not found in the classpath");
        }

        return prop.getProperty(propName);
    }
}
