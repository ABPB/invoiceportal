package com.abipb.invoiceportal.util;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */
public class JSONUtil {
    private static Logger log= LoggerFactory.getLogger(JSONUtil.class);
    private static ObjectMapper objectMapper;

    static {
        objectMapper=new ObjectMapper();
    }

    public static String convertJavaToJson(Object object){
        String jsonResult="";
        try {
            jsonResult=objectMapper.writeValueAsString(object);
        }catch (JsonParseException jpe){
            log.error("JsonParseException :: "+jpe);
        }catch (IOException ioe){
            log.error("IOException :: "+ioe);
        }
        return jsonResult;
    }

    public static Object convertJsonToJava(String jsonString,Class classType ){
        Object object=null;
        try {
            objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,true);
            objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT,true);
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY,true);
            object=objectMapper.readValue(jsonString,classType);
        }catch (IOException ioe){
            log.error("IOException :: "+ioe);
        }
        return  object;
    }

}
