package com.abipb.invoiceportal.util;

public enum DateFormatEnum {
	
	 DATE_FORMAT_DD_MMM_YY("DD-MMM-YY"),
	 DATE_FORMAT_DD_MM_YYYY("DDMMYYYY"),
     DATE_FORMAT_DD_$MM_$YYYY("dd/MM/YYYY");


	DateFormatEnum(String format) { this.format = format;}

   private final String format;

   public String getFormat() {
       return format;
   }



}
