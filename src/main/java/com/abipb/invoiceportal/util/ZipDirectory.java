package com.abipb.invoiceportal.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by abhay.kumar-v on 12/14/2017.
 */
public class ZipDirectory extends SimpleFileVisitor<Path>{
    private static ZipOutputStream zos;
    private Path sourceDir;

    private ZipDirectory(Path sourceDir) {
        this.sourceDir = sourceDir;
    }


    /**
     * Invoked for a file in a directory.
     * <p>
     * <p> Unless overridden, this method returns {@link FileVisitResult#CONTINUE
     * CONTINUE}.
     *
     * @param file
     * @param attrs
     */
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        try {
            Path targetFile = sourceDir.relativize(file);
            zos.putNextEntry(new ZipEntry(targetFile.toString()));
            byte[] bytes = Files.readAllBytes(file);
            zos.write(bytes, 0, bytes.length);
            zos.closeEntry();

        } catch (IOException ex) {
            System.err.println(ex);
        }

        return FileVisitResult.CONTINUE;
    }


   /* public static void main(String[] args) {
        String dirPath = "C:\\Users\\abhay.kumar-v\\Desktop\\Deplove_Module_doc\\NACH\\ToTeam\\Mandate Samples\\1.CREATE\\1.Input File\\New folder\\files\\";
        String targetPath="C:\\Users\\abhay.kumar-v\\Desktop\\Deplove_Module_doc\\NACH\\ToTeam\\Mandate Samples\\1.CREATE\\1.Input File\\New folder\\Zip\\myzip";

        createZipDirectory(dirPath,targetPath);
*//*        Path sourceDir = Paths.get(dirPath);
        try {
            String zipFileName = targetPath.concat(".zip");
            zos = new ZipOutputStream(new FileOutputStream(zipFileName));
            Files.walkFileTree(sourceDir, new ZipDirectory(sourceDir));

            zos.close();
        } catch (IOException ex) {
            System.err.println("I/O Error: " + ex);
        }*//*
    }*/

    public static void createZipDirectory(String sourceDirectoryPath,String targestDirectoryPathWithName){
        Path sourceDir = Paths.get(sourceDirectoryPath);
        File file=new File(sourceDirectoryPath);
        try {

            zos = new ZipOutputStream(new FileOutputStream(targestDirectoryPathWithName.concat(".zip")));
            Files.walkFileTree(sourceDir, new ZipDirectory(sourceDir));
            zos.close();
            Files.deleteIfExists(sourceDir);
        } catch (IOException ex) {
            System.err.println("I/O Error: " + ex);
        }
    }
}
