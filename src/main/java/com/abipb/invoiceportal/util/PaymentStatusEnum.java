package com.abipb.invoiceportal.util;

/**
 * Created by Venkatesh.R on 10/15/2017.
 */
public enum PaymentStatusEnum {

        PAYMENT_INITIATED("Initiated"),PAYMENT_SUCCESS("Success"), PAYMENT_FAILURE("Failure"),PAYMENT_RECONCILED("Reconciled");

        PaymentStatusEnum(String paymentStatus) { this.paymentStatus = paymentStatus;}

        private final String paymentStatus;

        public String getPaymentStatus() {
                return paymentStatus;
        }
}
