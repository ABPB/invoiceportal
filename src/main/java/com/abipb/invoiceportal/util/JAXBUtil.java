package com.abipb.invoiceportal.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by penchalaiah.g-v on 13-12-2017.
 */
public class JAXBUtil {
    private static Logger log= LoggerFactory.getLogger(JAXBUtil.class);
    public static Object convertXmlToObject(String xsdFilePath,String xmlFilePath,String category){
        return  null;
    }
    public static void convertObjectToXml(String xsdFilePath,String xmlFilePath,String category){

    }

    public static File convertMultipartFiletoStandardFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }
}
