package com.abipb.invoiceportal.util;

/**
 * Created by Venkatesh.R on 10/16/2017.
 */
public class PGConstants {

    public final static String PG_MSG_SEPARATOR = "|";
    public final static String PG_MSG_RESPONSE_SEPARATOR = "\\|";
    public final static String PG_FILLER_FIELD = "NA";
    public final static String PG_CURRENCY_TYPE = "INR";
    public final static String PG_ITEM_CODE = "DIRECT";
    public final static String PG_TYPE_FIELD_1 = "R";
    public final static String PG_TYPE_FIELD_2 = "F";


    public final static int PG_RESPONSE_MERCHANTID_INDEX = 0;
    public final static int PG_RESPONSE_CUSTOMERID_INDEX = 1;
    public final static int PG_RESPONSE_TXN_REF_NUM_INDEX = 2;
    public final static int PG_RESPONSE_BANK_REF_NUM_INDEX = 3;
    public final static int PG_RESPONSE_TXN_AMT_INDEX = 4;
    public final static int PG_RESPONSE_BANK_ID_INDEX = 5;
    public final static int PG_RESPONSE_BANK_MERCHANT_ID_INDEX = 6;
    public final static int PG_RESPONSE_TXN_TYPE_INDEX = 7;
    public final static int PG_RESPONSE_CURRENCY_NAME_INDEX = 8;
    public final static int PG_RESPONSE_ITEM_CODE_INDEX = 9;
    public final static int PG_RESPONSE_SECURITY_TYPE_INDEX = 10;
    public final static int PG_RESPONSE_SECURITY_ID_INDEX = 11;
    public final static int PG_RESPONSE_SECURITY_PASSWORD_INDEX = 12;
    public final static int PG_RESPONSE_TXN_DATE_INDEX = 13;
    public final static int PG_RESPONSE_AUTH_STATUS_INDEX = 14;
    public final static int PG_RESPONSE_SETTLEMENT_TYPE_INDEX = 15;
    public final static int PG_RESPONSE_ADDITIONAL_INFO1_INDEX = 16;
    public final static int PG_RESPONSE_ADDITIONAL_INFO2_INDEX = 17;
    public final static int PG_RESPONSE_ADDITIONAL_INFO3_INDEX = 18;
    public final static int PG_RESPONSE_ADDITIONAL_INFO4_INDEX = 19;
    public final static int PG_RESPONSE_ADDITIONAL_INFO5_INDEX = 20;
    public final static int PG_RESPONSE_ADDITIONAL_INFO6_INDEX = 21;
    public final static int PG_RESPONSE_ADDITIONAL_INFO7_INDEX = 22;
    public final static int PG_RESPONSE_ERROR_STATUS_INDEX = 23;
    public final static int PG_RESPONSE_ERROR_DESCRIPTION_INDEX = 24;
    public final static int PG_RESPONSE_CHECKSUM_INDEX = 25;

    public final static String PG_RESPONSE_AUTH_STATUS_SUCCESS="0300";

    public  final static String BLANK_STRING="";
    public final static  String PG_S2S_DATE_FORMAT="yyyyMMddHHmmss";
    public final static  String PG_REFUND_TXN_DATE_FORMAT="yyyyMMdd";
    public  final static String PG_QUERY_REQUEST_TYPE="0122";
    public  final static String PG_REFUND_REQUEST_TYPE="0400";


    public final static String PROC__S2S_B2B_RESPONSE_MESSAGE="P_B2B_PG_RESPONSE_MSG";


    public static final int S2S_RESPONSE_MERCHANT_ID_INDEX=0;
    public static final int S2S_RESPONSE_CUSTOMER_ID_INDEX=1;
    public static final int S2S_RESPONSE_TXN_REFERENCE_NO_INDEX=2;
    public static final int S2S_RESPONSE_BANK_REFERENCE_NO_INDEX=3;
    public static final int S2S_RESPONSE_TXN_AMOUNT_INDEX=4;
    public static final int S2S_RESPONSE_BANK_ID_INDEX=5;
    public static final int S2S_RESPONSE_BANK_MERCHANT_ID_INDEX=6;
    public static final int S2S_RESPONSE_TXN_TYPE_INDEX=7;
    public static final int S2S_RESPONSE_CURRENCY_NAME_INDEX=8;
    public static final int S2S_RESPONSE_ITEM_CODE_INDEX=9;
    public static final int S2S_RESPONSE_SECURITY_TYPE_INDEX=10;
    public static final int S2S_RESPONSE_SECURITY_ID_INDEX=11;
    public static final int S2S_RESPONSE_SECURITY_PASSWORD_INDEX=12;
    public static final int S2S_RESPONSE_TXN_DATE_INDEX=13;
    public static final int S2S_RESPONSE_AUTH_STATUS_INDEX=14;
    public static final int S2S_RESPONSE_SETTLEMENT_TYPE_INDEX=15;
    public static final int S2S_RESPONSE_ADDITIONAL_INFO_1_INDEX=16;
    public static final int S2S_RESPONSE_ADDITIONAL_INFO_2_INDEX=17;
    public static final int S2S_RESPONSE_ADDITIONAL_INFO_3_INDEX=18;
    public static final int S2S_RESPONSE_ADDITIONAL_INFO_4_INDEX=19;
    public static final int S2S_RESPONSE_ADDITIONAL_INFO_5_INDEX=20;
    public static final int S2S_RESPONSE_ADDITIONAL_INFO_6_INDEX=21;
    public static final int S2S_RESPONSE_ADDITIONAL_INFO_7_INDEX=22;
    public static final int S2S_RESPONSE_ERROR_STATUS_INDEX=23;
    public static final int S2S_RESPONSE_ERROR_DESCRIPTION_INDEX=24;
    public static final int S2S_RESPONSE_CHECKSUM_INDEX=25;
    public final static String PROC_CBS_B2B_RESPONSE_MESSAGE="p_b2b_cbs_response_msg";

}
