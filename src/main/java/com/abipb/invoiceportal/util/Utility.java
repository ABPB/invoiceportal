package com.abipb.invoiceportal.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Blob;
import java.sql.SQLException;

/**
 * Created by abhay.kumar-v on 12/21/2017.
 */
public class Utility {
    private static final Logger log= LoggerFactory.getLogger(Utility.class);

    public static Blob getBlob(String data){
        Blob blob=null;
        try {
            blob=new SerialBlob(data.getBytes());
        }catch (SQLException sqle){
            log.error("SQLException : ",sqle);
        }
        return blob;
    }
}
