package com.abipb.invoiceportal.util;

/**
 * Created by abhay.kumar-v on 11/27/2017.
 */
public enum  ErrorMessageEnum {

    INTERNAL_SERVER_ERROR("INTERNAL_SERVER_ERROR"),NO_DATA_FOUND("NO_DATA_FOUND");
    private String errorMessage;
     ErrorMessageEnum(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
