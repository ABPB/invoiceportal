package com.abipb.invoiceportal.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by abhay.kumar-v on 10/30/2017.
 */
@Component
public class PropertiesValue {

    @Value("${pg.merchant.id}")
    private String pgMerchantId;
    @Value("${pg.checksum.key}")
    private  String pgChecksumKey;
    @Value("${pg.security.id}")
    private String pgSecurityId;

    @Value("${upi.interface.hmac.salt}")
    private String upiHMACSalt;

    @Value("${server.ssl.key-store-password}")
    private String keyStorePassword;

    @Value("${server.ssl.key-store}")
    private String keyStore;

    @Value("${isupiValidationStubEnabled}")
    private String validationSwitch;

    @Value("${pg.netbanking.payment.endpoint}")
    private String pgNbEndpoint;

    @Value("${spring.datasource.driver-class-name}")
    private  String driverClass;

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private  String userName;

    @Value("${spring.datasource.password}")
    private  String password;
    public String getPgMerchantId() {
        return pgMerchantId;
    }

    public void setPgMerchantId(String pgMerchantId) {
        this.pgMerchantId = pgMerchantId;
    }

    public String getPgChecksumKey() {
        return pgChecksumKey;
    }

    public void setPgChecksumKey(String pgChecksumKey) {
        this.pgChecksumKey = pgChecksumKey;
    }

    public String getPgSecurityId() {
        return pgSecurityId;
    }

    public void setPgSecurityId(String pgSecurityId) {
        this.pgSecurityId = pgSecurityId;
    }

    public String getUpiHMACSalt() {
        return upiHMACSalt;
    }

    public void setUpiHMACSalt(String upiHMACSalt) {
        this.upiHMACSalt = upiHMACSalt;
    }

    public String getKeyStorePassword() {
        return keyStorePassword;
    }

    public void setKeyStorePassword(String keyStorePassword) {
        this.keyStorePassword = keyStorePassword;
    }

    public String getKeyStore() {
        return keyStore;
    }

    public void setKeyStore(String keyStore) {
        this.keyStore = keyStore;
    }

    public String getValidationSwitch() {
        return validationSwitch;
    }

    public void setValidationSwitch(String validationSwitch) {
        this.validationSwitch = validationSwitch;
    }

    public String getPgNbEndpoint() {
        return pgNbEndpoint;
    }

    public void setPgNbEndpoint(String pgNbEndpoint) {
        this.pgNbEndpoint = pgNbEndpoint;
    }

    public String getDriverClass() {
        return driverClass;
    }

    public void setDriverClass(String driverClass) {
        this.driverClass = driverClass;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
