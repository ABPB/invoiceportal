package com.abipb.invoiceportal.util;

/**
 * Created by Venkatesh.R on 10/20/2017.
 */
public enum ValidationEnum {
    VALIDATION_SUCCESS("Success"),
    VALIDATION_FAILURE("Failure"),
    VALIDATION_NOTINITIATE("NOT INITIATE"),
    VALIDATION_PENDING("PENDING"),
    MANDATE_FAILURE("FAILURE"),
    MANDATE_SUCCESS("SUCCESS");
    ValidationEnum(String validationState) { this.validationState = validationState;}

    private final String validationState;

    public String getValidationState() {
        return validationState;
    }
}
