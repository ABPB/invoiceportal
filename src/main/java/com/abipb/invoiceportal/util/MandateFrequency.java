package com.abipb.invoiceportal.util;

/**
 * Created by abhay.kumar-v on 12/12/2017.
 */
public enum MandateFrequency {
    MONTHLY("M"),
    BI_MONTHLY("BM"),
    QUARTLY("Q"),
    HALF_YEARLY("HY"),
    YEARLY("Y"),
    WHEN_PRESENTED("WP");

    MandateFrequency(String frequency){this.frequency=frequency; }
    private String frequency;

    public String getFrequency() {
        return frequency;
    }
}
