package com.abipb.invoiceportal.util;

import sun.security.rsa.RSAPrivateKeyImpl;
import sun.security.util.DerValue;

import java.io.FileInputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Enumeration;

public  abstract  class SignAPI {
	
	private KeyStore.PrivateKeyEntry keyEntry;
	private static final String KEY_STORE_TYPE = "PKCS12";
	

/*	
public static final void main(String [] s) {

	String data="56       CPSMSSY                                               000000003                           2000000000000000000003000005022014                       SYNB00005000000216ABCDEFG0000008    560025000                                     000000003                                                           ";
	try {
		sign(Base64ImageUtil.encodeImage(data.getBytes()).getBytes());
	}catch (Exception e){
		System.out.println(e);
	}
	
}
*/

public static byte[] sign(byte[] data) throws Exception { 
	
	try {
		
//    Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider()); 
    KeyStore inStore = KeyStore.getInstance(KEY_STORE_TYPE); 
    inStore.load(new FileInputStream("D:\\certificate\\certificate\\Docsigntest.p12"), "emudhra".toCharArray()); 
    String alias="";
Enumeration enumm =     inStore.aliases();
while (enumm.hasMoreElements()) {
    alias=(String)enumm.nextElement();
}

    Key key = inStore.getKey(alias, "emudhra".toCharArray()); 
    PrivateKey privateKey = RSAPrivateKeyImpl.parseKey(new DerValue(key.getEncoded())); 
    Certificate certificate = inStore.getCertificate(alias); 

    String signdate=sign(data,privateKey);
    
		System.out.println("sign(data,privateKey) :  "+signdate);
		System.out.println("==================================================================");
		
		System.out.println("alias ::   " +Base64.getEncoder().encodeToString(alias.getBytes()));
		System.out.println("==================================================================");
		String getPublicKey =Base64.getEncoder().encodeToString(certificate.getPublicKey().getEncoded());
		
		System.out.println("Public key    " +certificate.getPublicKey().getEncoded());
		System.out.println("getPublicKey ::  "+getPublicKey);
		
		
		byte[] publicBytes = Base64.getDecoder().decode(getPublicKey.getBytes());
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PublicKey pubKey = keyFactory.generatePublic(keySpec);
		
		String datatemp="56       CPSMSSY1                                              000000003                           2000000000000000000003000005022014                       SYNB00005000000216ABCDEFG0000008    560025000                                     000000003                                                           ";
		
		System.out.println("Verifiy :: "+verify(datatemp.getBytes(), signdate, pubKey));
		

  
	}catch(Exception e) {
		System.err.println(e);
	}
    return new byte[0]; 
} 

public static String sign(byte[] plainText, PrivateKey privateKey) throws Exception {
    Signature privateSignature = Signature.getInstance("SHA256withRSA");
    privateSignature.initSign(privateKey);
    privateSignature.update(plainText);

    byte[] signature = privateSignature.sign();

    return Base64.getEncoder().encodeToString(signature);
}




	public static boolean verify(byte[] plainText, String signature, PublicKey publicKey) throws Exception {
		Signature publicSignature = Signature.getInstance("SHA256withRSA");
		publicSignature.initVerify(publicKey);
		publicSignature.update(plainText);

		byte[] signatureBytes = Base64.getDecoder().decode(signature);

		return publicSignature.verify(signatureBytes);
	}
}
