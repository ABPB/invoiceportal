package com.abipb.invoiceportal.util;

/**
 * Created by Venkatesh.R on 10/15/2017.
 */
public enum PaymentChannelEnum {
    PG_NET_BANKING_CHANNEL("PG-NETBANKING"),
    PG_CREDIT_CARD_CHANNEL("PG-CREDITCARD"),
    PG_DEBIT_CARD_CHANNEL("PG-DEBITCARD"),
    UPI_CHANNEL("UPI"),
    RBL_CHANNEL("RBL"),
    CBS_CHANNEL("CBS"),
    UPI_CHANNEL_PUSH("UPI_PUSH"),
    UPI_CHANNEL_PULL("UPI_PULL");


    PaymentChannelEnum(String paymentChannel) { this.paymentChannel = paymentChannel;}

    private final String paymentChannel;

    public String getPaymentChannel() {
        return paymentChannel;
    }

}
