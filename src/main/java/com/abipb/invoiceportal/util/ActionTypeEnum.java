package com.abipb.invoiceportal.util;

public enum ActionTypeEnum {
	
	  MANDATE_API_CREATE("CREATE"),
	  MANDATE_API_AMEND("AMEND"),
	  MANDATE_API_CANCEL("CANCEL"),
	  MANDATE_API_STATUS_ACCEPTED("ACCEPTED"),
	  MANDATE_API_STATUS_REJECTED("REJECTED"),
	  MANDATE_API_STATUS_PENDING("PENDING");




	ActionTypeEnum(String action) { this.action = action;}

    private final String action;

    public String getAction() {
        return action;
    }


}
