package com.abipb.invoiceportal.util;

/**
 * Created by Venkatesh.R on 10/15/2017.
 */
public enum ClientPaymentChannelEnum {

        DIRECT_PAYMENT_CHANNEL("Direct"), REDIRECT_PAYMENT_CHANNEL("Redirect");

        ClientPaymentChannelEnum(String clientPaymentChannel) { this.clientPaymentChannel = clientPaymentChannel;}

        private final String clientPaymentChannel;

        public String getClientPaymentChannel() {
                return clientPaymentChannel;
        }
}
