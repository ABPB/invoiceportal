package com.abipb.invoiceportal.util;

/**
 * Created by abhay.kumar-v on 12/6/2017.
 */
public class MiddlewareResponse {

    private String errCode;
    private String errMsg;
    private String responseJson;

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getResponseJson() {
        return responseJson;
    }

    public void setResponseJson(String responseJson) {
        this.responseJson = responseJson;
    }
}
