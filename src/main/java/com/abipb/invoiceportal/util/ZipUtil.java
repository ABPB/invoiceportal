package com.abipb.invoiceportal.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtil {
	private final static Logger log=LoggerFactory.getLogger(ZipUtil.class);
	
	  public static void createZipForMultipleFile(String destinationPathForZip,ConcurrentMap<String, byte[]> fileNameDateMap)throws Exception {
	    	ZipOutputStream zipOutputStream=null;
	    	try {
	    		if(StringUtils.isEmpty(destinationPathForZip) && StringUtils.isEmpty(fileNameDateMap)) {
	    			log.debug("Destination Path for Zip : ",destinationPathForZip,"  File Name & Date  :  ",fileNameDateMap);
	    			return;
	    		}
	    		zipOutputStream=new ZipOutputStream(new FileOutputStream(destinationPathForZip));
	    		Set<String> keys=fileNameDateMap.keySet();
	    		for(String key: keys) {
	    			ZipEntry zipEntry=new ZipEntry(key);
	    			zipOutputStream.putNextEntry(zipEntry);
	    			zipOutputStream.write(fileNameDateMap.get(key));
	    			zipOutputStream.flush();
	    		}
	    	}catch(FileNotFoundException fnfe) {
	    		log.error("FileNotFoundException",fnfe);
	    	}finally {
				zipOutputStream.close();
			}
	    	
	    	
	    }

}
