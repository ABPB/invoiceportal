package com.abipb.invoiceportal.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by abhay.kumar-v on 10/26/2017.
 */
@Component
public class DateUtil {
    private static final Logger log= LoggerFactory.getLogger(DateUtil.class);

    public final static String  getFormattedDate(String format){
        if(format.contains("T")){
            Date sysDate = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
            return  sdf.format(sysDate);
        }else{
            SimpleDateFormat f = new SimpleDateFormat(format);
            return f.format(new Date());

        }

    }

    public final static Date getUtilDate(String strdate,String format){
        Date date=null;
       try {
          date= new SimpleDateFormat(format).parse(strdate);
       }catch (ParseException pe){
           log.error("ParserException :: ",pe);
       }
        return date;
    }


}
