/*
 * Created on Nov 1, 2013
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.abipb.invoiceportal.util;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import javax.net.ssl.*;
import java.io.*;
import java.net.*;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;


/* 1.This class is used for connectioning Server  with the help of URL*/
public class ConnectHttps 
{
	private static Logger log = Logger.getLogger(ConnectHttps.class);
	
	private static String ERR_GENERAL="Error in connecting NSDL";

	@Value("${cbs.isProxyReqd}")
	public  String isProxyReqd;
	
	public static class DummyTrustManager implements X509TrustManager {

		public DummyTrustManager() {
		}

		public boolean isClientTrusted(X509Certificate cert[]) {
			return true;
		}

		public boolean isServerTrusted(X509Certificate cert[]) {
			return true;
		}

		public X509Certificate[] getAcceptedIssuers() {
			return new X509Certificate[0];
		}

		public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {

		}

		public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {

		}
	}
	public static class DummyHostnameVerifier implements HostnameVerifier {

		public boolean verify( String urlHostname, String certHostname ) {
			return true;
		}

		public boolean verify(String arg0, SSLSession arg1) {
			return true;
		}
	}
	
	public static String getResponseFromHttps(String asaUrl,String xml)
	{
		SSLContext sslcontext = null;
		String  responseXML ="";
		try {
			sslcontext = SSLContext.getInstance("TLSv1.2");

			sslcontext.init(new KeyManager[0],
					new TrustManager[] { new DummyTrustManager() },
					new SecureRandom());
		} catch (NoSuchAlgorithmException e) {
		
			System.out.println("Exception in ConnectHttps :"+e);
			
		} catch (KeyManagementException e) {
			System.out.println("Exception in ConnectHttps :"+e);	
		}
		
		String urlParameters="eXml=";
		try{
			urlParameters =urlParameters + URLEncoder.encode(xml, "UTF-8");
		//	urlParameters =xml;
		}catch(Exception e){
			
			System.out.println("Exception in ConnectHttps :"+e);
			
		}
/*		if(urlParameters.contains("+"))
			urlParameters =  urlParameters.replaceAll("+", "%2b");*/
		
		try{
			SSLSocketFactory factory = sslcontext.getSocketFactory();
			URL url;
			HttpsURLConnection connection;
			InputStream is = null;
			Proxy proxy=null;	
			GetPropertyValue readProperty=new GetPropertyValue();
			try {
				if ("Y".equalsIgnoreCase(""))
				{
					// Proxy server is setup
					String proxyAddress=readProperty.getPropertyValues("proxyServer");
					int proxyPort=new Integer(readProperty.getPropertyValues("proxyPort"));
					SocketAddress proxySock=new InetSocketAddress(proxyAddress,proxyPort);
					proxy=new Proxy(Proxy.Type.HTTP,proxySock);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			String ip=asaUrl;
			url = new URL(null,ip,new sun.net.www.protocol.https.Handler());
			System.out.println("URL "+ip);

			if (proxy==null)
			{
				connection = (HttpsURLConnection) url.openConnection();
			}
			else
			{
				connection = (HttpsURLConnection) url.openConnection(proxy);
			}

			connection = (HttpsURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");
			connection.setUseCaches (false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setSSLSocketFactory(factory);
			connection.setHostnameVerifier(new DummyHostnameVerifier());
			OutputStream os = connection.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os);
			osw.write(urlParameters);
			osw.flush();
			osw.close();
			
			is =connection.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(is));
			
			String strTemp;
		    while ((strTemp = in.readLine()) != null) 
		    {
		    	responseXML = responseXML + strTemp;
		    	 //System.out.println(responseXML);
		    }
		    

			//System.out.println("Output: "+line);
			is.close();
			in.close();
			connection.disconnect();
		}
		catch(ConnectException e){
			System.out.println("Exception in ConnectHttps :"+e);
		}
		catch(Exception e){
			
			System.out.println("Exception in ConnectHttps :"+e);
		}
		System.out.println("In getResponseFromHttps method of ConnectHttps");
		return responseXML;
	}
	public static String getResponseFromHttps(String httpsUrl,String postParName, String postParValue)
	{
		System.out.println("getResponseFromHttps kua");
		System.out.println("In getResponseFromHttps method of ConnectHttps");
		SSLContext sslcontext = null;
		String  responseXML ="";
		try {
			sslcontext = SSLContext.getInstance("SSL");

			sslcontext.init(new KeyManager[0],
					new TrustManager[] { new DummyTrustManager() },
					new SecureRandom());
		} catch (NoSuchAlgorithmException e) {
		
			System.out.println("Exception in ConnectHttps :"+e);
			
		} catch (KeyManagementException e) {
			System.out.println("Exception in ConnectHttps :"+e);	
		}
		
		String urlParameters=postParName+"=";
		try{
			urlParameters =urlParameters + URLEncoder.encode(postParValue, "UTF-8");
		}catch(Exception e){
			
			System.out.println("Exception in ConnectHttps :"+e);
			
		}
		try{
			SSLSocketFactory factory = sslcontext.getSocketFactory();
			URL url;
			HttpsURLConnection connection;
			InputStream is = null;


			String ip=httpsUrl;
			url = new URL(ip);
			System.out.println("URL "+ip);
			connection = (HttpsURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");
			connection.setUseCaches (false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setSSLSocketFactory(factory);
			connection.setHostnameVerifier(new DummyHostnameVerifier());
			OutputStream os = connection.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os);
			osw.write(urlParameters);
			osw.flush();
			osw.close();
			is =connection.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(is));
			
			String strTemp;
		    while ((strTemp = in.readLine()) != null) 
		    {
		    	responseXML =  strTemp;
		    }
			is.close();
			in.close();
		}
		catch(ConnectException e){
			System.out.println("Exception in ConnectHttps for license key check :"+e);
			responseXML =   "Unable to connect to server" ;
			e.printStackTrace();
		}
		catch(Exception e){
			responseXML =  "Exception Occured" ;
			System.out.println("Exception in ConnectHttps for  license key check  :"+e);
			e.printStackTrace();
		}
		System.out.println("In getResponseFromHttps method of ConnectHttps for license key check  ");
		return responseXML;
	}
	
	/*** Multiple changes have been made to this method to allow for independent connection with NSDL ignoring IIB -19-07-2017***/
	public static MiddlewareResponse getResponseFromEsign(String asaUrl,String xml)
	{
		log.info("Entering public static NSDLResponse getResponseFromEsign(String asaUrl,String xml)");
		SSLContext sslcontext = null;
		String  responseXML ="";
		GetPropertyValue readProperty=new GetPropertyValue();
		MiddlewareResponse esignResp=new MiddlewareResponse();
		Proxy proxy=null;

		try {
			sslcontext = SSLContext.getInstance("TLSv1.2");

			sslcontext.init(new KeyManager[0],
					new TrustManager[] { new DummyTrustManager() },
					new SecureRandom());
		} catch (NoSuchAlgorithmException e) {
		
			log.info("Exception in ConnectHttps :"+e);
			esignResp.setErrCode("999");
			esignResp.setErrMsg(ERR_GENERAL);
			log.error("Exception ",e);
			return esignResp;
		} catch (KeyManagementException e) {
			log.info("Exception in ConnectHttps :"+e);
			esignResp.setErrCode("999");
			esignResp.setErrMsg(ERR_GENERAL);
			log.error("Exception ",e);
			return esignResp;
		}
		
		String urlParameters="eXml=";
		try{
			//urlParameters =urlParameters + URLEncoder.encode(xml, "UTF-8");
			urlParameters =xml;
		}catch(Exception e){
			
			log.info("Exception in ConnectHttps :"+e);
			esignResp.setErrCode("999");
			esignResp.setErrMsg(ERR_GENERAL);
			log.error("Exception ",e);
			return esignResp;
		}
/*		if(urlParameters.contains("+"))
			urlParameters =  urlParameters.replaceAll("+", "%2b");*/
		
		try{
			SSLSocketFactory factory = sslcontext.getSocketFactory();
			URL url;
			HttpsURLConnection connection;
			InputStream is = null;

			
			try {
				if ("Y".equalsIgnoreCase(""))
				{
					// Proxy server is setup
					String proxyAddress="10.1.0.236";//readProperty.getPropertyValues("proxyServer");
					int proxyPort=80;//new Integer(readProperty.getPropertyValues("proxyPort"));
					Authenticator.setDefault(
							new Authenticator(){
								protected PasswordAuthentication getPasswordAuthentication(){
									return new PasswordAuthentication("penchalaiah.g-v", new String("Oct_2017").toCharArray());
								}
							}
					);

					System.setProperty("https.proxyType", "https");
					System.setProperty("https.proxyPort", Integer.toString(proxyPort));
					System.setProperty("https.proxyHost", proxyAddress);
					System.setProperty("https.proxySet", "true");

					SocketAddress proxySock=new InetSocketAddress(proxyAddress,proxyPort);
					proxy=new Proxy(Proxy.Type.HTTP,proxySock);
					
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				log.info("Exception in ConnectHttps :"+e1);
				esignResp.setErrCode("999");
				esignResp.setErrMsg(ERR_GENERAL);
				log.error("Exception ",e1);
				return esignResp;
			}
			

			String ip=asaUrl;
			url = new URL(null,ip,new sun.net.www.protocol.https.Handler());
			System.out.println("URL "+ip);
			
			if (proxy==null)
			{
				connection = (HttpsURLConnection) url.openConnection();
			}
			else
			{
				connection = (HttpsURLConnection) url.openConnection(proxy);
			}
			
//			connection = (HttpsURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type","application/json");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");
			connection.setUseCaches (false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setSSLSocketFactory(factory);
			connection.setHostnameVerifier(new DummyHostnameVerifier());
			OutputStream os = connection.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os);
			osw.write(urlParameters);
			osw.flush();
			osw.close();
			
			is =connection.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(is));
			
			String strTemp;
		    while ((strTemp = in.readLine()) != null) 
		    {
		    	responseXML = responseXML + strTemp;
		    	 //System.out.println(responseXML);
		    }
		    
		    esignResp.setResponseJson(responseXML);
		    esignResp.setErrCode("000");
			//System.out.println("Output: "+line);
			is.close();
			in.close();
			connection.disconnect();
		}
		catch(ConnectException e){
			log.info("Exception in ConnectHttps :"+e);
			esignResp.setErrCode("999");
			esignResp.setErrMsg(ERR_GENERAL);
			log.error("Exception ",e);
			return esignResp;
		}
		catch(Exception e){
			
			log.info("Exception in ConnectHttps :"+e);
			esignResp.setErrCode("999");
			esignResp.setErrMsg(ERR_GENERAL);
			log.error("Exception ",e);
			return esignResp;
		}
		System.out.println("In getResponseFromHttps method of ConnectHttps");
		log.info("Exiting public static NSDLResponse getResponseFromEsign(String asaUrl,String xml)");
		return esignResp;
	}

	
	
	
	public static MiddlewareResponse getResponseFromIIB(String asaUrl,String xml)
	{
		log.info("Entering public static NSDLResponse getResponseFromIIB(String asaUrl,String xml)");
		SSLContext sslcontext = null;
		String  responseXML ="";
		GetPropertyValue readProperty=new GetPropertyValue();
		MiddlewareResponse esignResp=new MiddlewareResponse();
//		System.out.println(xml);
		String trustStore="";
		String certPath=System.getProperty("DECIMAL_PROPERTY_PATH")+"/Esign/certificate/";

		
		try {
			if (readProperty.getPropertyValues("useAltConn").equalsIgnoreCase("Y"))
			{
//				CreateIIBRequest iibRequest=new CreateIIBRequest();
				try {
//					responseXML=iibRequest.callPostMethod(asaUrl,xml);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					esignResp.setErrCode("999");
					esignResp.setErrMsg(ERR_GENERAL);
					e.printStackTrace();
					log.error("Exception ",e);
					return esignResp;
				}
			}
			else
			{
					String iibCertPwd="";
					try {
							iibCertPwd=readProperty.getPropertyValues("IIBCertPwd").trim();
							trustStore=readProperty.getPropertyValues("IIBTrustStore").trim();
					} catch (Exception e3) {
						// TODO Auto-generated catch block
						e3.printStackTrace();
						log.error("Exception ",e3);
						esignResp.setErrCode("999");
						esignResp.setErrMsg(ERR_GENERAL);
						return esignResp;
					}
					certPath=certPath+trustStore;
				
					KeyStore keyStore;
					KeyManager[] km = null;
					TrustManager[] tm = null;
					
					if (readProperty.getPropertyValues("ignoreCerts").equalsIgnoreCase("Y"))
					{
						tm=trustAllCerts();
					}
					else
					{
						try {
							keyStore = KeyStore.getInstance("JKS");
							keyStore.load(new FileInputStream(certPath),iibCertPwd.toCharArray());
						
						// Create key manager
							KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
							keyManagerFactory.init(keyStore, iibCertPwd.toCharArray());
							km = keyManagerFactory.getKeyManagers();
						
						//Create trust manager
							TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance("SunX509");
							trustManagerFactory.init(keyStore);
							tm = trustManagerFactory.getTrustManagers();
						} catch (KeyStoreException e2) {
							// TODO Auto-generated catch block
							esignResp.setErrCode("999");
							esignResp.setErrMsg(ERR_GENERAL);
							log.error("Exception ",e2);
							e2.printStackTrace();
							return esignResp;
						} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
							esignResp.setErrCode("999");
							esignResp.setErrMsg(ERR_GENERAL);
							log.error("Exception ",e);
							e.printStackTrace();
							return esignResp;
						} catch (CertificateException e) {
						// TODO Auto-generated catch block
							esignResp.setErrCode("999");
							esignResp.setErrMsg(ERR_GENERAL);
							log.error("Exception ",e);
							e.printStackTrace();
							return esignResp;
						} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
							esignResp.setErrCode("999");
							esignResp.setErrMsg(ERR_GENERAL);
							log.error("Exception ",e);
							e.printStackTrace();
							return esignResp;
						} catch (IOException e) {
						// TODO Auto-generated catch block
							esignResp.setErrCode("999");
							esignResp.setErrMsg(ERR_GENERAL);
							log.error("Exception ",e);
							e.printStackTrace();
							return esignResp;
						} catch (UnrecoverableKeyException e) {
						// TODO Auto-generated catch block
							esignResp.setErrCode("999");
							esignResp.setErrMsg(ERR_GENERAL);
							log.error("Exception ",e);
							e.printStackTrace();
							return esignResp;
						}
					}
					Proxy proxy=null;
					try {
			// Removing TLS since IIB load balancer is not configured to receive TLSv			
						sslcontext = SSLContext.getInstance("TLSv1");
						sslcontext.init(km,
								tm ,
								new SecureRandom());
						
					} catch (NoSuchAlgorithmException e) {
					
						System.out.println("Exception in ConnectHttps :"+e);
						esignResp.setErrCode("999");
						esignResp.setErrMsg(ERR_GENERAL);
						log.error("Exception ",e);
						e.printStackTrace();
						return esignResp;
					} catch (KeyManagementException e) {
						System.out.println("Exception in ConnectHttps :"+e);
						esignResp.setErrCode("999");
						esignResp.setErrMsg(ERR_GENERAL);
						log.error("Exception ",e);
						e.printStackTrace();
						return esignResp;
					}
					
					String urlParameters="eXml=";
					try{
						//urlParameters =urlParameters + URLEncoder.encode(xml, "UTF-8");
						urlParameters =xml;
					}catch(Exception e){
						
						System.out.println("Exception in ConnectHttps :"+e);
						esignResp.setErrCode("999");
						esignResp.setErrMsg(ERR_GENERAL);
						log.error("Exception ",e);
						return esignResp;
					}
			/*		if(urlParameters.contains("+"))
						urlParameters =  urlParameters.replaceAll("+", "%2b");*/
					
					try{
						SSLSocketFactory factory = sslcontext.getSocketFactory();
						URL url;
						HttpsURLConnection connection;
						InputStream is = null;
			
						
						try {
							if ("Y".equalsIgnoreCase(""))
							{
								// Proxy server is setup
								String proxyAddress=readProperty.getPropertyValues("proxyServer");
								int proxyPort=new Integer(readProperty.getPropertyValues("proxyPort"));
								SocketAddress proxySock=new InetSocketAddress(proxyAddress,proxyPort);
								proxy=new Proxy(Proxy.Type.HTTP,proxySock);
								
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							log.error("Exception ",e1);
							esignResp.setErrCode("999");
							esignResp.setErrMsg(ERR_GENERAL);
							e1.printStackTrace();
							return esignResp;
						}
						
			
						String ip=asaUrl;
						url = new URL(null,ip,new sun.net.www.protocol.https.Handler());
						System.out.println("URL "+ip);
						
						if (proxy==null)
						{
							connection = (HttpsURLConnection) url.openConnection();
						}
						else
						{
							connection = (HttpsURLConnection) url.openConnection(proxy);
						}
						connection.setSSLSocketFactory(factory);
			//			connection = (HttpsURLConnection) url.openConnection();
						connection.setRequestMethod("POST");
						connection.setRequestProperty("Content-Type","application/json");
						connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
						connection.setRequestProperty("Content-Language", "en-US");
						connection.setUseCaches (false);
						connection.setDoInput(true);
						connection.setDoOutput(true);
						connection.setSSLSocketFactory(factory);
						connection.setHostnameVerifier(new DummyHostnameVerifier());
						OutputStream os = connection.getOutputStream();
						OutputStreamWriter osw = new OutputStreamWriter(os);
						osw.write(urlParameters);
						osw.flush();
						osw.close();
						
						is =connection.getInputStream();
						BufferedReader in = new BufferedReader(new InputStreamReader(is));
						
						String strTemp;
					    while ((strTemp = in.readLine()) != null) 
					    {
					    	responseXML = responseXML + strTemp;
					    	 //System.out.println(responseXML);
					    }
						is.close();
						in.close();
						connection.disconnect();
				//System.out.println("Output: "+line);	
			}
			catch(ConnectException e){
				System.out.println("Exception in ConnectHttps :"+e);
				esignResp.setErrCode("999");
				esignResp.setErrMsg(e.getMessage());
				log.info("Exception in ConnectHttps :"+e.getMessage());
				log.error("Exception ",e);
				return esignResp;
			}
			catch(Exception e){
				
				System.out.println("Exception in ConnectHttps :"+e);
				esignResp.setErrCode("999");
				esignResp.setErrMsg(e.getMessage());
				log.info("Exception in ConnectHttps :"+e.getMessage());
				log.error("Exception ",e);
				return esignResp;
			}
}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		esignResp.setResponseJson(responseXML);
	    esignResp.setErrCode("000");

		System.out.println("In getResponseFromHttps method of ConnectHttps");
//		System.out.println("Response:"+responseXML);
		log.debug("NSDL Response:"+responseXML);
		log.info("Exiting public static NSDLResponse getResponseFromIIB(String asaUrl,String xml)");
		return esignResp;
	}

	
	public static TrustManager[] trustAllCerts()
	{
		TrustManager[] trustAllCerts = new TrustManager[] { 
			    new X509TrustManager() {     
			        public X509Certificate[] getAcceptedIssuers() {
			            return new X509Certificate[0];
			        } 
			        public void checkClientTrusted( 
			            X509Certificate[] certs, String authType) {
			            } 
			        public void checkServerTrusted( 
			            X509Certificate[] certs, String authType) {
			        }
			    } 
			}; 
		return trustAllCerts;
	}
	
}

