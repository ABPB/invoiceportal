package com.abipb.invoiceportal.util;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */
public class HttpClientPost {
    private static final Logger log= LoggerFactory.getLogger(HttpClientPost.class);

    public static String callHttpPost(String url,String object,String message){
        StringEntity inputEntity = null;
        String output="";
        try {
            HttpPost httpPost = new HttpPost(url);
            inputEntity = new StringEntity(object);
            inputEntity.setContentType("application/json");
            httpPost.setEntity(inputEntity);
            httpPost.setHeader("HASH",object);
            RequestConfig config = RequestConfig.custom().
            setConnectTimeout(3 * 1000).
            setConnectionRequestTimeout(3 * 1000).
            setSocketTimeout(3 * 1000).build();
            CloseableHttpClient client = HttpClientBuilder.create()
            .setDefaultRequestConfig(config).build();
            HttpResponse httpResponse = client.execute(httpPost);
            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            log.error("Failed response"
            + httpResponse.getStatusLine().getStatusCode());
            }
            log.debug("Output from Server .... \n");
            BufferedReader bufferedReader = new BufferedReader(
            new InputStreamReader(
            (httpResponse.getEntity().getContent())));
            while ((output = bufferedReader.readLine()) != null) {
                log.error(output);
            }
        }catch (UnsupportedEncodingException e) {
            log.error("UnsupportedEncodingException :: "+e);
        } catch (MalformedURLException e) {
            log.error("MalformedURLException :: "+e);
        } catch (IOException e) {
            log.error("IOException :: "+e);
        }

        return output;
    }

  public static String callHttpPost(String url,String contentType ,String headerKey,String headerValue,String data){
        StringEntity inputEntity = null;
        String output="";
        try {
            HttpPost httpPost = new HttpPost(url);
            inputEntity = new StringEntity(data);
            inputEntity.setContentType(contentType);
            httpPost.setEntity(inputEntity);
            httpPost.setHeader(headerKey,headerValue);//HASH
            RequestConfig config = RequestConfig.custom().
                    setConnectTimeout(3 * 1000).
                    setConnectionRequestTimeout(3 * 1000).
                    setSocketTimeout(3 * 1000).build();
            CloseableHttpClient client = HttpClientBuilder.create()
                    .setDefaultRequestConfig(config).build();
            HttpResponse httpResponse = client.execute(httpPost);
            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                log.error("Failed response"
                        + httpResponse.getStatusLine().getStatusCode());
            }
            log.debug("Output from Server .... \n");
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            (httpResponse.getEntity().getContent())));
            while ((output = bufferedReader.readLine()) != null) {
                log.error(output);
            }
        }catch (UnsupportedEncodingException e) {
            log.error("UnsupportedEncodingException :: "+e);
        } catch (MalformedURLException e) {
            log.error("MalformedURLException :: "+e);
        } catch (IOException e) {
            log.error("IOException :: "+e);
        }

        return output;
    }
}
