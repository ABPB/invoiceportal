package com.abipb.invoiceportal.util;

/**
 * Created by penchalaiah.g-v on 21-12-2017.
 */
public class NACHConstants {

    public  static  final String NACH_UPLOAD_CREATE="/mandate/fileupload//create";
    public  static  final String NACH_UPLOAD_AMEND="/mandate/fileupload/amend";
    public  static  final String NACH_UPLOAD_CANCEL="/mandate/fileupload/cancel";
    public  static  final String NACH_UPLOAD_DR_TXN="/transaction/fileupload";
/*    public  static  final String NACH_UPLOAD_AMEND="/amend";
    public  static  final String NACH_UPLOAD_CANCEL="/cancel";*/
    public  static  final String NACH_DOWNLOAD_ACK="/NACH/mandate/filedownload/ack";
    public  static  final String NACH_DOWNLOAD_RESPONSE="/NACH/mandate/filedownload/response";
    public static final String NACH_DEBIT_TXN = "ACHDEBIT";

/*    public static final String ENCODE_JPG_IMAGE="/9j/4AAQSkZJRgABAQEASABIAAD//gATQ3JlYXRlZCB3aXRoIEdJTVD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx7";

    public static final String ENCODE_TIFF_IMAGE="SUkqAFgkAACAACBQOCQWDQeEQmFQuGQ2GgKHRGJROKRWLReMRmNRuOR2PR+QSGRSOSSWTSeUSmVSuWS2XS+YTGZTOaTWbTecTmdTueT2fT+gUGhUOiUWjUekUmlUumU2nU+oVGpVOqVWDmCrRcgVmLVuuSsEAACWIAAGBIAAV4AWGIQO0CAABC1wS0Vu7QO5QkA3K5BGy3+BkyvwS93EAX6zQUiQ48xiw2Ox260wW22cAXC5WHJXeCXmEYW";*/

}
