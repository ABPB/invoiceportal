package com.abipb.invoiceportal.util;

/*import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Base64;*/


import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

/**
 * Created by rohann on 08/09/2017.
 */

public class AESHelper {
    private static final Logger log= LoggerFactory.getLogger(AESHelper.class);

    public static final String SALT = "00000000000000000000000000000000";
    public static final String INITIAL_VECTOR = "00000000000000000000000000000000";
    public static final int AES_KEY_LENGTH = 128;
    public static final int AES_KEY_GENERATION_ITERATION_COUNT = 100;
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static final String ABPB_VERSION_TOOL_1 = "12345";
    public static final String ABPB_VERSION_TOOL_2 = "67897";
    public static final String ABPB_VERSION_TOOL_3 = "456321";

    // old
    public static String encryptFile(String seed, String clearText) {
        if (null == clearText) {
            return null;
        }
        return encrypt(seed, clearText);
    }


    public static String decryptFile(String seed, String encryptedText) {
        if (null == encryptedText) {
            return null;
        }
        return decrypt(seed, encryptedText);
    }


    public static String encryptFile(String clearText) {
        if (null == clearText) {
            return null;
        }
        String seed = ABPB_VERSION_TOOL_1 + ABPB_VERSION_TOOL_2 + ABPB_VERSION_TOOL_3;

        return encrypt(seed, clearText);
    }

    /**
     * Decrypts the text
     *
     * @param encryptedText The text you want to encryptFile
     * @return Decrypted data if successful, or null if unsucessful
     */
    public static String decryptFile(String encryptedText) {
        if (null == encryptedText) {
            return null;
        }
        String seed = ABPB_VERSION_TOOL_1 + ABPB_VERSION_TOOL_2 + ABPB_VERSION_TOOL_3;

        return decrypt(seed, encryptedText);
    }


//    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String toHex(String arg) {
        return String.format("%040x", new BigInteger(1, arg.getBytes(StandardCharsets.UTF_8)));
    }

    /*Function to convert Hex to String */
    public static String hexToString(String HexStr) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < HexStr.length(); i += 2) {
            String str = HexStr.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));

        }
        return output.toString();
    }


    public static String encrypt(String passphrase, String plaintext) {
        if (null == plaintext) {
            return null;
        }
        try {
            SecretKey key = generateKey(SALT, passphrase);
            byte[] encrypted = doFinal(Cipher.ENCRYPT_MODE, key, INITIAL_VECTOR, plaintext.getBytes("UTF-8"));
            return base64(encrypted);
        } catch (Exception e) {
            log.error("Exception ::",e);
            return null;
        }
    }


    public static String encrypt(String plaintext) {
        if (null == plaintext) {
            return null;
        }
        try {

            String seed = ABPB_VERSION_TOOL_1 + ABPB_VERSION_TOOL_2 + ABPB_VERSION_TOOL_3;
            return encrypt(seed, plaintext);
        } catch (Exception e) {
            log.error("Exception ::",e);
            return null;
        }
    }


    public static String decrypt(String passphrase, String ciphertext) {
        if (null == ciphertext) {
            return null;
        }
        try {
            SecretKey key = generateKey(SALT, passphrase);
            byte[] sd = base64(ciphertext);
            byte[] decrypted = doFinal(Cipher.DECRYPT_MODE, key, INITIAL_VECTOR, sd);
            return new String(decrypted, "UTF-8");
        } catch (Exception e) {
            log.error("Exception ::",e);
            return null;

        }
    }

    public static String decrypt(String ciphertext) {
        if (null == ciphertext) {
            return null;
        }
        try {
            String seed = ABPB_VERSION_TOOL_1 + ABPB_VERSION_TOOL_2 + ABPB_VERSION_TOOL_3;
            return decrypt(seed, ciphertext);
        } catch (Exception e) {
            log.error("Exception ::",e);
            return null;

        }
    }

    private static byte[] doFinal(int encryptMode, SecretKey key, String iv, byte[] bytes) {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] ivArray = hexStringtoByteArray(iv);
//			Log.d("byte[] of IV", ivArray.toString());
            cipher.init(encryptMode, key, new IvParameterSpec(ivArray));
            return cipher.doFinal(bytes);
        } catch (Exception e) {
            log.error("Exception ::",e);
            return null;
        }
    }

    private static SecretKey generateKey(String salt, String passphrase) {
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            KeySpec spec = new PBEKeySpec(passphrase.toCharArray(), hexStringtoByteArray(salt), AES_KEY_GENERATION_ITERATION_COUNT, AES_KEY_LENGTH);
            SecretKey key = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
            return key;
        } catch (Exception e) {
            log.error("Exception ::",e);
            return null;
        }
    }

    public static String random(int length) {
        byte[] salt = new byte[length];
        new SecureRandom().nextBytes(salt);
        return hexByteArrayToString(salt);
    }

    public static String base64(byte[] bytes) {
        return Base64File.encode(bytes);
    }

    public static byte[] base64(String str) {

        return Base64.decodeBase64(str);//decode(str, Base64.DEFAULT);
    }

    public static String hexByteArrayToString(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
        // return Hex.encodeHexString(bytes);
    }

    public static byte[] hexStringtoByteArray(String str) {
        int len = str.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
        }
        return data;

    }
}
