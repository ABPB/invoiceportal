package com.abipb.invoiceportal.constants;

/**
 * Created by Venkatesh.R on 9/18/2017.
 */
public class CrystalConstants {

    public static final String ENTERPRISE_PUSH_CHANNEL = "01";
    public static final String ENTERPRISE_PULL_CHANNEL = "02";


    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_USER = "USER";


}
