package com.abipb.invoiceportal.constants;



import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Venkatesh.R on 9/19/2017.
 */
public class CrystalUtils {
    public static Date convertTimestampToDate (Timestamp batchTime) {
        if (batchTime != null) {
            long milliseconds = batchTime.getTime() + (batchTime.getNanos() / 1000000);
            Date batchconvertedDate = new java.util.Date(milliseconds);
            return batchconvertedDate;
        } else {
            return null;
        }

    }
}
