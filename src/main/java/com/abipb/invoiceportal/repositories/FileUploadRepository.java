package com.abipb.invoiceportal.repositories;

import com.abipb.invoiceportal.entities.FileUpload;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileUploadRepository extends JpaRepository<FileUpload, String> {

    Iterable<FileUpload> findLast5FileUploads();

}
