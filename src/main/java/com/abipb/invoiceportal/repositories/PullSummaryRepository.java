package com.abipb.invoiceportal.repositories;

import com.abipb.invoiceportal.entities.CrsSummary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PullSummaryRepository extends JpaRepository<CrsSummary, Long> {
}
