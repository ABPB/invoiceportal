package com.abipb.invoiceportal.repositories;

import com.abipb.invoiceportal.entities.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer> {

}
