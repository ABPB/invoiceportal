package com.abipb.invoiceportal.repositories;

import com.abipb.invoiceportal.entities.CrsSummaryPush;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PushSummaryRepository extends JpaRepository<CrsSummaryPush, Long> {
}
