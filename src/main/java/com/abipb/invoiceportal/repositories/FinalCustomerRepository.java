package com.abipb.invoiceportal.repositories;

/**
 * Created by Venkatesh.R on 8/30/2017.
 */

import com.abipb.invoiceportal.entities.CrsFinalCustomer;
import com.abipb.invoiceportal.entities.CrsFinalCustomerPK;
import org.springframework.data.repository.CrudRepository;


public interface FinalCustomerRepository extends CrudRepository<CrsFinalCustomer, CrsFinalCustomerPK>{
    Iterable<CrsFinalCustomer>  findByFcChannel(String channel);
}
