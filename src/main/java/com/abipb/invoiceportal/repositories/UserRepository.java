package com.abipb.invoiceportal.repositories;

import com.abipb.invoiceportal.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
    User findByNlmUserId(String username);
}
