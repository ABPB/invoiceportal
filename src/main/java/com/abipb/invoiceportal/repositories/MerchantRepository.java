package com.abipb.invoiceportal.repositories;

import com.abipb.invoiceportal.entities.Merchant;
import org.springframework.data.repository.CrudRepository;

public interface MerchantRepository extends CrudRepository<Merchant, String> {

}
