package com.abipb.invoiceportal.repositories;

import com.abipb.invoiceportal.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
