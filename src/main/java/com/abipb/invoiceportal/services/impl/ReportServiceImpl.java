package com.abipb.invoiceportal.services.impl;


import com.abipb.invoiceportal.constants.CrystalConstants;
import com.abipb.invoiceportal.constants.CrystalUtils;
import com.abipb.invoiceportal.dto.BatchDTO;
import com.abipb.invoiceportal.dto.ReportDTO;
import com.abipb.invoiceportal.dto.ReportSummaryDTO;
import com.abipb.invoiceportal.entities.CrsSummary;
import com.abipb.invoiceportal.entities.CrsSummaryPush;
import com.abipb.invoiceportal.repositories.FinalCustomerRepository;
import com.abipb.invoiceportal.repositories.PullSummaryRepository;
import com.abipb.invoiceportal.repositories.PushSummaryRepository;
import com.abipb.invoiceportal.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * Created by Venkatesh.R on 8/30/2017.
 */

@Service
public class ReportServiceImpl implements ReportService {

    private FinalCustomerRepository finalCustomerRepository;

    private PullSummaryRepository pullSummaryRepository;

    private PushSummaryRepository pushSummaryRepository;

    @Autowired
    public void setFinalCustomerRepository(FinalCustomerRepository finalCustomerRepository, PullSummaryRepository pullSummaryRepository, PushSummaryRepository pushSummaryRepository) {
        this.finalCustomerRepository = finalCustomerRepository;
        this.pullSummaryRepository = pullSummaryRepository;
        this.pushSummaryRepository = pushSummaryRepository;
    }

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<BatchDTO> listAllUniqueBatches(String channel, String merchId) {
        //return finalCustomerRepository.findByFcChannel(channel);

        if (CrystalConstants.ENTERPRISE_PULL_CHANNEL.equalsIgnoreCase(channel)) {
            Iterable<CrsSummary> pullBatchList = pullSummaryRepository.findAll();

            if (merchId != null && !isEmpty(merchId)) {
                List<CrsSummary> filteredPullBatchList = new ArrayList<CrsSummary>();
                for (CrsSummary batch : pullBatchList) {
                    if (batch.getCsMerchId() != null
                            && !isEmpty(batch.getCsMerchId())
                            && merchId.equalsIgnoreCase(batch.getCsMerchId())) {

                         filteredPullBatchList.add(batch);
                    }
                }
                pullBatchList = filteredPullBatchList;
            }

            BatchDTO batchDTO = null;
            Map<String, BatchDTO> batchDTOMap = new TreeMap<>();

            for (CrsSummary batch : pullBatchList) {
                String batchId = batch.getCsBatchId();
                Timestamp batchTime = batch.getCsCreatedDate();

                if (batchId != null && !isEmpty(batchId)) {
                    batchDTO = new BatchDTO();
                    batchDTO.setBatchId(batchId);
                    batchDTO.setChannel(CrystalConstants.ENTERPRISE_PULL_CHANNEL);
                }
                if (batchTime != null) {
                    java.util.Date batchDate = CrystalUtils.convertTimestampToDate(batchTime);
                    batchDTO.setBatchDate(batchDate);
                }
                batchDTOMap.put(batchDTO.getBatchId(), batchDTO);

            }

            return new ArrayList<>(batchDTOMap.values());

        } else {

            BatchDTO batchDTO = null;
            Map<String, BatchDTO> batchDTOMap = new TreeMap<>();
            Iterable<CrsSummaryPush> pushBatchList = pushSummaryRepository.findAll();

            if (merchId != null && !isEmpty(merchId)) {
                List<CrsSummaryPush> filteredPushBatchList = new ArrayList<CrsSummaryPush>();
                for (CrsSummaryPush batch : pushBatchList) {
                    if (batch.getCsMerchId() != null
                            && !isEmpty(batch.getCsMerchId())
                            && merchId.equalsIgnoreCase(batch.getCsMerchId())) {

                            filteredPushBatchList.add(batch);
                    }
                }
                pushBatchList = filteredPushBatchList;
            }

            for (CrsSummaryPush batch : pushBatchList) {
                String batchId = batch.getCsBatchId();
                Timestamp batchTime = batch.getCsCreatedDate();

                if (batchId != null && !isEmpty(batchId)) {
                    batchDTO = new BatchDTO();
                    batchDTO.setBatchId(batchId);
                    batchDTO.setChannel(CrystalConstants.ENTERPRISE_PUSH_CHANNEL);
                }
                if (batchTime != null) {
                    java.util.Date batchDate = CrystalUtils.convertTimestampToDate(batchTime);
                    batchDTO.setBatchDate(batchDate);
                }
                batchDTOMap.put(batchDTO.getBatchId(), batchDTO);

            }

            return new ArrayList<>(batchDTOMap.values());
        }
    }

    @Override
    public ReportSummaryDTO getReportSummaryDetails(String batchId, java.util.Date batchDate, String channel) {

        String parsedDate = null;

        if (CrystalConstants.ENTERPRISE_PULL_CHANNEL.equalsIgnoreCase(channel)) {


            StringBuilder queryStr = new StringBuilder("select fc_batch_id, count(fc_batch_id) AS TOTAL,")
                    .append("count(decode(fc_status,'1',cs_status,'2',cs_status,'3',cs_status)) AS INPROCESS_COUNT,")
                    .append("count(decode(fc_status,'4',cs_status)) AS SUCCESS_COUNT,")
                    .append("count(decode(fc_status,'5',cs_status)) AS FAILURE_COUNT,")
                    .append("count(decode(fc_status,'6',cs_status)) AS EXPIRED_COUNT ")
                    .append("from crs_final_customer, crs_status where fc_status=cs_status_id ");

            if (batchId != null && !isEmpty(batchId)) {
                queryStr.append("and fc_batch_id ='")
                        .append(batchId)
                        .append("'");
            }

            if (batchDate != null && !isEmpty(batchDate.toString())) {
                parsedDate = new SimpleDateFormat("dd/MM/yyyy").format(batchDate);
                queryStr.append(" and ")
                        .append("to_char(fc_created_date,'DD/MM/YYYY')=")
                        .append("'")
                        .append(parsedDate)
                        .append("'");
            }

            /*if (channel != null && !StringUtils.isEmpty(channel)) {
                queryStr.append("and fc_channel ='")
                        .append(channel)
                        .append("'");

            }*/

            queryStr.append(" GROUP BY fc_batch_id");
            System.out.println("queryStr" + queryStr);
            ReportSummaryDTO reportSummaryDTO = new ReportSummaryDTO();
            Object[] result = (Object[]) entityManager.createNativeQuery(queryStr.toString()).getSingleResult();

            if (result != null && result.length >= 6) {
                reportSummaryDTO.setBatchId((String) result[0]);
                reportSummaryDTO.setTotal((BigDecimal) result[1]);
                reportSummaryDTO.setProcessedCount((BigDecimal) result[2]);
                reportSummaryDTO.setSuccessCount((BigDecimal) result[3]);
                reportSummaryDTO.setFailureCount((BigDecimal) result[4]);
                reportSummaryDTO.setExpiredCount((BigDecimal) result[5]);

            }


            return reportSummaryDTO;
        } else {

            StringBuilder queryStr = new StringBuilder("select cc_batch_id, count(cc_batch_id) AS TOTAL,")
                    .append("count(decode(cc_status,'1',cs_status,'2',cs_status,'3',cs_status)) AS INPROCESS_COUNT,")
                    .append("count(decode(cc_status,'4',cs_status)) AS SUCCESS_COUNT,")
                    .append("count(decode(cc_status,'5',cs_status,'7',cs_status)) AS FAILURE_COUNT,")
                    .append("count(decode(cc_status,'6',cs_status)) AS EXPIRED_COUNT ")
                    .append("from crs_customer_push, crs_status where cc_status=cs_status_id ");

            if (batchId != null && !isEmpty(batchId)) {
                queryStr.append("and cc_batch_id ='")
                        .append(batchId)
                        .append("'");
            }

            if (batchDate != null && !isEmpty(batchDate.toString())) {
                parsedDate = new SimpleDateFormat("dd/MM/yyyy").format(batchDate);
                queryStr.append(" and ")
                        .append("to_char(cc_created_date,'DD/MM/YYYY')=")
                        .append("'")
                        .append(parsedDate)
                        .append("'");
            }

            /*
            if (channel != null && !StringUtils.isEmpty(channel)) {
                queryStr.append("and fc_channel ='")
                        .append(channel)
                        .append("'");

            }
            */

            queryStr.append(" GROUP BY cc_batch_id");
            System.out.println("queryStr" + queryStr);
            ReportSummaryDTO reportSummaryDTO = new ReportSummaryDTO();
            Object[] result = (Object[]) entityManager.createNativeQuery(queryStr.toString()).getSingleResult();

            if (result != null && result.length >= 6) {
                reportSummaryDTO.setBatchId((String) result[0]);
                reportSummaryDTO.setTotal((BigDecimal) result[1]);
                reportSummaryDTO.setProcessedCount((BigDecimal) result[2]);
                reportSummaryDTO.setSuccessCount((BigDecimal) result[3]);
                reportSummaryDTO.setFailureCount((BigDecimal) result[4]);
                reportSummaryDTO.setExpiredCount((BigDecimal) result[5]);

            }
            return reportSummaryDTO;
        }
    }


    @Override
    public List<ReportDTO> getReportDetails(String batchId, java.util.Date batchDate, String channel) {
        String parsedDate = null;
        if (CrystalConstants.ENTERPRISE_PULL_CHANNEL.equalsIgnoreCase(channel)) {
            StringBuilder queryStr = new StringBuilder("select FC_RUNG_SEQ,FC_CUST_ID,FC_CUST_NAME,FC_CUST_VPA,FC_CUST_MOBNO,FC_BILL_NO,")
                    .append("FC_BILL_MONTH,FC_BILL_AMOUNT,FC_BILL_DUEDATE, decode(fc_status,fc_status,cs_status) as STATUS,")
                    .append("FC_REMARK,FC_RECEIVED_AMT,FC_CHANNEL,FC_CALC_AMT,FC_BILL_STAGE,FC_BILL_STAGE_DUEDATE ")
                    .append("from crs_final_customer, crs_status where fc_status = cs_status_id");


            if (batchId != null && !isEmpty(batchId)) {
                queryStr.append(" and fc_batch_id ='")
                        .append(batchId)
                        .append("'");
            }

            if (batchDate != null && !isEmpty(batchDate.toString())) {
                parsedDate = new SimpleDateFormat("dd/MM/yyyy").format(batchDate);
                queryStr.append(" and ")
                        .append("to_char(fc_created_date,'DD/MM/YYYY')=")
                        .append("'")
                        .append(parsedDate)
                        .append("'");
            }

            queryStr.append(" order by fc_cust_id, fc_rung_seq");

            System.out.println("queryStr Detail" + queryStr.toString());

            List<ReportDTO> reportDetailsList = new ArrayList<>();
            List<Object> resultSet = entityManager.createNativeQuery(queryStr.toString()).getResultList();

            if (resultSet != null && resultSet.size() > 0) {
                for (Object rec : resultSet) {
                    ReportDTO reportDTO = new ReportDTO();
                    Object[] typedRec = (Object[]) rec;


                    if (typedRec != null && typedRec.length >= 16) {
                        reportDTO.setRunSeq((BigDecimal) typedRec[0]);
                        reportDTO.setCustId((String) typedRec[1]);
                        reportDTO.setCustName((String) typedRec[2]);
                        reportDTO.setCustVPA((String) typedRec[3]);
                        reportDTO.setCustMobNo((BigDecimal) typedRec[4]);
                        reportDTO.setBillNo((String) typedRec[5]);
                        reportDTO.setBillMonth((String) typedRec[6]);
                        reportDTO.setBillAmount((BigDecimal) typedRec[7]);
                        reportDTO.setBillDueDate((Timestamp) typedRec[8]);
                        reportDTO.setStatus((String) typedRec[9]);
                        reportDTO.setRemark((String) typedRec[10]);
                        reportDTO.setReceivedAmt((BigDecimal) typedRec[11]);
                        reportDTO.setChannel((String) typedRec[12]);
                        reportDTO.setCalcAmt((BigDecimal) typedRec[13]);
                        reportDTO.setBillStage((String) typedRec[14]);
                        reportDTO.setBillStageDueDate((Timestamp) typedRec[15]);

                        reportDetailsList.add(reportDTO);
                    }

                }
            }

            return reportDetailsList;
        } else {
            StringBuilder queryStr = new StringBuilder("select CC_RUNG_SEQ,CC_CUSTID,CC_CUSTOMER_NAME,CC_CUSTOMER_VPA,CC_CUSTOMER_ACC,CC_CUSTOMER_IFSC,")
                    .append("CC_TXN_MODE,CC_AMOUNT, decode(a.cc_status,a.cc_status,b.cs_status) as STATUS,")
                    .append("CC_REMARK,CC_CREATED_DATE,CC_RECEIVED_AMT ")
                    .append("from crs_customer_push a, crs_status b where a.cc_status = b.cs_status_id");

            if (batchId != null && !isEmpty(batchId)) {
                queryStr.append(" and cc_batch_id ='")
                        .append(batchId)
                        .append("'");
            }

            if (batchDate != null && !isEmpty(batchDate.toString())) {
                parsedDate = new SimpleDateFormat("dd/MM/yyyy").format(batchDate);
                queryStr.append(" and ")
                        .append("to_char(cc_created_date,'DD/MM/YYYY')=")
                        .append("'")
                        .append(parsedDate)
                        .append("'");
            }

            queryStr.append(" order by cc_custid, cc_rung_seq");

            System.out.println("queryStr Detail" + queryStr.toString());

            List<ReportDTO> reportDetailsList = new ArrayList<>();
            List<Object> resultSet = entityManager.createNativeQuery(queryStr.toString()).getResultList();

            if (resultSet != null && resultSet.size() > 0) {
                for (Object rec : resultSet) {
                    ReportDTO reportDTO = new ReportDTO();
                    Object[] typedRec = (Object[]) rec;

                    if (typedRec != null && typedRec.length >= 12) {
                        reportDTO.setRunSeq((typedRec[0] != null) ? (BigDecimal) typedRec[0] : null);
                        reportDTO.setCustId((String) typedRec[1]);
                        reportDTO.setCustName((String) typedRec[2]);
                        reportDTO.setCustVPA((String) typedRec[3]);
                        reportDTO.setCustAcct((String) typedRec[4]);
                        reportDTO.setCustIFSC((String) typedRec[5]);
                        reportDTO.setTxnMode((String) typedRec[6]);
                        reportDTO.setPushAmount((BigDecimal) typedRec[7]);
                        reportDTO.setStatus((String) typedRec[8]);
                        reportDTO.setRemark((String) typedRec[9]);
                        reportDTO.setCreatedDate(CrystalUtils.convertTimestampToDate((Timestamp) typedRec[10]));
                        reportDTO.setReceivedAmt((BigDecimal) typedRec[11]);

                        reportDetailsList.add(reportDTO);
                    }
                }

            }

            return reportDetailsList;
        }

    }
}
