package com.abipb.invoiceportal.services.impl;


import com.abipb.invoiceportal.dto.NachAckData;
import com.abipb.invoiceportal.repository.MandateTransactionRepository;
import com.abipb.invoiceportal.services.MandateTransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by abhay.kumar-v on 12/18/2017.
 */
@Service
public class MandateTransactionServiceImpl implements MandateTransactionService {
    private static final Logger log= LoggerFactory.getLogger(MandateTransactionServiceImpl.class);

    @Autowired
    MandateTransactionRepository mandateTransactionRepository;
    @Override
    public String saveTransactionAck(List<NachAckData> nachAckDataList) {
        return mandateTransactionRepository.saveTransactionAck(nachAckDataList);
    }


     /*
    @Autowired
    private MandateTransactionHelper mandateTransactionHelper;
    
   @Autowired
    private MandateTransactionRepository mandateTransactionRepository;
*/
}
