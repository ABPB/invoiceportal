package com.abipb.invoiceportal.services.impl;

import com.abipb.invoiceportal.entities.User;
import com.abipb.invoiceportal.repositories.UserRepository;
import com.abipb.invoiceportal.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    //@Autowired
    //private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        //user.setClrPwd(bCryptPasswordEncoder.encode(user.getClrPwd()));
//        user.setRoles(new HashSet<>(roleRepository.findAll()));
        user.setNlmPassword(user.getNlmPassword());
        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByNlmUserId(username);
//        return userRepository.findOne(username);
    }
}
