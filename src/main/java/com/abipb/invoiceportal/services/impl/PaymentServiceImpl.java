package com.abipb.invoiceportal.services.impl;



import com.abipb.invoiceportal.dto.*;
import com.abipb.invoiceportal.exception.PaymentException;
import com.abipb.invoiceportal.repository.PaymentRepository;
import com.abipb.invoiceportal.services.PaymentService;
import com.abipb.invoiceportal.services.UPIInterfaceService;
import com.abipb.invoiceportal.util.*;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.ws.rs.core.UriBuilder;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


/**
 * Created by Venkatesh.R on 10/15/2017.
  */

@Service
public class PaymentServiceImpl implements PaymentService {

    private static final Logger log= LoggerFactory.getLogger(PaymentServiceImpl.class);
  /*  @Autowired
    BankDetailsRepository bankDetailsRepository;

    @Autowired
    UPIDetailsRepository upiDetailsRepository;

    @Autowired
    PersonalInformationRepository personalInformationRepository;

    @Autowired
    BankIFSCRepository bankIFSCRepository;*/

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private Bookmarks bookmarks;
    @Autowired
    private PaymentRepository paymentRepository;

    @Value("${pg.netbanking.payment.endpoint}")
    private String pgNbEndpoint;

    @Value("${spring.datasource.driver-class-name}")
    private  String driverClass;

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private  String userName;

    @Value("${spring.datasource.password}")
    private  String password;

    @Autowired
    private PropertiesValue propertiesValue;
    @Autowired
    private PaymentBuilder paymentBuilder;
    @Autowired
    private UPIInterfaceService upiInterfaceService;
    private static final String PAYMENT_CONTROLLER_URI = "/doPayment";

   // private  CBSService cbsService;
    @Override
    @Transactional
    public PaymentCreateClientResponse createPayment( PaymentCreateRequest paymentCreateRequest) throws PaymentException{
        log.debug("paymentCreateRequest :"+paymentCreateRequest);
        PaymentCreateResponse paymentCreateResponse = null;
        PaymentCreateClientResponse paymentCreateClientResponse = new PaymentCreateClientResponse();
            Connection connection = null;
            CallableStatement stmt=null;
            try {
                if (driverClass != null && url != null && userName != null && password != null) {
                    Class.forName(driverClass);
                    connection = DriverManager.getConnection(url, userName, password);
                }else{
                    throw  new PaymentException("500","Invalid Connection Informatin");
                }
                if ((connection != null) && !connection.isClosed() ){

                    ArrayDescriptor invoiceDetailsDescriptor = ArrayDescriptor.createDescriptor("ABPG_INVOICE_DETAILS_TYP", connection);
                    StructDescriptor invoiceDetailsStruct = StructDescriptor.createDescriptor("ABPG_INVOICE_DETAILS_OBJ", connection);

                    List<InvoiceDtls> invoiceDetailsList = paymentCreateRequest.getInvoiceDtlsList();
                    STRUCT[] invoiceDetailsStructs = null;
                    invoiceDetailsStructs = new STRUCT[invoiceDetailsList.size()];
                    InvoiceDtls invoiveDetails = null;

                    for (int index = 0; index < invoiceDetailsList.size(); index++) {
                        Object[] params = new Object[3];
                        invoiveDetails = (InvoiceDtls) invoiceDetailsList.get(index);
                        if (invoiveDetails != null) {
                            params[0] = invoiveDetails.getInvoiceId();
                            params[1] = invoiveDetails.getDueDate();
                            params[2] = invoiveDetails.getInvoiceAmount();
                        }
                        STRUCT struct = new STRUCT(invoiceDetailsStruct, connection, params);
                        invoiceDetailsStructs[index] = struct;
                    }
                    ARRAY invoiceDetailsArray = new ARRAY(invoiceDetailsDescriptor, connection, invoiceDetailsStructs);
                    int index = 1;
                    stmt = connection.prepareCall("{ call p_b2b_pg_upi_integration_v1(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) } ");
                    stmt.setString(index++, paymentCreateRequest.getFromEntityId());
                    stmt.setString(index++, paymentCreateRequest.getToEntityId());
                    stmt.setString(index++, paymentCreateRequest.getPaymentMode());
                    stmt.setArray(index++, invoiceDetailsArray);
                    stmt.registerOutParameter(index++, Types.NUMERIC); //p_sum_amount
                    stmt.registerOutParameter(index++, Types.VARCHAR);  //p_channel
                    stmt.registerOutParameter(index++, Types.VARCHAR); //p_trans_intnl_ref_no
                    stmt.registerOutParameter(index++, Types.VARCHAR); // p_from_bank_id
                    stmt.registerOutParameter(index++, Types.NUMERIC); // p_from_bank_account_no
                    stmt.registerOutParameter(index++, Types.VARCHAR); // p_from_bank_ifsc
                    stmt.registerOutParameter(index++, Types.VARCHAR); // p_from_upi_handler
                    stmt.registerOutParameter(index++, Types.VARCHAR); // p_to_bank_id
                    stmt.registerOutParameter(index++, Types.NUMERIC); // p_to_bank_account_no
                    stmt.registerOutParameter(index++, Types.VARCHAR); // p_to_bank_ifsc

                    stmt.registerOutParameter(index++, Types.VARCHAR); // p_to_upi_handler

                    stmt.registerOutParameter(index++, Types.NUMERIC); // p_to_mobile_no
                    stmt.registerOutParameter(index++, Types.NUMERIC); // p_from_mobile_no
                    stmt.registerOutParameter(index++, Types.VARCHAR); // p_from_email_id
                    stmt.registerOutParameter(index++, Types.VARCHAR);   //p_msg
                    stmt.setLong(index, Long.parseLong(paymentCreateRequest.getMerchantId()));//p_mer_mst_id added by abhay for UPI


                    stmt.execute();

                    paymentCreateResponse = new PaymentCreateResponse();

                    paymentCreateResponse.setMwTxnRef(stmt.getString(7));
                    paymentCreateResponse.setFromBankAcctNumber(stmt.getString(9));
                    paymentCreateResponse.setFromBankId(stmt.getString(8));
                    paymentCreateResponse.setToBankAcctNumber(stmt.getString(13));
                    paymentCreateResponse.setToBankId(stmt.getString(12));
                    paymentCreateResponse.setPaymentAmount(stmt.getBigDecimal(5));
                    paymentCreateResponse.setPaymentChannel(stmt.getString(6));
                    //   paymentCreateResponse.setClientTxnRef(stmt.getString(paymentCreateRequest.getClientTxnRef()));
                    paymentCreateResponse.setPaymentStatus(stmt.getString(19));
           //start to add by abhay for UPI
                    if (PaymentChannelEnum.UPI_CHANNEL.getPaymentChannel().equalsIgnoreCase(paymentCreateResponse.getPaymentChannel())) {
                        paymentCreateResponse = new PaymentCreateResponse();
                        paymentCreateResponse.setMwTxnRef(stmt.getString(7));
                        paymentCreateResponse.setFromUPIHandler(stmt.getString(11));
                        paymentCreateResponse.setToUPIHandler(stmt.getString(15));
                        paymentCreateResponse.setPaymentAmount(stmt.getBigDecimal(5));
                        log.debug("payerHandler : " + paymentCreateResponse.getFromUPIHandler() +"payeeHandler :: "+paymentCreateResponse.getToUPIHandler() +" amount :: "
                                +paymentCreateResponse.getPaymentAmount() +" txnId :: "+paymentCreateResponse.getMwTxnRef());
                        String httpResponse="";

                        httpResponse = upiInterfaceService.getUPICollection(paymentCreateResponse.getFromUPIHandler(), paymentCreateResponse.getToUPIHandler()
                                , StringUtils.isEmpty(paymentCreateResponse.getPaymentAmount()) ? "0.00" : String.format("%.2f", paymentCreateResponse.getPaymentAmount()),
                                paymentCreateResponse.getMwTxnRef());

                       /* if (paymentCreateRequest.getPaymentMode().equalsIgnoreCase(PaymentChannelEnum.UPI_CHANNEL_PUSH.getPaymentChannel())){
                             httpResponse = upiInterfaceService.getUPICollection(paymentCreateResponse.getToUPIHandler(), paymentCreateResponse.getFromUPIHandler()
                                    , StringUtils.isEmpty(paymentCreateResponse.getPaymentAmount()) ? "0.00" : String.format("%.2f",paymentCreateResponse.getPaymentAmount()),
                                    paymentCreateResponse.getMwTxnRef());
                        }else {
                             httpResponse = upiInterfaceService.getUPICollection(paymentCreateResponse.getFromUPIHandler(), paymentCreateResponse.getToUPIHandler()
                                    , StringUtils.isEmpty(paymentCreateResponse.getPaymentAmount()) ? "0.00" : String.format("%.2f", paymentCreateResponse.getPaymentAmount()),
                                    paymentCreateResponse.getMwTxnRef());
                        }*/
                        log.debug("httpResponse ::  " + httpResponse);
                        paymentCreateClientResponse.setStatus(
                                ValidationEnum.VALIDATION_SUCCESS.getValidationState().equalsIgnoreCase(httpResponse) ?
                                        ValidationEnum.VALIDATION_SUCCESS.getValidationState() : ValidationEnum.VALIDATION_FAILURE.getValidationState()
                            );
                        paymentCreateClientResponse.setMwTxnRef(paymentCreateResponse.getMwTxnRef());
                        paymentCreateClientResponse.setClientPaymentChannel(ClientPaymentChannelEnum.DIRECT_PAYMENT_CHANNEL.getClientPaymentChannel());
                        paymentCreateClientResponse.setPaymentAmount(paymentCreateResponse.getPaymentAmount());
//end to add by abhay fro UPI
                    }
                    // CBS Payment by G Penchalaiah
                    /*else if(PaymentChannelEnum.CBS_CHANNEL.getPaymentChannel().equalsIgnoreCase(paymentCreateResponse.getPaymentChannel()))
                    {
                        ConcurrentMap<String, String> concurrentMap = new ConcurrentHashMap<>();
                        RequestCBS requestCBS=new RequestCBS();
                        ADBServiceRequest adbServiceRequest=new ADBServiceRequest();
                        ADBMessageHeader adbMessageHeader=new ADBMessageHeader();
                        RequestData requestData=new RequestData();

                        MessageIdentifyGroup messageIdentifyGroup=new MessageIdentifyGroup();
                        UserAttributeGroup userAttributeGroup=new UserAttributeGroup();
                        AdditionalInfo additionalInfo=new AdditionalInfo();

                        RetailerInfo retailerInfo=new RetailerInfo();
                        RemitInfo remitInfo=new RemitInfo();
                        CreditorInfo creditorInfo = new CreditorInfo();
                        DebitorInfo debitorInfo=new DebitorInfo();

                        StaffId staffId=new StaffId();

                        SourceChannelInfoGroup sourceChannelInfoGroup=new SourceChannelInfoGroup();
                        MessageAttributeGroup messageAttributeGroup=new MessageAttributeGroup();

                        AddressInfo addressInfo=new AddressInfo();
                        com.abipb.middleware.domain.cbs.BankDetails bankDetails=new com.abipb.middleware.domain.cbs.BankDetails();


                        sourceChannelInfoGroup.setSourceChannel("RAA");
                        sourceChannelInfoGroup.setSourceApplication("RAA");

                        messageAttributeGroup.setServiceName("FundTransfer");
                        messageAttributeGroup.setServiceVersionNumber("1.0");


                        messageIdentifyGroup.setApplicationTranId(paymentCreateResponse.getMwTxnRef()); //setting txnNo
                        messageIdentifyGroup.setRqstDateTime( DateUtil.getFormattedDate("yyyy-MM-dd'T'HH:mm:ss"));
                        messageIdentifyGroup.setSourceChannelInfoGroup(sourceChannelInfoGroup);
                        messageIdentifyGroup.setMessageAttributeGroup(messageAttributeGroup);

                        userAttributeGroup.setChannelId("RAA");
                        userAttributeGroup.setStaffId(staffId);
                        userAttributeGroup.setDeviceId("172.28.114.22");
                        additionalInfo.setValue("23462        |   |          |001|MU001|");
                        additionalInfo.setName("CommissionData");


                        adbMessageHeader.setMessageIdentifyGroup(messageIdentifyGroup);
                        adbMessageHeader.setUserAttributeGroup(userAttributeGroup);
                        adbMessageHeader.setAdditionalInfo(additionalInfo);

                        requestData.setXmlns("");

                        remitInfo.setRemitAmount(String.valueOf(paymentCreateResponse.getPaymentAmount()));
                        remitInfo.setRemitCurrency("INR");
                        remitInfo.setRemitReversal("false");
                        bankDetails.setBankName("IdeaPayment");
                        bankDetails.setAccountId(*//*paymentCreateResponse.getToBankAcctNumber()*//*"7904158324");
                        bankDetails.setAddressInfo(addressInfo);
                        bankDetails.setBankId(paymentCreateResponse.getToBankId());
                        bankDetails.setBranchId("000000");

                        creditorInfo.setAddressInfo(addressInfo);
                        creditorInfo.setBankDetails(bankDetails);
                        creditorInfo.setMobileNumber("9494833112");
                        creditorInfo.setCreditRemark("Cash Deposit");

                        requestData.setRetailerInfo(retailerInfo);
                        requestData.setRemitInfo(remitInfo);
                        requestData.setCreditorInfo(creditorInfo);
                        requestData.setDebitorInfo(debitorInfo);
                        requestData.setPaymentMode("1");
                        requestData.setPaymentProd("AssistedCD");

                        debitorInfo.setAccountId(*//*paymentCreateResponse.getFromBankAcctNumber()*//*"9789051932");

                        adbServiceRequest.setAdbMessageHeader(adbMessageHeader);
                        adbServiceRequest.setRequestData(requestData);
                        requestCBS.setAdbServiceRequest(adbServiceRequest);
                        log.debug("requestCBS ::  " + requestCBS);
                        ResponseCBS cbsResponse = cbsService.intraBankFundTransfer(requestCBS);
                        log.debug("cbsResponse ::  " + cbsResponse);

                        StringBuilder responseMsg=new StringBuilder();
                        responseMsg.append(cbsResponse.getAdbServiceResponse().getAdbMessageHeader().getMessageIdentifyGroup().getApplicationTranId());
                        responseMsg.append("|");
                        responseMsg.append(cbsResponse.getAdbServiceResponse().getStatus().getReasonCode());
                        responseMsg.append("|");
                        responseMsg.append(cbsResponse.getAdbServiceResponse().getStatus().getReasonDesc());
                        responseMsg.append("|");
                        responseMsg.append(cbsResponse.getAdbServiceResponse().getResponseData().getUtrNumber());
                        String cbsProcRes=getCBSResponse(responseMsg.toString());

                        if(cbsProcRes==null ||cbsProcRes.equalsIgnoreCase("Failure") || cbsResponse==null){
                            throw  new CBSException("Internal Server Error");
                        }else {
                            paymentCreateClientResponse.setStatus(cbsResponse.getAdbServiceResponse().getStatus().getReasonCode().equalsIgnoreCase("000")&& cbsProcRes.equalsIgnoreCase("Success")?"Success":"Failure");
                            paymentCreateClientResponse.setPaymentAmount(paymentCreateResponse.getPaymentAmount());
                            paymentCreateClientResponse.setMwTxnRef(paymentCreateResponse.getMwTxnRef());
                        }
                    }*/
                    else{

                    if (PaymentChannelEnum.PG_NET_BANKING_CHANNEL.getPaymentChannel().equalsIgnoreCase(paymentCreateResponse.getPaymentChannel()) || PaymentChannelEnum.PG_CREDIT_CARD_CHANNEL.getPaymentChannel().equalsIgnoreCase(paymentCreateResponse.getPaymentChannel())
                            || PaymentChannelEnum.PG_DEBIT_CARD_CHANNEL.getPaymentChannel().equalsIgnoreCase(paymentCreateResponse.getPaymentChannel()))
                        paymentCreateClientResponse.setClientPaymentChannel(ClientPaymentChannelEnum.REDIRECT_PAYMENT_CHANNEL.getClientPaymentChannel());
                    else
                        paymentCreateClientResponse.setClientPaymentChannel(ClientPaymentChannelEnum.DIRECT_PAYMENT_CHANNEL.getClientPaymentChannel());

                    paymentCreateClientResponse.setRedirectURL(UriBuilder
                            .fromUri(bookmarks.getMiddlewareApplicationUrl())
                            .path(PAYMENT_CONTROLLER_URI)
                            .queryParam("mwTxnRef", paymentCreateResponse.getMwTxnRef())
                            .queryParam("merchantId", paymentCreateRequest.getMerchantId())
                            .build().toString());
                    paymentCreateClientResponse.setPaymentAmount(paymentCreateResponse.getPaymentAmount());
                    paymentCreateClientResponse.setMwTxnRef(paymentCreateResponse.getMwTxnRef());

                    paymentCreateClientResponse.setStatus(stmt.getString(19));
                }
                    return paymentCreateClientResponse;
                }else{
                    paymentCreateClientResponse.setStatus("Failure");

                    throw new PaymentException("500","Not able to get connection from Database");
                }


            }catch (Exception e){
                log.error("Exception ::",e);
                paymentCreateClientResponse.setStatus("Failure");
            }
            finally{
                if(connection !=null){
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        log.error("SQLException ::",e);
                    }
                }
                if(stmt!=null){
                    try {
                        stmt.close();
                    } catch (SQLException e) {
                        log.error("SQLException ::",e);
                    }
                }
            }

        return paymentCreateClientResponse;
    }

    @Override
    @Transactional
    public PaymentCreateResponse getPaymentDetails(String mwTxnRef) {
        log.debug("mwTxnRef ::"+ mwTxnRef);
        try {
            StoredProcedureQuery storedProcedureQuery = entityManager.
                    createStoredProcedureQuery("P_B2B_TRANS_DETAILS2").
                    registerStoredProcedureParameter("p_intr_trans_id", String.class, ParameterMode.IN)
                    .setParameter("p_intr_trans_id", mwTxnRef)
                    .registerStoredProcedureParameter("p_payment_mode", String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_channel", String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_debt_cust_id", Long.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_debt_bank_ifsc", String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_debt_bank_ac_no", Long.class, ParameterMode.OUT) //
                    .registerStoredProcedureParameter("p_beni_cust_id", Long.class, ParameterMode.OUT) //
                    .registerStoredProcedureParameter("p_beni_bank_ifsc", String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_beni_bank_ac_no", Long.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_trns_tot_amt", BigDecimal.class, ParameterMode.OUT)//
                    .registerStoredProcedureParameter("p_txn_chrg", Long.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_txn_stax", Long.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_txn_date", java.util.Date.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_txn_time", Timestamp.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_from_upi_handler", String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_to_upi_handler", String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_txn_status", String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_msg", String.class, ParameterMode.OUT)
                    // .registerStoredProcedureParameter("p_beni_mob_no", Long.class, ParameterMode.OUT)
                    //  .registerStoredProcedureParameter("p_debt_mob_no", Long.class, ParameterMode.OUT)
                    // .registerStoredProcedureParameter("p_debt_email_id", String.class, ParameterMode.OUT)

                    .registerStoredProcedureParameter("p_debt_bank_id", String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_beni_bank_id", String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_beni_mob_no", Long.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_debt_mob_no", Long.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_debt_email_id", String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter("p_mer_mst_id", Long.class, ParameterMode.OUT);

            storedProcedureQuery.execute();

            //getpaymentdetails from DB based on mwTxnRef (repository or stored proc
            //hardcoded response below
            PaymentCreateResponse paymentCreateResponse = new PaymentCreateResponse();

            paymentCreateResponse.setMwTxnRef(mwTxnRef);
            paymentCreateResponse.setFromBankAcctNumber(String.valueOf(storedProcedureQuery.getOutputParameterValue("p_debt_bank_ac_no")));
            paymentCreateResponse.setFromBankId((String)storedProcedureQuery.getOutputParameterValue("p_debt_bank_id"));
            paymentCreateResponse.setPaymentAmount((BigDecimal) storedProcedureQuery.getOutputParameterValue("p_trns_tot_amt"));
            paymentCreateResponse.setToBankAcctNumber(String.valueOf( storedProcedureQuery.getOutputParameterValue("p_beni_bank_ac_no")));
            paymentCreateResponse.setToBankId((String) storedProcedureQuery.getOutputParameterValue("p_beni_bank_id"));
            paymentCreateResponse.setPaymentChannel(String.valueOf(storedProcedureQuery.getOutputParameterValue("p_channel")));
            paymentCreateResponse.setPaymentStatus(String.valueOf(storedProcedureQuery.getOutputParameterValue("p_txn_status")));
            paymentCreateResponse.setClientTxnRef("" + new Random().nextInt(9999999));
            paymentCreateResponse.setCustomerMobileNo(String.valueOf( storedProcedureQuery.getOutputParameterValue("p_debt_mob_no")));
            paymentCreateResponse.setCustomerEmailId(String.valueOf( storedProcedureQuery.getOutputParameterValue("p_debt_email_id")));

            return paymentCreateResponse;
        } catch (Exception e) {
            log.error("Exception :",e);
        }
        return null;

    }
    @Override
    public PaymentResponse cancelPayment(PaymentRequest paymentRequest) {
            PaymentResponse paymentResponse=new PaymentResponse();
            log.debug("Payment Request : "+paymentRequest);
            paymentResponse.setId(paymentRequest.getId());
            paymentResponse.setStatus(ValidationEnum.VALIDATION_SUCCESS.getValidationState());
            log.debug("Payment Response : "+paymentResponse);
        return paymentResponse;
    }

    @Override
    public PaymentResponse holdPayment(PaymentRequest paymentRequest) {
        PaymentResponse paymentResponse=new PaymentResponse();
        log.debug("Payment Request : "+paymentRequest);
        paymentResponse.setId(paymentRequest.getId());
        paymentResponse.setStatus(ValidationEnum.VALIDATION_SUCCESS.getValidationState());
        log.debug("Payment Response : "+paymentResponse);
        return paymentResponse;
    }

    @Override
    public PaymentRootResponse getPaymentDetail(String transactionId) {
        PaymentRootResponse paymentRootResponse=new PaymentRootResponse();
        paymentRootResponse.setId(transactionId);
        paymentRootResponse.setCreateTime("22-09-2017T20:53:43");
        paymentRootResponse.setPaymentMode("NETB");
        paymentRootResponse.setPaymentStatus(ValidationEnum.VALIDATION_SUCCESS.getValidationState());
        PaymentLink paymentLink1=new PaymentLink();
        paymentLink1.setHref("https://middleware.adityabirla.com/payment/"+transactionId);
        paymentLink1.setMethod("GET");
        paymentLink1.setRel("self");

        PaymentLink paymentLink2=new PaymentLink();
        paymentLink2.setHref("https://middleware.adityabirla.com/payment/"+transactionId+"/cancel");
        paymentLink2.setMethod("POST");
        paymentLink2.setRel("Cancel");

        PaymentLink paymentLink3=new PaymentLink();
        paymentLink3.setHref("https://middleware.adityabirla.com/payment/"+transactionId+"/cancel");
        paymentLink3.setMethod("POST");
        paymentLink3.setRel("Hold");

        PaymentLink paymentLink4=new PaymentLink();
        paymentLink4.setHref("https://middleware.adityabirla.com/payment/"+transactionId+"/cancel");
        paymentLink4.setMethod("Post");
        paymentLink4.setRel("Unhold");

        List <PaymentLink> paymentLinks=new ArrayList<>();
        paymentLinks.add(paymentLink1);
        paymentLinks.add(paymentLink2);
        paymentLinks.add(paymentLink3);
        paymentLinks.add(paymentLink4);
        paymentRootResponse.setPaymentLinks(paymentLinks);
        return paymentRootResponse;
    }


    @Override
    public PaymentQueryResponse callPaymentQuery(String transactionId) {
        String messageRequest="";
        String messageResponse="";
        try {
            if (!StringUtils.isEmpty(transactionId)) {
                PaymentQueryRequest paymentQueryRequest = new PaymentQueryRequest();
                paymentQueryRequest.setRequestType(PGConstants.PG_QUERY_REQUEST_TYPE);
                paymentQueryRequest.setMerchantId(propertiesValue.getPgMerchantId());
                paymentQueryRequest.setCustomerId(transactionId);
                paymentQueryRequest.setCurrentDateTimestamp(DateUtil.getFormattedDate(PGConstants.PG_S2S_DATE_FORMAT));
                messageRequest = paymentBuilder.buildPaymentQueryRequest(paymentQueryRequest);
                log.debug("messageRequest :: " + messageRequest);
                ConcurrentMap<String, String> concurrentMap = new ConcurrentHashMap<>();
                concurrentMap.put("msg", messageRequest);
                messageResponse = HttpPostClient.callHttpPost(bookmarks.getBilldeskQueryURL(),
                        null, concurrentMap,null);
                log.debug("messageResponse :: " + messageResponse);
                return paymentBuilder.buildPaymentQueryResponse(messageResponse);
            }
        }catch (Exception e){
            log.error("Exception :: "+e);
        }
        return null;
    }

    @Override
    public PaymentRefundResponse callPaymentRefund(PaymentRefundRequest paymentRefundRequest) {
        String messageRequest="";
        String messageResponse="";
        try {
            if (!StringUtils.isEmpty(paymentRefundRequest)) {
                paymentRefundRequest.setRequestType(PGConstants.PG_REFUND_REQUEST_TYPE);
                paymentRefundRequest.setMerchantId(propertiesValue.getPgMerchantId());
                paymentRefundRequest.setRefDataTime(DateUtil.getFormattedDate(PGConstants.PG_S2S_DATE_FORMAT));
                paymentRefundRequest.setChecksum(propertiesValue.getPgChecksumKey());
                messageRequest = paymentBuilder.buildPaymentRefundRequest(paymentRefundRequest);
                log.debug("messageRequest :: " + messageRequest);
                ConcurrentMap<String, String> concurrentMap = new ConcurrentHashMap<>();
                concurrentMap.put("msg", messageRequest);
                messageResponse = HttpPostClient.callHttpPost(bookmarks.getBilldeskRefundURL(),null, concurrentMap,null);
                log.debug("messageResponse :: " + messageResponse);
                return paymentBuilder.buildPaymentRefundResponse(messageResponse);
            }
        }catch (Exception e){
            log.error("Exception ::"+ e);
        }
        return null;
    }

    @Override
    public PaymentModesResponse getPaymentModes(Long customerId, String establishmentId) {
        Connection connection = null;
        CallableStatement stmt=null;
        ResultSet rs = null;
        PaymentModesResponse paymentModesResponse = new PaymentModesResponse();
        Mode mode=null;
        List<BankDetails> bankDetailsList=new ArrayList();
        List<UPIDetails> upiDetailsList=new ArrayList();
        BankDetails bankDetails=null;
        UPIDetails upiDetails=null;
        PaymentModes paymentModes=null;
        List<Mode> modeList=new ArrayList<>();
        try{

            if (driverClass != null && url != null && userName != null && password != null) {
                Class.forName(driverClass);
                connection = DriverManager.getConnection(url, userName, password);
            }
         
            int index = 1;
            if(connection!=null) {
                stmt = connection.prepareCall("{ call P_B2B_CUST_BANK_UPI_DETAILS(?,?,?,?,?,?,?) } ");
                stmt.setLong(index++, customerId);
                stmt.setString(index++, establishmentId);
                stmt.registerOutParameter(index++, OracleTypes.CURSOR);
                stmt.registerOutParameter(index++, OracleTypes.CURSOR);
                stmt.registerOutParameter(index++, Types.VARCHAR);
                stmt.registerOutParameter(index++, Types.VARCHAR);
                stmt.registerOutParameter(index, Types.VARCHAR);
                stmt.executeUpdate();

                if(stmt!=null){
                    paymentModesResponse.setEntityId(customerId);
                    paymentModesResponse.setEstablishmentId(establishmentId);
                    if(stmt.getString(5)!=null)
                    paymentModesResponse.setCreditCardStatus(stmt.getString(5));
                    else
                        paymentModesResponse.setCreditCardStatus("disabled");
                    if(stmt.getString(6)!=null)
                        paymentModesResponse.setDebitCardStatus(stmt.getString(6));
                    else
                        paymentModesResponse.setDebitCardStatus("disabled");

                    paymentModesResponse.setStatus(stmt.getString(7));

                    rs = (ResultSet) stmt.getObject(3);
                    while (rs.next()) {
                        bankDetails=new BankDetails();
                        bankDetails.setAccountNo( rs.getString("abd_account_no"));
                        bankDetails.setBankName( rs.getString("abd_bank_name"));
                        if(rs.getString("abd_preferred_bank_flg")!=null /*&& !rs.getString("abd_preferred_bank_flg").equalsIgnoreCase("N")*/) {
                            bankDetails.setPreferredFlag(rs.getString("abd_preferred_bank_flg"));
                            bankDetailsList.add(bankDetails);
                        }
                        else {
                            bankDetails.setPreferredFlag("N");
                        }
                       // bankDetailsList.add(bankDetails);
                    }
                    if(bankDetailsList.size()>0){
                        mode=new Mode();
                        mode.setMode("NB");
                        mode.setBankDetails(bankDetailsList);
                        modeList.add(mode);
                    }

                    rs = (ResultSet) stmt.getObject(4);
                    while (rs.next()) {
                        upiDetails=new UPIDetails();
                        upiDetails.setHandlerName( rs.getString("aud_upa_handler"));
                        if(rs.getString("aud_upa_dft_flg")!=null /*&& !rs.getString("aud_upa_dft_flg").equalsIgnoreCase("N")*/) {
                            upiDetails.setPreferredFlag(rs.getString("aud_upa_dft_flg"));
                            upiDetailsList.add(upiDetails);
                        }
                        else {
                            upiDetails.setPreferredFlag("N");
                        }

                        //upiDetailsList.add(upiDetails);

                    }
                    if(upiDetailsList.size()>0){
                        mode=new Mode();
                        mode.setMode("UPI");
                        mode.setUpiDetails(upiDetailsList);
                     modeList.add(mode);
                    }

                    if(modeList!=null&&modeList.size()>0){
                     paymentModes=new PaymentModes();
                        paymentModes.setMode(modeList);
                        paymentModesResponse.setPaymentModes(paymentModes);
                    }

                }else{
                    paymentModesResponse.setStatus("Failure");
                }


                return paymentModesResponse;
            }else{
                paymentModesResponse.setStatus("Failure");
            }

        }catch (Exception e){
            log.error("Exception :",e);
            paymentModesResponse.setStatus("Failure");
        }finally {
            if(connection!=null&& stmt!=null&&rs!=null){
                try {
                    connection.close();
                    stmt.close();
                    rs.close();
                } catch (SQLException e) {
                    log.error("SQLException :",e);
                }
            }
        }
        return paymentModesResponse;
    }

    @Override
    public PaymentModesResponse getPaymentModes(Long customerId, String establishmentId,String type)throws PaymentException {
        PaymentModesResponse paymentModesResponse=null;
        try {
            log.debug("Customer Id : ",customerId," Estatblishment Id : ",establishmentId," Type : ",type);
            if(!StringUtils.isEmpty(customerId)&& !StringUtils.isEmpty(establishmentId)){
                paymentModesResponse=paymentRepository.getPaymentModes(customerId,establishmentId,type);
            }else{
                    throw new PaymentException("500","Blank should be not required" );
            }
        }catch (Exception e){
            log.error("Exception :",e);
            throw new PaymentException("500",e.getMessage() );
        }
        return paymentModesResponse;
    }

    @Transactional
    @Override
    public String getPGResponse(String message, String responseType) {
        StoredProcedureQuery storedProcedureQuery=null;
        try{
            log.debug("Message :: "+message  +"responseType :: "+responseType);
            if(!StringUtils.isEmpty(message) && !StringUtils.isEmpty(responseType)){
                storedProcedureQuery = entityManager.createStoredProcedureQuery(PGConstants.PROC__S2S_B2B_RESPONSE_MESSAGE)
                        .registerStoredProcedureParameter("P_TRANS_RES", String.class, ParameterMode.IN)
                        .setParameter("P_TRANS_RES", message)
                        .registerStoredProcedureParameter("P_RES_TYPE",String.class,ParameterMode.IN)
                        .setParameter("P_RES_TYPE",responseType)
                        .registerStoredProcedureParameter("P_MSG", String.class, ParameterMode.OUT);
                storedProcedureQuery.execute();
                return (String) storedProcedureQuery.getOutputParameterValue("P_MSG");
            }
        }catch (Exception e){
            log.error("Exception : "+e);
        }
        return null;
    }

    @Override
    @Transactional
    public String getCBSResponse(String message) {
        StoredProcedureQuery storedProcedureQuery=null;
        try{
            log.debug("Message :: "+message );
            if(!StringUtils.isEmpty(message) ){
                storedProcedureQuery = entityManager.createStoredProcedureQuery(PGConstants.PROC_CBS_B2B_RESPONSE_MESSAGE)
                        .registerStoredProcedureParameter("P_TRANS_RES", String.class, ParameterMode.IN)
                        .setParameter("P_TRANS_RES", message)
                        .registerStoredProcedureParameter("P_MSG", String.class, ParameterMode.OUT);
                storedProcedureQuery.execute();
                return (String) storedProcedureQuery.getOutputParameterValue("P_MSG");
            }
        }catch (Exception e){
            log.error("Exception : "+e);
        }
        return null;
    }
}

