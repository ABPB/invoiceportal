package com.abipb.invoiceportal.services.impl;


import com.abipb.invoiceportal.dto.BulkCreateInvoiceFlatFileData;

import com.abipb.invoiceportal.dto.BulkCreateInvoiceImageFileData;
import com.abipb.invoiceportal.exception.DataBaseException;
import com.abipb.invoiceportal.exception.MandateException;
import com.abipb.invoiceportal.repository.InvoiceRepository;
import com.abipb.invoiceportal.services.InvoiceService;
import com.abipb.invoiceportal.util.ProcNameEnum;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Types;
import java.util.List;

/**
 * Created by abhay.kumar-v on 12/12/2017.
 */
@Service
public class InvoiceServiceImpl implements InvoiceService {
    private final static Logger log= LoggerFactory.getLogger(InvoiceServiceImpl.class);
    @Autowired
    private InvoiceRepository invoiceRepository;
    @Value("${spring.datasource.driver-class-name}")
    private  String driverClass;

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private  String userName;

    @Value("${spring.datasource.password}")
    private  String password;



    @Override
    @Transactional
    public String saveInvoiceFlateFile(List<BulkCreateInvoiceFlatFileData> bulkCreateMandateFlatFileDataList,String fileName,String clientId)throws MandateException  {
        Connection connection = null;
        CallableStatement stmt=null;
        String procStatus=null;
        try {
            if (driverClass != null && url != null && userName != null && password != null) {
                Class.forName(driverClass);
                connection = DriverManager.getConnection(url, userName, password);
            } else {
                throw new MandateException("Invalid Connection Informatin");
            }
            if ((connection != null) && !connection.isClosed()) {

                ArrayDescriptor bulkCreateMandateFlatFileDataDescriptor = ArrayDescriptor.createDescriptor("ABPG_BULKINV_TYP", connection);
                StructDescriptor bulkCreateMandateFlatFileDataStruct = StructDescriptor.createDescriptor("ABPG_BULKINV_OBJ", connection);


                STRUCT[]  bulkCreateMandateFlatFileDataStructs = null;
                bulkCreateMandateFlatFileDataStructs = new STRUCT[bulkCreateMandateFlatFileDataList.size()];
                BulkCreateInvoiceFlatFileData bulkCreateMandateFlatFileData= null;

                for (int index = 0; index < bulkCreateMandateFlatFileDataList.size(); index++) {
                    Object[] params = new Object[1];
                    bulkCreateMandateFlatFileData = (BulkCreateInvoiceFlatFileData) bulkCreateMandateFlatFileDataList.get(index);
                    if (bulkCreateMandateFlatFileData != null) {
                        params[0] = bulkCreateMandateFlatFileData.getCreateMandateDetails();

                    }
                    STRUCT struct = new STRUCT(bulkCreateMandateFlatFileDataStruct, connection, params);
                    bulkCreateMandateFlatFileDataStructs[index] = struct;
                }
                ARRAY bulkCreateMandateFlatFileDataArray = new ARRAY(bulkCreateMandateFlatFileDataDescriptor, connection, bulkCreateMandateFlatFileDataStructs);
                stmt = connection.prepareCall(ProcNameEnum.INVOICE_PROC_NAME_P_B2B_UPLOAD_INVOICE.getProcName());

                stmt.setArray(1, bulkCreateMandateFlatFileDataArray);
                stmt.setString(2,fileName);
                stmt.setString(3,clientId);
                stmt.registerOutParameter(4, Types.VARCHAR);   //p_msg

                stmt.execute();

                if(stmt!=null){
                    procStatus=stmt.getString(4);
                }
            }
        }catch (Exception e){
            log.error("Exception ::"+ e);
        }
        return procStatus;
    }
    @Transactional
    @Override
    public String bulkCreateInvoiceImageFile(BulkCreateInvoiceImageFileData bulkCreateInvoiceImageFileData) throws MandateException {
        try {
            log.info("bulkCreateMandateImageFile method of MandateService ");
            return invoiceRepository.bulkCreateInvoiceImageFile(bulkCreateInvoiceImageFileData);
        } catch (DataBaseException e) {
            e.printStackTrace();
        }
        return  null;
    }

}
