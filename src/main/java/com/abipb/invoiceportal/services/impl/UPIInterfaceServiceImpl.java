package com.abipb.invoiceportal.services.impl;

import com.abipb.invoiceportal.dto.UPIValidationRequest;
import com.abipb.invoiceportal.dto.UPIValidationResponse;
import com.abipb.invoiceportal.exception.UPIHandlerException;
import com.abipb.invoiceportal.services.UPIInterfaceService;
import com.abipb.invoiceportal.util.*;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


/**
 * Created by abhay.kumar-v on 10/20/2017.
 */

@Service
public class UPIInterfaceServiceImpl implements UPIInterfaceService {
    public static final String PROC_NAME_P_B2B_UPI_RESPONSE_MSG="P_B2B_UPI_RESPONSE_MSG";
    private  final static Logger log= LoggerFactory.getLogger(UPIInterfaceServiceImpl.class);

    @Autowired
    private Bookmarks bookmarks;

    @Autowired
    private PropertiesValue propertiesValue;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public String getUPICollection(String payerHandler, String payeeHandler, String amount,String txnId) throws UPIHandlerException {
        log.debug("payerHandler : " + payerHandler + "payeeHandler :: " + payeeHandler + " amount :: " + amount + " txnId :: " + txnId);
        String decryptedResponse = "";
        String returnStatus = "";
        try {
            if (!StringUtils.isEmpty(payerHandler)) {
                JSONObject jsonMain = new JSONObject();
                JSONObject jsonPWSESSIONRS = new JSONObject();
                JSONObject jsonPWPROCESSRS = new JSONObject();
                JSONObject jsonPWHEADER = new JSONObject();
                JSONObject josnPWDATA = new JSONObject();
                JSONObject jsonREQCOLLECT = new JSONObject();
                JSONArray arrayRow = new JSONArray();
                JSONObject jsonInputString = new JSONObject();
                JSONObject json = new JSONObject();
                // header params
                jsonPWHEADER = this.generateHeader("REQCOLLECT");
                //data
                json = this.generateRequestCollectionData(payerHandler, payeeHandler, amount, txnId);
                jsonMain.put("PWSESSIONRS", jsonPWSESSIONRS);
                jsonPWSESSIONRS.put("PWPROCESSRS", jsonPWPROCESSRS);
                jsonPWPROCESSRS.put("PWHEADER", jsonPWHEADER);
                jsonPWPROCESSRS.put("PWDATA", josnPWDATA);
                josnPWDATA.put("REQCOLLECT", jsonREQCOLLECT);
                jsonREQCOLLECT.put("Row", arrayRow);
                arrayRow.put(0, jsonInputString);
                jsonInputString.put("input_json", json.toString());
                jsonPWPROCESSRS.put("PWERROR", new JSONObject());
                log.debug("jsonMain.toString() :: " + jsonMain.toString());
                String randamId = "1234567897456321";
                String dataEnc = AESHelper.encrypt(randamId, jsonMain.toString()); // encrypt request data
                String aesId = AESHelper.encrypt(randamId);    //encrypt randamId
                String aesHex = AESHelper.toHex(aesId);      // Hex encrypted randamId
                JSONObject jsonRequestNew = new JSONObject();
                jsonRequestNew.put("id", aesHex);
                jsonRequestNew.put("data", dataEnc);
                jsonRequestNew.put("platform", "ANDROID");
                ConcurrentMap<String, String> concurrentMap = new ConcurrentHashMap<>();
                concurrentMap.put("HASH", UPIHMAC.HmacSHA256(jsonRequestNew.toString(), propertiesValue.getUpiHMACSalt()));
                String httpResponse = HttpPostClient.callHttpPost(bookmarks.getUpiInterfaceURL(), concurrentMap, null, jsonRequestNew.toString());
                log.debug("httpResponse :: ",httpResponse);
                if(StringUtils.isEmpty(httpResponse)){
                    throw new UPIHandlerException("INTERNAL SERVER ERROR",HttpStatus.SC_INTERNAL_SERVER_ERROR);
                }
                JSONObject jsonObject = new JSONObject(httpResponse);
                String id = jsonObject.optString("id");
                String data = jsonObject.optString("data");
                // Decrypt the Encrypted requested
                String unhexId = AESHelper.hexToString(id);
                String decryptedId = AESHelper.decrypt(unhexId);
                decryptedResponse = AESHelper.decrypt(decryptedId, data);
                log.debug("decryptedResponse ::  " + decryptedResponse);
                String status = new JSONObject(
                                            new JSONObject(decryptedResponse).
                                            getJSONArray("PWSESSIONRS")
                                            .getJSONObject(0)
                                            .getJSONObject("PWPROCESSRS")
                                            .getJSONObject("PWDATA")
                                            .getJSONObject("REQCOLLECT")
                                            .optString("Row")
                                            ).optString("status");
                if (ValidationEnum.VALIDATION_SUCCESS.getValidationState().equalsIgnoreCase(status)) {
                    decryptedResponse = this.checkUPITransactionStatus(txnId);
                    //                    logic for calling a proc after getting a response from Check transaction Status
                    if (!StringUtils.isEmpty(decryptedResponse)) {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append(txnId).append(PGConstants.PG_MSG_SEPARATOR);//TXN_NO
                        stringBuilder.append(payerHandler).append(PGConstants.PG_MSG_SEPARATOR);//PAYER_HANDLER
                        stringBuilder.append(payeeHandler).append(PGConstants.PG_MSG_SEPARATOR);//PAYEE_HANDLER
                        stringBuilder.append(amount).append(PGConstants.PG_MSG_SEPARATOR);//TXN_AMOUNT

                        JSONObject jsonObjectResponse = new JSONObject(
                                                            new JSONObject(decryptedResponse).
                                                            getJSONArray("PWSESSIONRS")
                                                            .getJSONObject(0)
                                                            .getJSONObject("PWPROCESSRS")
                                                            .getJSONObject("PWDATA")
                                                            .getJSONObject("CHKREQPAYTXN")
                                                            .optString("Row")
                                                            );

                        String customStatus = ValidationEnum.VALIDATION_SUCCESS
                                                .getValidationState().equalsIgnoreCase(jsonObjectResponse.optString("status"))
                                                && ValidationEnum.VALIDATION_PENDING.getValidationState().equalsIgnoreCase(jsonObjectResponse.optString("info"))
                                                ? jsonObjectResponse.optString("info")
                                                : jsonObjectResponse.optString("status");

                        log.debug("Status for passing a UPI proc :: " + customStatus);
                        returnStatus = customStatus;
                        stringBuilder.append(ValidationEnum.VALIDATION_PENDING.getValidationState().equalsIgnoreCase(customStatus) ?
                                ErrorCodeEnum.UPI_FAILURE_CODE.getErrorCode() :
                                ValidationEnum.VALIDATION_SUCCESS.getValidationState().equalsIgnoreCase(customStatus) ?
                                        ErrorCodeEnum.UPI_SCCESS_CODE.getErrorCode() : ErrorCodeEnum.UPI_FAILURE_CODE.getErrorCode())
                                .append(PGConstants.PG_MSG_SEPARATOR);//TRAN_STATUS

                        stringBuilder.append(ValidationEnum.VALIDATION_PENDING.getValidationState().equalsIgnoreCase(customStatus) ?
                                                ErrorCodeEnum.UPI_PENDING_CODE.getErrorCode() :
                                                ValidationEnum.VALIDATION_SUCCESS.getValidationState().equalsIgnoreCase(customStatus) ?
                                                ErrorCodeEnum.UPI_SCCESS_CODE.getErrorCode() : jsonObjectResponse.optString("code"))
                                                .append(PGConstants.PG_MSG_SEPARATOR);//ERROR_CODE

                        stringBuilder.append(jsonObjectResponse.optString("info"))
                                    .append(PGConstants.PG_MSG_SEPARATOR);//ERROR_DES

                        stringBuilder.append(new JSONObject(decryptedResponse)
                                            .getJSONArray("PWSESSIONRS")
                                            .getJSONObject(0)
                                            .getJSONObject("PWPROCESSRS")
                                            .getJSONObject("PWHEADER").optString("SERVER_TIMESTAMP")
                                        );//TRANS_DATE
                        log.debug("UPI Response For UPI proc :: " + stringBuilder.toString());
                        log.debug("UPI Proc Status :: " + this.getUPIResponse(stringBuilder.toString()));
                    }
                } else {
                    // logic for calling a proc after getting a response from Request Collection in not initiated
                    returnStatus = ValidationEnum.VALIDATION_NOTINITIATE.getValidationState();
                    if (!StringUtils.isEmpty(decryptedResponse)) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(txnId).append(PGConstants.PG_MSG_SEPARATOR);//TXN_NO
                    stringBuilder.append(payerHandler).append(PGConstants.PG_MSG_SEPARATOR);//PAYER_HANDLER
                    stringBuilder.append(payeeHandler).append(PGConstants.PG_MSG_SEPARATOR);//PAYEE_HANDLER
                    stringBuilder.append(amount).append(PGConstants.PG_MSG_SEPARATOR);//TXN_AMOUNT

                    JSONObject jsonObjectResponse = new JSONObject(
                                                    new JSONObject(decryptedResponse).
                                                    getJSONArray("PWSESSIONRS")
                                                    .getJSONObject(0)
                                                    .getJSONObject("PWPROCESSRS")
                                                    .getJSONObject("PWDATA")
                                                    .getJSONObject("REQCOLLECT")
                                                    .optString("Row")
                                                    );
                    stringBuilder.append(ErrorCodeEnum.UPI_FAILURE_CODE.getErrorCode())
                                .append(PGConstants.PG_MSG_SEPARATOR);//TRAN_STATUS

                    stringBuilder.append(ErrorCodeEnum.UPI_FAILURE_CODE.getErrorCode())
                            .append(PGConstants.PG_MSG_SEPARATOR);//ERROR_CODE

                    stringBuilder.append(jsonObjectResponse.optString("info"))
                                .append(PGConstants.PG_MSG_SEPARATOR);//ERROR_DES

                    stringBuilder.append(new JSONObject(decryptedResponse)
                                            .getJSONArray("PWSESSIONRS")
                                            .getJSONObject(0)
                                            .getJSONObject("PWPROCESSRS")
                                            .getJSONObject("PWHEADER").optString("SERVER_TIMESTAMP")
                                    );//TRANS_DATE

                    log.debug("In not Initiated, UPI Response For UPI proc :: " + stringBuilder.toString());
                    log.debug("In not Initiated, UPI Proc Status :: " + this.getUPIResponse(stringBuilder.toString()));
                    }

                }
            } else {
            //handle null upihandle ?? 404
            log.error("payerHandler : " + payerHandler + "payeeHandler :: " + payeeHandler + " amount :: " + amount + " txnId :: " + txnId);
                throw new UPIHandlerException("UPI Handler should not be empty.",HttpStatus.SC_BAD_REQUEST);
            }
        }catch (JSONException jsone) {
            log.error("JSONException "+jsone);
            throw new UPIHandlerException(jsone.getMessage(),jsone, HttpStatus.SC_BAD_REQUEST);
        }catch (Exception e){
            log.error("Exception :: "+e);
            throw new UPIHandlerException(e.getMessage(),e, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
        return returnStatus;

    }

    @Override
    public String checkUPITransactionStatus(String upiHandler) throws UPIHandlerException{
        log.debug("upiHandler : " + upiHandler);
        UPIValidationRequest upiRequest = new UPIValidationRequest();
        UPIValidationResponse upiValidationResponse = new UPIValidationResponse();
        String decryptedResponse="";
        try {
            if (!StringUtils.isEmpty(upiHandler)) {
                upiRequest.setUpihandle(upiHandler);
                upiValidationResponse.setUpiHandle(upiHandler);
                JSONObject jsonMain = new JSONObject();
                JSONObject jsonPWSESSIONRS = new JSONObject();
                JSONObject jsonPWPROCESSRS = new JSONObject();
                JSONObject jsonPWHEADER = new JSONObject();
                JSONObject josnPWDATA = new JSONObject();
                JSONObject jsonCHKREQPAYTXN = new JSONObject();
                JSONArray arrayRow = new JSONArray();
                JSONObject jsonInputString = new JSONObject();
                JSONObject json = new JSONObject();
                // header params
                    jsonPWHEADER = this.generateHeader("CHKREQPAYTXN");
                    //data
                    json.put("txnId", upiHandler);
                    jsonMain.put("PWSESSIONRS", jsonPWSESSIONRS);
                    jsonPWSESSIONRS.put("PWPROCESSRS", jsonPWPROCESSRS);
                    jsonPWPROCESSRS.put("PWHEADER", jsonPWHEADER);
                    jsonPWPROCESSRS.put("PWDATA", josnPWDATA);
                    josnPWDATA.put("CHKREQPAYTXN", jsonCHKREQPAYTXN);
                    jsonCHKREQPAYTXN.put("Row", arrayRow);
                    arrayRow.put(0, jsonInputString);
                    jsonInputString.put("input_json", json.toString());
                    jsonPWPROCESSRS.put("PWERROR", new JSONObject());
                String randamId = "1234567897456321";
                String dataEnc = AESHelper.encrypt(randamId, jsonMain.toString()); // encrypt request data
                String aesId = AESHelper.encrypt(randamId);    //encrypt randamId
                String aesHex = AESHelper.toHex(aesId);      // Hex encrypted randamId
                JSONObject jsonRequestNew = new JSONObject();
                    jsonRequestNew.put("id", aesHex);
                    jsonRequestNew.put("data", dataEnc);
                    jsonRequestNew.put("platform", "ANDROID");
                    ConcurrentMap<String, String> concurrentMap = new ConcurrentHashMap<>();
                    concurrentMap.put("HASH", UPIHMAC.HmacSHA256(jsonRequestNew.toString(), propertiesValue.getUpiHMACSalt()));


                    long ONE_SECOND_MILLISECONDS = 1000;
                    long ONE_MINUTE_MILLISECONDS = 60 * ONE_SECOND_MILLISECONDS;
                    long PERIOD_OF_MINUTE = 2 * ONE_MINUTE_MILLISECONDS;
                    Date date = new Date();
                    long missiseconds = date.getTime();
                    PERIOD_OF_MINUTE += missiseconds;
                    int count = 1;
                    log.debug("missiseconds :: " + missiseconds);

                    for (long start = missiseconds; start < PERIOD_OF_MINUTE; start += (20 * ONE_SECOND_MILLISECONDS)) {
                        String messageResponse = HttpPostClient.callHttpPost(bookmarks.getUpiInterfaceURL(), concurrentMap, null, jsonRequestNew.toString());

                        if(StringUtils.isEmpty(messageResponse)){
                            log.debug("messageResponse :: ",messageResponse);
                            throw new UPIHandlerException("INTERNAL SERVER ERROR",HttpStatus.SC_INTERNAL_SERVER_ERROR);
                        }
                        JSONObject jsonObject = new JSONObject(messageResponse);
                        String id = jsonObject.optString("id");
                        String data = jsonObject.optString("data");
                        // Decrypt the Encrypted requested
                        String unhexId = AESHelper.hexToString(id);
                        String decryptedId = AESHelper.decrypt(unhexId);
                        decryptedResponse = AESHelper.decrypt(decryptedId, data);
                        log.debug("decryptedResponse ::  " + decryptedResponse);

                        JSONObject jsonObjectResponse = new JSONObject(
                                new JSONObject(decryptedResponse).
                                        getJSONArray("PWSESSIONRS")
                                        .getJSONObject(0)
                                        .getJSONObject("PWPROCESSRS")
                                        .getJSONObject("PWDATA")
                                        .getJSONObject("CHKREQPAYTXN")
                                        .optString("Row")
                        );
                        String status = jsonObjectResponse.optString("status");
                        if (!StringUtils.isEmpty(status)) {
                            if (!"success".equalsIgnoreCase(status)
                                    || !StringUtils.isEmpty(jsonObjectResponse.optString("info"))) {
                                log.debug((count++) + "          " + start + "    " + PERIOD_OF_MINUTE);
                                log.debug("going to sleep");
                                Thread.sleep(20 * ONE_SECOND_MILLISECONDS);
                            } else {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
            } else {
                log.error("UPI Handler should not be empty : " + upiHandler);
                throw new UPIHandlerException("UPI Handler should not be empty.",HttpStatus.SC_BAD_REQUEST);
            }
        }catch (JSONException jsone) {
            log.error("JSONException "+jsone);
            throw new UPIHandlerException(jsone.getMessage(),jsone, HttpStatus.SC_BAD_REQUEST);
        }catch (Exception e){
            log.error("Exception :: "+e);
            throw new UPIHandlerException(e.getMessage(),e, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
        return decryptedResponse;
    }

    @Override
    public String getUPIResponse(String  upiResponse) throws UPIHandlerException{
               try{
            StoredProcedureQuery storedProcedureQuery = entityManager.
                    createStoredProcedureQuery(PROC_NAME_P_B2B_UPI_RESPONSE_MSG).
                    registerStoredProcedureParameter("P_UPI_RES", String.class, ParameterMode.IN)
                    .setParameter("P_UPI_RES", upiResponse)
                    .registerStoredProcedureParameter("P_MSG", String.class, ParameterMode.OUT);
            storedProcedureQuery.execute();
            return (String) storedProcedureQuery.getOutputParameterValue("P_MSG");
        }catch (Exception e){
            log.error("Exception :: "+ e);
            throw  new UPIHandlerException(e.getLocalizedMessage(),e.getCause(),HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

    private JSONObject generateHeader(String outProcessId ){
        JSONObject jsonPWHEADER = new JSONObject();
        jsonPWHEADER.put("ORG_ID", "IDEA");
        jsonPWHEADER.put("IN_PROCESS_ID", "");
        jsonPWHEADER.put("OUT_PROCESS_ID", outProcessId);
        jsonPWHEADER.put("USER_ID", "6HnaB1yy0XuyhoDrBI3jEg==");
        jsonPWHEADER.put("PASSWORD", "");
        jsonPWHEADER.put("APP_ID", "IDEAUPI");
        jsonPWHEADER.put("LOGIN_ID", "7838326944@birla");
        jsonPWHEADER.put("PLATFORM", "ANDROID");
        jsonPWHEADER.put("PLATFORM_UPI", "MOB");
        jsonPWHEADER.put("SERVER_TIMESTAMP", "");
        jsonPWHEADER.put("DEVICE_TIMESTAMP", DateUtil.getFormattedDate("yyyy-MM-dd HH:mm:ss"));
        jsonPWHEADER.put("DEVICE_LATITUDE", "20.5937");
        jsonPWHEADER.put("DEVICE_LONGITUDE", "78.9629");
        jsonPWHEADER.put("LOCATION", "India");
        jsonPWHEADER.put("USER_SESSION_ID", "");
        jsonPWHEADER.put("PW_SESSION_ID", "lnNNjLq+XhsZQXRPlquxGK/7DQMDw0MalYiPIXWaFvg=");
        jsonPWHEADER.put("PW_VERSION", "");
        jsonPWHEADER.put("PW_CLIENT_VERSION", "");
        jsonPWHEADER.put("SESSION_EXPIRE_TIME", "");
        jsonPWHEADER.put("INSTALLATION_ID", "");
        jsonPWHEADER.put("VERSION_ID", "com.abipbl.upi");
        jsonPWHEADER.put("IMEI_NO", "868049027379831");
        jsonPWHEADER.put("OS_VERSION", "6.0.1");
        jsonPWHEADER.put("DEVICE_MAKE", "OnePlus");
        jsonPWHEADER.put("DEVICE_MODEL", "ONE E1003");
        jsonPWHEADER.put("SIM_ID", "89911190185073569809");

        return jsonPWHEADER;
    }

    private  JSONObject generateRequestCollectionData(String payerHandler,String payeeHandler,String amount,String txnId){
        JSONObject json=new JSONObject();
        json.put("note", "requesting money");
        json.put("txnID", txnId);
        json.put("requestName", "ReqPay");
        json.put("txnType", "COLLECT");
        json.put("custRef", "100000002997");
        json.put("mobile", "917838326944");
        json.put("addrType", "ACCOUNT");
        json.put("scoresValue1", "0");
        json.put("scoresValue2", "1");
        json.put("ruleName2", "MINAMOUNT");
        json.put("ruleValue2", "12.12");
        json.put("ruleName1", "EXPIREAFTER");
        json.put("ruleValue1", "30");
        json.put("identityType", "ACCOUNT");
        json.put("accType1", "IFSC");
        json.put("accType2", "ACTYPE");
        json.put("accValue1", "ABPB0000001");
        json.put("accValue2", "SAVINGS");
        json.put("accType3", "ACNUM");
        json.put("accValue3", "7838326944");
        json.put("payerType", "PERSON");
        json.put("payerName", "Aditya Pawar");
        json.put("payerAddr", payerHandler);
        json.put("payerSeqNum", "1");
        json.put("payerCode", "0000");
        json.put("payerAmountValue", amount);
        json.put("payeeAdd", payeeHandler);
        json.put("payeeCode", "0000");
        json.put("payeeName", "unknown");
        json.put("payerAmountCurr", "INR");
        json.put("payeeSeq", "1");
        json.put("payeeType", "PERSON");
        json.put("payeeAmountCurr", "INR");
        json.put("payeeAmountValue", amount);
        json.put("payeeSplitValue", "0.00");
        json.put("payeeSplitName", "PURCHASE");
        json.put("verifiedTypeName", "unknown");
        json.put("deviceTs", DateUtil.getFormattedDate("yyyy-MM-dd HH:mm:ss"));
        json.put("capability", "5200000200010004000549959420123");
        json.put("payType", "VPA");
        return json;
    }

    @Override
    public boolean validateUPI(String upiHandler)throws UPIHandlerException{
        log.debug("upiHandler : " + upiHandler );
        String decryptedResponse="";
        try {
            if (!StringUtils.isEmpty(upiHandler)) {
                JSONObject jsonMain = new JSONObject();
                JSONObject jsonPWSESSIONRS = new JSONObject();
                JSONObject jsonPWPROCESSRS = new JSONObject();
                JSONObject jsonPWHEADER = new JSONObject();
                JSONObject josnPWDATA = new JSONObject();
                JSONObject jsonVALADDR = new JSONObject();
                JSONArray arrayRow = new JSONArray();
                JSONObject jsonInputString = new JSONObject();
                JSONObject json = new JSONObject();
                // header params
                jsonPWHEADER=this.generateHeader("VALADDR");
                //data
                json.put("note", "verify vpa");
                json.put("txnID", "ABP"+ UUID.randomUUID().toString().toUpperCase().replaceAll("-", ""));//ABI8792278614939
                json.put("requestName", "ReqValAdd");//ReqValAdd
                json.put("txnType", "VAL_ADD");
                json.put("identityType","ACCOUNT");
                json.put("verifiedName", "unknown");
                json.put("verifiedAddress", "TRUE");
                json.put("payerType", "PERSON");
                json.put("payerName", "unknown");
                json.put("payerAddr",upiHandler);
                json.put("payerSeqNum", "1");
                json.put("payerCode", "");
                json.put("payeeSeqNum", "1");
                json.put("payeeAddr", upiHandler);
                json.put("payeeName", "");
                json.put("deviceTs", DateUtil.getFormattedDate("yyyy-MM-dd HH:mm:ss"));
                json.put("capability", "5200000200010004000549959420123");
                jsonMain.put("PWSESSIONRS", jsonPWSESSIONRS);
                jsonPWSESSIONRS.put("PWPROCESSRS", jsonPWPROCESSRS);
                jsonPWPROCESSRS.put("PWHEADER", jsonPWHEADER);
                jsonPWPROCESSRS.put("PWDATA", josnPWDATA);
                josnPWDATA.put("VALADDR", jsonVALADDR);
                jsonVALADDR.put("Row", arrayRow);
                arrayRow.put(0, jsonInputString);
                jsonInputString.put("input_json", json.toString());
                jsonPWPROCESSRS.put("PWERROR", new JSONObject());

                log.debug("jsonMain.toString() :: "+jsonMain.toString());
                String randamId = "1234567897456321";
                String dataEnc = AESHelper.encrypt(randamId, jsonMain.toString()); // encrypt request data
                String aesId = AESHelper.encrypt(randamId);    //encrypt randamId
                String aesHex = AESHelper.toHex(aesId);      // Hex encrypted randamId
                JSONObject jsonRequestNew = new JSONObject();

                jsonRequestNew.put("id", aesHex);
                jsonRequestNew.put("data", dataEnc);
                jsonRequestNew.put("platform", "ANDROID");
                ConcurrentMap<String, String> concurrentMap = new ConcurrentHashMap<>();
                concurrentMap.put("HASH", UPIHMAC.HmacSHA256(jsonRequestNew.toString(), propertiesValue.getUpiHMACSalt()));
                String httpResponse = HttpPostClient.callHttpPost(bookmarks.getUpiInterfaceURL(),concurrentMap, null,jsonRequestNew.toString());
                log.debug("httpResponse ::  "+httpResponse);
                if(StringUtils.isEmpty(httpResponse)){
                    throw new UPIHandlerException("INTERNAL SERVER ERROR",HttpStatus.SC_INTERNAL_SERVER_ERROR);
                }
                JSONObject jsonObject = new JSONObject(httpResponse);
                String id = jsonObject.optString("id");
                String data = jsonObject.optString("data");
                // Decrypt the Encrypted requested
                String unhexId = AESHelper.hexToString(id);
                String decryptedId = AESHelper.decrypt(unhexId);
                decryptedResponse = AESHelper.decrypt(decryptedId, data);

                log.debug("decryptedResponse ::  "+decryptedResponse);
                String status = new JSONObject(
                    new JSONObject(decryptedResponse).
                    getJSONArray("PWSESSIONRS")
                    .getJSONObject(0)
                    .getJSONObject("PWPROCESSRS")
                    .getJSONObject("PWDATA")
                    .getJSONObject("VALADDR")
                    .optString("Row")
                    ).optString("status");
                    return ValidationEnum.VALIDATION_SUCCESS.getValidationState().equalsIgnoreCase(status)?  true:false;
                } else {
                //handle null upihandle ?? 404
                log.debug("upiHandler : " + upiHandler );
                throw new UPIHandlerException("UPI Handler should not be empty.",HttpStatus.SC_BAD_REQUEST);
            }
        }catch (JSONException jsone) {
            log.error("JSONException "+jsone);
            throw new UPIHandlerException(jsone.getMessage(),jsone, HttpStatus.SC_BAD_REQUEST);
        }catch (Exception e){
            log.error("Exception :: "+e);
            throw new UPIHandlerException(e.getMessage(),e, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

    private List handlerList;


    @Override
    public boolean validateUPIStub(String  upiHandler) {
        log.debug("validateUPI : "+upiHandler);
        this.populateHandlerName();
        if (null != upiHandler && !StringUtils.isEmpty(upiHandler) && upiHandler.endsWith("birla")) {
            return getHandlerList().contains(upiHandler.toLowerCase());
        }
        return false;


    }

    public List getHandlerList(){
        return handlerList;
    }
    private  void populateHandlerName(){
        handlerList=new ArrayList();
        handlerList.add("u.mishra1@birla");
        handlerList.add("aditya@birla");
        handlerList.add("krishna@birla");
        handlerList.add("sundar@birla");
        handlerList.add("ramesh@birla");
    }
}
