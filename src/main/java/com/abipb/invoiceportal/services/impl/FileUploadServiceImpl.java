package com.abipb.invoiceportal.services.impl;

import com.abipb.invoiceportal.entities.FileUpload;
import com.abipb.invoiceportal.repositories.FileUploadRepository;
import com.abipb.invoiceportal.services.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Venkatesh.R on 8/10/2017.
 */

@Service
public class FileUploadServiceImpl implements FileUploadService {


    private FileUploadRepository fileUploadRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    public void setFileUploadRepository(FileUploadRepository fileUploadRepository) {
        this.fileUploadRepository = fileUploadRepository;
    }

    @Override
    public Iterable<FileUpload> listLast5FileUploads() {
//        return fileUploadRepository.findLast5FileUploads();
        return entityManager.createNamedQuery("FileUpload.findLast5FileUploads").setMaxResults(5).getResultList();
    }
}