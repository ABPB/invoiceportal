package com.abipb.invoiceportal.services.impl;

import com.abipb.invoiceportal.entities.Merchant;
import com.abipb.invoiceportal.repositories.MerchantRepository;
import com.abipb.invoiceportal.services.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Merchant service implementation.
 */
@Service
public class MerchantServiceImpl implements MerchantService {

    private com.abipb.invoiceportal.repositories.MerchantRepository MerchantRepository;

    @Autowired
    public void setMerchantRepository(MerchantRepository MerchantRepository) {
        this.MerchantRepository = MerchantRepository;
    }

    @Override
    public Iterable<Merchant> listAllMerchants() {
        return MerchantRepository.findAll();
    }

    @Override
    public Merchant getMerchantById(String cid) {
        return MerchantRepository.findOne(cid);
    }

    @Override
    public Merchant saveMerchant(Merchant merchant) {
        return MerchantRepository.save(merchant);
    }

    @Override
    public void deleteMerchant(String cid) {
        MerchantRepository.delete(cid);
    }

}
