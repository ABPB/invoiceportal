package com.abipb.invoiceportal.services;


import com.abipb.invoiceportal.dto.BulkCreateInvoiceFlatFileData;

import com.abipb.invoiceportal.dto.BulkCreateInvoiceImageFileData;
import com.abipb.invoiceportal.exception.MandateException;

import java.util.List;

/**
 * Created by abhay.kumar-v on 12/12/2017.
 */
public interface InvoiceService {

    public String saveInvoiceFlateFile(List<BulkCreateInvoiceFlatFileData> bulkCreateMandateFlatFileDataList, String fileName, String clientId) throws MandateException;
    public String bulkCreateInvoiceImageFile(BulkCreateInvoiceImageFileData bulkCreateInvoiceImageFileData) throws MandateException;

}
