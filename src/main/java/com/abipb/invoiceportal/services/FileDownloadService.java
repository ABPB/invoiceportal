package com.abipb.invoiceportal.services;

/**
 * Created by penchalaiah.g-v on 09-01-2018.
 */
public interface FileDownloadService {
    public Object getdownloadFilesList();
}
