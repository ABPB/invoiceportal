package com.abipb.invoiceportal.services;


import com.abipb.invoiceportal.exception.UPIHandlerException;

/**
 * Created by Venkatesh.R on 10/20/2017.
 */
public interface UPIInterfaceService {
    public boolean validateUPI(String upiHandler) throws UPIHandlerException;
    public String getUPICollection(String payerHandler, String payeeHandler, String amount, String txnId)throws UPIHandlerException;
    public String checkUPITransactionStatus(String upiHandler)throws UPIHandlerException;
    public String getUPIResponse(String upiRespone)throws UPIHandlerException;
    public boolean validateUPIStub(String upiHandler) throws UPIHandlerException;
}
