package com.abipb.invoiceportal.services;

import com.abipb.invoiceportal.dto.BatchDTO;
import com.abipb.invoiceportal.dto.ReportSummaryDTO;
import com.abipb.invoiceportal.dto.ReportDTO;

import java.util.Date;
import java.util.List;

public interface ReportService {

//    Iterable<CrsFinalCustomer> listAllUniqueBatches();

    List<BatchDTO> listAllUniqueBatches(String channel, String merchId);

    List<ReportDTO> getReportDetails (String batchId, Date batchDate, String channel);

    ReportSummaryDTO getReportSummaryDetails (String batchId, Date batchDate, String channel);
}
