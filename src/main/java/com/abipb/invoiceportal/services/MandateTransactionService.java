package com.abipb.invoiceportal.services;


import com.abipb.invoiceportal.dto.NachAckData;

import java.util.List;

/**
 * Created by abhay.kumar-v on 12/18/2017.
 */
public interface MandateTransactionService {

    public String saveTransactionAck(List<NachAckData> nachAckDataList);
}
