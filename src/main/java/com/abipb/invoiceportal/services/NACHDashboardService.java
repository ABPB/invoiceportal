package com.abipb.invoiceportal.services;


import com.abipb.invoiceportal.dto.DashboardDetails;
import com.abipb.invoiceportal.exception.BusinessException;

/**
 * Created by penchalaiah.g-v on 12-01-2018.
 */
public interface NACHDashboardService {
    DashboardDetails getDashboardDetails(String mfilter, String cfilter) throws BusinessException;
}


