package com.abipb.invoiceportal.services;



import com.abipb.invoiceportal.entities.FileUpload;

/**
 * Created by Venkatesh.R on 8/10/2017.
 */


public interface FileUploadService {

    Iterable<FileUpload> listLast5FileUploads();

}
