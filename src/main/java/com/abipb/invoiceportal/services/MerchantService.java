package com.abipb.invoiceportal.services;

import com.abipb.invoiceportal.entities.Merchant;

public interface MerchantService {

    Iterable<Merchant> listAllMerchants();

    Merchant getMerchantById(String cid);

    Merchant saveMerchant(Merchant merchant);

    void deleteMerchant(String cid);

}
