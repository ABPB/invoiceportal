package com.abipb.invoiceportal.services;


import com.abipb.invoiceportal.dto.*;
import com.abipb.invoiceportal.exception.PaymentException;

/**
 * Created by Venkatesh.R on 10/15/2017.
 */
public interface PaymentService {
   public PaymentCreateClientResponse createPayment(PaymentCreateRequest paymentCreateRequest) throws PaymentException;
   public PaymentCreateResponse getPaymentDetails(String mwTxnRef);


   public PaymentResponse cancelPayment(PaymentRequest paymentRequest);
   public PaymentResponse holdPayment(PaymentRequest paymentRequest);
   public PaymentRootResponse getPaymentDetail(String transactionId);

   public PaymentQueryResponse callPaymentQuery(String transactionId);
   public PaymentRefundResponse callPaymentRefund(PaymentRefundRequest paymentRefundRequest);

   public PaymentModesResponse getPaymentModes(Long customerId, String establishmentId);
   public PaymentModesResponse getPaymentModes(Long customerId, String establishmentId, String type)throws PaymentException;

   public String getPGResponse(String message, String responseType);

   public String getCBSResponse(String message);
}
