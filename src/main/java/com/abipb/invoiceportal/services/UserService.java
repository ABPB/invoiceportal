package com.abipb.invoiceportal.services;

import com.abipb.invoiceportal.entities.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
