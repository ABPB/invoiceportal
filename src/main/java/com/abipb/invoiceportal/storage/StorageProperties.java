package com.abipb.invoiceportal.storage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("storage")

public class StorageProperties {

    /**
     * Folder location for storing files
     */
//    private String location = "upload-dir";
    @Value("${upload-location}")
    //private String location = "C:\\Projects\\FileUpload\\UploadedFiles";
    private String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
