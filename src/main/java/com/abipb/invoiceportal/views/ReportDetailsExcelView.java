package com.abipb.invoiceportal.views;

import org.apache.poi.hssf.usermodel.*;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Venkatesh.R on 9/3/2017.
 */
public class ReportDetailsExcelView extends AbstractExcelView {

    @SuppressWarnings("unchecked")
        @Override
        protected void buildExcelDocument(Map<String,Object> model,
                                          HSSFWorkbook workbook,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Exception {

/*            HSSFSheet excelSheet = workbook.createSheet("Report Details");
            setExcelHeader(excelSheet);

            List animalList = (List) model.get("animalList");
            setExcelRows(excelSheet,animalList);*/

        String sheetName = (String)model.get("sheetname");
        List<String> headers = (List<String>)model.get("headers");
        List<List<String>> results = (List<List<String>>)model.get("results");
        List<String> numericColumns = new ArrayList<String>();
        if (model.containsKey("numericcolumns"))
            numericColumns = (List<String>)model.get("numericcolumns");
        //BUILD DOC
        HSSFSheet sheet = workbook.createSheet(sheetName);
        sheet.setDefaultColumnWidth((short) 12);
        int currentRow = 0;
        short currentColumn = 0;
        //CREATE STYLE FOR HEADER
        HSSFCellStyle headerStyle = workbook.createCellStyle();
        HSSFFont headerFont = workbook.createFont();
        headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        headerStyle.setFont(headerFont);
        //POPULATE HEADER COLUMNS
        HSSFRow headerRow = sheet.createRow(currentRow);
        for(String header:headers){
            HSSFRichTextString text = new HSSFRichTextString(header);
            HSSFCell cell = headerRow.createCell(currentColumn);
            cell.setCellStyle(headerStyle);
            cell.setCellValue(text);
            currentColumn++;
        }
        //POPULATE VALUE ROWS/COLUMNS
        currentRow++;//exclude header
        for(List<String> result: results){
            currentColumn = 0;
            HSSFRow row = sheet.createRow(currentRow);
            for(String value : result){//used to count number of columns
                HSSFCell cell = row.createCell(currentColumn);
                if (numericColumns.contains(headers.get(currentColumn))){
                    cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
                    cell.setCellValue("test");
                } else {
                    HSSFRichTextString text = new HSSFRichTextString(value);
                    cell.setCellValue(text);
                }
                currentColumn++;
            }
            currentRow++;
        }
    }
}



       /* public void setExcelHeader(HSSFSheet excelSheet) {
            HSSFRow excelHeader = excelSheet.createRow(0);
            excelHeader.createCell(0).setCellValue("Id");
            excelHeader.createCell(1).setCellValue("Name");
            excelHeader.createCell(2).setCellValue("Type");
            excelHeader.createCell(3).setCellValue("Aggressive");
            excelHeader.createCell(4).setCellValue("Weight");
        }

        public void setExcelRows(HSSFSheet excelSheet, List animalList){
            int record = 1;
            for (Animal animal : animalList) {
                HSSFRow excelRow = excelSheet.createRow(record++);
                excelRow.createCell(0).setCellValue(animal.getId());
                excelRow.createCell(1).setCellValue(animal.getAnimalName());
                excelRow.createCell(2).setCellValue(animal.getAnimalType());
                excelRow.createCell(3).setCellValue(animal.getAggressive());
                excelRow.createCell(4).setCellValue(animal.getWeight());
            }
        }*/

