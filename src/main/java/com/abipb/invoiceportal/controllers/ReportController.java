package com.abipb.invoiceportal.controllers;

import com.abipb.invoiceportal.constants.CrystalConstants;
import com.abipb.invoiceportal.dto.BatchDTO;
import com.abipb.invoiceportal.dto.ReportDTO;
import com.abipb.invoiceportal.dto.ReportSessionDTO;
import com.abipb.invoiceportal.dto.ReportSummaryDTO;
import com.abipb.invoiceportal.entities.User;
import com.abipb.invoiceportal.services.ReportService;
import com.abipb.invoiceportal.services.UserService;
import com.abipb.invoiceportal.views.ReportDetailsExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.util.StringUtils;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Merchant controller.
 */
@Controller
@SessionAttributes({"sessionMap","pushBatchList","pullBatchList"})
//@SessionAttributes("batchList")
//@SessionAttributes("reportDetails")
public class ReportController {

    public static final String ENTERPRISE_PUSH_CHANNEL = "01";
    public static final String ENTERPRISE_PULL_CHANNEL = "02";

    private ReportService reportService;

    @Autowired
    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }


    private UserService userService;

    @Autowired
    public void setUserService (UserService userService) {this.userService = userService;}

    /**
     * List all batches.
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/report", method = RequestMethod.GET)
    public String list(Model model) {

        List<BatchDTO> batchList = reportService.listAllUniqueBatches(ENTERPRISE_PUSH_CHANNEL, getCurrentUserMerchantId());

//        List<BatchDTO> batchList = reportService.listAllUniqueBatches(ENTERPRISE_PULL_CHANNEL);

        if (batchList.size() == 0 && batchList.size() == 0) {
            model.addAttribute("error", "No Batch Id/Date returned from DB");
        } else {
            model.addAttribute("batchList",batchList);
            model.addAttribute("pushBatchList",batchList);
            List<String> uniqueBatchDateList = listUniqueBatchDates(batchList);
            if (uniqueBatchDateList.size() > 0)
                model.addAttribute("batchDateList", uniqueBatchDateList);
        }

        System.out.println("Returning unique batch id  and date list:");
        ReportSummaryDTO reportSummaryDTO = new ReportSummaryDTO();
        reportSummaryDTO.setTransType("EnterprisePush");
        model.addAttribute("reportSummaryDTO", reportSummaryDTO);
        return "reportForm";
    }

    public List<String> listUniqueBatchDates(Iterable<BatchDTO> batchList) {
        Map<String, BatchDTO> uniqueBatchDateMap = new TreeMap<String, BatchDTO>();

        for (BatchDTO batch : batchList) {
            Date batchDate = batch.getBatchDate();
            if (batchDate != null && !StringUtils.isEmpty(batchDate.toString())) {
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                String strDate = formatter.format(batchDate);
                uniqueBatchDateMap.put(strDate, batch);
            }
        }
        List<String> modelBatchDateList = new ArrayList<>(uniqueBatchDateMap.keySet());
        return modelBatchDateList;
    }

    /**
     * View a specific Merchant by its id.
     * <p>
     * //     * @param bid
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "reportSummaryDTO", method = RequestMethod.POST)
    public String showBatchDetails(ReportSummaryDTO reportSummaryDTO, Model model,HttpServletRequest request) {

        String retrieve = request.getParameter("Retrieve");
        if (retrieve == null || StringUtils.isEmpty(retrieve)) {
            String transactionType = null;
            if (StringUtils.isEmpty(reportSummaryDTO.getBatchId()) && StringUtils.isEmpty(reportSummaryDTO.getBatchDate())) {
                model.addAttribute("error", "Please select atleast one filter");
                List<BatchDTO> batchList = reportService.listAllUniqueBatches(ENTERPRISE_PUSH_CHANNEL,getCurrentUserMerchantId());

                return "reportForm";
            }

            String transTypeParam = reportSummaryDTO.getTransType();

            if (transTypeParam != null && "EnterprisePush".equalsIgnoreCase(transTypeParam)) {
                //enterprisePushBatchList = reportService.listAllUniqueBatches("01");
                transactionType = ENTERPRISE_PUSH_CHANNEL;
            } else {
                transactionType = ENTERPRISE_PULL_CHANNEL;
            }

            List<BatchDTO> batchList = reportService.listAllUniqueBatches(transactionType,getCurrentUserMerchantId());
            model.addAttribute("batchList", batchList);
            List<String> uniqueBatchDateList = listUniqueBatchDates(batchList);
            if (uniqueBatchDateList.size() > 0)
                model.addAttribute("batchDateList", uniqueBatchDateList);

            if (reportSummaryDTO.getBatchDate() != null && !StringUtils.isEmpty(reportSummaryDTO.getBatchDate())) {
                try {
                    Date parsedDate = new SimpleDateFormat("dd/MM/yyyy").parse(reportSummaryDTO.getBatchDate());
                    model.addAttribute("parsedSelectedDate", new SimpleDateFormat("dd-MM-yyyy").format(parsedDate));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            ReportSummaryDTO reportSummary = null;
            try {
                reportSummary = reportService.getReportSummaryDetails(reportSummaryDTO.getBatchId(), (reportSummaryDTO.getBatchDate() != null && !StringUtils.isEmpty(reportSummaryDTO.getBatchDate())) ? new SimpleDateFormat("dd/MM/yyyy").parse(reportSummaryDTO.getBatchDate()) : null, transactionType);
                reportSummary.setTransType(transactionType);
            } catch (Exception ex) {

                ex.printStackTrace();
                if (ex instanceof NoResultException) {
                    model.addAttribute("error", "Report Summary information not returned from DB");
                } else {
                    model.addAttribute("error", "Error getting Report Summary information from DB");
                }
            }
            if (reportSummary == null) {
                model.addAttribute("error", "Report Summary information not returned from DB");
            } else {
                model.addAttribute("reportSummaryDetails", reportSummary);
            }

            List<ReportDTO> reportDetails = null;

            try {

                reportDetails = reportService.getReportDetails(reportSummaryDTO.getBatchId(), (reportSummaryDTO.getBatchDate() != null && !StringUtils.isEmpty(reportSummaryDTO.getBatchDate())) ? new SimpleDateFormat("dd/MM/yyyy").parse(reportSummaryDTO.getBatchDate()) : null, transactionType);

            } catch (Exception ex) {
                ex.printStackTrace();
                if (ex instanceof NoResultException) {
                    model.addAttribute("error", "Report Details information not returned from DB");
                } else {
                    model.addAttribute("error", "Error getting Report Details information from DB");
                }
            }


            if (reportDetails != null && reportDetails.size() > 0) {
                model.addAttribute("reportDetails", reportDetails);

                ReportSessionDTO reportSession = new ReportSessionDTO();
                reportSession.setReportSummary(reportSummary);
                reportSession.setReportDetails(reportDetails);
                Map<String, ReportSessionDTO> sessionMap = new HashMap<>();

                String sessionKey = new StringBuilder(transactionType)
                        .append("|")
                        .append(reportSummaryDTO.getBatchId())
                        .append("|")
                        .append(reportSummaryDTO.getBatchDate()).toString();
                sessionMap.put(sessionKey, reportSession);
                model.addAttribute("sessionMap", sessionMap);
//            }

            } else {

                model.addAttribute("error", "Report Summary / Detail information not returned from DB");

            }

            model.addAttribute("selectedBatchId", reportSummaryDTO.getBatchId());
            model.addAttribute("selectedBatchDate", reportSummaryDTO.getBatchDate());
            model.addAttribute("selectedTransType", reportSummaryDTO.getTransType());
        } else {

            String transTypeParam = reportSummaryDTO.getTransType();
            String transactionType = null;

            if (transTypeParam != null && "EnterprisePush".equalsIgnoreCase(transTypeParam)) {
                //enterprisePushBatchList = reportService.listAllUniqueBatches("01");
                transactionType = ENTERPRISE_PUSH_CHANNEL;
            } else {
                transactionType = ENTERPRISE_PULL_CHANNEL;
            }

            List<BatchDTO> batchList = reportService.listAllUniqueBatches(transactionType, getCurrentUserMerchantId());
            model.addAttribute("batchList", batchList);
            List<String> uniqueBatchDateList = listUniqueBatchDates(batchList);
            if (uniqueBatchDateList.size() > 0)
                model.addAttribute("batchDateList", uniqueBatchDateList);
        }

        return "reportForm";
    }

    @RequestMapping(value = "/export", method = RequestMethod.GET)
//    public ModelAndView exportToExcel(HttpServletRequest request, HttpServletResponse response, @SessionAttribute List<ReportDTO> reportDetails) {
    public ModelAndView exportToExcel(HttpServletRequest request, HttpServletResponse response, @SessionAttribute Map<String, ReportSessionDTO> sessionMap) {
//    public ModelAndView exportToExcel(HttpServletRequest request, HttpServletResponse response) {

        List<ReportDTO> reportDetails = null;
        String batchId = request.getParameter("batchId");
        String batchDate = request.getParameter("batchDate");
        String transType = request.getParameter("transType");
        StringBuilder sessionKey = new StringBuilder("");

        if (transType != null && "EnterprisePull".equalsIgnoreCase(transType)) {
            sessionKey.append(ENTERPRISE_PULL_CHANNEL);

        } else {
            sessionKey.append(ENTERPRISE_PUSH_CHANNEL);
        }

        sessionKey.append("|");

        if (batchId != null) {
            sessionKey.append(batchId);
        }

        sessionKey.append("|");

        if (batchDate != null) {
            sessionKey.append(batchDate);
        }

        System.out.println("sessionKey" + sessionKey.toString());

        ReportSessionDTO reportSession = sessionMap.get(sessionKey.toString());
        if (reportSession != null) {
           reportDetails =  reportSession.getReportDetails();
        }
        Map<String, Object> model = new HashMap<String, Object>();

        //Sheet Name
        model.put("sheetname", "Final Customer Report");
        //Headers List

        List<List<String>> results = new ArrayList<List<String>>();

        if ("EnterprisePull".equalsIgnoreCase(transType)) {
            List<String> headers = new ArrayList<String>();
            //headers.add("Run Sequence");
            headers.add("Customer Id");
            headers.add("Customer Name");
            headers.add("Customer VPA");
            headers.add("Customer Mobile Number");
            headers.add("Bill Number");
            headers.add("Bill Month");
            headers.add("Bill Amount");
            headers.add("Bill Due Date");
            headers.add("Status");
            headers.add("Remark");
            headers.add("Received Amount");
            headers.add("Channel");
            headers.add("Calculated Amount");
            headers.add("Bill Stage");
            headers.add("Bill Stage Due Date");
            model.put("headers", headers);

            for (ReportDTO reportDetail : reportDetails) {
                List<String> record = new ArrayList<>();
                //record.add(reportDetail.getRunSeq().toString());
                record.add(reportDetail.getCustId());
                record.add(reportDetail.getCustName());
                record.add(reportDetail.getCustVPA());
                record.add(reportDetail.getCustMobNo() != null ? reportDetail.getCustMobNo().toString() : "");
                record.add(reportDetail.getBillNo());
                record.add(reportDetail.getBillMonth());
                record.add(reportDetail.getBillAmount().toString());
                DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                record.add(reportDetail.getBillDueDate() != null ? df.format(reportDetail.getBillDueDate()).toString() : "");
                record.add(reportDetail.getStatus());
                record.add(reportDetail.getRemark());
                record.add((reportDetail.getReceivedAmt() != null) ? reportDetail.getReceivedAmt().toString() : "");
                record.add(reportDetail.getChannel());
                record.add(reportDetail.getCalcAmt() != null ? reportDetail.getCalcAmt().toString() : "");
                record.add(reportDetail.getBillStage());
                record.add(reportDetail.getBillStageDueDate() != null ? df.format(reportDetail.getBillStageDueDate()).toString() : "");
                results.add(record);
            }
        } else {

            List<String> headers = new ArrayList<String>();
            //headers.add("Run Sequence");
            headers.add("Customer Id");
            headers.add("Customer Name");
            headers.add("Customer VPA");
            headers.add("Customer Account");
            headers.add("Customer IFSC");
            headers.add("Transaction Mode");
            headers.add("Status");
            headers.add("Remark");
            headers.add(" Amount");
            headers.add("Received Amount");

            model.put("headers", headers);

            for (ReportDTO reportDetail : reportDetails) {
                List<String> record = new ArrayList<>();
                //record.add(reportDetail.getRunSeq().toString());
                record.add(reportDetail.getCustId());
                record.add(reportDetail.getCustName());
                record.add(reportDetail.getCustVPA());
                record.add(reportDetail.getCustAcct());
                record.add(reportDetail.getCustIFSC());
                record.add(reportDetail.getTxnMode());
                record.add(reportDetail.getStatus());
                record.add(reportDetail.getRemark());
                record.add((reportDetail.getPushAmount() != null) ? reportDetail.getPushAmount().toString() : "");
                record.add((reportDetail.getReceivedAmt() != null) ? reportDetail.getReceivedAmt().toString() : "");
                results.add(record);
            }
        }

        model.put("results", results);
        response.setContentType("application/ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=FinalCustomerReport.xls");
        return new ModelAndView(new ReportDetailsExcelView(), model);
    }

    @RequestMapping(value = "/getBatchIdList")
    @ResponseBody
    public Set<String> getBatchIdList(@RequestParam String batchDate ) {
//                                      @RequestParam String transType) {
 //                                     @SessionAttribute List<BatchDTO> pushBatchList) {
//                                      @SessionAttribute List<BatchDTO> pullBatchList) {
        String[] tokens = batchDate.split("\\|");
        batchDate = tokens[0];
        String transType = tokens[1];

        Iterable<BatchDTO> batchList1 = null;

        /*if ("EnterprisePull".equalsIgnoreCase(transType)) {
            batchList1 = pullBatchList;
        } else {
            batchList1 = pushBatchList;
        }*/

        if (batchList1 == null) {

            batchList1 = reportService.listAllUniqueBatches( "EnterprisePull".equalsIgnoreCase(transType) ? ENTERPRISE_PULL_CHANNEL : ENTERPRISE_PUSH_CHANNEL,getCurrentUserMerchantId());
        }

        Map<String,Set<String>> batchMap = getBatchMap(batchList1);

        String formatChangedDate= null;
        if (batchDate != null && !StringUtils.isEmpty(batchDate)) {
            try {
                Date parsedDate = new SimpleDateFormat("dd/MM/yyyy").parse(batchDate);
                formatChangedDate = new SimpleDateFormat("ddMMyyyy").format(parsedDate);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return batchMap.get(formatChangedDate);
    }

    Map<String,Set<String>> getBatchMap (Iterable<BatchDTO> batchList) {

        Map<String, Set<String>> batchMap = new HashMap<>();
        Set<String> batchIdSet = null;
        String previousToken = null;
        String tokenBatchDate = null;
        for (BatchDTO batch : batchList) {
            String batchId = batch.getBatchId();
            if (batchId != null && !StringUtils.isEmpty(batchId)) {
                String[] tokens = batchId.split("_");
                tokenBatchDate = tokens[1];

                if (previousToken == null)
                {
                    batchIdSet = new HashSet<>();
                }

                if ( previousToken != null && !previousToken.equalsIgnoreCase(tokenBatchDate)) {

                    batchMap.put(previousToken, batchIdSet);
                    batchIdSet = new HashSet<>();

                }
                batchIdSet.add(batchId);
                previousToken = tokenBatchDate;
            }
        }

        batchMap.put(tokenBatchDate, batchIdSet);
        return batchMap;
    }


    @RequestMapping(value = "/getBatchDateList")
    @ResponseBody
    public Set<String> getBatchDateList(@RequestParam String transType,Model model)
   {

        Iterable<BatchDTO> batchList1 = null;

        if ("EnterprisePull".equalsIgnoreCase(transType)) {
           // batchList1 = pullBatchList;
        } else {
           // batchList1 = pushBatchList;
        }

        if (batchList1 == null) {

                  batchList1 = reportService.listAllUniqueBatches( "EnterprisePull".equalsIgnoreCase(transType) ? ENTERPRISE_PULL_CHANNEL : ENTERPRISE_PUSH_CHANNEL, getCurrentUserMerchantId());

                if (transType != null && ENTERPRISE_PULL_CHANNEL.equalsIgnoreCase(transType)) {
                    model.addAttribute("pullBatchList",batchList1);
                } else {
                    model.addAttribute("pushBatchList",batchList1);
                }
        }


       /* Map<String,Set<String>> batchMap = getBatchMap(batchList1);

        String formatChangedDate= null;
        if (batchDate != null && !StringUtils.isEmpty(batchDate)) {
            try {
                Date parsedDate = new SimpleDateFormat("dd/MM/yyyy").parse(batchDate);
                formatChangedDate = new SimpleDateFormat("ddMMyyyy").format(parsedDate);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return batchMap.get(formatChangedDate);*/

       Set<String> batchDates =  new HashSet<>();
        for (BatchDTO batch :  batchList1)  {
            Date batchDate = batch.getBatchDate();
            if (batchDate != null) {
                batchDates.add(new SimpleDateFormat("dd/MM/yyyy").format(batchDate));
            }
        }

        return batchDates;
    }

    public String getCurrentUserMerchantId() {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        if ( userName != null && !StringUtils.isEmpty(userName)) {
            User user = userService.findByUsername(userName);
            String role = user.getRole().getNrtRoleName();
            if (role != null && !StringUtils.isEmpty(role) && CrystalConstants.ROLE_USER.equalsIgnoreCase(role)) {
                String merchantId = user.getNlmMerchantId();
                if (merchantId != null && !StringUtils.isEmpty(merchantId)) {
                    return merchantId;

                }
            }
        }
        return null;
    }
}





