package com.abipb.invoiceportal.controllers;

/**
 * Created by Venkatesh.R on 10/15/2017.
 */


import com.abipb.invoiceportal.dto.PGPaymentRequest;
import com.abipb.invoiceportal.dto.PaymentCreateResponse;
import com.abipb.invoiceportal.exception.BaseException;
import com.abipb.invoiceportal.services.PaymentService;
import com.abipb.invoiceportal.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.UriBuilder;

import static com.abipb.invoiceportal.util.PGConstants.PG_FILLER_FIELD;


@Controller
public class PaymentController {

    private final Logger log = LoggerFactory.getLogger(PaymentController.class);
    private static final String PAYMENT_RETURN_URI = "/PGResponse"; //TO BE MOVED TO BOOKMARKS
    

    @Autowired
    PaymentService paymentService;

//move to bookmarks
    @Value("${pg.netbanking.payment.endpoint}")
    private String pgNbEndpoint;

    @Value("${pg.card.payment.endpoint}")
    private String pgCardEndpoint;

    @Autowired
    private Bookmarks bookmarks;
//
    @Value("${pg.checksum.key}")
    private String pgChecksumKey;

    @Value("${pg.security.id}")
    private String pgSecurityId;

    @Value("${pg.merchant.id}")
    private String pgMerchantId;

    @RequestMapping("/doPayment")
    public  String doPayment(Model model,
                                      @RequestParam(value="mwTxnRef", required=true) String mwTxnRef,
                                      @RequestParam(value="merchantId", required=true) String merchantId) throws BaseException {
        System.out.println("mwTxnRef" + mwTxnRef);
        System.out.println("merchantId" + merchantId);
try {
    PGPaymentRequest payReq = buildPGPaymentRequest(merchantId, mwTxnRef);

    if (payReq != null) {
        model.addAttribute("msg", payReq.getMsg());
        if (PaymentChannelEnum.PG_CREDIT_CARD_CHANNEL.getPaymentChannel().equals(payReq.getPaymentChannel())) {
            model.addAttribute("paymentMode", "CREDIT");
            model.addAttribute("pgURL", pgCardEndpoint);
        } else if (PaymentChannelEnum.PG_DEBIT_CARD_CHANNEL.getPaymentChannel().equals(payReq.getPaymentChannel())) {
            model.addAttribute("paymentMode", "DEBIT");
            model.addAttribute("pgURL", pgCardEndpoint);
        } else if (PaymentChannelEnum.PG_NET_BANKING_CHANNEL.getPaymentChannel().equals(payReq.getPaymentChannel())) {
            model.addAttribute("pgURL", pgNbEndpoint);
            model.addAttribute("paymentMode", "");
        }
        model.addAttribute("status", "Success");
    }else{
        model.addAttribute("status", "Failure");
        throw  new BaseException("500","unable to process payment request");
    }
}catch (BaseException be){
    throw  new BaseException(be.getErrCode(),be.getErrMsg());
}
catch (Exception e){
    log.error("Exception :",e);
    model.addAttribute("status", "Failure");
   throw  new BaseException("500","Invalid payment request");
}
        return "PGRedirection";
    }

    private PGPaymentRequest buildPGPaymentRequest(String merchantId, String mwTxnRef) {
        PGPaymentRequest pgPayReq = new PGPaymentRequest();
        //based on mwTxnRef get payment details for building pg payment request
        PaymentCreateResponse paymentCreateResponse = paymentService.getPaymentDetails(mwTxnRef);
        String msg;
//        return "redirect:" + pgNbEndpoint;
            if (paymentCreateResponse != null) {
                if(paymentCreateResponse.getPaymentAmount()==null||paymentCreateResponse.getClientTxnRef()==null || paymentCreateResponse.getPaymentChannel()==null ||paymentCreateResponse.getPaymentChannel().equalsIgnoreCase(""))  {
                    log.debug("Payment Amount==== "+paymentCreateResponse.getPaymentAmount());
                    log.debug("TxnRef No==== "+paymentCreateResponse.getClientTxnRef());
                    throw  new BaseException("500","Basic Information missing to make payment getway request");
                }
                if(paymentCreateResponse.getPaymentChannel().equalsIgnoreCase(PaymentChannelEnum.PG_CREDIT_CARD_CHANNEL.getPaymentChannel())||paymentCreateResponse.getPaymentChannel().equalsIgnoreCase(PaymentChannelEnum.PG_DEBIT_CARD_CHANNEL.getPaymentChannel()))  {
                    log.debug("Payment Amount==== "+paymentCreateResponse.getPaymentAmount());
                    log.debug("TxnRef No==== "+paymentCreateResponse.getClientTxnRef());
                    if(paymentCreateResponse.getCustomerMobileNo()==null || paymentCreateResponse.getCustomerEmailId()==null){
                        log.debug("Customer MobileNo==== "+paymentCreateResponse.getCustomerMobileNo());
                        log.debug("Email Id==== "+paymentCreateResponse.getCustomerEmailId());
                        throw  new BaseException("500","Basic Information missing to make payment getway request");
                    }

                }
                StringBuilder msgStrBuilder = new StringBuilder();
            //get merchant id assigned by BD based on input merchant id sent by client - merchantId
            //currently taken from property file
            //replace with JPA repository/stored proc input

//            merchant id
            msgStrBuilder.append(pgMerchantId);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

//            CustomerID
            msgStrBuilder.append(mwTxnRef);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

            String paymentChannel = paymentCreateResponse.getPaymentChannel();

            if ((paymentChannel != null) && PaymentChannelEnum.PG_NET_BANKING_CHANNEL.getPaymentChannel().equals(paymentChannel)) {
                if (paymentCreateResponse.getFromBankAcctNumber() != null) {
                    //            AccountNumber
                    msgStrBuilder.append(paymentCreateResponse.getFromBankAcctNumber());
                    //            msgStrBuilder.append("165601000009288");
                    msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);
                 } else {

                    msgStrBuilder.append(PG_FILLER_FIELD);
                    msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);
                }

            } else {

                msgStrBuilder.append(PG_FILLER_FIELD);
                msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);
            }

//           TxnAmount
            msgStrBuilder.append(paymentCreateResponse.getPaymentAmount());
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

            if ((paymentChannel != null) && PaymentChannelEnum.PG_NET_BANKING_CHANNEL.getPaymentChannel().equals(paymentChannel)) {
                if (paymentCreateResponse.getFromBankId() != null) {
                    //            BankID
                     msgStrBuilder.append(paymentCreateResponse.getFromBankId());
//            msgStrBuilder.append("IOB");
                    // hardcoded values to  be removed
                    //msgStrBuilder.append("SCB");
                    msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);
                } else {

                    msgStrBuilder.append(PG_FILLER_FIELD);
                    msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);
                }


            } else if ((paymentChannel != null) && PaymentChannelEnum.PG_CREDIT_CARD_CHANNEL.getPaymentChannel().equals(paymentChannel) || PaymentChannelEnum.PG_DEBIT_CARD_CHANNEL.getPaymentChannel().equals(paymentChannel)) {
                // Filler field
                msgStrBuilder.append(PG_FILLER_FIELD);
                msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);
            }

//            Filler2
            msgStrBuilder.append(PG_FILLER_FIELD);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);
//            Filler3
            msgStrBuilder.append(PG_FILLER_FIELD);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);
//            CurrencyType
            msgStrBuilder.append(PGConstants.PG_CURRENCY_TYPE);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

//            ItemCode
            if ((paymentChannel != null) && PaymentChannelEnum.PG_CREDIT_CARD_CHANNEL.getPaymentChannel().equals(paymentChannel) || PaymentChannelEnum.PG_DEBIT_CARD_CHANNEL.getPaymentChannel().equals(paymentChannel)) {
                msgStrBuilder.append(PG_FILLER_FIELD);
            } else {
                msgStrBuilder.append(PGConstants.PG_ITEM_CODE);
            }
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

//            TypeField1
            msgStrBuilder.append(PGConstants.PG_TYPE_FIELD_1);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

//            SecurityID
            msgStrBuilder.append(pgSecurityId);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);
//            Filler4
            msgStrBuilder.append(PG_FILLER_FIELD);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

//            Filler5
            msgStrBuilder.append(PG_FILLER_FIELD);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

//            TypeField2
            msgStrBuilder.append(PGConstants.PG_TYPE_FIELD_2);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

//            AdditionalInfo1 Sub-Merchant’s Unique Order Number
            msgStrBuilder.append(paymentCreateResponse.getClientTxnRef());
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);


//                    AdditionalInfo2 Customer’s Mobile Number
//            hardcoded mobile number - to be clarified if empty
            //msgStrBuilder.append("9790786046");
                String mobileNum = paymentCreateResponse.getCustomerMobileNo();
                String updMobile = StringUtils.isEmpty(mobileNum) ? "" : mobileNum;
                String email = paymentCreateResponse.getCustomerEmailId();
                String  updEmail = StringUtils.isEmpty(email) ? "" : email;

            msgStrBuilder.append(updMobile);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

//            AdditionalInfo3  Customer’s Email ID - to be clarified if empty
            //msgStrBuilder.append("venkatesh.r@adityabirla.bank");
            msgStrBuilder.append(updEmail);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

//                    AdditionalInfo4
            msgStrBuilder.append(PG_FILLER_FIELD);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

//            AdditionalInfo5
            msgStrBuilder.append(PG_FILLER_FIELD);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

//                    AdditionalInfo6
            msgStrBuilder.append(PG_FILLER_FIELD);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

//            AdditionalInfo7
            msgStrBuilder.append(PG_FILLER_FIELD);
            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);

//            Return URL
            msgStrBuilder.append(UriBuilder
                    .fromUri(bookmarks.getMiddlewareApplicationUrl())
                    .path(PAYMENT_RETURN_URI)
//                    .queryParam("mwTxnRef",paymentCreateResponse.getMwTxnRef())
//                    .queryParam("merchantId",paymentCreateRequest.getMerchantId())
                    .build().toString());

            log.debug("Before checksum request msg" + msgStrBuilder.toString());

//            Checksum
            String checksum = PGCheckSumConverter.HmacSHA256(msgStrBuilder.toString(),pgChecksumKey);

            log.debug("Computed Checksum" + checksum);

            msgStrBuilder.append(PGConstants.PG_MSG_SEPARATOR);
            msgStrBuilder.append(checksum);

            log.debug("After checksum request msg" + msgStrBuilder.toString());

            pgPayReq.setMsg(msgStrBuilder.toString());
        } else {
            pgPayReq.setMsg("");
        }

        pgPayReq.setPaymentChannel(StringUtils.isEmpty(paymentCreateResponse)?"":paymentCreateResponse.getPaymentChannel());

        if ((paymentCreateResponse.getPaymentChannel() != null) && PaymentChannelEnum.PG_CREDIT_CARD_CHANNEL.getPaymentChannel().equals(paymentCreateResponse.getPaymentChannel()) || PaymentChannelEnum.PG_DEBIT_CARD_CHANNEL.getPaymentChannel().equals(paymentCreateResponse.getPaymentChannel())) {
            pgPayReq.setPaymentChannel(PaymentChannelEnum.PG_CREDIT_CARD_CHANNEL.getPaymentChannel())  ;
        }


        return pgPayReq;
    }

    @PostMapping("/PGResponse")
    public String handlePGResponse(Model model, HttpServletRequest request,
                            @RequestParam(value="msg", required=true) String msg) throws  BaseException {

        log.debug("Getting Billdesk response" + msg);
        String[] msgParamsArr = null;
        String merchantId = null;
        String paymentStatus = null;
        String errCode = null;
        String errDesc = null;
        String txnRefNo = null;
        String bankReferenceNo = null;
        String txnAmount = null;
        String bankId = null;
        String bankMerchantId = null;
        String txnDate = null;
        String procStatus = null;

try {

    if (msg != null && !StringUtils.isEmpty(msg)) {
            msgParamsArr = msg.split(PGConstants.PG_MSG_RESPONSE_SEPARATOR);
            String authStatus = msgParamsArr[PGConstants.PG_RESPONSE_AUTH_STATUS_INDEX];
            log.debug("authStatus" + authStatus);

            String mwTxnRef = msgParamsArr[PGConstants.PG_RESPONSE_CUSTOMERID_INDEX];
            merchantId = msgParamsArr[PGConstants.PG_RESPONSE_MERCHANTID_INDEX];

            if (!PGConstants.PG_RESPONSE_AUTH_STATUS_SUCCESS.equalsIgnoreCase(authStatus)) {
                paymentStatus = PaymentStatusEnum.PAYMENT_FAILURE.getPaymentStatus();
                errCode = msgParamsArr[PGConstants.PG_RESPONSE_ERROR_STATUS_INDEX];
                errDesc = msgParamsArr[PGConstants.PG_RESPONSE_ERROR_DESCRIPTION_INDEX];

            } else {
                paymentStatus = PaymentStatusEnum.PAYMENT_SUCCESS.getPaymentStatus();
                txnRefNo = msgParamsArr[PGConstants.PG_RESPONSE_TXN_REF_NUM_INDEX];
                bankReferenceNo = msgParamsArr[PGConstants.PG_RESPONSE_BANK_REF_NUM_INDEX];
                txnAmount = msgParamsArr[PGConstants.PG_RESPONSE_TXN_AMT_INDEX];
                bankId = msgParamsArr[PGConstants.PG_RESPONSE_BANK_ID_INDEX];
                bankMerchantId = msgParamsArr[PGConstants.PG_RESPONSE_BANK_MERCHANT_ID_INDEX];
                txnDate = msgParamsArr[PGConstants.PG_RESPONSE_TXN_DATE_INDEX];
            }
          procStatus = paymentService.getPGResponse(msg, PGResponseEnum.SECONDRY_RESPONSE.getResponseType());
            if(!"Success".equalsIgnoreCase(procStatus)){
                log.debug("Procedure Status :" ,procStatus);
                throw new BaseException("500","Internal server error");
            }
        if(paymentStatus==null||paymentStatus.equalsIgnoreCase("Failure")){
            log.debug("Payment status" + paymentStatus);
            model.addAttribute("status", "Failure");
        }else{
            log.debug("Payment status" + paymentStatus);
            model.addAttribute("status", paymentStatus);
        }
    } else {
        log.debug("PG Response String :" ,msg);
        throw new BaseException("500","Internal server error");
    }
    log.debug("PG Response String :" ,msg);

}catch (Exception e){
    log.debug("Exception occurred in handlePGResponse method :" ,e);
    throw new BaseException("500",e.getMessage());
}
        return "success";
    }

    @RequestMapping("/getTransactionStatus")
    public String getTransactionStatusByTransactionId(Model model,  @RequestParam(value = "transactonId",required = true) String transactionId){
        String strResponse="";
        if(!StringUtils.isEmpty(transactionId)){

        }
        return null;
    }

    @RequestMapping("/redirectToResource")
    public String redirectToResource(Model model,  @RequestParam(value = "transactonId",required = true) String transactionId){
        String strResponse="";
        if(!StringUtils.isEmpty(transactionId)){

        }
        return null;
    }
}

