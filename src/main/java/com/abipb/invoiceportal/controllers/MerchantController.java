package com.abipb.invoiceportal.controllers;

import com.abipb.invoiceportal.entities.Merchant;
import com.abipb.invoiceportal.entities.User;
import com.abipb.invoiceportal.services.MerchantService;
import com.abipb.invoiceportal.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import javax.servlet.http.HttpServletRequest;

/**
 * Merchant controller.
 */
@Controller
public class MerchantController {

    private MerchantService merchantService;


    private UserService userService;

    @Autowired
    public void setMerchantService(MerchantService merchantService) {
        this.merchantService = merchantService;
    }

    @Autowired
    public void setUserService (UserService userService) {this.userService = userService;}

    /**
     * List all Merchants.
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/merchants", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("merchants", merchantService.listAllMerchants());
        //System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
        //System.out.println("Returning merchants:");
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        if ( userName != null && !StringUtils.isEmpty(userName)) {
            User user = userService.findByUsername(userName);
            model.addAttribute("currentUser", user);
            //System.out.println(user.getClrMerchId());
        }
        return "merchants";
    }

    /**
     * View a specific Merchant by its id.
     *
     * @param cid
     * @param model
     * @return
     */
    @RequestMapping("merchant/{cid}")
    public String showMerchant(@PathVariable String cid, Model model) {
        model.addAttribute("merchant", merchantService.getMerchantById(cid));
        return "merchantshow";
    }

    // Afficher le formulaire de modification du Merchant
    @RequestMapping("merchant/edit/{cid}")
    public String edit(@PathVariable String cid, Model model, HttpServletRequest request) {
        model.addAttribute("merchant", merchantService.getMerchantById(cid));
        return "merchantform";
    }

    /**
     * New merchant.
     *
     * @param model
     * @return
     */
    @RequestMapping("merchant/new")
    public String newMerchant(Model model) {
        Merchant merchant = new Merchant();
        merchant.setMerchantPushCheck(true);
        model.addAttribute("merchant", merchant);
        return "merchantform";
    }

    /**
     * Save merchant to database.
     *
     * @param merchant
     * @return
     */
    @RequestMapping(value = "merchant", method = RequestMethod.POST)
    public String saveMerchant(Merchant merchant, HttpServletRequest request) {

        setCheckBoxFields(request,merchant);



        merchantService.saveMerchant(merchant);
        return "redirect:/merchant/" + merchant.getCid();
    }

    /**
     * Delete merchant by its id.
     *
     * @param cid
     * @return
     */
    @RequestMapping("merchant/delete/{cid}")
    public String delete(@PathVariable String cid) {
        merchantService.deleteMerchant(cid);
        return "redirect:/merchants";
    }

//    @InitBinder
//    private void dateBinder(WebDataBinder binder) {
//        //The date format to parse or output your dates
//        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-YYYY");
//        //Create a new CustomDateEditor
//        CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
//        //Register it as custom editor for the Date type
//        binder.registerCustomEditor(Date.class, editor);
//    }

    public void setCheckBoxFields(HttpServletRequest request,Merchant merchant) {

        String merchantPushCheckParam =request.getParameter("merchantPushCheck");
        if (merchantPushCheckParam != null && !StringUtils.isEmpty(merchantPushCheckParam) &&"true".equalsIgnoreCase(merchantPushCheckParam))
        {
            merchant.setMerchantpushFlg("Y");
        }
        else {
            merchant.setMerchantpushFlg("N");
        }

        String merchantPullCheckParam =request.getParameter("merchantPullCheck");
        if (merchantPullCheckParam != null && !StringUtils.isEmpty(merchantPullCheckParam) &&"true".equalsIgnoreCase(merchantPullCheckParam))
        {
            merchant.setMerchantpullFlg("Y");
        }
        else {
            merchant.setMerchantpullFlg("N");
        }

        String cartCheckoutCheckParam =request.getParameter("cartCheckoutCheck");
        if (cartCheckoutCheckParam != null && !StringUtils.isEmpty(cartCheckoutCheckParam) &&"true".equalsIgnoreCase(cartCheckoutCheckParam))
        {
            merchant.setCartcheckoutFlg("Y");
        }
        else {
            merchant.setCartcheckoutFlg("N");
        }

        String cashOnDeliveryCheckParam =request.getParameter("cashOnDeliveryCheck");
        if (cashOnDeliveryCheckParam != null && !StringUtils.isEmpty(cashOnDeliveryCheckParam) &&"true".equalsIgnoreCase(cashOnDeliveryCheckParam))
        {
            merchant.setCashonDeliveryFlg("Y");
        }
        else {
            merchant.setCashonDeliveryFlg("N");
        }

    }
}

