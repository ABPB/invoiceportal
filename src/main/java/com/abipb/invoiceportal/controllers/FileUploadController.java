package com.abipb.invoiceportal.controllers;



import com.abipb.invoiceportal.dto.*;
import com.abipb.invoiceportal.exception.MandateException;
import com.abipb.invoiceportal.services.FileUploadService;
import com.abipb.invoiceportal.services.InvoiceService;
import com.abipb.invoiceportal.services.MandateTransactionService;
import com.abipb.invoiceportal.services.StorageService;
import com.abipb.invoiceportal.storage.StorageFileNotFoundException;

import com.abipb.invoiceportal.util.Base64ImageUtil;
import com.abipb.invoiceportal.util.JAXBUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Controller
public class FileUploadController {

    private final StorageService storageService;
    private final FileUploadService fileUploadService;
    private static final Logger log= LoggerFactory.getLogger(FileUploadController.class);
    @Autowired
    public FileUploadController(StorageService storageService, FileUploadService fileUploadService) {
        this.storageService = storageService;
        this.fileUploadService = fileUploadService;
    }
    @Autowired
    InvoiceService invoiceService;
    @Autowired
    MandateTransactionService mandateTransactionService;

    @Value("${upload-location}")
    private  String uploadFiles;

   @GetMapping("/uploadedFiles")
    public String listUploadedFiles(Model model) throws IOException {

       Path rootLocation=null;
       rootLocation= Paths.get(uploadFiles);
       model.addAttribute("files", storageService
               .loadAll(rootLocation)
               .map(path ->
                       MvcUriComponentsBuilder
                               .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().getFileName().toString())
                               .build().toString())
               .collect(Collectors.toList()));
        return "uploadForm";


    }

    @GetMapping("/delete/files/{filename}")
    public String deleteFile(Model  model,@PathVariable String filename) throws IOException {

        Resource file = storageService.loadAsResource(uploadFiles  + "/" + filename+".csv");
        if(file.exists()) {
            storageService.deleteFile(uploadFiles  + "/" + filename+".csv");
        }

        Path rootLocation=null;
        rootLocation= Paths.get(uploadFiles);
        model.addAttribute("files", storageService
                .loadAll(rootLocation)
                .map(path ->
                        MvcUriComponentsBuilder
                                .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().getFileName().toString())
                                .build().toString())
                .collect(Collectors.toList()));
        return "uploadForm";

    }
    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+file.getFilename()+"\"")
                .body(file);
    }


    @PostMapping("/uploadFile")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes,Model model) throws IOException, MandateException {
        String forwardView="uploadForm";
try {
    log.info("handleFileUpload method of File upload controller ===" + file.getOriginalFilename());

    ArrayList al = new ArrayList();
   al.add("zip");
    /* al.add("xml");*/
    al.add("csv");
    al.add("txt");
    al.add("jpg");
    al.add("jpeg");
    al.add("png");
 /*   al.add("jpg");
    al.add("tiff");*/
    String fileType = "xml";
    String category = "";
    String procStatus = "";
    FileOutputStream fos = null;
    List<JpgFileData> jpgFileDataList = new ArrayList();

    BulkCreateInvoiceImageFileData bulkCreateInvoiceImageFileData = new BulkCreateInvoiceImageFileData();
    JpgFileData jpgFileData = null;
    TiffFileData tiffFileData = null;
    if (file.getSize() == 0) {
        model.addAttribute("error", "Upload file not selected or upload file size is zero");
    } else if (!al.contains(file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1))) {
        model.addAttribute("error", "Uploaded File not in specified formats(csv,txt)");

    } else {
        log.info("else block for checking file existance ===");
        Path rootLocation=null;
        rootLocation= Paths.get(uploadFiles);
        List<String> existingFileList = storageService
                .loadAll(rootLocation)
                .map(path ->
                        MvcUriComponentsBuilder
                                .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().toString())
                                .build().toString())
                .collect(Collectors.toList());

        for (String fileHandle : existingFileList) {
            if (!fileHandle.isEmpty()) {
                String[] pathArr = fileHandle.split("/");
                String fileName = pathArr[pathArr.length - 1];
                if (file.getOriginalFilename().equalsIgnoreCase(fileName)) {
                    model.addAttribute("error", "A file with same name already exists on the server upload location. Please verify if the file is already uploaded");
                   // return "redirect:/uploadFile";
                }

            }

        }
        storageService.store(file);
      
        if (file != null && (file.getOriginalFilename().endsWith(".csv")) || (file.getOriginalFilename().endsWith(".txt"))) {
            forwardView="uploadForm";
            log.info("csv file reading");
            BufferedReader br = null;
            String line = "";
            String cvsSplitBy = ",";
            List<BulkCreateInvoiceFlatFileData> bulkCreateMandateFlatFileDataList = new ArrayList();
            BulkCreateInvoiceFlatFileData bulkCreateInvoiceFlatFileData = null;
            try {

                InputStream is = file.getInputStream();
                br = new BufferedReader(new InputStreamReader(is));
               // String   headerLine = br.readLine();
                while ((line = br.readLine()) != null) {
                    // use comma as separator
                    String[] country = line.split(cvsSplitBy);
                    bulkCreateInvoiceFlatFileData = new BulkCreateInvoiceFlatFileData();
                    bulkCreateInvoiceFlatFileData.setCreateInvoiceDetails(line);
                    bulkCreateMandateFlatFileDataList.add(bulkCreateInvoiceFlatFileData);
                }
                if (bulkCreateMandateFlatFileDataList.size() > 0) {
                    procStatus = invoiceService.saveInvoiceFlateFile(bulkCreateMandateFlatFileDataList,file.getOriginalFilename(),"101");
                    if("Success".equalsIgnoreCase(procStatus)){
                        model.addAttribute("uploadStatus","Success");
                        redirectAttributes.addFlashAttribute("uploadStatus",
                                "Success" );
                        redirectAttributes.addFlashAttribute("message",
                                "file uploaded successfully" );
                    }else{
                        model.addAttribute("uploadStatus","Failed");
                        redirectAttributes.addFlashAttribute("uploadStatus",
                                "Failed" );
                    }
                }

            } catch (FileNotFoundException e) {
                model.addAttribute("uploadStatus","Failed");
                forwardView="transactionsUploadDownload";
                e.printStackTrace();
            } catch (IOException e) {
                forwardView="transactionsUploadDownload";
                model.addAttribute("uploadStatus","Failed");
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }else if((file.getOriginalFilename().endsWith(".zip") && file.getOriginalFilename().startsWith("INV"))) {
            forwardView = "uploadForm";
            log.info("Bulk Images upload");
            ZipInputStream zis =
                    new ZipInputStream(new FileInputStream(JAXBUtil.convertMultipartFiletoStandardFile(file)));
            byte[] buffer = new byte[1024];


            ZipEntry ze = zis.getNextEntry();
            log.info("Zip file with Mandate Images ");
            while (ze != null) {
                String fileName = ze.getName();
                File newFile = new File(uploadFiles + File.separator + fileName);
                log.info("Image File Location  " + newFile);
                log.info("file unzip : " + newFile.getAbsoluteFile());

                //create all non exists folders
                //else you will hit FileNotFoundException for compressed folder
                new File(newFile.getParent()).mkdirs();

                fos = new FileOutputStream(newFile);

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                String strEncodedImage = Base64ImageUtil.encodeImage(newFile.getAbsolutePath());


                log.info("strEncodedImage size== " + strEncodedImage.length());
                // Base64ImageUtil.decoderImage(strEncodedImage ,"D:/uploadFiles/decode_"+fileName);

                if (fileName.endsWith(".jpg")) {
                    jpgFileData = new JpgFileData();
                    jpgFileData.setEncodedImage(strEncodedImage);
                    jpgFileData.setFileName(fileName);
                    jpgFileDataList.add(jpgFileData);
                }
                ze = zis.getNextEntry();
                fos.close();
            }

            zis.closeEntry();
            zis.close();
        }else if((file.getOriginalFilename().endsWith(".pdf"))
                || (file.getOriginalFilename().endsWith(".jpg") )|| (file.getOriginalFilename().endsWith(".jpeg") )
                || (file.getOriginalFilename().endsWith(".png"))){
            String fileName = file.getOriginalFilename();
            FileInputStream fileInputStream=new FileInputStream(JAXBUtil.convertMultipartFiletoStandardFile(file));
            File newFile = new File(uploadFiles + File.separator + fileName);
            log.info("Image File Location  " + newFile);
            log.info("file unzip : " + newFile.getAbsoluteFile());
            byte[] buffer = new byte[1024];
            //create all non exists folders
            //else you will hit FileNotFoundException for compressed folder
            new File(newFile.getParent()).mkdirs();

            fos = new FileOutputStream(newFile);

            int len;
            while ((len = fileInputStream.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }

            String strEncodedImage = Base64ImageUtil.encodeImage(newFile.getAbsolutePath());


            log.info("strEncodedImage size== " + strEncodedImage.length());
            // Base64ImageUtil.decoderImage(strEncodedImage ,"D:/uploadFiles/decode_"+fileName);

            if (fileName.endsWith(".jpg")) {
                jpgFileData = new JpgFileData();
                jpgFileData.setEncodedImage(strEncodedImage);
                jpgFileData.setFileName(fileName);
                jpgFileDataList.add(jpgFileData);
            }
            fileInputStream.close();
            fos.close();
        }


            log.debug("jpgFileDataList  " + jpgFileDataList.size());

            if (jpgFileDataList.size() > 0) {
                bulkCreateInvoiceImageFileData.setJpgFileDataList(jpgFileDataList);
                procStatus = invoiceService.bulkCreateInvoiceImageFile(bulkCreateInvoiceImageFileData);
            }

            if("Success".equalsIgnoreCase(procStatus)){
                model.addAttribute("uploadStatus","Success");
                redirectAttributes.addFlashAttribute("message",
                        "file uploaded successfully" );
            }else{
                model.addAttribute("uploadStatus","Failed");
                redirectAttributes.addFlashAttribute("error",
                        "file upload failed" );
            }
            forwardView="uploadForm";
        }
        //end of unzip
    Path rootLocation=null;
    rootLocation= Paths.get(uploadFiles);
    model.addAttribute("files", storageService
            .loadAll(rootLocation)
            .map(path ->
                    MvcUriComponentsBuilder
                            .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().getFileName().toString())
                            .build().toString())
            .collect(Collectors.toList()));
}catch (Exception e){
    model.addAttribute("uploadStatus","Failed");
    log.info("Exception block" +e.getMessage());
    e.printStackTrace();
}
            return forwardView;

    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

}
