package com.abipb.invoiceportal.controllers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by penchalaiah.g-v on 06-10-2017.
 */
@Controller
public class MerchantRegistrationController {
private final Logger log= LoggerFactory.getLogger(MerchantRegistrationController.class);
    @RequestMapping(value = "/onboarduser", method = RequestMethod.POST)
    public String getmerchantRegistrationDetails(Model model, HttpServletRequest request) {


        return "onboard";
    }

    @RequestMapping(value = "/merchantRegistration", method = RequestMethod.GET)
    public String getmerchantRegistrationPage(Model model, HttpServletRequest request) {
        return "index";
    }

    @RequestMapping(value = "/initiatePayment", method = RequestMethod.GET)
    public String getPaymentInitiationPage(Model model, HttpServletRequest request) {
        return "payments";
    }
}
