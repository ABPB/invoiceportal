package com.abipb.invoiceportal.controllers;


import com.abipb.invoiceportal.services.StorageService;
import com.abipb.invoiceportal.storage.StorageFileNotFoundException;
import com.abipb.invoiceportal.util.ActionTypeEnum;
import com.abipb.invoiceportal.util.NACHConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

/**
 * Created by penchalaiah.g-v on 09-01-2018.
 */
@Controller
public class FileDownloadController {
   /* @Autowired
    FileDownloadService reportService;*/

    @Autowired
    StorageService storageService;

    @Value("${upload-location}")
    private  String uploadFiles;

    @GetMapping("/downloadFile")
    public String listDownloadFiles(Model model) throws IOException {
       // model.addAttribute("downloadfilesList",reportService.getdownloadFilesList());
        return "downloadForm";
    }

    @GetMapping("/files/{filename}/{fileCategory}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename,@PathVariable String fileCategory) {


        /*f(fileCategory.equalsIgnoreCase(ActionTypeEnum.MANDATE_API_CREATE.getAction())) {
            Resource file = storageService.loadAsResource(filename);
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, uploadFiles+ NACHConstants.NACH_UPLOAD_CREATE+"attachment; filename=\""+file.getFilename()+"\"")
                    .body(file);
        }else if(fileCategory.equalsIgnoreCase(ActionTypeEnum.MANDATE_API_CANCEL.getAction())){
            Resource file = storageService.loadAsResource(filename);
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, uploadFiles+NACHConstants.NACH_UPLOAD_CANCEL+"attachment; filename=\""+file.getFilename()+"\"")
                    .body(file);
        }else if(fileCategory.equalsIgnoreCase(ActionTypeEnum.MANDATE_API_AMEND.getAction())){
            Resource file = storageService.loadAsResource(filename);
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, uploadFiles+ NACHConstants.NACH_UPLOAD_AMEND+"attachment; filename=\""+file.getFilename()+"\"")
                    .body(file);
        }*/

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+file.getFilename()+"\"")
                .body(file);
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/uploadedFiles/{fileCategory}")
    public String listUploadedFiles(Model model,@PathVariable String fileCategory) throws IOException {
        Path rootLocation=null;

       if(fileCategory.equalsIgnoreCase(ActionTypeEnum.MANDATE_API_CREATE.getAction())) {

           rootLocation= Paths.get(uploadFiles+NACHConstants.NACH_UPLOAD_CREATE);
           model.addAttribute("files", storageService
                   .loadAll(rootLocation)
                   .map(path ->
                           MvcUriComponentsBuilder
                                   .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().getFileName().toString())
                                   .build().toString())
                   .collect(Collectors.toList()));
       }else if(fileCategory.equalsIgnoreCase(ActionTypeEnum.MANDATE_API_CANCEL.getAction())){
           rootLocation= Paths.get(uploadFiles+NACHConstants.NACH_UPLOAD_CANCEL);
           model.addAttribute("files", storageService
                   .loadAll(rootLocation)
                   .map(path ->
                           MvcUriComponentsBuilder
                                   .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().toString())
                                   .build().toString())
                   .collect(Collectors.toList()));
       }else if(fileCategory.equalsIgnoreCase(ActionTypeEnum.MANDATE_API_AMEND.getAction())){
           rootLocation= Paths.get(uploadFiles+NACHConstants.NACH_UPLOAD_AMEND);
           model.addAttribute("files", storageService
                   .loadAll(rootLocation)
                   .map(path ->
                           MvcUriComponentsBuilder
                                   .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().toString())
                                   .build().toString())
                   .collect(Collectors.toList()));
       } else if(fileCategory.equalsIgnoreCase(NACHConstants.NACH_DEBIT_TXN)){
           rootLocation= Paths.get(uploadFiles+ NACHConstants.NACH_UPLOAD_DR_TXN);
           model.addAttribute("files", storageService
                   .loadAll(rootLocation)
                   .map(path ->
                           MvcUriComponentsBuilder
                                   .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().toString())
                                   .build().toString())
                   .collect(Collectors.toList()));
       }
//        if(fileCategory.equalsIgnoreCase(ActionTypeEnum.MANDATE_API_CREATE.getAction())) {

        return fileCategory.equalsIgnoreCase(NACHConstants.NACH_DEBIT_TXN) ? "collectionoperations":"mandateoperations";
    }


}
