package com.abipb.invoiceportal.controllers;

import com.abipb.invoiceportal.services.ReportService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Merchant controller.
 */
@Controller
public class TestController {

    private ReportService reportService;




     @RequestMapping(value = "testPost", method = RequestMethod.POST)
     public void testPost(HttpServletRequest request, HttpServletResponse response) {
           String objParam =  request.getParameter("obj");
            String msgParam =  request.getParameter("msg");
            System.out.println("objParam is " + objParam);
         System.out.println("msgParam is " + msgParam);

    }

    @RequestMapping(value = "/testPost", method = RequestMethod.GET)
    public String testPost() {

        return "testPost";


    }
    @RequestMapping(value = "/greet", method = RequestMethod.GET)
    public String greet() {

        return "greet";


    }
    @RequestMapping("/redirectTest")
    public String redirectToResource(Model model){

                model.addAttribute("status","Success");
   return "PGRedirection";
    }

}

