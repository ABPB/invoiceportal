package com.abipb.invoiceportal.controllers;

import com.abipb.invoiceportal.dto.InvoiceUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Homepage controller.
 */
@Controller
public class InvoiceController {

    @RequestMapping("/")
    String index(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = (authentication !=null ) ? authentication.getName():"";
        if (!StringUtils.isEmpty(username)) {
                model.addAttribute("userName",username);
            if (username.startsWith("bank")) {
                model.addAttribute("userType","bank");
            }
            else if (username.startsWith("client")) {
                model.addAttribute("userType","client");
            }
            InvoiceUser invoiceUser=(InvoiceUser)authentication.getPrincipal();
            model.addAttribute("entityId",invoiceUser!=null?invoiceUser.getEntityId():"");
            model.addAttribute("merchantId",invoiceUser!=null?invoiceUser.getMerchantId():"");
            model.addAttribute("roleId",invoiceUser!=null?invoiceUser.getRoleId():0);
        }
        return "index";
    }

    @RequestMapping("/dashboard")
    String dashboard() {
        return "dashboard";
    }

    /*@RequestMapping("/transactions")
    String clientupload() {
        return "transactions";
    }*/

    @RequestMapping("/operations")
    String operations() {
        return "operations";
    }

    @RequestMapping("/clientupload")
    String clientUpload() {
        return "uploadForm";
    }

    @RequestMapping("/transactions")
    String mandateTransactions() {
        return "paymentcheckers";
    }

    @RequestMapping("/collectionoperations")
    String collectionOperations() {
        return "collectionoperations";
    }

    @RequestMapping("/mandateoperations")
    String mandateOperations() {
        return "mandateoperations";
    }

}
