package com.abipb.invoiceportal.controllers;

/**
 * Created by Venkatesh.R on 10/15/2017.
 */

import com.abipb.invoiceportal.dto.PaymentQueryResponse;
import com.abipb.invoiceportal.dto.PaymentRefundRequest;
import com.abipb.invoiceportal.dto.PaymentRefundResponse;
import com.abipb.invoiceportal.exception.BaseException;
import com.abipb.invoiceportal.services.PaymentService;
import com.abipb.invoiceportal.util.PGConstants;
import com.abipb.invoiceportal.util.PGResponseEnum;
import com.abipb.invoiceportal.util.PaymentStatusEnum;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.ws.rs.QueryParam;


@Controller
public class BillDeskResponseController {


    @Autowired
    PaymentService paymentService;

    private final Logger log = LoggerFactory.getLogger(com.abipb.invoiceportal.controllers.BillDeskResponseController.class);
//    MerchantID|CustomerID|TxnReferenceNo|BankReferenceNo|TxnAmount|BankID|BankMerc
//    hantID|TxnType|CurrencyName|ItemCode|SecurityType|SecurityID|SecurityPassword|Txn
//    Date|AuthStatus|SettlementType|AdditionalInfo1|AdditionalInfo2|AdditionalInfo3|Additiona
//    lInfo4|AdditionalInfo5|AdditionalInfo6|AdditionalInfo7|ErrorStatus|ErrorDescription|CheckSum
    @RequestMapping(value = "/billdesk/S2SResponse",method ={RequestMethod.GET,RequestMethod.POST} )
    public String handlePGResponse(Model model,@RequestParam(value = "msg", required = true) String msg) {
        String[] msgParamsArr = null;
        String s2sResponseStatus = null;
        String s2sProcStatus=null;
        String errCode = null;
        String errDesc = null;
        log.debug("S2S Bill Desk Response :: "+msg );
        try {
            if(!StringUtils.isEmpty(msg)){
                msgParamsArr = msg.split(PGConstants.PG_MSG_RESPONSE_SEPARATOR);
                String authStatus = msgParamsArr[PGConstants.S2S_RESPONSE_AUTH_STATUS_INDEX];
                log.debug("S2S Response Status :: " + authStatus);
                if (!PGConstants.PG_RESPONSE_AUTH_STATUS_SUCCESS.equalsIgnoreCase(authStatus)) {
                    s2sResponseStatus = PaymentStatusEnum.PAYMENT_FAILURE.getPaymentStatus();
                    errCode = msgParamsArr[PGConstants.S2S_RESPONSE_ERROR_STATUS_INDEX];
                    errDesc = msgParamsArr[PGConstants.S2S_RESPONSE_ERROR_DESCRIPTION_INDEX];
                } else{
                    s2sResponseStatus = PaymentStatusEnum.PAYMENT_SUCCESS.getPaymentStatus();
                }
                s2sProcStatus=paymentService.getPGResponse(msg, PGResponseEnum.PRIMARY_RESPONSE.getResponseType());
                if(StringUtils.isEmpty(s2sProcStatus)||
                        PaymentStatusEnum.PAYMENT_FAILURE.getPaymentStatus().equalsIgnoreCase(s2sProcStatus)){
                    s2sProcStatus = PaymentStatusEnum.PAYMENT_FAILURE.getPaymentStatus();
                    log.debug("Payment status" + s2sProcStatus);
                    model.addAttribute("status", "Failure");
                    model.addAttribute("errCode", errCode);
                    model.addAttribute("errDesc", errDesc);
                }else{
                    model.addAttribute("status", s2sProcStatus);
                }
            }else {
            // response msg itself is null - error response.
                s2sResponseStatus = PaymentStatusEnum.PAYMENT_FAILURE.getPaymentStatus();
                model.addAttribute("status", "Failure");
                model.addAttribute("errCode", errCode);
                model.addAttribute("errDesc", errDesc);
            }

    }catch (Exception e){
        log.error("Exception ::",e);
        model.addAttribute("status", "Failure");
        model.addAttribute("errCode", errCode);
        model.addAttribute("errDesc", errDesc);
        throw new BaseException(errCode,errDesc);
    }
        return "success";
}

    @RequestMapping("/testQuery")
    public String  getsample( Model model, @QueryParam("txnId" ) String txnId){
       PaymentQueryResponse paymentQueryResponse= paymentService.callPaymentQuery(txnId);
       log.debug("MSG :: "+ paymentQueryResponse.getMsg());
        model.addAttribute("querymsg",paymentQueryResponse.getMsg());
        return "PGTest";
    }



    @RequestMapping("/testRefund")
    public String  getsampleRefund( Model model, @QueryParam("txnReferenceNo" ) String txnReferenceNo, @QueryParam("txnAamount" )String txnAamount){
        PaymentRefundRequest paymentRefundRequest=new PaymentRefundRequest();
        paymentRefundRequest.setTxnAmount(txnAamount);
        paymentRefundRequest.setTxnReferenceNo(txnReferenceNo);
        PaymentRefundResponse paymentRefundResponse=paymentService.callPaymentRefund(paymentRefundRequest);
        model.addAttribute("refundmsg",paymentRefundResponse);
        return "PGTest";
    }
}