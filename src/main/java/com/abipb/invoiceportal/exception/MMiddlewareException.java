package com.abipb.invoiceportal.exception;

import java.io.Serializable;

/**
 * Created by penchalaiah.g-v on 07-11-2017.
 */
public class MMiddlewareException extends Throwable implements Serializable {

    private String errCode;
    private String message;

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MMiddlewareException that = (MMiddlewareException) o;

        if (errCode != null ? !errCode.equals(that.errCode) : that.errCode != null) return false;
        return message != null ? message.equals(that.message) : that.message == null;
    }

    @Override
    public int hashCode() {
        int result = errCode != null ? errCode.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }



    public MMiddlewareException(String errCode, String message) {
        this.errCode = errCode;
        this.message = message;
    }



}
