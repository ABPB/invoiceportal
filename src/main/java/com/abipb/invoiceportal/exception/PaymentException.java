package com.abipb.invoiceportal.exception;

/**
 * Created by penchalaiah.g-v on 08-11-2017.
 */
public class PaymentException extends MMiddlewareException {
    public PaymentException(String errCode, String message) {
        super(errCode, message);
    }
}
