package com.abipb.invoiceportal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService userDetailsService;

    //@Bean
    //public BCryptPasswordEncoder bCryptPasswordEncoder() {
    //    return new BCryptPasswordEncoder();
    //}

    @Override
    protected void configure(HttpSecurity http) throws Exception {
       http
              .headers().disable()
              .csrf().disable()
                .formLogin()
                .loginPage("/login")
                    .permitAll()
                .defaultSuccessUrl("/")
                    .and()
                .logout()
//                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET"))
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login")
                    .permitAll()
                    .and()
                .authorizeRequests()
//                .antMatchers("/resources/**", "/registration").permitAll()
//                .antMatchers("/css/**","/js/**","/images/**").permitAll()
//
//                .antMatchers("/resources/**", "/registration").permitAll()
                .antMatchers("/css/**","/js/**","/images/**","/bower_components/**","/controller/**","/views/**","/ui/images/**","/ui/bower_components/**","/ui/controller/**","/ui/views/**").permitAll()
                .anyRequest().authenticated();

    }

//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
//    }

        @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
   }
}