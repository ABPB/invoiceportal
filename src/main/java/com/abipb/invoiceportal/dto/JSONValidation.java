package com.abipb.invoiceportal.dto;

/**
 * Created by vamsi.b-v on 27/10/2017.
 */
public class JSONValidation {
    private String message;
    private String status;
    private String statusCode;
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
