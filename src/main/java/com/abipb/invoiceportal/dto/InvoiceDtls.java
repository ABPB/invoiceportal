package com.abipb.invoiceportal.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Date;


/**
 * Created by Venkatesh.R on 10/14/2017.
 */
public class InvoiceDtls {

    @NotNull(message = "PaymentCreateRequest.invoiceDtls:invoiceId cannot be empty")
    @Length(min = 1, max = 50)
    String invoiceId;
    Date dueDate;
    BigDecimal invoiceAmount;

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public BigDecimal getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(BigDecimal invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    @Override
    public String toString() {
        return "InvoiceDtls{" +
                "invoiceId='" + invoiceId + '\'' +
                ", dueDate=" + dueDate +
                ", invoiceAmount=" + invoiceAmount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvoiceDtls that = (InvoiceDtls) o;

        if (!getInvoiceId().equals(that.getInvoiceId())) return false;
        if (getDueDate() != null ? !getDueDate().equals(that.getDueDate()) : that.getDueDate() != null) return false;
        return getInvoiceAmount() != null ? getInvoiceAmount().equals(that.getInvoiceAmount()) : that.getInvoiceAmount() == null;
    }

    @Override
    public int hashCode() {
        int result = getInvoiceId().hashCode();
        result = 31 * result + (getDueDate() != null ? getDueDate().hashCode() : 0);
        result = 31 * result + (getInvoiceAmount() != null ? getInvoiceAmount().hashCode() : 0);
        return result;
    }
}
