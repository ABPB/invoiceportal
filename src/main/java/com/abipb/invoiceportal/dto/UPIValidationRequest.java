package com.abipb.invoiceportal.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * Created by Venkatesh.R on 10/20/2017.
 */
public class UPIValidationRequest {

    @NotNull(message = "UPIValidationRequest:upihandle cannot be empty")
    @Length(min = 1, max = 50)
    String upihandle;

    public String getUpihandle() {
        return upihandle;
    }

    public void setUpihandle(String upihandle) {
        this.upihandle = upihandle;
    }
}
