package com.abipb.invoiceportal.dto;

import java.io.Serializable;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */
public class UPIRow implements Serializable{
    private String input_json="";

    public void setInput_json(String input_json){
        this.input_json = input_json;
    }
    public String getInput_json(){
        return this.input_json;
    }

}
