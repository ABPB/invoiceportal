package com.abipb.invoiceportal.dto;

import java.io.Serializable;

/**
 * Created by abhay.kumar-v on 11/27/2017.
 */
public class Invoice implements Serializable{
    private static final long serializableUID=2L;
    private String invoiceId;
    private String invoiceAmt;
    private String invoiceDueDate;


    public Invoice() {
        //no operation
    }

    public Invoice(String invoiceId, String invoiceAmt, String invoiceDueDate) {
        this.invoiceId = invoiceId;
        this.invoiceAmt = invoiceAmt;
        this.invoiceDueDate = invoiceDueDate;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceAmt() {
        return invoiceAmt;
    }

    public void setInvoiceAmt(String invoiceAmt) {
        this.invoiceAmt = invoiceAmt;
    }

    public String getInvoiceDueDate() {
        return invoiceDueDate;
    }

    public void setInvoiceDueDate(String invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Invoice invoice = (Invoice) o;

        if (invoiceId != null ? !invoiceId.equals(invoice.invoiceId) : invoice.invoiceId != null) return false;
        if (invoiceAmt != null ? !invoiceAmt.equals(invoice.invoiceAmt) : invoice.invoiceAmt != null) return false;
        return invoiceDueDate != null ? invoiceDueDate.equals(invoice.invoiceDueDate) : invoice.invoiceDueDate == null;
    }

    @Override
    public int hashCode() {
        int result = invoiceId != null ? invoiceId.hashCode() : 0;
        result = 31 * result + (invoiceAmt != null ? invoiceAmt.hashCode() : 0);
        result = 31 * result + (invoiceDueDate != null ? invoiceDueDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "invoiceId='" + invoiceId + '\'' +
                ", invoiceAmt='" + invoiceAmt + '\'' +
                ", invoiceDueDate='" + invoiceDueDate + '\'' +
                '}';
    }
}

