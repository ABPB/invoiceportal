package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */
public class UPIPWSessionRequest implements Serializable {

    @JsonProperty("PWPROCESSRS")
    private UPIPWProcessRequest upipwProcessRequest=new UPIPWProcessRequest();

    public UPIPWProcessRequest getUpipwProcessRequest() {
        return upipwProcessRequest;
    }

    public void setUpipwProcessRequest(UPIPWProcessRequest upipwProcessRequest) {
        this.upipwProcessRequest = upipwProcessRequest;
    }

    @Override
    public String toString() {
        return "UPIPWSessionRequest{" +
                "upipwProcessRequest=" + upipwProcessRequest +
                '}';
    }
}

