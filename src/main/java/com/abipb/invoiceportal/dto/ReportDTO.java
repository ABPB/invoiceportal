package com.abipb.invoiceportal.dto;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by Venkatesh.R on 8/30/2017.
 */
public class ReportDTO {

    private BigDecimal runSeq;
    private String custId;
    private String custName;
    private String custVPA;

    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private BigDecimal custMobNo;
    private String billNo;
    private String billMonth;
    private BigDecimal billAmount;
    @DateTimeFormat(pattern="dd-MM-YYYY")
    private Timestamp billDueDate;
    private String status;
    private String remark;

    private BigDecimal receivedAmt;
    private String channel;
    private BigDecimal calcAmt;
    private String billStage;
    private BigDecimal pushAmount;

    public BigDecimal getPushAmount() {
        return pushAmount;
    }

    public void setPushAmount(BigDecimal pushAmount) {
        this.pushAmount = pushAmount;
    }

    @DateTimeFormat(pattern="dd-MM-YYYY")
    private Timestamp billStageDueDate;

    //for enterprise push
    private String custAcct;
    private String custIFSC;
    private String txnMode;

    public java.util.Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(java.util.Date createdDate) {
        this.createdDate = createdDate;
    }

    private java.util.Date createdDate;

    public BigDecimal getRunSeq() {
        return runSeq;
    }

    public void setRunSeq(BigDecimal runSeq) {
        this.runSeq = runSeq;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustVPA() {
        return custVPA;
    }

    public void setCustVPA(String custVPA) {
        this.custVPA = custVPA;
    }

    public BigDecimal getCustMobNo() {
        return custMobNo;
    }

    public void setCustMobNo(BigDecimal custMobNo) {
        this.custMobNo = custMobNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getBillMonth() {
        return billMonth;
    }

    public void setBillMonth(String billMonth) {
        this.billMonth = billMonth;
    }

    public BigDecimal getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(BigDecimal billAmount) {
        this.billAmount = billAmount;
    }

    public Timestamp getBillDueDate() {
        return billDueDate;
    }

    public void setBillDueDate(Timestamp billDueDate) {
        this.billDueDate = billDueDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getReceivedAmt() {
        return receivedAmt;
    }

    public void setReceivedAmt(BigDecimal receivedAmt) {
        this.receivedAmt = receivedAmt;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public BigDecimal getCalcAmt() {
        return calcAmt;
    }

    public void setCalcAmt(BigDecimal calcAmt) {
        this.calcAmt = calcAmt;
    }

    public String getBillStage() {
        return billStage;
    }

    public void setBillStage(String billStage) {
        this.billStage = billStage;
    }

    public Timestamp getBillStageDueDate() {
        return billStageDueDate;
    }

    public void setBillStageDueDate(Timestamp billStageDueDate) {
        this.billStageDueDate = billStageDueDate;
    }

    public String getCustAcct() {
        return custAcct;
    }

    public void setCustAcct(String custAcct) {
        this.custAcct = custAcct;
    }

    public String getCustIFSC() {
        return custIFSC;
    }

    public void setCustIFSC(String custIFSC) {
        this.custIFSC = custIFSC;
    }

    public String getTxnMode() {
        return txnMode;
    }

    public void setTxnMode(String txnMode) {
        this.txnMode = txnMode;
    }

}
