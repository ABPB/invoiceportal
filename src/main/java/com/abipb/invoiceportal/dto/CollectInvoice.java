package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by abhay.kumar-v on 11/29/2017.
 */
public class CollectInvoice extends Invoice {
    private static final long serializableUID=4L;
    private String fromEstablishmentId;
    @JsonProperty(value ="payerName",defaultValue ="")
    private String invoicePayerName;

    public CollectInvoice() {
        super();
        //no operation
    }

    public CollectInvoice(String invoiceId, String invoiceAmt, String invoiceDueDate, String fromEstablishmentId, String invoicePayerName) {
        super(invoiceId,invoiceAmt,invoiceDueDate);
        this.fromEstablishmentId = fromEstablishmentId;
        this.invoicePayerName = invoicePayerName;
    }

    public static long getSerializableUID() {
        return serializableUID;
    }

    public String getFromEstablishmentId() {
        return fromEstablishmentId;
    }

    public void setFromEstablishmentId(String fromEstablishmentId) {
        this.fromEstablishmentId = fromEstablishmentId;
    }

    public String getInvoicePayerName() {
        return invoicePayerName;
    }

    public void setInvoicePayerName(String invoicePayerName) {
        this.invoicePayerName = invoicePayerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CollectInvoice that = (CollectInvoice) o;

        if (fromEstablishmentId != null ? !fromEstablishmentId.equals(that.fromEstablishmentId) : that.fromEstablishmentId != null)
            return false;
        return invoicePayerName != null ? invoicePayerName.equals(that.invoicePayerName) : that.invoicePayerName == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (fromEstablishmentId != null ? fromEstablishmentId.hashCode() : 0);
        result = 31 * result + (invoicePayerName != null ? invoicePayerName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CollectInvoice{" +
                "fromEstablishmentId='" + fromEstablishmentId + '\'' +
                ", invoicePayerName='" + invoicePayerName + '\'' +
                '}';
    }
}
