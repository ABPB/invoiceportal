package com.abipb.invoiceportal.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * Created by abhay.kumar-v on 10/25/2017.
 */
public class PaymentQueryRequest extends PaymentBillDesk  {

    @NotNull(message = "PaymentQueryRequest:currentDateTimestamp cannot be empty")
    @Length(min = 1, max = 50)
    private String currentDateTimestamp="";

    public String getCurrentDateTimestamp() {
        return currentDateTimestamp;
    }

    public void setCurrentDateTimestamp(String currentDateTimestamp) {
        this.currentDateTimestamp = currentDateTimestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PaymentQueryRequest that = (PaymentQueryRequest) o;

        return currentDateTimestamp != null ? currentDateTimestamp.equals(that.currentDateTimestamp) : that.currentDateTimestamp == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (currentDateTimestamp != null ? currentDateTimestamp.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PaymentQueryRequest{" +
                "currentDateTimestamp='" + currentDateTimestamp + '\'' +
                '}';
    }
}
