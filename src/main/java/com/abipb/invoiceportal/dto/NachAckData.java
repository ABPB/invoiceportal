package com.abipb.invoiceportal.dto;

/**
 * Created by penchalaiah.g-v on 02-01-2018.
 */
public class NachAckData {

    private String msgId;

    private String umrnNo;

    private  String merchantId;

    private  String conRefNo;

    private String status;

    private  String reason;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NachAckData that = (NachAckData) o;

        if (msgId != null ? !msgId.equals(that.msgId) : that.msgId != null) return false;
        if (umrnNo != null ? !umrnNo.equals(that.umrnNo) : that.umrnNo != null) return false;
        if (merchantId != null ? !merchantId.equals(that.merchantId) : that.merchantId != null) return false;
        if (conRefNo != null ? !conRefNo.equals(that.conRefNo) : that.conRefNo != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        return reason != null ? reason.equals(that.reason) : that.reason == null;
    }

    @Override
    public int hashCode() {
        int result = msgId != null ? msgId.hashCode() : 0;
        result = 31 * result + (umrnNo != null ? umrnNo.hashCode() : 0);
        result = 31 * result + (merchantId != null ? merchantId.hashCode() : 0);
        result = 31 * result + (conRefNo != null ? conRefNo.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (reason != null ? reason.hashCode() : 0);
        return result;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getUmrnNo() {
        return umrnNo;
    }

    public void setUmrnNo(String umrnNo) {
        this.umrnNo = umrnNo;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getConRefNo() {
        return conRefNo;
    }

    public void setConRefNo(String conRefNo) {
        this.conRefNo = conRefNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
