package com.abipb.invoiceportal.dto;

/**
 * Created by penchalaiah.g-v on 06-11-2017.
 */
public class ErrorResponse {
    public ErrorResponse(String errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ErrorResponse that = (ErrorResponse) o;

        if (errCode != null ? !errCode.equals(that.errCode) : that.errCode != null) return false;
        return errMsg != null ? errMsg.equals(that.errMsg) : that.errMsg == null;
    }

    @Override
    public int hashCode() {
        int result = errCode != null ? errCode.hashCode() : 0;
        result = 31 * result + (errMsg != null ? errMsg.hashCode() : 0);
        return result;
    }

    private String errCode;

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "errCode='" + errCode + '\'' +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }

    private String errMsg;

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }


}
