package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */
public class UPIPWProcessRequest implements Serializable{

    @JsonProperty("PWHEADER")
    private UPIPWHeader upipwHeader =new UPIPWHeader();

    @JsonProperty("PWDATA")
    private UPIPWData upipwData =new UPIPWData();

    @JsonProperty("PWERROR")
    private UPIPWError upipwError=new UPIPWError();

    public UPIPWHeader getUpipwHeader() {
        return upipwHeader;
    }

    public void setUpipwHeader(UPIPWHeader upipwHeader) {
        this.upipwHeader = upipwHeader;
    }

    public UPIPWData getUpipwData() {
        return upipwData;
    }

    public void setUpipwData(UPIPWData upipwData) {
        this.upipwData = upipwData;
    }

    public UPIPWError getUpipwError() {
        return upipwError;
    }

    public void setUpipwError(UPIPWError upipwError) {
        this.upipwError = upipwError;
    }

    @Override
    public String toString() {
        return "UPIPWProcessRequest{" +
                "upipwHeader=" + upipwHeader +
                ", upipwData=" + upipwData +
                ", upipwError=" + upipwError +
                '}';
    }
}
