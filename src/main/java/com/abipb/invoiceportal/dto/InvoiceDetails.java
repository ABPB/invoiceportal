package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * Created by abhay.kumar-v on 11/27/2017.
 */

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "pay",
        "collect"
})
public class InvoiceDetails implements Serializable{
    private static final long serialVersionUID=1L;
    @JsonProperty(value = "pay",required = false,defaultValue = "null")
    private List<PayInvoice> pay;
    @JsonProperty(value = "collect",required = false,defaultValue = "null")
    private List<CollectInvoice> collect;

    public InvoiceDetails(List<PayInvoice> pay, List<CollectInvoice> collect) {
        this.pay = pay;
        this.collect = collect;
    }

    public List<PayInvoice> getPay() {
        return pay;
    }

    public void setPay(List<PayInvoice> pay) {
        this.pay = pay;
    }

    public List<CollectInvoice> getCollect() {
        return collect;
    }

    public void setCollect(List<CollectInvoice> collect) {
        this.collect = collect;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvoiceDetails that = (InvoiceDetails) o;

        if (pay != null ? !pay.equals(that.pay) : that.pay != null) return false;
        return collect != null ? collect.equals(that.collect) : that.collect == null;
    }

    @Override
    public int hashCode() {
        int result = pay != null ? pay.hashCode() : 0;
        result = 31 * result + (collect != null ? collect.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InvoiceDetails{" +
                "pay=" + pay +
                ", collect=" + collect +
                '}';
    }
}
