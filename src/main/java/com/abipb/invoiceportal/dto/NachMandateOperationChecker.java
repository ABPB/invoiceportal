package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * Created by abhay.kumar-v on 1/15/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "utilityCode",
        "sponoserCode",
        "sysDate",
        "mandateTransactions"
})
public class NachMandateOperationChecker {
    @JsonProperty(value ="utilityCode",defaultValue = "",required = false)
    private String utilityCode;
    @JsonProperty(value ="sponoserCode",defaultValue = "",required = false)
    private String sponoserCode;
    @JsonProperty(value ="sysDate",defaultValue = "",required = false)
    private String sysDate;
    @JsonProperty(value ="mandateTransactions",defaultValue = "",required = false)
    private List<NachMandateOperation> mandateTransactions;

    public NachMandateOperationChecker() {
    }

    public NachMandateOperationChecker(String utilityCode, String sponoserCode, String sysDate, List<NachMandateOperation> mandateTransactions) {
        this.utilityCode = utilityCode;
        this.sponoserCode = sponoserCode;
        this.sysDate = sysDate;
        this.mandateTransactions = mandateTransactions;
    }

    public String getUtilityCode() {
        return utilityCode;
    }

    public void setUtilityCode(String utilityCode) {
        this.utilityCode = utilityCode;
    }

    public String getSponoserCode() {
        return sponoserCode;
    }

    public void setSponoserCode(String sponoserCode) {
        this.sponoserCode = sponoserCode;
    }

    public String getSysDate() {
        return sysDate;
    }

    public void setSysDate(String sysDate) {
        this.sysDate = sysDate;
    }

    public List<NachMandateOperation> getMandateTransactions() {
        return mandateTransactions;
    }
}
