package com.abipb.invoiceportal.dto;

/**
 * Created by penchalaiah.g-v on 01-11-2017.
 */
public class UPIDetails {

    public String getHandlerName() {
        return handlerName;
    }

    public void setHandlerName(String handlerName) {
        this.handlerName = handlerName;
    }

    public String getPreferredFlag() {
        return preferredFlag;
    }

    public void setPreferredFlag(String preferredFlag) {
        this.preferredFlag = preferredFlag;
    }

    private String handlerName;
  private  String  preferredFlag;


}
