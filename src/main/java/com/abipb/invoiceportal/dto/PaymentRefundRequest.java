package com.abipb.invoiceportal.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * Created by abhay.kumar-v on 10/25/2017.
 */
public class PaymentRefundRequest extends PaymentBillDesk {

    @NotNull(message = "PaymentRefundRequest:txnReferenceNo cannot be empty")
    @Length(min = 1, max = 50)
    private String txnReferenceNo="";

    @NotNull(message = "PaymentRefundRequest:txnDate cannot be empty")
    @Length(min = 1, max = 50)
    private  String txnDate="";

    @NotNull(message = "PaymentRefundRequest:txnAmount cannot be empty")
    @Length(min = 1, max = 50)
    private String txnAmount="";

    @NotNull(message = "PaymentRefundRequest:refAmount cannot be empty")
    @Length(min = 1, max = 50)
    private String refAmount="";

    @NotNull(message = "PaymentRefundRequest:refDataTime cannot be empty")
    @Length(min = 1, max = 50)
    private String refDataTime="";

    @NotNull(message = "PaymentRefundRequest:merchantRefNo cannot be empty")
    @Length(min = 1, max = 50)
    private String merchantRefNo="";

    @NotNull(message = "PaymentRefundRequest:filler1 cannot be empty")
    @Length(min = 1, max = 50)
    private String filler1="";

    @NotNull(message = "PaymentRefundRequest:filler2 cannot be empty")
    @Length(min = 1, max = 50)
    private String filler2="";

    @NotNull(message = "PaymentRefundRequest:filler3 cannot be empty" )
    @Length(min = 1, max = 50)
    private String filler3="";


    public String getTxnReferenceNo() {
        return txnReferenceNo;
    }

    public void setTxnReferenceNo(String txnReferenceNo) {
        this.txnReferenceNo = txnReferenceNo;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(String txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getRefAmount() {
        return refAmount;
    }

    public void setRefAmount(String refAmount) {
        this.refAmount = refAmount;
    }

    public String getRefDataTime() {
        return refDataTime;
    }

    public void setRefDataTime(String refDataTime) {
        this.refDataTime = refDataTime;
    }

    public String getMerchantRefNo() {
        return merchantRefNo;
    }

    public void setMerchantRefNo(String merchantRefNo) {
        this.merchantRefNo = merchantRefNo;
    }

    public String getFiller1() {
        return filler1;
    }

    public void setFiller1(String filler1) {
        this.filler1 = filler1;
    }

    public String getFiller2() {
        return filler2;
    }

    public void setFiller2(String filler2) {
        this.filler2 = filler2;
    }

    public String getFiller3() {
        return filler3;
    }

    public void setFiller3(String filler3) {
        this.filler3 = filler3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PaymentRefundRequest that = (PaymentRefundRequest) o;

        if (txnReferenceNo != null ? !txnReferenceNo.equals(that.txnReferenceNo) : that.txnReferenceNo != null)
            return false;
        if (txnDate != null ? !txnDate.equals(that.txnDate) : that.txnDate != null) return false;
        if (txnAmount != null ? !txnAmount.equals(that.txnAmount) : that.txnAmount != null) return false;
        if (refAmount != null ? !refAmount.equals(that.refAmount) : that.refAmount != null) return false;
        if (refDataTime != null ? !refDataTime.equals(that.refDataTime) : that.refDataTime != null) return false;
        if (merchantRefNo != null ? !merchantRefNo.equals(that.merchantRefNo) : that.merchantRefNo != null)
            return false;
        if (filler1 != null ? !filler1.equals(that.filler1) : that.filler1 != null) return false;
        if (filler2 != null ? !filler2.equals(that.filler2) : that.filler2 != null) return false;
        return filler3 != null ? filler3.equals(that.filler3) : that.filler3 == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (txnReferenceNo != null ? txnReferenceNo.hashCode() : 0);
        result = 31 * result + (txnDate != null ? txnDate.hashCode() : 0);
        result = 31 * result + (txnAmount != null ? txnAmount.hashCode() : 0);
        result = 31 * result + (refAmount != null ? refAmount.hashCode() : 0);
        result = 31 * result + (refDataTime != null ? refDataTime.hashCode() : 0);
        result = 31 * result + (merchantRefNo != null ? merchantRefNo.hashCode() : 0);
        result = 31 * result + (filler1 != null ? filler1.hashCode() : 0);
        result = 31 * result + (filler2 != null ? filler2.hashCode() : 0);
        result = 31 * result + (filler3 != null ? filler3.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PaymentRefundRequest{" +
                "txnReferenceNo='" + txnReferenceNo + '\'' +
                ", txnDate='" + txnDate + '\'' +
                ", txnAmount='" + txnAmount + '\'' +
                ", refAmount='" + refAmount + '\'' +
                ", refDataTime='" + refDataTime + '\'' +
                ", merchantRefNo='" + merchantRefNo + '\'' +
                ", filler1='" + filler1 + '\'' +
                ", filler2='" + filler2 + '\'' +
                ", filler3='" + filler3 + '\'' +
                '}';
    }
}
