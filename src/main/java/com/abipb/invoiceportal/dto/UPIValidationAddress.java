package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */
public class UPIValidationAddress implements Serializable{

    @JsonProperty("Row")
    private List<UPIRow> upiRows =new ArrayList<>();

    public List<UPIRow> getUpiRows() {
        return upiRows;
    }

    public void setUpiRows(List<UPIRow> upiRows) {
        this.upiRows = upiRows;
    }

    @Override
    public String toString() {
        return "UPIValidationAddress{" +
                "upiRows=" + upiRows +
                '}';
    }
}
