package com.abipb.invoiceportal.dto;

import java.util.List;

/**
 * Created by penchalaiah.g-v on 01-11-2017.
 */
public class PaymentModes {

    List<Mode> mode;
    public List<Mode> getMode() {
        return mode;
    }

    public void setMode(List<Mode> mode) {
        this.mode = mode;
    }

}
