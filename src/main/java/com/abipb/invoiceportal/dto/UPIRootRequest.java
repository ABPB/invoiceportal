package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */

public class UPIRootRequest implements Serializable {

    @JsonProperty("PWSESSIONRS")
    private UPIPWSessionRequest upipwSessionRequest =new UPIPWSessionRequest();

    public UPIPWSessionRequest getUpipwSessionRequest() {
        return upipwSessionRequest;
    }

    public void setUpipwSessionRequest(UPIPWSessionRequest upipwSessionRequest) {
        this.upipwSessionRequest = upipwSessionRequest;
    }

    @Override
    public String toString() {
        return "UPIRootRequest{" +
                "upipwSessionRequest=" + upipwSessionRequest +
                '}';
    }
}
