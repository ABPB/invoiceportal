package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by abhay.kumar-v on 12/12/2017.
 */

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "status",
        "description"
})
public class MandateResponse {
    private static final long serialVersionUID=1L;
    @JsonProperty(value = "status",required = false,defaultValue = "")
    private String status;
    @JsonProperty(value = "description",required = false,defaultValue = "")
    private String description;


    public MandateResponse() {
    }

    public MandateResponse(String status, String description) {
        this.status = status;
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MandateResponse that = (MandateResponse) o;

        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = status != null ? status.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MandateResponse{" +
                "status='" + status + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
