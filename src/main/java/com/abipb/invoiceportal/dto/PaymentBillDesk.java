package com.abipb.invoiceportal.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by abhay.kumar-v on 10/25/2017.
 */
public class PaymentBillDesk implements Serializable {

    @NotNull(message = "PaymentBillDesk:requestType cannot be empty")
    @Length(min = 1, max = 50)
    private String requestType="";

    @NotNull(message = "PaymentBillDesk:merchantId cannot be empty")
    @Length(min = 1, max = 50)
    private  String merchantId="";

    @NotNull(message = "PaymentBillDesk:customerId cannot be empty")
    @Length(min = 1, max = 50)
    private String customerId="";

    @NotNull(message = "PaymentBillDesk:checksum cannot be empty")
    @Length(min = 1, max = 50)
    private String checksum="";


    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaymentBillDesk that = (PaymentBillDesk) o;

        if (requestType != null ? !requestType.equals(that.requestType) : that.requestType != null) return false;
        if (merchantId != null ? !merchantId.equals(that.merchantId) : that.merchantId != null) return false;
        if (customerId != null ? !customerId.equals(that.customerId) : that.customerId != null) return false;
        return checksum != null ? checksum.equals(that.checksum) : that.checksum == null;
    }

    @Override
    public int hashCode() {
        int result = requestType != null ? requestType.hashCode() : 0;
        result = 31 * result + (merchantId != null ? merchantId.hashCode() : 0);
        result = 31 * result + (customerId != null ? customerId.hashCode() : 0);
        result = 31 * result + (checksum != null ? checksum.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PaymentBillDesk{" +
                "requestType='" + requestType + '\'' +
                ", merchantId='" + merchantId + '\'' +
                ", customerId='" + customerId + '\'' +
                ", checksum='" + checksum + '\'' +
                '}';
    }
}
