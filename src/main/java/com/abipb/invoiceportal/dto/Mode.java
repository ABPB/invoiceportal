package com.abipb.invoiceportal.dto;

import java.util.List;

/**
 * Created by penchalaiah.g-v on 01-11-2017.
 */
public class Mode {

    private String mode;
    private List<BankDetails> bankDetails;
    private List<UPIDetails> upiDetails;
    private List<String> type;
    public String getMode() {
        return mode;
    }
    public void setMode(String mode) {
        this.mode = mode;
    }

    public List<BankDetails> getBankDetails() {
        return bankDetails;
    }

    public void setBankDetails(List<BankDetails> bankDetails) {
        this.bankDetails = bankDetails;
    }

    public List<UPIDetails> getUpiDetails() {
        return upiDetails;
    }

    public void setUpiDetails(List<UPIDetails> upiDetails) {
        this.upiDetails = upiDetails;
    }

    public List<String> getType() {
        return type;
    }

    public void setType(List<String> type) {
        this.type = type;
    }
}
