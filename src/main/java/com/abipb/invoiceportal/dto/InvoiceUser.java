package com.abipb.invoiceportal.dto;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by penchalaiah.g on 17-02-2018.
 */
public class InvoiceUser extends org.springframework.security.core.userdetails.User {
    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    private  String entityId;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    private String  merchantId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    private Long  roleId;
    public InvoiceUser(String username, String password, Collection<? extends GrantedAuthority> authorities,String entityId,String merchantId,Long roleId) {
        super(username, password, authorities);
        this.entityId=entityId;
        this.merchantId=merchantId;
        this.roleId=roleId;
    }

    public InvoiceUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }
}
