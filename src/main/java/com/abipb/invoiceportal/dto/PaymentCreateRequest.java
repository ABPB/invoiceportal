package com.abipb.invoiceportal.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh.R on 10/14/2017.
 */
public class PaymentCreateRequest {
    @NotNull(message = "PaymentCreateRequest:merchantId cannot be empty")
    @Length(min = 1, max = 50)
    String merchantId;

    @NotNull(message = "PaymentCreateRequest:fromEntityId cannot be empty")
    @Length(min = 1, max = 50)
    String fromEntityId;

    @NotNull(message = "PaymentCreateRequest:toEntityId cannot be empty")
    @Length(min = 1, max = 50)
    String toEntityId;

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    @NotNull(message = "PaymentCreateRequest:paymentMode cannot be empty")
    @Length(min = 1, max = 10)
    String paymentMode;

    public String getClientTxnRef() {
        return clientTxnRef;
    }

    public void setClientTxnRef(String clientTxnRef) {
        this.clientTxnRef = clientTxnRef;
    }

    String clientTxnRef;

    List<InvoiceDtls> invoiceDtlsList = new ArrayList<>();

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getFromEntityId() {
        return fromEntityId;
    }

    public void setFromEntityId(String fromEntityId) {
        this.fromEntityId = fromEntityId;
    }

    public String getToEntityId() {
        return toEntityId;
    }

    public void setToEntityId(String toEntityId) {
        this.toEntityId = toEntityId;
    }

    public List<InvoiceDtls> getInvoiceDtlsList() {
        return invoiceDtlsList;
    }

    public void setInvoiceDtlsList(List<InvoiceDtls> invoiceDtlsList) {
        this.invoiceDtlsList = invoiceDtlsList;
    }

    @Override
    public String toString() {
        return "PaymentCreateRequest{" +
                "merchantId='" + merchantId + '\'' +
                ", fromEntityId='" + fromEntityId + '\'' +
                ", toEntityId='" + toEntityId + '\'' +
                ", clientTxnRef='" + clientTxnRef + '\'' +
                ", paymentMode='" + paymentMode + '\'' +
                ", invoiceDtlsList=" + invoiceDtlsList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaymentCreateRequest that = (PaymentCreateRequest) o;

        if (getMerchantId() != null ? !getMerchantId().equals(that.getMerchantId()) : that.getMerchantId() != null)
            return false;
        if (getFromEntityId() != null ? !getFromEntityId().equals(that.getFromEntityId()) : that.getFromEntityId() != null)
            return false;
        if (getToEntityId() != null ? !getToEntityId().equals(that.getToEntityId()) : that.getToEntityId() != null)
            return false;
        if (getClientTxnRef() != null ? !getClientTxnRef().equals(that.getClientTxnRef()) : that.getClientTxnRef() != null)
            return false;
        if (getPaymentMode() != null ? !getPaymentMode().equals(that.getPaymentMode()) : that.getPaymentMode() != null)
            return false;
        return getInvoiceDtlsList() != null ? getInvoiceDtlsList().equals(that.getInvoiceDtlsList()) : that.getInvoiceDtlsList() == null;
    }

    @Override
    public int hashCode() {
        int result = getMerchantId() != null ? getMerchantId().hashCode() : 0;
        result = 31 * result + (getFromEntityId() != null ? getFromEntityId().hashCode() : 0);
        result = 31 * result + (getToEntityId() != null ? getToEntityId().hashCode() : 0);
        result = 31 * result + (getClientTxnRef() != null ? getClientTxnRef().hashCode() : 0);
        result = 31 * result + (getPaymentMode() != null ? getPaymentMode().hashCode() : 0);
//        result = 31 * result + (getInvoiceDtlsList() != null ? getInvoiceDtlsList().hashCode() : 0);
        return result;
    }
}
