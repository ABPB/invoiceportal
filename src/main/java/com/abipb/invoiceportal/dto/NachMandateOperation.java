package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by abhay.kumar-v on 1/12/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "customerName",
        "custId",
        "installment",
        "loanAmount",
        "loanType",
        "outstanding",
        "dueAmount",
        "dueDate",
        "collectionDate",
        "collectionAmount",
        "umrn",
        "remarks",
        "selectFlag"
})
public class NachMandateOperation {

    @JsonProperty(value ="customerName",defaultValue = "",required = false)
    private String customerName;
    @JsonProperty(value ="custId",defaultValue = "",required = false)
    private String custId;
    @JsonProperty(value ="installment",defaultValue = "",required = false)
    private String installment;
    @JsonProperty(value ="loanAmount",defaultValue = "",required = false)
    private String loanAmount;
    @JsonProperty(value ="loanType",defaultValue = "",required = false)
    private String loanType;
    @JsonProperty(value ="outstanding",defaultValue = "",required = false)
    private String outstanding;
    @JsonProperty(value ="dueAmount",defaultValue = "",required = false)
    private String dueAmount;
    @JsonProperty(value ="dueDate",defaultValue = "",required = false)
    private String dueDate;
    @JsonProperty(value ="collectionDate",defaultValue = "",required = false)
    private String collectionDate;
    @JsonProperty(value ="collectionAmount",defaultValue = "",required = false)
    private String collectionAmount;
    @JsonProperty(value ="umrn",defaultValue = "",required = false)
    private String umrn;
    @JsonProperty(value ="remarks",defaultValue = "",required = false)
    private String remarks;
    @JsonProperty(value ="selectFlag",defaultValue = "",required = false)
    private String selectFlag;

    public NachMandateOperation() {
    }

    public NachMandateOperation(String customerName, String custId, String installment, String loanAmount, String loanType, String outstanding, String dueAmount, String dueDate, String collectionDate, String collectionAmount,String umrn, String remarks, String selectFlag) {
        this.customerName = customerName;
        this.custId = custId;
        this.installment = installment;
        this.loanAmount = loanAmount;
        this.loanType = loanType;
        this.outstanding = outstanding;
        this.dueAmount = dueAmount;
        this.dueDate = dueDate;
        this.collectionDate = collectionDate;
        this.collectionAmount = collectionAmount;
        this.umrn=umrn;
        this.remarks = remarks;
        this.selectFlag = selectFlag;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getOutstanding() {
        return outstanding;
    }

    public void setOutstanding(String outstanding) {
        this.outstanding = outstanding;
    }

    public String getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(String dueAmount) {
        this.dueAmount = dueAmount;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getCollectionDate() {
        return collectionDate;
    }

    public void setCollectionDate(String collectionDate) {
        this.collectionDate = collectionDate;
    }

    public String getCollectionAmount() {
        return collectionAmount;
    }

    public void setCollectionAmount(String collectionAmount) {
        this.collectionAmount = collectionAmount;
    }

    public String getUmrn() {
        return umrn;
    }

    public void setUmrn(String umrn) {
        this.umrn = umrn;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSelectFlag() {
        return selectFlag;
    }

    public void setSelectFlag(String selectFlag) {
        this.selectFlag = selectFlag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NachMandateOperation operation = (NachMandateOperation) o;

        if (customerName != null ? !customerName.equals(operation.customerName) : operation.customerName != null)
            return false;
        if (custId != null ? !custId.equals(operation.custId) : operation.custId != null) return false;
        if (installment != null ? !installment.equals(operation.installment) : operation.installment != null)
            return false;
        if (loanAmount != null ? !loanAmount.equals(operation.loanAmount) : operation.loanAmount != null) return false;
        if (loanType != null ? !loanType.equals(operation.loanType) : operation.loanType != null) return false;
        if (outstanding != null ? !outstanding.equals(operation.outstanding) : operation.outstanding != null)
            return false;
        if (dueAmount != null ? !dueAmount.equals(operation.dueAmount) : operation.dueAmount != null) return false;
        if (dueDate != null ? !dueDate.equals(operation.dueDate) : operation.dueDate != null) return false;
        if (collectionDate != null ? !collectionDate.equals(operation.collectionDate) : operation.collectionDate != null)
            return false;
        if (collectionAmount != null ? !collectionAmount.equals(operation.collectionAmount) : operation.collectionAmount != null)
            return false;
        if (umrn != null ? !umrn.equals(operation.umrn) : operation.umrn != null) return false;
        if (remarks != null ? !remarks.equals(operation.remarks) : operation.remarks != null) return false;
        return selectFlag != null ? selectFlag.equals(operation.selectFlag) : operation.selectFlag == null;
    }

    @Override
    public int hashCode() {
        int result = customerName != null ? customerName.hashCode() : 0;
        result = 31 * result + (custId != null ? custId.hashCode() : 0);
        result = 31 * result + (installment != null ? installment.hashCode() : 0);
        result = 31 * result + (loanAmount != null ? loanAmount.hashCode() : 0);
        result = 31 * result + (loanType != null ? loanType.hashCode() : 0);
        result = 31 * result + (outstanding != null ? outstanding.hashCode() : 0);
        result = 31 * result + (dueAmount != null ? dueAmount.hashCode() : 0);
        result = 31 * result + (dueDate != null ? dueDate.hashCode() : 0);
        result = 31 * result + (collectionDate != null ? collectionDate.hashCode() : 0);
        result = 31 * result + (collectionAmount != null ? collectionAmount.hashCode() : 0);
        result = 31 * result + (umrn != null ? umrn.hashCode() : 0);
        result = 31 * result + (remarks != null ? remarks.hashCode() : 0);
        result = 31 * result + (selectFlag != null ? selectFlag.hashCode() : 0);
        return result;
    }


}
