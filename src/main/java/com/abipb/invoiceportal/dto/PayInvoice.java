package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by abhay.kumar-v on 11/29/2017.
 */
public class PayInvoice extends Invoice {
    private static final long serializableUID=3L;
    private String toEstablishmentId;
    @JsonProperty(value ="payeeName",defaultValue ="")
    private String invoicePayeeName;

    public PayInvoice(){
        // no operation
    }
    public PayInvoice(String invoiceId, String invoiceAmt, String invoiceDueDate, String toEstablishmentId, String invoicePayeeName) {
        super(invoiceId, invoiceAmt, invoiceDueDate);
        this.toEstablishmentId = toEstablishmentId;
        this.invoicePayeeName = invoicePayeeName;
    }

    public String getToEstablishmentId() {
        return toEstablishmentId;
    }

    public void setToEstablishmentId(String toEstablishmentId) {
        this.toEstablishmentId = toEstablishmentId;
    }

    public String getInvoicePayeeName() {
        return invoicePayeeName;
    }

    public void setInvoicePayeeName(String invoicePayeeName) {
        this.invoicePayeeName = invoicePayeeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PayInvoice that = (PayInvoice) o;

        if (toEstablishmentId != null ? !toEstablishmentId.equals(that.toEstablishmentId) : that.toEstablishmentId != null)
            return false;
        return invoicePayeeName != null ? invoicePayeeName.equals(that.invoicePayeeName) : that.invoicePayeeName == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (toEstablishmentId != null ? toEstablishmentId.hashCode() : 0);
        result = 31 * result + (invoicePayeeName != null ? invoicePayeeName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PayInvoice{" +
                "toEstablishmentId='" + toEstablishmentId + '\'' +
                ", invoicePayeeName='" + invoicePayeeName + '\'' +
                '}';
    }
}
