package com.abipb.invoiceportal.dto;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */
public class UPIPWHeaderResponse {
    private String IS_AUTH="";

    private String IN_PROCESS_ID="";

    private String USER_SESSION_ID="";

    private String SERVER_TIMESTAMP="";

    private String PW_SESSION_ID="";

    private String OUT_PROCESS_ID="";

    public String getIS_AUTH() {
        return IS_AUTH;
    }

    public void setIS_AUTH(String IS_AUTH) {
        this.IS_AUTH = IS_AUTH;
    }

    public String getIN_PROCESS_ID() {
        return IN_PROCESS_ID;
    }

    public void setIN_PROCESS_ID(String IN_PROCESS_ID) {
        this.IN_PROCESS_ID = IN_PROCESS_ID;
    }

    public String getUSER_SESSION_ID() {
        return USER_SESSION_ID;
    }

    public void setUSER_SESSION_ID(String USER_SESSION_ID) {
        this.USER_SESSION_ID = USER_SESSION_ID;
    }

    public String getSERVER_TIMESTAMP() {
        return SERVER_TIMESTAMP;
    }

    public void setSERVER_TIMESTAMP(String SERVER_TIMESTAMP) {
        this.SERVER_TIMESTAMP = SERVER_TIMESTAMP;
    }

    public String getPW_SESSION_ID() {
        return PW_SESSION_ID;
    }

    public void setPW_SESSION_ID(String PW_SESSION_ID) {
        this.PW_SESSION_ID = PW_SESSION_ID;
    }

    public String getOUT_PROCESS_ID() {
        return OUT_PROCESS_ID;
    }

    public void setOUT_PROCESS_ID(String OUT_PROCESS_ID) {
        this.OUT_PROCESS_ID = OUT_PROCESS_ID;
    }

    @Override
    public String toString() {
        return "UPIPWHeaderResponse{" +
                "IS_AUTH='" + IS_AUTH + '\'' +
                ", IN_PROCESS_ID='" + IN_PROCESS_ID + '\'' +
                ", USER_SESSION_ID='" + USER_SESSION_ID + '\'' +
                ", SERVER_TIMESTAMP='" + SERVER_TIMESTAMP + '\'' +
                ", PW_SESSION_ID='" + PW_SESSION_ID + '\'' +
                ", OUT_PROCESS_ID='" + OUT_PROCESS_ID + '\'' +
                '}';
    }
}
