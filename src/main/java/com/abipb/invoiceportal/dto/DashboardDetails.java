package com.abipb.invoiceportal.dto;

/**
 * Created by penchalaiah.g-v on 12-01-2018.
 */
public class DashboardDetails {

    private String totalMandatesCreated;

    private String totalResponseReceived;

    private String totalResponsePending;

    private String responseReceivedSuccess;

    private String responseReceivedFailure;

    private String responsePendingLessthan10days;

    private String responsePendingmorethan10days;

    private String totalCollectionRequestSent;

    private String totalCollectionResponseReceived;

    private String CollectionResponseReceivedSuccess;

    private String CollectionResponseReceivedFailure;

    private String failedCollectionsAmount;

    private String successCollectionsAmount;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DashboardDetails that = (DashboardDetails) o;

        if (totalMandatesCreated != null ? !totalMandatesCreated.equals(that.totalMandatesCreated) : that.totalMandatesCreated != null)
            return false;
        if (totalResponseReceived != null ? !totalResponseReceived.equals(that.totalResponseReceived) : that.totalResponseReceived != null)
            return false;
        if (totalResponsePending != null ? !totalResponsePending.equals(that.totalResponsePending) : that.totalResponsePending != null)
            return false;
        if (responseReceivedSuccess != null ? !responseReceivedSuccess.equals(that.responseReceivedSuccess) : that.responseReceivedSuccess != null)
            return false;
        if (responseReceivedFailure != null ? !responseReceivedFailure.equals(that.responseReceivedFailure) : that.responseReceivedFailure != null)
            return false;
        if (responsePendingLessthan10days != null ? !responsePendingLessthan10days.equals(that.responsePendingLessthan10days) : that.responsePendingLessthan10days != null)
            return false;
        if (responsePendingmorethan10days != null ? !responsePendingmorethan10days.equals(that.responsePendingmorethan10days) : that.responsePendingmorethan10days != null)
            return false;
        if (totalCollectionRequestSent != null ? !totalCollectionRequestSent.equals(that.totalCollectionRequestSent) : that.totalCollectionRequestSent != null)
            return false;
        if (totalCollectionResponseReceived != null ? !totalCollectionResponseReceived.equals(that.totalCollectionResponseReceived) : that.totalCollectionResponseReceived != null)
            return false;
        if (CollectionResponseReceivedSuccess != null ? !CollectionResponseReceivedSuccess.equals(that.CollectionResponseReceivedSuccess) : that.CollectionResponseReceivedSuccess != null)
            return false;
        if (CollectionResponseReceivedFailure != null ? !CollectionResponseReceivedFailure.equals(that.CollectionResponseReceivedFailure) : that.CollectionResponseReceivedFailure != null)
            return false;
        if (failedCollectionsAmount != null ? !failedCollectionsAmount.equals(that.failedCollectionsAmount) : that.failedCollectionsAmount != null)
            return false;
        return successCollectionsAmount != null ? successCollectionsAmount.equals(that.successCollectionsAmount) : that.successCollectionsAmount == null;
    }

    @Override
    public int hashCode() {
        int result = totalMandatesCreated != null ? totalMandatesCreated.hashCode() : 0;
        result = 31 * result + (totalResponseReceived != null ? totalResponseReceived.hashCode() : 0);
        result = 31 * result + (totalResponsePending != null ? totalResponsePending.hashCode() : 0);
        result = 31 * result + (responseReceivedSuccess != null ? responseReceivedSuccess.hashCode() : 0);
        result = 31 * result + (responseReceivedFailure != null ? responseReceivedFailure.hashCode() : 0);
        result = 31 * result + (responsePendingLessthan10days != null ? responsePendingLessthan10days.hashCode() : 0);
        result = 31 * result + (responsePendingmorethan10days != null ? responsePendingmorethan10days.hashCode() : 0);
        result = 31 * result + (totalCollectionRequestSent != null ? totalCollectionRequestSent.hashCode() : 0);
        result = 31 * result + (totalCollectionResponseReceived != null ? totalCollectionResponseReceived.hashCode() : 0);
        result = 31 * result + (CollectionResponseReceivedSuccess != null ? CollectionResponseReceivedSuccess.hashCode() : 0);
        result = 31 * result + (CollectionResponseReceivedFailure != null ? CollectionResponseReceivedFailure.hashCode() : 0);
        result = 31 * result + (failedCollectionsAmount != null ? failedCollectionsAmount.hashCode() : 0);
        result = 31 * result + (successCollectionsAmount != null ? successCollectionsAmount.hashCode() : 0);
        return result;
    }



    public String getTotalMandatesCreated() {
        return totalMandatesCreated;
    }

    public void setTotalMandatesCreated(String totalMandatesCreated) {
        this.totalMandatesCreated = totalMandatesCreated;
    }

    public String getTotalResponseReceived() {
        return totalResponseReceived;
    }

    public void setTotalResponseReceived(String totalResponseReceived) {
        this.totalResponseReceived = totalResponseReceived;
    }

    public String getTotalResponsePending() {
        return totalResponsePending;
    }

    public void setTotalResponsePending(String totalResponsePending) {
        this.totalResponsePending = totalResponsePending;
    }

    public String getResponseReceivedSuccess() {
        return responseReceivedSuccess;
    }

    public void setResponseReceivedSuccess(String responseReceivedSuccess) {
        this.responseReceivedSuccess = responseReceivedSuccess;
    }

    public String getResponseReceivedFailure() {
        return responseReceivedFailure;
    }

    public void setResponseReceivedFailure(String responseReceivedFailure) {
        this.responseReceivedFailure = responseReceivedFailure;
    }

    public String getResponsePendingLessthan10days() {
        return responsePendingLessthan10days;
    }

    public void setResponsePendingLessthan10days(String responsePendingLessthan10days) {
        this.responsePendingLessthan10days = responsePendingLessthan10days;
    }

    public String getResponsePendingmorethan10days() {
        return responsePendingmorethan10days;
    }

    public void setResponsePendingmorethan10days(String responsePendingmorethan10days) {
        this.responsePendingmorethan10days = responsePendingmorethan10days;
    }

    public String getTotalCollectionRequestSent() {
        return totalCollectionRequestSent;
    }

    public void setTotalCollectionRequestSent(String totalCollectionRequestSent) {
        this.totalCollectionRequestSent = totalCollectionRequestSent;
    }

    public String getTotalCollectionResponseReceived() {
        return totalCollectionResponseReceived;
    }

    public void setTotalCollectionResponseReceived(String totalCollectionResponseReceived) {
        this.totalCollectionResponseReceived = totalCollectionResponseReceived;
    }

    public String getCollectionResponseReceivedSuccess() {
        return CollectionResponseReceivedSuccess;
    }

    public void setCollectionResponseReceivedSuccess(String collectionResponseReceivedSuccess) {
        CollectionResponseReceivedSuccess = collectionResponseReceivedSuccess;
    }

    public String getCollectionResponseReceivedFailure() {
        return CollectionResponseReceivedFailure;
    }

    public void setCollectionResponseReceivedFailure(String collectionResponseReceivedFailure) {
        CollectionResponseReceivedFailure = collectionResponseReceivedFailure;
    }

    public String getFailedCollectionsAmount() {
        return failedCollectionsAmount;
    }

    public void setFailedCollectionsAmount(String failedCollectionsAmount) {
        this.failedCollectionsAmount = failedCollectionsAmount;
    }

    public String getSuccessCollectionsAmount() {
        return successCollectionsAmount;
    }

    public void setSuccessCollectionsAmount(String successCollectionsAmount) {
        this.successCollectionsAmount = successCollectionsAmount;
    }

    @Override
    public String toString() {
        return "DashboardDetails{" +
                "totalMandatesCreated='" + totalMandatesCreated + '\'' +
                ", totalResponseReceived='" + totalResponseReceived + '\'' +
                ", totalResponsePending='" + totalResponsePending + '\'' +
                ", responseReceivedSuccess='" + responseReceivedSuccess + '\'' +
                ", responseReceivedFailure='" + responseReceivedFailure + '\'' +
                ", responsePendingLessthan10days='" + responsePendingLessthan10days + '\'' +
                ", responsePendingmorethan10days='" + responsePendingmorethan10days + '\'' +
                ", totalCollectionRequestSent='" + totalCollectionRequestSent + '\'' +
                ", totalCollectionResponseReceived='" + totalCollectionResponseReceived + '\'' +
                ", CollectionResponseReceivedSuccess='" + CollectionResponseReceivedSuccess + '\'' +
                ", CollectionResponseReceivedFailure='" + CollectionResponseReceivedFailure + '\'' +
                ", failedCollectionsAmount='" + failedCollectionsAmount + '\'' +
                ", successCollectionsAmount='" + successCollectionsAmount + '\'' +
                '}';
    }
}
