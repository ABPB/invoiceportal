package com.abipb.invoiceportal.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * Created by abhay.kumar-v on 10/25/2017.
 */
public class PaymentRefundResponse extends PaymentBillDesk {

    @NotNull(message = "PaymentRefundResponse:txnReferenceNo cannot be empty")
    @Length(min = 1, max = 50)
    private String txnReferenceNo="";

    @NotNull(message = "PaymentRefundResponse:txnDate cannot be empty")
    @Length(min = 1, max = 50)
    private  String txnDate="";

    @NotNull(message = "PaymentRefundResponse:txnAmount cannot be empty")
    @Length(min = 1, max = 50)
    private String txnAmount="";

    @NotNull(message = "PaymentRefundResponse:refAmount cannot be empty")
    @Length(min = 1, max = 50)
    private String refAmount="";

    @NotNull(message = "PaymentRefundResponse:refDateTime cannot be empty")
    @Length(min = 1, max = 50)
    private String refDateTime="";

    @NotNull(message = "PaymentRefundResponse:refStatus cannot be empty")
    @Length(min = 1, max = 50)
    private  String refStatus="";

    @NotNull(message = "PaymentRefundResponse:refundId cannot be empty")
    @Length(min = 1, max = 50)
    private String refundId="";

    @NotNull(message = "PaymentRefundResponse:errorCode cannot be empty")
    @Length(min = 1, max = 50)
    private  String errorCode="";

    @NotNull(message = "PaymentRefundResponse:errorReason cannot be empty")
    @Length(min = 1, max = 50)
    private  String errorReason="";

    @NotNull(message = "PaymentRefundResponse:processStatus cannot be empty")
    @Length(min = 1, max = 50)
    private  String  processStatus="";


    public String getTxnReferenceNo() {
        return txnReferenceNo;
    }

    public void setTxnReferenceNo(String txnReferenceNo) {
        this.txnReferenceNo = txnReferenceNo;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(String txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getRefAmount() {
        return refAmount;
    }

    public void setRefAmount(String refAmount) {
        this.refAmount = refAmount;
    }

    public String getRefDateTime() {
        return refDateTime;
    }

    public void setRefDateTime(String refDateTime) {
        this.refDateTime = refDateTime;
    }

    public String getRefStatus() {
        return refStatus;
    }

    public void setRefStatus(String refStatus) {
        this.refStatus = refStatus;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorReason() {
        return errorReason;
    }

    public void setErrorReason(String errorReason) {
        this.errorReason = errorReason;
    }

    public String getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PaymentRefundResponse that = (PaymentRefundResponse) o;

        if (txnReferenceNo != null ? !txnReferenceNo.equals(that.txnReferenceNo) : that.txnReferenceNo != null)
            return false;
        if (txnDate != null ? !txnDate.equals(that.txnDate) : that.txnDate != null) return false;
        if (txnAmount != null ? !txnAmount.equals(that.txnAmount) : that.txnAmount != null) return false;
        if (refAmount != null ? !refAmount.equals(that.refAmount) : that.refAmount != null) return false;
        if (refDateTime != null ? !refDateTime.equals(that.refDateTime) : that.refDateTime != null) return false;
        if (refStatus != null ? !refStatus.equals(that.refStatus) : that.refStatus != null) return false;
        if (refundId != null ? !refundId.equals(that.refundId) : that.refundId != null) return false;
        if (errorCode != null ? !errorCode.equals(that.errorCode) : that.errorCode != null) return false;
        if (errorReason != null ? !errorReason.equals(that.errorReason) : that.errorReason != null) return false;
        return processStatus != null ? processStatus.equals(that.processStatus) : that.processStatus == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (txnReferenceNo != null ? txnReferenceNo.hashCode() : 0);
        result = 31 * result + (txnDate != null ? txnDate.hashCode() : 0);
        result = 31 * result + (txnAmount != null ? txnAmount.hashCode() : 0);
        result = 31 * result + (refAmount != null ? refAmount.hashCode() : 0);
        result = 31 * result + (refDateTime != null ? refDateTime.hashCode() : 0);
        result = 31 * result + (refStatus != null ? refStatus.hashCode() : 0);
        result = 31 * result + (refundId != null ? refundId.hashCode() : 0);
        result = 31 * result + (errorCode != null ? errorCode.hashCode() : 0);
        result = 31 * result + (errorReason != null ? errorReason.hashCode() : 0);
        result = 31 * result + (processStatus != null ? processStatus.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        return "PaymentRefundResponse{" +
                "txnReferenceNo='" + txnReferenceNo + '\'' +
                ", txnDate='" + txnDate + '\'' +
                ", txnAmount='" + txnAmount + '\'' +
                ", refAmount='" + refAmount + '\'' +
                ", refDateTime='" + refDateTime + '\'' +
                ", refStatus='" + refStatus + '\'' +
                ", refundId='" + refundId + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", errorReason='" + errorReason + '\'' +
                ", processStatus='" + processStatus + '\'' +
                '}';
    }
}
