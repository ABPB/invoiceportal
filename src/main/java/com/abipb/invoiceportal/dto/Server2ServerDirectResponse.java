package com.abipb.invoiceportal.dto;

/**
 * Created by abhay.kumar-v on 10/27/2017.
 */
public class Server2ServerDirectResponse extends PaymentBillDesk {
    private  String txnReferenceNo="";
    private  String bankReferenceNo="";
    private  String txnAmount="";
    private  String bankId="";
    private  String bankMerchantId="";
    private  String txnType="";
    private  String currencyName="";
    private  String itemCode="";
    private  String securityType="";
    private  String securityId="";
    private  String securityPassword;
    private  String txnDate="";
    private  String authStatus="";
    private  String settlementType="";
    private  String additionalInfo1="";
    private  String additionalInfo2="";
    private  String additionalInfo3="";
    private  String additionalInfo4="";
    private  String additionalInfo5="";
    private  String additionalInfo6="";
    private  String additionalInfo7="";
    private  String errorStatus="";
    private  String errorDescription="";

    public String getTxnReferenceNo() {
        return txnReferenceNo;
    }

    public void setTxnReferenceNo(String txnReferenceNo) {
        this.txnReferenceNo = txnReferenceNo;
    }

    public String getBankReferenceNo() {
        return bankReferenceNo;
    }

    public void setBankReferenceNo(String bankReferenceNo) {
        this.bankReferenceNo = bankReferenceNo;
    }

    public String getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(String txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankMerchantId() {
        return bankMerchantId;
    }

    public void setBankMerchantId(String bankMerchantId) {
        this.bankMerchantId = bankMerchantId;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getSecurityType() {
        return securityType;
    }

    public void setSecurityType(String securityType) {
        this.securityType = securityType;
    }

    public String getSecurityId() {
        return securityId;
    }

    public void setSecurityId(String securityId) {
        this.securityId = securityId;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public String getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }

    public String getAdditionalInfo1() {
        return additionalInfo1;
    }

    public void setAdditionalInfo1(String additionalInfo1) {
        this.additionalInfo1 = additionalInfo1;
    }

    public String getAdditionalInfo2() {
        return additionalInfo2;
    }

    public void setAdditionalInfo2(String additionalInfo2) {
        this.additionalInfo2 = additionalInfo2;
    }

    public String getAdditionalInfo3() {
        return additionalInfo3;
    }

    public void setAdditionalInfo3(String additionalInfo3) {
        this.additionalInfo3 = additionalInfo3;
    }

    public String getAdditionalInfo4() {
        return additionalInfo4;
    }

    public void setAdditionalInfo4(String additionalInfo4) {
        this.additionalInfo4 = additionalInfo4;
    }

    public String getAdditionalInfo5() {
        return additionalInfo5;
    }

    public void setAdditionalInfo5(String additionalInfo5) {
        this.additionalInfo5 = additionalInfo5;
    }

    public String getAdditionalInfo6() {
        return additionalInfo6;
    }

    public void setAdditionalInfo6(String additionalInfo6) {
        this.additionalInfo6 = additionalInfo6;
    }

    public String getAdditionalInfo7() {
        return additionalInfo7;
    }

    public void setAdditionalInfo7(String additionalInfo7) {
        this.additionalInfo7 = additionalInfo7;
    }

    public String getErrorStatus() {
        return errorStatus;
    }

    public void setErrorStatus(String errorStatus) {
        this.errorStatus = errorStatus;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Server2ServerDirectResponse that = (Server2ServerDirectResponse) o;

        if (txnReferenceNo != null ? !txnReferenceNo.equals(that.txnReferenceNo) : that.txnReferenceNo != null)
            return false;
        if (bankReferenceNo != null ? !bankReferenceNo.equals(that.bankReferenceNo) : that.bankReferenceNo != null)
            return false;
        if (txnAmount != null ? !txnAmount.equals(that.txnAmount) : that.txnAmount != null) return false;
        if (bankId != null ? !bankId.equals(that.bankId) : that.bankId != null) return false;
        if (bankMerchantId != null ? !bankMerchantId.equals(that.bankMerchantId) : that.bankMerchantId != null)
            return false;
        if (txnType != null ? !txnType.equals(that.txnType) : that.txnType != null) return false;
        if (currencyName != null ? !currencyName.equals(that.currencyName) : that.currencyName != null) return false;
        if (itemCode != null ? !itemCode.equals(that.itemCode) : that.itemCode != null) return false;
        if (securityType != null ? !securityType.equals(that.securityType) : that.securityType != null) return false;
        if (securityId != null ? !securityId.equals(that.securityId) : that.securityId != null) return false;
        if (securityPassword != null ? !securityPassword.equals(that.securityPassword) : that.securityPassword != null)
            return false;
        if (txnDate != null ? !txnDate.equals(that.txnDate) : that.txnDate != null) return false;
        if (authStatus != null ? !authStatus.equals(that.authStatus) : that.authStatus != null) return false;
        if (settlementType != null ? !settlementType.equals(that.settlementType) : that.settlementType != null)
            return false;
        if (additionalInfo1 != null ? !additionalInfo1.equals(that.additionalInfo1) : that.additionalInfo1 != null)
            return false;
        if (additionalInfo2 != null ? !additionalInfo2.equals(that.additionalInfo2) : that.additionalInfo2 != null)
            return false;
        if (additionalInfo3 != null ? !additionalInfo3.equals(that.additionalInfo3) : that.additionalInfo3 != null)
            return false;
        if (additionalInfo4 != null ? !additionalInfo4.equals(that.additionalInfo4) : that.additionalInfo4 != null)
            return false;
        if (additionalInfo5 != null ? !additionalInfo5.equals(that.additionalInfo5) : that.additionalInfo5 != null)
            return false;
        if (additionalInfo6 != null ? !additionalInfo6.equals(that.additionalInfo6) : that.additionalInfo6 != null)
            return false;
        if (additionalInfo7 != null ? !additionalInfo7.equals(that.additionalInfo7) : that.additionalInfo7 != null)
            return false;
        if (errorStatus != null ? !errorStatus.equals(that.errorStatus) : that.errorStatus != null) return false;
        return errorDescription != null ? errorDescription.equals(that.errorDescription) : that.errorDescription == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (txnReferenceNo != null ? txnReferenceNo.hashCode() : 0);
        result = 31 * result + (bankReferenceNo != null ? bankReferenceNo.hashCode() : 0);
        result = 31 * result + (txnAmount != null ? txnAmount.hashCode() : 0);
        result = 31 * result + (bankId != null ? bankId.hashCode() : 0);
        result = 31 * result + (bankMerchantId != null ? bankMerchantId.hashCode() : 0);
        result = 31 * result + (txnType != null ? txnType.hashCode() : 0);
        result = 31 * result + (currencyName != null ? currencyName.hashCode() : 0);
        result = 31 * result + (itemCode != null ? itemCode.hashCode() : 0);
        result = 31 * result + (securityType != null ? securityType.hashCode() : 0);
        result = 31 * result + (securityId != null ? securityId.hashCode() : 0);
        result = 31 * result + (securityPassword != null ? securityPassword.hashCode() : 0);
        result = 31 * result + (txnDate != null ? txnDate.hashCode() : 0);
        result = 31 * result + (authStatus != null ? authStatus.hashCode() : 0);
        result = 31 * result + (settlementType != null ? settlementType.hashCode() : 0);
        result = 31 * result + (additionalInfo1 != null ? additionalInfo1.hashCode() : 0);
        result = 31 * result + (additionalInfo2 != null ? additionalInfo2.hashCode() : 0);
        result = 31 * result + (additionalInfo3 != null ? additionalInfo3.hashCode() : 0);
        result = 31 * result + (additionalInfo4 != null ? additionalInfo4.hashCode() : 0);
        result = 31 * result + (additionalInfo5 != null ? additionalInfo5.hashCode() : 0);
        result = 31 * result + (additionalInfo6 != null ? additionalInfo6.hashCode() : 0);
        result = 31 * result + (additionalInfo7 != null ? additionalInfo7.hashCode() : 0);
        result = 31 * result + (errorStatus != null ? errorStatus.hashCode() : 0);
        result = 31 * result + (errorDescription != null ? errorDescription.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        return "Server2ServerDirectResponse{" +
                "txnReferenceNo='" + txnReferenceNo + '\'' +
                ", bankReferenceNo='" + bankReferenceNo + '\'' +
                ", txnAmount='" + txnAmount + '\'' +
                ", bankId='" + bankId + '\'' +
                ", bankMerchantId='" + bankMerchantId + '\'' +
                ", txnType='" + txnType + '\'' +
                ", currencyName='" + currencyName + '\'' +
                ", itemCode='" + itemCode + '\'' +
                ", securityType='" + securityType + '\'' +
                ", securityId='" + securityId + '\'' +
                ", securityPassword='" + securityPassword + '\'' +
                ", txnDate='" + txnDate + '\'' +
                ", authStatus='" + authStatus + '\'' +
                ", settlementType='" + settlementType + '\'' +
                ", additionalInfo1='" + additionalInfo1 + '\'' +
                ", additionalInfo2='" + additionalInfo2 + '\'' +
                ", additionalInfo3='" + additionalInfo3 + '\'' +
                ", additionalInfo4='" + additionalInfo4 + '\'' +
                ", additionalInfo5='" + additionalInfo5 + '\'' +
                ", additionalInfo6='" + additionalInfo6 + '\'' +
                ", additionalInfo7='" + additionalInfo7 + '\'' +
                ", errorStatus='" + errorStatus + '\'' +
                ", errorDescription='" + errorDescription + '\'' +
                '}';
    }
}
