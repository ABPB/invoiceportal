package com.abipb.invoiceportal.dto;

import java.math.BigDecimal;

/**
 * Created by Venkatesh.R on 8/30/2017.
 */
public class ReportSummaryDTO {
    private String batchId;

//    @DateTimeFormat(pattern="dd/MM/YYYY")
    private String batchDate;

    private String transType;

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    private BigDecimal total;
    private BigDecimal initialCount;
    private BigDecimal readyToGoCount;
    private BigDecimal processedCount;
    private BigDecimal successCount;
    private BigDecimal failureCount;
    private BigDecimal expiredCount;
    private BigDecimal forgedCount;


    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getInitialCount() {
        return initialCount;
    }

    public void setInitialCount(BigDecimal initialCount) {
        this.initialCount = initialCount;
    }

    public BigDecimal getReadyToGoCount() {
        return readyToGoCount;
    }

    public void setReadyToGoCount(BigDecimal readyToGoCount) {
        this.readyToGoCount = readyToGoCount;
    }

    public BigDecimal getProcessedCount() {
        return processedCount;
    }

    public void setProcessedCount(BigDecimal processedCount) {
        this.processedCount = processedCount;
    }

    public BigDecimal getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(BigDecimal successCount) {
        this.successCount = successCount;
    }

    public BigDecimal getFailureCount() {
        return failureCount;
    }

    public void setFailureCount(BigDecimal failureCount) {
        this.failureCount = failureCount;
    }

    public BigDecimal getExpiredCount() {
        return expiredCount;
    }

    public void setExpiredCount(BigDecimal expiredCount) {
        this.expiredCount = expiredCount;
    }

    public BigDecimal getForgedCount() {
        return forgedCount;
    }

    public void setForgedCount(BigDecimal forgedCount) {
        this.forgedCount = forgedCount;
    }

    public String getBatchDate() {
        return batchDate;
    }

    public void setBatchDate(String batchDate) {
        this.batchDate = batchDate;
    }

}
