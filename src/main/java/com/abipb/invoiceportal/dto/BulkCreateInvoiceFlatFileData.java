package com.abipb.invoiceportal.dto;

/**
 * Created by penchalaiah.g-v on 27-12-2017.
 */
public class BulkCreateInvoiceFlatFileData {

    private String createInvoiceDetails;

    public String getCreateMandateDetails() {
        return createInvoiceDetails;
    }

    public void setCreateInvoiceDetails(String createInvoiceDetails) {
        this.createInvoiceDetails = createInvoiceDetails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BulkCreateInvoiceFlatFileData that = (BulkCreateInvoiceFlatFileData) o;

        return createInvoiceDetails != null ? createInvoiceDetails.equals(that.createInvoiceDetails) : that.createInvoiceDetails == null;
    }

    @Override
    public int hashCode() {
        return createInvoiceDetails != null ? createInvoiceDetails.hashCode() : 0;
    }


}
