package com.abipb.invoiceportal.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * Created by abhay.kumar-v on 10/25/2017.
 */
public class PaymentQueryResponse extends PaymentBillDesk {

    @NotNull(message = "PaymentQueryResponse:txnReferenceNo cannot be empty")
    @Length(min = 1, max = 50)
    private String txnReferenceNo="";

    @NotNull(message = "PaymentQueryResponse:txnAmount cannot be empty")
    @Length(min = 1, max = 50)
    private String txnAmount="";

    @NotNull(message = "PaymentQueryResponse:AuthStatus cannot be empty")
    @Length(min = 1, max = 50)
    private String AuthStatus="";

    @NotNull(message = "PaymentQueryResponse:filler1 cannot be empty")
    @Length(min = 1, max = 50)
    private String filler1="";

    @NotNull(message = "PaymentQueryResponse:refeundStatus cannot be empty")
    @Length(min = 1, max = 50)
    private String refeundStatus="";

    @NotNull(message = "PaymentQueryResponse:totalRefundAmount cannot be empty")
    @Length(min = 1, max = 50)
    private String totalRefundAmount="";

    @NotNull(message = "PaymentQueryResponse:lastRefundDate cannot be empty")
    @Length(min = 1, max = 50)
    private  String lastRefundDate="";

    @NotNull(message = "PaymentQueryResponse:lastRefundRefNo cannot be empty")
    @Length(min = 1, max = 50)
    private String lastRefundRefNo="";

    @NotNull(message = "PaymentQueryResponse:queryStatus cannot be empty")
    @Length(min = 1, max = 50)
    private String queryStatus="";

    @NotNull(message = "PaymentQueryResponse:bankReferenceNo cannot be empty")
    @Length(min = 1, max = 50)
    private String bankReferenceNo="";

    @NotNull(message = "PaymentQueryResponse:bankMarchantId cannot be empty")
    @Length(min = 1, max = 50)
    private String bankId="";

    @NotNull(message = "PaymentQueryResponse:txnReferenceNo cannot be empty")
    @Length(min = 1, max = 50)
    private String bankMarchantId="";

    @NotNull(message = "PaymentQueryResponse:txnType cannot be empty")
    @Length(min = 1, max = 50)
    private String txnType="";

    @NotNull(message = "PaymentQueryResponse:currencyName cannot be empty")
    @Length(min = 1, max = 50)
    private String currencyName="";

    @NotNull(message = "PaymentQueryResponse:itemCode cannot be empty")
    @Length(min = 1, max = 50)
    private String itemCode="";

    @NotNull(message = "PaymentQueryResponse:securityType cannot be empty")
    @Length(min = 1, max = 50)
    private  String securityType="";

    @NotNull(message = "PaymentQueryResponse:securityId cannot be empty")
    @Length(min = 1, max = 50)
    private String securityId="";

    @NotNull(message = "PaymentQueryResponse:securityPassword cannot be empty")
    @Length(min = 1, max = 50)
    private String securityPassword="";

    @NotNull(message = "PaymentQueryResponse:txnDate cannot be empty")
    @Length(min = 1, max = 50)
    private String txnDate="";

    @NotNull(message = "PaymentQueryResponse:settlementType cannot be empty")
    @Length(min = 1, max = 50)
    private String settlementType="";

    @NotNull(message = "PaymentQueryResponse:additionalInfo1 cannot be empty")
    @Length(min = 1, max = 50)
    private String additionalInfo1="";

    @NotNull(message = "PaymentQueryResponse:additionalInfo2 cannot be empty")
    @Length(min = 1, max = 50)
    private String additionalInfo2="";

    @NotNull(message = "PaymentQueryResponse:additionalInfo3 cannot be empty")
    @Length(min = 1, max = 50)
    private String additionalInfo3="";

    @NotNull(message = "PaymentQueryResponse:additionalInfo4 cannot be empty")
    @Length(min = 1, max = 50)
    private String additionalInfo4="";

    @NotNull(message = "PaymentQueryResponse:additionalInfo5 cannot be empty")
    @Length(min = 1, max = 50)
    private String additionalInfo5="";

    @NotNull(message = "PaymentQueryResponse:additionalInfo6 cannot be empty")
    @Length(min = 1, max = 50)
    private String additionalInfo6="";

    @NotNull(message = "PaymentQueryResponse:additionalInfo7 cannot be empty")
    @Length(min = 1, max = 50)
    private String additionalInfo7="";

    @NotNull(message = "PaymentQueryResponse:errorStatus cannot be empty")
    @Length(min = 1, max = 50)
    private String errorStatus="";

    @NotNull(message = "PaymentQueryResponse:errorDescription cannot be empty")
    @Length(min = 1, max = 50)
    private String errorDescription="";

    private String msg="";

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTxnReferenceNo() {
        return txnReferenceNo;
    }

    public void setTxnReferenceNo(String txnReferenceNo) {
        this.txnReferenceNo = txnReferenceNo;
    }

    public String getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(String txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getAuthStatus() {
        return AuthStatus;
    }

    public void setAuthStatus(String authStatus) {
        AuthStatus = authStatus;
    }

    public String getFiller1() {
        return filler1;
    }

    public void setFiller1(String filler1) {
        this.filler1 = filler1;
    }

    public String getRefeundStatus() {
        return refeundStatus;
    }

    public void setRefeundStatus(String refeundStatus) {
        this.refeundStatus = refeundStatus;
    }

    public String getTotalRefundAmount() {
        return totalRefundAmount;
    }

    public void setTotalRefundAmount(String totalRefundAmount) {
        this.totalRefundAmount = totalRefundAmount;
    }

    public String getLastRefundDate() {
        return lastRefundDate;
    }

    public void setLastRefundDate(String lastRefundDate) {
        this.lastRefundDate = lastRefundDate;
    }

    public String getLastRefundRefNo() {
        return lastRefundRefNo;
    }

    public void setLastRefundRefNo(String lastRefundRefNo) {
        this.lastRefundRefNo = lastRefundRefNo;
    }

    public String getQueryStatus() {
        return queryStatus;
    }

    public void setQueryStatus(String queryStatus) {
        this.queryStatus = queryStatus;
    }

    public String getBankReferenceNo() {
        return bankReferenceNo;
    }

    public void setBankReferenceNo(String bankReferenceNo) {
        this.bankReferenceNo = bankReferenceNo;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankMarchantId() {
        return bankMarchantId;
    }

    public void setBankMarchantId(String bankMarchantId) {
        this.bankMarchantId = bankMarchantId;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getSecurityType() {
        return securityType;
    }

    public void setSecurityType(String securityType) {
        this.securityType = securityType;
    }

    public String getSecurityId() {
        return securityId;
    }

    public void setSecurityId(String securityId) {
        this.securityId = securityId;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }

    public String getAdditionalInfo1() {
        return additionalInfo1;
    }

    public void setAdditionalInfo1(String additionalInfo1) {
        this.additionalInfo1 = additionalInfo1;
    }

    public String getAdditionalInfo2() {
        return additionalInfo2;
    }

    public void setAdditionalInfo2(String additionalInfo2) {
        this.additionalInfo2 = additionalInfo2;
    }

    public String getAdditionalInfo3() {
        return additionalInfo3;
    }

    public void setAdditionalInfo3(String additionalInfo3) {
        this.additionalInfo3 = additionalInfo3;
    }

    public String getAdditionalInfo4() {
        return additionalInfo4;
    }

    public void setAdditionalInfo4(String additionalInfo4) {
        this.additionalInfo4 = additionalInfo4;
    }

    public String getAdditionalInfo5() {
        return additionalInfo5;
    }

    public void setAdditionalInfo5(String additionalInfo5) {
        this.additionalInfo5 = additionalInfo5;
    }

    public String getAdditionalInfo6() {
        return additionalInfo6;
    }

    public void setAdditionalInfo6(String additionalInfo6) {
        this.additionalInfo6 = additionalInfo6;
    }

    public String getAdditionalInfo7() {
        return additionalInfo7;
    }

    public void setAdditionalInfo7(String additionalInfo7) {
        this.additionalInfo7 = additionalInfo7;
    }

    public String getErrorStatus() {
        return errorStatus;
    }

    public void setErrorStatus(String errorStatus) {
        this.errorStatus = errorStatus;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PaymentQueryResponse that = (PaymentQueryResponse) o;

        if (txnReferenceNo != null ? !txnReferenceNo.equals(that.txnReferenceNo) : that.txnReferenceNo != null)
            return false;
        if (txnAmount != null ? !txnAmount.equals(that.txnAmount) : that.txnAmount != null) return false;
        if (AuthStatus != null ? !AuthStatus.equals(that.AuthStatus) : that.AuthStatus != null) return false;
        if (filler1 != null ? !filler1.equals(that.filler1) : that.filler1 != null) return false;
        if (refeundStatus != null ? !refeundStatus.equals(that.refeundStatus) : that.refeundStatus != null)
            return false;
        if (totalRefundAmount != null ? !totalRefundAmount.equals(that.totalRefundAmount) : that.totalRefundAmount != null)
            return false;
        if (lastRefundDate != null ? !lastRefundDate.equals(that.lastRefundDate) : that.lastRefundDate != null)
            return false;
        if (lastRefundRefNo != null ? !lastRefundRefNo.equals(that.lastRefundRefNo) : that.lastRefundRefNo != null)
            return false;
        if (queryStatus != null ? !queryStatus.equals(that.queryStatus) : that.queryStatus != null) return false;
        if (bankReferenceNo != null ? !bankReferenceNo.equals(that.bankReferenceNo) : that.bankReferenceNo != null)
            return false;
        if (bankId != null ? !bankId.equals(that.bankId) : that.bankId != null) return false;
        if (bankMarchantId != null ? !bankMarchantId.equals(that.bankMarchantId) : that.bankMarchantId != null)
            return false;
        if (txnType != null ? !txnType.equals(that.txnType) : that.txnType != null) return false;
        if (currencyName != null ? !currencyName.equals(that.currencyName) : that.currencyName != null) return false;
        if (itemCode != null ? !itemCode.equals(that.itemCode) : that.itemCode != null) return false;
        if (securityType != null ? !securityType.equals(that.securityType) : that.securityType != null) return false;
        if (securityId != null ? !securityId.equals(that.securityId) : that.securityId != null) return false;
        if (securityPassword != null ? !securityPassword.equals(that.securityPassword) : that.securityPassword != null)
            return false;
        if (txnDate != null ? !txnDate.equals(that.txnDate) : that.txnDate != null) return false;
        if (settlementType != null ? !settlementType.equals(that.settlementType) : that.settlementType != null)
            return false;
        if (additionalInfo1 != null ? !additionalInfo1.equals(that.additionalInfo1) : that.additionalInfo1 != null)
            return false;
        if (additionalInfo2 != null ? !additionalInfo2.equals(that.additionalInfo2) : that.additionalInfo2 != null)
            return false;
        if (additionalInfo3 != null ? !additionalInfo3.equals(that.additionalInfo3) : that.additionalInfo3 != null)
            return false;
        if (additionalInfo4 != null ? !additionalInfo4.equals(that.additionalInfo4) : that.additionalInfo4 != null)
            return false;
        if (additionalInfo5 != null ? !additionalInfo5.equals(that.additionalInfo5) : that.additionalInfo5 != null)
            return false;
        if (additionalInfo6 != null ? !additionalInfo6.equals(that.additionalInfo6) : that.additionalInfo6 != null)
            return false;
        if (additionalInfo7 != null ? !additionalInfo7.equals(that.additionalInfo7) : that.additionalInfo7 != null)
            return false;
        if (errorStatus != null ? !errorStatus.equals(that.errorStatus) : that.errorStatus != null) return false;
        return errorDescription != null ? errorDescription.equals(that.errorDescription) : that.errorDescription == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (txnReferenceNo != null ? txnReferenceNo.hashCode() : 0);
        result = 31 * result + (txnAmount != null ? txnAmount.hashCode() : 0);
        result = 31 * result + (AuthStatus != null ? AuthStatus.hashCode() : 0);
        result = 31 * result + (filler1 != null ? filler1.hashCode() : 0);
        result = 31 * result + (refeundStatus != null ? refeundStatus.hashCode() : 0);
        result = 31 * result + (totalRefundAmount != null ? totalRefundAmount.hashCode() : 0);
        result = 31 * result + (lastRefundDate != null ? lastRefundDate.hashCode() : 0);
        result = 31 * result + (lastRefundRefNo != null ? lastRefundRefNo.hashCode() : 0);
        result = 31 * result + (queryStatus != null ? queryStatus.hashCode() : 0);
        result = 31 * result + (bankReferenceNo != null ? bankReferenceNo.hashCode() : 0);
        result = 31 * result + (bankId != null ? bankId.hashCode() : 0);
        result = 31 * result + (bankMarchantId != null ? bankMarchantId.hashCode() : 0);
        result = 31 * result + (txnType != null ? txnType.hashCode() : 0);
        result = 31 * result + (currencyName != null ? currencyName.hashCode() : 0);
        result = 31 * result + (itemCode != null ? itemCode.hashCode() : 0);
        result = 31 * result + (securityType != null ? securityType.hashCode() : 0);
        result = 31 * result + (securityId != null ? securityId.hashCode() : 0);
        result = 31 * result + (securityPassword != null ? securityPassword.hashCode() : 0);
        result = 31 * result + (txnDate != null ? txnDate.hashCode() : 0);
        result = 31 * result + (settlementType != null ? settlementType.hashCode() : 0);
        result = 31 * result + (additionalInfo1 != null ? additionalInfo1.hashCode() : 0);
        result = 31 * result + (additionalInfo2 != null ? additionalInfo2.hashCode() : 0);
        result = 31 * result + (additionalInfo3 != null ? additionalInfo3.hashCode() : 0);
        result = 31 * result + (additionalInfo4 != null ? additionalInfo4.hashCode() : 0);
        result = 31 * result + (additionalInfo5 != null ? additionalInfo5.hashCode() : 0);
        result = 31 * result + (additionalInfo6 != null ? additionalInfo6.hashCode() : 0);
        result = 31 * result + (additionalInfo7 != null ? additionalInfo7.hashCode() : 0);
        result = 31 * result + (errorStatus != null ? errorStatus.hashCode() : 0);
        result = 31 * result + (errorDescription != null ? errorDescription.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PaymentQueryResponse{" +
                "txnReferenceNo='" + txnReferenceNo + '\'' +
                ", txnAmount='" + txnAmount + '\'' +
                ", AuthStatus='" + AuthStatus + '\'' +
                ", filler1='" + filler1 + '\'' +
                ", refeundStatus='" + refeundStatus + '\'' +
                ", totalRefundAmount='" + totalRefundAmount + '\'' +
                ", lastRefundDate='" + lastRefundDate + '\'' +
                ", lastRefundRefNo='" + lastRefundRefNo + '\'' +
                ", queryStatus='" + queryStatus + '\'' +
                ", bankReferenceNo='" + bankReferenceNo + '\'' +
                ", bankId='" + bankId + '\'' +
                ", bankMarchantId='" + bankMarchantId + '\'' +
                ", txnType='" + txnType + '\'' +
                ", currencyName='" + currencyName + '\'' +
                ", itemCode='" + itemCode + '\'' +
                ", securityType='" + securityType + '\'' +
                ", securityId='" + securityId + '\'' +
                ", securityPassword='" + securityPassword + '\'' +
                ", txnDate='" + txnDate + '\'' +
                ", settlementType='" + settlementType + '\'' +
                ", additionalInfo1='" + additionalInfo1 + '\'' +
                ", additionalInfo2='" + additionalInfo2 + '\'' +
                ", additionalInfo3='" + additionalInfo3 + '\'' +
                ", additionalInfo4='" + additionalInfo4 + '\'' +
                ", additionalInfo5='" + additionalInfo5 + '\'' +
                ", additionalInfo6='" + additionalInfo6 + '\'' +
                ", additionalInfo7='" + additionalInfo7 + '\'' +
                ", errorStatus='" + errorStatus + '\'' +
                ", errorDescription='" + errorDescription + '\'' +
                '}';
    }
}
