package com.abipb.invoiceportal.dto;

import java.io.Serializable;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */
public class UPIPWHeader implements Serializable {

    private String ORG_ID="";

    private String IN_PROCESS_ID="";

    private String OUT_PROCESS_ID="";

    private String USER_ID="";

    private String PASSWORD;

    private String APP_ID="";

    private String LOGIN_ID="";

    private String PLATFORM="";

    private String SERVER_TIMESTAMP="";

    private String DEVICE_TIMESTAMP="";

    private String DEVICE_LATITUDE="";

    private String DEVICE_LONGITUDE="";

    private String LOCATION="";

    private String USER_SESSION_ID="";

    private String PW_SESSION_ID="";

    private String PW_VERSION="";

    private String PW_CLIENT_VERSION="";

    private String SESSION_EXPIRE_TIME="";

    private String INSTALLATION_ID="";

    private String DEVICE_MAKE="";

    private String IMEI_NO="";

    private String VERSION_ID="";

    private String DEVICE_MODEL="";

    private int OS_VERSION;

    private String SIM_ID="";

    public void setORG_ID(String ORG_ID){
        this.ORG_ID = ORG_ID;
    }
    public String getORG_ID(){
        return this.ORG_ID;
    }
    public void setIN_PROCESS_ID(String IN_PROCESS_ID){
        this.IN_PROCESS_ID = IN_PROCESS_ID;
    }
    public String getIN_PROCESS_ID(){
        return this.IN_PROCESS_ID;
    }
    public void setOUT_PROCESS_ID(String OUT_PROCESS_ID){
        this.OUT_PROCESS_ID = OUT_PROCESS_ID;
    }
    public String getOUT_PROCESS_ID(){
        return this.OUT_PROCESS_ID;
    }
    public void setUSER_ID(String USER_ID){
        this.USER_ID = USER_ID;
    }
    public String getUSER_ID(){
        return this.USER_ID;
    }
    public void setPASSWORD(String PASSWORD){
        this.PASSWORD = PASSWORD;
    }
    public String getPASSWORD(){
        return this.PASSWORD;
    }
    public void setAPP_ID(String APP_ID){
        this.APP_ID = APP_ID;
    }
    public String getAPP_ID(){
        return this.APP_ID;
    }
    public void setLOGIN_ID(String LOGIN_ID){
        this.LOGIN_ID = LOGIN_ID;
    }
    public String getLOGIN_ID(){
        return this.LOGIN_ID;
    }
    public void setPLATFORM(String PLATFORM){
        this.PLATFORM = PLATFORM;
    }
    public String getPLATFORM(){
        return this.PLATFORM;
    }
    public void setSERVER_TIMESTAMP(String SERVER_TIMESTAMP){
        this.SERVER_TIMESTAMP = SERVER_TIMESTAMP;
    }
    public String getSERVER_TIMESTAMP(){
        return this.SERVER_TIMESTAMP;
    }
    public void setDEVICE_TIMESTAMP(String DEVICE_TIMESTAMP){
        this.DEVICE_TIMESTAMP = DEVICE_TIMESTAMP;
    }
    public String getDEVICE_TIMESTAMP(){
        return this.DEVICE_TIMESTAMP;
    }
    public void setDEVICE_LATITUDE(String DEVICE_LATITUDE){
        this.DEVICE_LATITUDE = DEVICE_LATITUDE;
    }
    public String getDEVICE_LATITUDE(){
        return this.DEVICE_LATITUDE;
    }
    public void setDEVICE_LONGITUDE(String DEVICE_LONGITUDE){
        this.DEVICE_LONGITUDE = DEVICE_LONGITUDE;
    }
    public String getDEVICE_LONGITUDE(){
        return this.DEVICE_LONGITUDE;
    }
    public void setLOCATION(String LOCATION){
        this.LOCATION = LOCATION;
    }
    public String getLOCATION(){
        return this.LOCATION;
    }
    public void setUSER_SESSION_ID(String USER_SESSION_ID){
        this.USER_SESSION_ID = USER_SESSION_ID;
    }
    public String getUSER_SESSION_ID(){
        return this.USER_SESSION_ID;
    }
    public void setPW_SESSION_ID(String PW_SESSION_ID){
        this.PW_SESSION_ID = PW_SESSION_ID;
    }
    public String getPW_SESSION_ID(){
        return this.PW_SESSION_ID;
    }
    public void setPW_VERSION(String PW_VERSION){
        this.PW_VERSION = PW_VERSION;
    }
    public String getPW_VERSION(){
        return this.PW_VERSION;
    }
    public void setPW_CLIENT_VERSION(String PW_CLIENT_VERSION){
        this.PW_CLIENT_VERSION = PW_CLIENT_VERSION;
    }
    public String getPW_CLIENT_VERSION(){
        return this.PW_CLIENT_VERSION;
    }
    public void setSESSION_EXPIRE_TIME(String SESSION_EXPIRE_TIME){
        this.SESSION_EXPIRE_TIME = SESSION_EXPIRE_TIME;
    }
    public String getSESSION_EXPIRE_TIME(){
        return this.SESSION_EXPIRE_TIME;
    }
    public void setINSTALLATION_ID(String INSTALLATION_ID){
        this.INSTALLATION_ID = INSTALLATION_ID;
    }
    public String getINSTALLATION_ID(){
        return this.INSTALLATION_ID;
    }
    public void setDEVICE_MAKE(String DEVICE_MAKE){
        this.DEVICE_MAKE = DEVICE_MAKE;
    }
    public String getDEVICE_MAKE(){
        return this.DEVICE_MAKE;
    }
    public void setIMEI_NO(String IMEI_NO){
        this.IMEI_NO = IMEI_NO;
    }
    public String getIMEI_NO(){
        return this.IMEI_NO;
    }
    public void setVERSION_ID(String VERSION_ID){
        this.VERSION_ID = VERSION_ID;
    }
    public String getVERSION_ID(){
        return this.VERSION_ID;
    }
    public void setDEVICE_MODEL(String DEVICE_MODEL){
        this.DEVICE_MODEL = DEVICE_MODEL;
    }
    public String getDEVICE_MODEL(){
        return this.DEVICE_MODEL;
    }
    public void setOS_VERSION(int OS_VERSION){
        this.OS_VERSION = OS_VERSION;
    }
    public int getOS_VERSION(){
        return this.OS_VERSION;
    }
    public void setSIM_ID(String SIM_ID){
        this.SIM_ID = SIM_ID;
    }
    public String getSIM_ID(){
        return this.SIM_ID;
    }

    @Override
    public String toString() {
        return "UPIPWHeader{" +
                "ORG_ID='" + ORG_ID + '\'' +
                ", IN_PROCESS_ID='" + IN_PROCESS_ID + '\'' +
                ", OUT_PROCESS_ID='" + OUT_PROCESS_ID + '\'' +
                ", USER_ID='" + USER_ID + '\'' +
                ", PASSWORD='" + PASSWORD + '\'' +
                ", APP_ID='" + APP_ID + '\'' +
                ", LOGIN_ID='" + LOGIN_ID + '\'' +
                ", PLATFORM='" + PLATFORM + '\'' +
                ", SERVER_TIMESTAMP='" + SERVER_TIMESTAMP + '\'' +
                ", DEVICE_TIMESTAMP=" + DEVICE_TIMESTAMP +
                ", DEVICE_LATITUDE='" + DEVICE_LATITUDE + '\'' +
                ", DEVICE_LONGITUDE='" + DEVICE_LONGITUDE + '\'' +
                ", LOCATION='" + LOCATION + '\'' +
                ", USER_SESSION_ID='" + USER_SESSION_ID + '\'' +
                ", PW_SESSION_ID='" + PW_SESSION_ID + '\'' +
                ", PW_VERSION='" + PW_VERSION + '\'' +
                ", PW_CLIENT_VERSION='" + PW_CLIENT_VERSION + '\'' +
                ", SESSION_EXPIRE_TIME='" + SESSION_EXPIRE_TIME + '\'' +
                ", INSTALLATION_ID='" + INSTALLATION_ID + '\'' +
                ", DEVICE_MAKE='" + DEVICE_MAKE + '\'' +
                ", IMEI_NO='" + IMEI_NO + '\'' +
                ", VERSION_ID='" + VERSION_ID + '\'' +
                ", DEVICE_MODEL='" + DEVICE_MODEL + '\'' +
                ", OS_VERSION=" + OS_VERSION +
                ", SIM_ID='" + SIM_ID + '\'' +
                '}';
    }
}
