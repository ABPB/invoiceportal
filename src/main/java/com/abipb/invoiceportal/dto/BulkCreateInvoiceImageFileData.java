package com.abipb.invoiceportal.dto;

import java.util.List;

/**
 * Created by penchalaiah.g-v on 27-12-2017.
 */
public class BulkCreateInvoiceImageFileData {


    public List<JpgFileData> getJpgFileDataList() {
        return jpgFileDataList;
    }

    public void setJpgFileDataList(List<JpgFileData> jpgFileDataList) {
        this.jpgFileDataList = jpgFileDataList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BulkCreateInvoiceImageFileData that = (BulkCreateInvoiceImageFileData) o;

        return jpgFileDataList != null ? jpgFileDataList.equals(that.jpgFileDataList) : that.jpgFileDataList == null;
    }

    @Override
    public int hashCode() {
        return jpgFileDataList != null ? jpgFileDataList.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "BulkCreateInvoiceImageFileData{" +
                "jpgFileDataList=" + jpgFileDataList +
                '}';
    }

    List<JpgFileData> jpgFileDataList;

}
