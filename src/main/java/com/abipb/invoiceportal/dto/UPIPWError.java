package com.abipb.invoiceportal.dto;

import java.io.Serializable;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */
public class UPIPWError implements Serializable {
    private String PWERROR="";

    public String getPWERROR() {
        return PWERROR;
    }

    public void setPWERROR(String PWERROR) {
        this.PWERROR = PWERROR;
    }

    @Override
    public String toString() {
        return "UPIPWError{" +
                "PWERROR='" + PWERROR + '\'' +
                '}';
    }
}
