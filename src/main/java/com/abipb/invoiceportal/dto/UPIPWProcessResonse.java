package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */
public class UPIPWProcessResonse {

    @JsonProperty("PWERROR")
    private UPIPWError upipwError =new UPIPWError();

    @JsonProperty("PWHEADER")
    private UPIPWHeaderResponse upipwHeaderResponse =new UPIPWHeaderResponse();

    @JsonProperty("PWDATA")
    private UPIPWData upipwData =new UPIPWData();


    public UPIPWError getUpipwError() {
        return upipwError;
    }

    public void setUpipwError(UPIPWError upipwError) {
        this.upipwError = upipwError;
    }

    public UPIPWHeaderResponse getUpipwHeaderResponse() {
        return upipwHeaderResponse;
    }

    public void setUpipwHeaderResponse(UPIPWHeaderResponse upipwHeaderResponse) {
        this.upipwHeaderResponse = upipwHeaderResponse;
    }

    public UPIPWData getUpipwData() {
        return upipwData;
    }

    public void setUpipwData(UPIPWData upipwData) {
        this.upipwData = upipwData;
    }

    @Override
    public String toString() {
        return "UPIPWProcessResonse{" +
                "upipwError=" + upipwError +
                ", upipwHeaderResponse=" + upipwHeaderResponse +
                ", upipwData=" + upipwData +
                '}';
    }
}
