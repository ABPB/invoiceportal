package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by abhay.kumar-v on 10/24/2017.
 */
public class PaymentRootResponse implements Serializable {
    @JsonProperty("id")
    private String id="";
    @JsonProperty("createTime")
    private String createTime="";
    @JsonProperty("paymentStatus")
    private String paymentStatus="";
    @JsonProperty("paymentMode")
    private String paymentMode="";
    @JsonProperty("links")
    private  List<PaymentLink> paymentLinks = new ArrayList<>();
    @JsonIgnore
    private transient Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("createTime")
    public String getCreateTime() {
        return createTime;
    }

    @JsonProperty("createTime")
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @JsonProperty("paymentStatus")
    public String getPaymentStatus() {
        return paymentStatus;
    }

    @JsonProperty("paymentStatus")
    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    @JsonProperty("paymentMode")
    public String getPaymentMode() {
        return paymentMode;
    }

    @JsonProperty("paymentMode")
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    @JsonProperty("links")
    public List<PaymentLink> getPaymentLinks() {
        return paymentLinks;
    }

    @JsonProperty("links")
    public void setPaymentLinks(List<PaymentLink> paymentLinks) {
        this.paymentLinks = paymentLinks;
    }




    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


}
