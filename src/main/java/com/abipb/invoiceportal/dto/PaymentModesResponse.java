package com.abipb.invoiceportal.dto;

/**
 * Created by penchalaiah.g-v on 01-11-2017.
 */
public class PaymentModesResponse {
  private   Long entityId;

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEstablishmentId() {
        return establishmentId;
    }

    public void setEstablishmentId(String establishmentId) {
        this.establishmentId = establishmentId;
    }

    public PaymentModes getPaymentModes() {
        return paymentModes;
    }

    public void setPaymentModes(PaymentModes paymentModes) {
        this.paymentModes = paymentModes;
    }

    public String getCreditCardStatus() {
        return creditCardStatus;
    }

    public void setCreditCardStatus(String creditCardStatus) {
        this.creditCardStatus = creditCardStatus;
    }

    public String getDebitCardStatus() {
        return debitCardStatus;
    }

    public void setDebitCardStatus(String debitCardStatus) {
        this.debitCardStatus = debitCardStatus;
    }

    private   String establishmentId;

  private  PaymentModes paymentModes;

  private String creditCardStatus;

  private String debitCardStatus;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private  String status;

}
