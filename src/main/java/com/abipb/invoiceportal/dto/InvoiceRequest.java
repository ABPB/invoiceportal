package com.abipb.invoiceportal.dto;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * Created by abhay.kumar-v on 11/27/2017.
 */

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "merchantId",
        "establishmentId"
})
public class InvoiceRequest implements Serializable{
    @JsonProperty(value = "merchantId",required = false,defaultValue = "")
    private String merchantId;
    @JsonProperty(value = "establishmentId",required = false,defaultValue = "")
    private String establishmentId;

    public InvoiceRequest() {
        //no operation
    }

    public InvoiceRequest(String merchantId, String establishmentId) {
        this.merchantId = merchantId;
        this.establishmentId = establishmentId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getEstablishmentId() {
        return establishmentId;
    }

    public void setEstablishmentId(String establishmentId) {
        this.establishmentId = establishmentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvoiceRequest that = (InvoiceRequest) o;

        if (merchantId != null ? !merchantId.equals(that.merchantId) : that.merchantId != null) return false;
        return establishmentId != null ? establishmentId.equals(that.establishmentId) : that.establishmentId == null;
    }

    @Override
    public int hashCode() {
        int result = merchantId != null ? merchantId.hashCode() : 0;
        result = 31 * result + (establishmentId != null ? establishmentId.hashCode() : 0);
        return result;
    }

    @Override
    public String
    toString() {
        return "InvoiceRequest{" +
                "merchantId='" + merchantId + '\'' +
                ", establishmentId='" + establishmentId + '\'' +
                '}';
    }
}
