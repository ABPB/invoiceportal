package com.abipb.invoiceportal.dto;

import java.util.List;

/**
 * Created by Venkatesh.R on 8/30/2017.
 */
public class ReportSessionDTO {

    private List<ReportDTO> reportDetails;
    private ReportSummaryDTO reportSummary ;

    public List<ReportDTO> getReportDetails() {
        return reportDetails;
    }

    public void setReportDetails(List<ReportDTO> reportDetails) {
        this.reportDetails = reportDetails;
    }

    public ReportSummaryDTO getReportSummary() {
        return reportSummary;
    }

    public void setReportSummary(ReportSummaryDTO reportSummary) {
        this.reportSummary = reportSummary;
    }

}
