package com.abipb.invoiceportal.dto;

/**
 * Created by Venkatesh.R on 9/23/2017.
 */

import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class CrystalUser extends User{

    private String merchantId;

    public CrystalUser(String username, String password, boolean enabled,
                    boolean accountNonExpired, boolean credentialsNonExpired,
                    boolean accountNonLocked,
                    Collection authorities,
                    String merchantId) {

        super(username, password, enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, authorities);

        this.merchantId = merchantId;

    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

}
