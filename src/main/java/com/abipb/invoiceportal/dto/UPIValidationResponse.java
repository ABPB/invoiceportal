package com.abipb.invoiceportal.dto;

/**
 * Created by Venkatesh.R on 10/20/2017.
 */
public class UPIValidationResponse {

    String upiHandle;
    String validationStatus;

    public String getUpiHandle() {
        return upiHandle;
    }

    public void setUpiHandle(String upiHandle) {
        this.upiHandle = upiHandle;
    }

    public String getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(String validationStatus) {
        this.validationStatus = validationStatus;
    }
}
