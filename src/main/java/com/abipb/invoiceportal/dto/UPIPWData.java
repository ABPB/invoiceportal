package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */
public class UPIPWData implements Serializable {
    @JsonProperty("VALADDR")
    private UPIValidationAddress upiValidationAddress =new UPIValidationAddress();

    public UPIValidationAddress getUpiValidationAddress() {
        return upiValidationAddress;
    }

    public void setUpiValidationAddress(UPIValidationAddress upiValidationAddress) {
        this.upiValidationAddress = upiValidationAddress;
    }

    @Override
    public String toString() {
        return "UPIPWData{" +
                "upiValidationAddress=" + upiValidationAddress +
                '}';
    }
}
