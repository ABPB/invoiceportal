package com.abipb.invoiceportal.dto;

import java.util.Date;

/**
 * Created by Venkatesh.R on 8/30/2017.
 */
public class BatchDTO {
    private String batchId;
    private Date batchDate;

    public Date getBatchDate() {
        return batchDate;
    }

    private String channel;
  /*  private String parsedBatchDate;

    public String getParsedBatchDate() {
        return parsedBatchDate;
    }

    public void setParsedBatchDate(String parsedBatchDate) {
        this.parsedBatchDate = parsedBatchDate;
    }*/

    public void setBatchDate(Date batchDate) {
        this.batchDate = batchDate;
    }
    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

}
