package com.abipb.invoiceportal.dto;

import java.math.BigDecimal;

/**
 * Created by Venkatesh.R on 10/14/2017.
 */
public class PaymentCreateClientResponse {

    String mwTxnRef;
    String clientPaymentChannel;
    String redirectURL;
    BigDecimal paymentAmount;


    private String status;

    public String getClientPaymentChannel() {
        return clientPaymentChannel;
    }

    public void setClientPaymentChannel(String clientPaymentChannel) {
        this.clientPaymentChannel = clientPaymentChannel;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }

    public String getMwTxnRef() {
        return mwTxnRef;
    }

    public void setMwTxnRef(String mwTxnRef) {
        this.mwTxnRef = mwTxnRef;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaymentCreateClientResponse that = (PaymentCreateClientResponse) o;

        if (mwTxnRef != null ? !mwTxnRef.equals(that.mwTxnRef) : that.mwTxnRef != null) return false;
        if (clientPaymentChannel != null ? !clientPaymentChannel.equals(that.clientPaymentChannel) : that.clientPaymentChannel != null)
            return false;
        if (redirectURL != null ? !redirectURL.equals(that.redirectURL) : that.redirectURL != null) return false;
        if (paymentAmount != null ? !paymentAmount.equals(that.paymentAmount) : that.paymentAmount != null)
            return false;
        return status != null ? status.equals(that.status) : that.status == null;
    }

    @Override
    public int hashCode() {
        int result = mwTxnRef != null ? mwTxnRef.hashCode() : 0;
        result = 31 * result + (clientPaymentChannel != null ? clientPaymentChannel.hashCode() : 0);
        result = 31 * result + (redirectURL != null ? redirectURL.hashCode() : 0);
        result = 31 * result + (paymentAmount != null ? paymentAmount.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PaymentCreateClientResponse{" +
                "mwTxnRef='" + mwTxnRef + '\'' +
                ", clientPaymentChannel='" + clientPaymentChannel + '\'' +
                ", redirectURL='" + redirectURL + '\'' +
                ", paymentAmount=" + paymentAmount +
                ", status='" + status + '\'' +
                '}';
    }
}
