package com.abipb.invoiceportal.dto;

import java.math.BigDecimal;

/**
 * Created by Venkatesh.R on 10/14/2017.
 */
public class PaymentCreateResponse {

    String mwTxnRef;
    String paymentChannel;
    String fromBankAcctNumber;
    String fromBankId;
    String toBankAcctNumber;
    String toBankId;

    BigDecimal paymentAmount;
    String paymentStatus;
    String clientTxnRef;
    String customerMobileNo;


    String customerEmailId;

    private  String fromUPIHandler;
    private String toUPIHandler;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaymentCreateResponse that = (PaymentCreateResponse) o;

        if (mwTxnRef != null ? !mwTxnRef.equals(that.mwTxnRef) : that.mwTxnRef != null) return false;
        if (paymentChannel != null ? !paymentChannel.equals(that.paymentChannel) : that.paymentChannel != null)
            return false;
        if (fromBankAcctNumber != null ? !fromBankAcctNumber.equals(that.fromBankAcctNumber) : that.fromBankAcctNumber != null)
            return false;
        if (fromBankId != null ? !fromBankId.equals(that.fromBankId) : that.fromBankId != null) return false;
        if (toBankAcctNumber != null ? !toBankAcctNumber.equals(that.toBankAcctNumber) : that.toBankAcctNumber != null)
            return false;
        if (toBankId != null ? !toBankId.equals(that.toBankId) : that.toBankId != null) return false;
        if (paymentAmount != null ? !paymentAmount.equals(that.paymentAmount) : that.paymentAmount != null)
            return false;
        if (paymentStatus != null ? !paymentStatus.equals(that.paymentStatus) : that.paymentStatus != null)
            return false;
        if (clientTxnRef != null ? !clientTxnRef.equals(that.clientTxnRef) : that.clientTxnRef != null) return false;
        if (customerMobileNo != null ? !customerMobileNo.equals(that.customerMobileNo) : that.customerMobileNo != null)
            return false;
        return customerEmailId != null ? customerEmailId.equals(that.customerEmailId) : that.customerEmailId == null;
    }

    @Override
    public int hashCode() {
        int result = mwTxnRef != null ? mwTxnRef.hashCode() : 0;
        result = 31 * result + (paymentChannel != null ? paymentChannel.hashCode() : 0);
        result = 31 * result + (fromBankAcctNumber != null ? fromBankAcctNumber.hashCode() : 0);
        result = 31 * result + (fromBankId != null ? fromBankId.hashCode() : 0);
        result = 31 * result + (toBankAcctNumber != null ? toBankAcctNumber.hashCode() : 0);
        result = 31 * result + (toBankId != null ? toBankId.hashCode() : 0);
        result = 31 * result + (paymentAmount != null ? paymentAmount.hashCode() : 0);
        result = 31 * result + (paymentStatus != null ? paymentStatus.hashCode() : 0);
        result = 31 * result + (clientTxnRef != null ? clientTxnRef.hashCode() : 0);
        result = 31 * result + (customerMobileNo != null ? customerMobileNo.hashCode() : 0);
        result = 31 * result + (customerEmailId != null ? customerEmailId.hashCode() : 0);
        return result;
    }





    @Override
    public String toString() {
        return "PaymentCreateResponse{" +
                "mwTxnRef='" + mwTxnRef + '\'' +
                ", paymentChannel='" + paymentChannel + '\'' +
                ", fromBankAcctNumber='" + fromBankAcctNumber + '\'' +
                ", fromBankId='" + fromBankId + '\'' +
                ", toBankAcctNumber='" + toBankAcctNumber + '\'' +
                ", toBankId='" + toBankId + '\'' +
                ", paymentAmount=" + paymentAmount +
                ", paymentStatus='" + paymentStatus + '\'' +
                ", customerMobileNo='" + customerMobileNo + '\'' +
                ", customerEmailId='" + customerEmailId + '\'' +
                '}';
    }

    public String getClientTxnRef() {
        return clientTxnRef;
    }

    public void setClientTxnRef(String clientTxnRef) {
        this.clientTxnRef = clientTxnRef;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getMwTxnRef() {
        return mwTxnRef;
    }

    public void setMwTxnRef(String mwTxnRef) {
        this.mwTxnRef = mwTxnRef;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public String getFromBankAcctNumber() {
        return fromBankAcctNumber;
    }

    public void setFromBankAcctNumber(String fromBankAcctNumber) {
        this.fromBankAcctNumber = fromBankAcctNumber;
    }

    public String getFromBankId() {
        return fromBankId;
    }

    public void setFromBankId(String fromBankId) {
        this.fromBankId = fromBankId;
    }

    public String getToBankAcctNumber() {
        return toBankAcctNumber;
    }

    public void setToBankAcctNumber(String toBankAcctNumber) {
        this.toBankAcctNumber = toBankAcctNumber;
    }

    public String getToBankId() {
        return toBankId;
    }

    public void setToBankId(String toBankId) {
        this.toBankId = toBankId;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getCustomerMobileNo() {
        return customerMobileNo;
    }

    public void setCustomerMobileNo(String customerMobileNo) {
        this.customerMobileNo = customerMobileNo;
    }

    public String getCustomerEmailId() {
        return customerEmailId;
    }

    public void setCustomerEmailId(String customerEmailId) {
        this.customerEmailId = customerEmailId;
    }

    public String getFromUPIHandler() {
        return fromUPIHandler;
    }

    public void setFromUPIHandler(String fromUPIHandler) {
        this.fromUPIHandler = fromUPIHandler;
    }

    public String getToUPIHandler() {
        return toUPIHandler;
    }

    public void setToUPIHandler(String toUPIHandler) {
        this.toUPIHandler = toUPIHandler;
    }
}
