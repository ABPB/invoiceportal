package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */
public class UPIPWSessionResponse implements Serializable {

    @JsonProperty("PWPROCESSRS")
    private transient UPIPWProcessResonse upipwProcessResonse =new UPIPWProcessResonse();

    public UPIPWProcessResonse getUpipwProcessResonse() {
        return upipwProcessResonse;
    }

    public void setUpipwProcessResonse(UPIPWProcessResonse upipwProcessResonse) {
        this.upipwProcessResonse = upipwProcessResonse;
    }

    @Override
    public String toString() {
        return "UPIPWSessionResponse{" +
                "upipwProcessResonse=" + upipwProcessResonse +
                '}';
    }
}
