package com.abipb.invoiceportal.dto;

/**
 * Created by Venkatesh.R on 10/31/2017.
 */
public class PGPaymentRequest {

    String msg;
    String paymentChannel;

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
