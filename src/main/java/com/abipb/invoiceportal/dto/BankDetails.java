package com.abipb.invoiceportal.dto;

/**
 * Created by penchalaiah.g-v on 01-11-2017.
 */
public class BankDetails {

    private String accountNo;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getPreferredFlag() {
        return preferredFlag;
    }

    public void setPreferredFlag(String preferredFlag) {
        this.preferredFlag = preferredFlag;
    }

    private  String bankName;

    private String preferredFlag;
}
