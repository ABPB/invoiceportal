package com.abipb.invoiceportal.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */
public class UPIRootResponse  implements Serializable{

    @JsonProperty("PWSESSIONRS")
    private UPIPWSessionResponse upipwSessionResponse =new UPIPWSessionResponse();

    public UPIPWSessionResponse getUpipwSessionResponse() {
        return upipwSessionResponse;
    }

    public void setUpipwSessionResponse(UPIPWSessionResponse upipwSessionResponse) {
        this.upipwSessionResponse = upipwSessionResponse;
    }

    @Override
    public String toString() {
        return "UPIRootResponse{" +
                "upipwSessionResponse=" + upipwSessionResponse +
                '}';
    }
}
