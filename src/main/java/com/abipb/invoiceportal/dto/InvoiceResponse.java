package com.abipb.invoiceportal.dto;



import java.io.Serializable;
import java.util.List;

/**
 * Created by abhay.kumar-v on 11/27/2017.
 */
public class InvoiceResponse implements Serializable {

        private static final long serialVersionUID = 1L;
        private String merchantId;
        private String establishmentId;
        private List<InvoiceDetails> invoiceDtls;

    public InvoiceResponse(String merchantId, String establishmentId, List<InvoiceDetails> invoiceDtls) {
        this.merchantId = merchantId;
        this.establishmentId = establishmentId;
        this.invoiceDtls = invoiceDtls;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getEstablishmentId() {
        return establishmentId;
    }

    public void setEstablishmentId(String establishmentId) {
        this.establishmentId = establishmentId;
    }

    public List<InvoiceDetails> getInvoiceDtls() {
        return invoiceDtls;
    }

    public void setInvoiceDtls(List<InvoiceDetails> invoiceDtls) {
        this.invoiceDtls = invoiceDtls;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvoiceResponse that = (InvoiceResponse) o;

        if (merchantId != null ? !merchantId.equals(that.merchantId) : that.merchantId != null) return false;
        if (establishmentId != null ? !establishmentId.equals(that.establishmentId) : that.establishmentId != null)
            return false;
        return invoiceDtls != null ? invoiceDtls.equals(that.invoiceDtls) : that.invoiceDtls == null;
    }

    @Override
    public int hashCode() {
        int result = merchantId != null ? merchantId.hashCode() : 0;
        result = 31 * result + (establishmentId != null ? establishmentId.hashCode() : 0);
        result = 31 * result + (invoiceDtls != null ? invoiceDtls.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InvoiceResponse{" +
                "merchantId='" + merchantId + '\'' +
                ", establishmentId='" + establishmentId + '\'' +
                ", invoiceDtls=" + invoiceDtls +
                '}';
    }
}
