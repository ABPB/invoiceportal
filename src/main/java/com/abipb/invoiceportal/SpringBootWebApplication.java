package com.abipb.invoiceportal;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

@SpringBootApplication
public class SpringBootWebApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = new SpringApplicationBuilder(SpringBootWebApplication.class)
                .properties("spring.config.name:application,pgConfig,UPIConfig",    "spring.config.location:classpath:/properties/").build().run(args);
        ConfigurableEnvironment environment = applicationContext.getEnvironment();
    }

}
