package com.abipb.invoiceportal.repository;


import com.abipb.invoiceportal.dto.NachAckData;

import java.util.List;

public interface MandateTransactionRepository {
  public  String   saveTransactionAck(List<NachAckData> nachAckDataList);
}
