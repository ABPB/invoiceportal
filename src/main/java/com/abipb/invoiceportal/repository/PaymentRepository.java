package com.abipb.invoiceportal.repository;

import com.abipb.invoiceportal.dto.PaymentModesResponse;


/**
 * Created by abhay.kumar-v on 11/29/2017.
 */
public interface PaymentRepository {

    public PaymentModesResponse getPaymentModes(Long customerId, String establishmentId, String type);
}

