package com.abipb.invoiceportal.repository;



import com.abipb.invoiceportal.dto.BulkCreateInvoiceImageFileData;
import com.abipb.invoiceportal.dto.DashboardDetails;
import com.abipb.invoiceportal.dto.NachAckData;
import com.abipb.invoiceportal.exception.DataBaseException;

import java.util.List;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by abhay.kumar-v on 12/12/2017.
 */
public interface InvoiceRepository {

    public String bulkCreateInvoiceImageFile(BulkCreateInvoiceImageFileData bulkCreateInvoiceImageFileData) throws DataBaseException;
    public String saveNachAck(List<NachAckData> nachAckData, String responseType)throws DataBaseException;

    public ConcurrentMap<String, String> checkMandateStatus(String merchantId, String consumerRefNo) throws DataBaseException;

    public DashboardDetails getDashboardDetails(String mfilter, String cfilter)throws DataBaseException;
}
