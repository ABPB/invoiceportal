package com.abipb.invoiceportal.repository.impl;


import com.abipb.invoiceportal.dto.*;
import com.abipb.invoiceportal.exception.DataBaseException;
import com.abipb.invoiceportal.repository.InvoiceRepository;
import com.abipb.invoiceportal.util.PaymentStatusEnum;
import com.abipb.invoiceportal.util.ProcNameEnum;
import com.abipb.invoiceportal.util.PropertiesValue;
import oracle.sql.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.sql.*;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by abhay.kumar-v on 12/12/2017.
 */
@Repository
public class InvoiceRepositoryImpl implements InvoiceRepository {
    private static final Logger log= LoggerFactory.getLogger(InvoiceRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Resource
    private PropertiesValue propertiesValue;

    @Override
    public String bulkCreateInvoiceImageFile( BulkCreateInvoiceImageFileData bulkCreateInvoiceImageFileData) throws DataBaseException {
        log.info("BulkCreateInvoiceImageFileData :"+bulkCreateInvoiceImageFileData);
        Connection connection = null;
        CallableStatement stmt=null;
        Clob clob=null;
        try {
            if(propertiesValue.getDriverClass()!=null&&propertiesValue.getUrl()!=null&&propertiesValue.getUserName()!=null&&propertiesValue.getPassword()!=null) {
                Class.forName(propertiesValue.getDriverClass());
                connection = DriverManager.getConnection(propertiesValue.getUrl(), propertiesValue.getUserName(), propertiesValue.getPassword());
                log.info("connection :"+connection);
            }
            if(connection!=null) {

                ArrayDescriptor jpgFileDataDescriptor = ArrayDescriptor.createDescriptor("ABPG_INVIMG_TYP", connection);
                StructDescriptor jpgFileDataStruct = StructDescriptor.createDescriptor("ABPG_INVIMG_OBJ", connection);

                List<JpgFileData> jpgFileDataList = bulkCreateInvoiceImageFileData.getJpgFileDataList();
;
                STRUCT[] jpgFileDataStructs = null;
                STRUCT[] tiffFileDataStructs = null;
                jpgFileDataStructs = new STRUCT[jpgFileDataList.size()];


                JpgFileData jpgFileData=null;
                TiffFileData tiffFileData=null;

                for (int index = 0; index < jpgFileDataList.size(); index++) {
                    Object[] params = new Object[2];
                    jpgFileData = (JpgFileData) jpgFileDataList.get(index);
                    if (jpgFileData != null) {
                        log.info("JpgFileData :: "+jpgFileData);
                        params[0] = jpgFileData.getFileName();
                        clob=CLOB.createTemporary(connection,
                                false, CLOB.DURATION_SESSION);
                        clob.setString(1,jpgFileData.getEncodedImage());
                        params[1]=clob;
                       /* params[2] = jpgFileData.getStatus();
                        params[3] = jpgFileData.getReason();*/

                    }
                    STRUCT struct = new STRUCT(jpgFileDataStruct, connection, params);
                    jpgFileDataStructs[index] = struct;
                }

                ARRAY jpgFileDataArray = new ARRAY(jpgFileDataDescriptor, connection, jpgFileDataStructs);

                int index = 1;
                stmt = connection.prepareCall("{ call " + ProcNameEnum.INVOICE_SERVICE_PROC_NAME_P_B2B_INV_IMAGE_UPLOAD.getProcName() + "(?,?) }");

                log.info("jpgFileDataArray : "+jpgFileDataArray);

                stmt.setArray(index++, jpgFileDataArray);

                stmt.registerOutParameter(index, Types.VARCHAR);
                stmt.execute();
                log.info("Executed the sql query:");
                log.info("Status :"+stmt.getString(2));
                return stmt.getString(2);
            }else{
                return  "Not able to establish connection please contact administrator";
            }

        }catch (Exception e){
            log.error("Exception :",e);
        }finally {
            if(connection !=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    log.error("SQLException :",e);
                }
            }
            if(stmt!=null){
                try {
                    stmt.close();
                } catch (SQLException e) {
                    log.error("SQLException :",e);
                }
            }
        }
        return null;
    }

    @Override
    public String saveNachAck(List<NachAckData> nachAckDataList,String responseType) throws DataBaseException {
        log.debug("NachAckData :"+nachAckDataList);
        Connection connection = null;
        CallableStatement stmt=null;
        try {
            if(propertiesValue.getDriverClass()!=null&&propertiesValue.getUrl()!=null&&propertiesValue.getUserName()!=null&&propertiesValue.getPassword()!=null) {
                Class.forName(propertiesValue.getDriverClass());
                connection = DriverManager.getConnection(propertiesValue.getUrl(), propertiesValue.getUserName(), propertiesValue.getPassword());
                log.debug("connection :"+connection);
            }
            if(connection!=null) {

                ArrayDescriptor nachAckDataDescriptor = ArrayDescriptor.createDescriptor("NACHDBUSER.NACH_NPCI_ACK", connection);
                StructDescriptor nachAckDataStruct = StructDescriptor.createDescriptor("NACHDBUSER.NACH_NPCI_ACK_OBJ", connection);


                STRUCT[] nachAckDataStructs = null;
                nachAckDataStructs = new STRUCT[nachAckDataList.size()];


                NachAckData nachAckData=null;
                TiffFileData tiffFileData=null;

                for (int index = 0; index < nachAckDataList.size(); index++) {
                    Object[] params = new Object[6];
                    nachAckData = (NachAckData) nachAckDataList.get(index);
                    if (nachAckData != null) {
                        log.debug("nachAckData :: "+nachAckData);
                        params[0] = nachAckData.getMsgId();
                        params[1] = nachAckData.getUmrnNo();
                        params[2] = nachAckData.getMerchantId();
                        params[3] = nachAckData.getConRefNo();
                        params[4] = nachAckData.getStatus();
                        params[5] = nachAckData.getReason();

                    }
                    STRUCT struct = new STRUCT(nachAckDataStruct, connection, params);
                    nachAckDataStructs[index] = struct;
                }

                ARRAY nachAckDataArray = new ARRAY(nachAckDataDescriptor, connection, nachAckDataStructs);

                if("ack".equalsIgnoreCase(responseType)) {
                    stmt = connection.prepareCall("{ call " + ProcNameEnum.MANDATE_SERVICE_PROC_NAME_P_NACH_NPCI_ACK.getProcName() + "(?,?) }");
                }else{
                    stmt = connection.prepareCall("{ call " + ProcNameEnum.MANDATE_SERVICE_PROC_NAME_P_NACG_NPCI_FINAL_RESPONSE.getProcName() + "(?,?) }");
                }
                log.debug("nachAckDataArray : "+nachAckDataArray);

                stmt.setArray(1, nachAckDataArray);
                stmt.registerOutParameter(2, Types.VARCHAR);
                stmt.execute();
                log.debug("Executed the sql query:");
                log.debug("Status :"+stmt.getString(2));
                return stmt.getString(2);
            }else{
                return  "Not able to establish connection please contact administrator";
            }

        }catch (Exception e){
            log.error("Exception :",e);
        }finally {
            if(connection !=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    log.error("SQLException :",e);
                }
            }
            if(stmt!=null){
                try {
                    stmt.close();
                } catch (SQLException e) {
                    log.error("SQLException :",e);
                }
            }
        }
        return null;
    }

	@Override
	public ConcurrentMap<String, String> checkMandateStatus(String merchantId, String consumerRefNo) throws DataBaseException {
        log.debug("Merchant Id : ",merchantId," Consumer Ref No : ",consumerRefNo);
        ConcurrentMap< String, String> concurrentMap=null;
        try {
          StoredProcedureQuery storedProcedureQuery =
                    entityManager.createStoredProcedureQuery(ProcNameEnum.MANDATE_SERVICE_STATUS_PROC_NAME_P_NACH_API_STATUS.getProcName())

                    .registerStoredProcedureParameter("p_mer_id", Integer.class, ParameterMode.IN)
                    .setParameter("p_mer_id", Integer.parseInt(merchantId))

                    .registerStoredProcedureParameter("p_consumer_ref_id", String.class, ParameterMode.IN)
                    .setParameter("p_consumer_ref_id", consumerRefNo)

                   .registerStoredProcedureParameter("p_umrn_no", String.class, ParameterMode.OUT)
                   .registerStoredProcedureParameter("p_status", String.class, ParameterMode.OUT)
                   .registerStoredProcedureParameter("p_bank_flag", String.class, ParameterMode.OUT)
                   .registerStoredProcedureParameter("p_msg", String.class, ParameterMode.OUT);
            
            storedProcedureQuery.execute();
            log.debug("Executed query successfully");
            String procStatus=storedProcedureQuery.getOutputParameterValue("p_msg").toString();            
            log.debug("Proc Status :"+procStatus);
            
            if(!PaymentStatusEnum.PAYMENT_SUCCESS.getPaymentStatus().equals(procStatus)){
                throw new DataBaseException("Proc is failing ");
            }
            concurrentMap=new ConcurrentHashMap<String, String>();
            concurrentMap.put("UMRN", storedProcedureQuery.getOutputParameterValue("p_umrn_no").toString());
            concurrentMap.put("status", storedProcedureQuery.getOutputParameterValue("p_status").toString());
            concurrentMap.put("bankFlag", storedProcedureQuery.getOutputParameterValue("p_bank_flag").toString()); 
            
           log.debug("Concurent Map : ",concurrentMap);     
        } catch (Exception e) {
            log.error("Exception :",e);
            throw new DataBaseException(e.getMessage(),e);
        }
		return concurrentMap;


    }


    @Override
    public DashboardDetails getDashboardDetails(String mfilter, String cfilter)throws DataBaseException {
        DashboardDetails dashboardDetails=null;
        try { StoredProcedureQuery storedProcedureQuery =
                entityManager.createStoredProcedureQuery(ProcNameEnum.DASHBOARD_SERVICE__PROC_NAME_P_NACH_DASHBOARD.getProcName())
                        .registerStoredProcedureParameter("p_mandate_period", String.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("p_collection_period", String.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("p_total_mandates", String.class, ParameterMode.OUT)
                        .registerStoredProcedureParameter("p_total_response", String.class, ParameterMode.OUT)
                        .registerStoredProcedureParameter("p_respman_success", String.class, ParameterMode.OUT)
                        .registerStoredProcedureParameter("p_response_pending", String.class, ParameterMode.OUT)
                        .registerStoredProcedureParameter("p_respman_fail", String.class, ParameterMode.OUT)
                        .registerStoredProcedureParameter("p_respend_lesstendays", String.class, ParameterMode.OUT)
                        .registerStoredProcedureParameter("p_respend_greattendays", String.class, ParameterMode.OUT)
                        .registerStoredProcedureParameter("p_total_coll_reqsent", String.class, ParameterMode.OUT)
                        .registerStoredProcedureParameter("p_total_coll_resprcvd", String.class, ParameterMode.OUT)
                        .registerStoredProcedureParameter("p_respcoll_success", String.class, ParameterMode.OUT)
                        .registerStoredProcedureParameter("p_respcoll_fail", String.class, ParameterMode.OUT)
                        .registerStoredProcedureParameter("p_total_failedcoll", String.class, ParameterMode.OUT)
                        .registerStoredProcedureParameter("p_total_successfull", String.class, ParameterMode.OUT)
                        .setParameter("p_mandate_period", mfilter!=null?mfilter:" ")
                        .setParameter("p_collection_period", cfilter!=null?cfilter:" ");
            storedProcedureQuery.execute();
            log.debug("Executed query successfully");
            dashboardDetails = new DashboardDetails();
            dashboardDetails.setTotalMandatesCreated(storedProcedureQuery.getOutputParameterValue("p_total_mandates").toString());
            dashboardDetails.setTotalResponseReceived(storedProcedureQuery.getOutputParameterValue("p_total_response").toString());
            dashboardDetails.setTotalResponsePending(storedProcedureQuery.getOutputParameterValue("p_response_pending").toString());
            dashboardDetails.setResponseReceivedSuccess(storedProcedureQuery.getOutputParameterValue("p_respman_success").toString());
            dashboardDetails.setResponseReceivedFailure(storedProcedureQuery.getOutputParameterValue("p_respman_fail").toString());
            dashboardDetails.setResponsePendingLessthan10days(storedProcedureQuery.getOutputParameterValue("p_respend_lesstendays")!=null?storedProcedureQuery.getOutputParameterValue("p_respend_lesstendays").toString():"0");
            dashboardDetails.setResponsePendingmorethan10days(storedProcedureQuery.getOutputParameterValue("p_respend_greattendays")!=null?storedProcedureQuery.getOutputParameterValue("p_respend_greattendays").toString():"0");
            dashboardDetails.setTotalCollectionRequestSent(storedProcedureQuery.getOutputParameterValue("p_total_coll_reqsent")!=null?storedProcedureQuery.getOutputParameterValue("p_total_coll_reqsent").toString():"0");
            dashboardDetails.setTotalCollectionResponseReceived(storedProcedureQuery.getOutputParameterValue("p_total_coll_resprcvd")!=null?storedProcedureQuery.getOutputParameterValue("p_total_coll_resprcvd").toString():"0");
            dashboardDetails.setCollectionResponseReceivedSuccess(storedProcedureQuery.getOutputParameterValue("p_respcoll_success")!=null?storedProcedureQuery.getOutputParameterValue("p_respcoll_success").toString():"0");
            dashboardDetails.setCollectionResponseReceivedFailure(storedProcedureQuery.getOutputParameterValue("p_respcoll_fail")!=null?storedProcedureQuery.getOutputParameterValue("p_respcoll_fail").toString():"0");
            dashboardDetails.setSuccessCollectionsAmount(storedProcedureQuery.getOutputParameterValue("p_total_failedcoll")!=null?storedProcedureQuery.getOutputParameterValue("p_total_failedcoll").toString():"0");
            dashboardDetails.setFailedCollectionsAmount(storedProcedureQuery.getOutputParameterValue("p_total_successfull")!=null?storedProcedureQuery.getOutputParameterValue("p_total_successfull").toString():"0");
        }catch (Exception e) {
            log.error("Exception :",e);
            throw new DataBaseException(e.getMessage(),e);
        }

        return dashboardDetails;
    }
}
