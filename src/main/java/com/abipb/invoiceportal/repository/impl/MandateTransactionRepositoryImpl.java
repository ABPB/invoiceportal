package com.abipb.invoiceportal.repository.impl;


import com.abipb.invoiceportal.dto.NachAckData;
import com.abipb.invoiceportal.dto.TiffFileData;
import com.abipb.invoiceportal.repository.MandateTransactionRepository;
import com.abipb.invoiceportal.util.ProcNameEnum;
import com.abipb.invoiceportal.util.PropertiesValue;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.*;
import java.util.List;

@Repository
public class MandateTransactionRepositoryImpl implements MandateTransactionRepository {
    private static final Logger log= LoggerFactory.getLogger(MandateTransactionRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Resource
    private PropertiesValue propertiesValue;

    @Override
    public String saveTransactionAck(List<NachAckData> nachAckDataList) {
        log.debug("NachAckData :"+nachAckDataList);
        Connection connection = null;
        CallableStatement stmt=null;
        try {
            if(propertiesValue.getDriverClass()!=null&&propertiesValue.getUrl()!=null&&propertiesValue.getUserName()!=null&&propertiesValue.getPassword()!=null) {
                Class.forName(propertiesValue.getDriverClass());
                connection = DriverManager.getConnection(propertiesValue.getUrl(), propertiesValue.getUserName(), propertiesValue.getPassword());
                log.debug("connection :"+connection);
            }
            if(connection!=null) {

                ArrayDescriptor nachAckDataDescriptor = ArrayDescriptor.createDescriptor("NACHDBUSER.NACH_NPCI_ACK", connection);
                StructDescriptor nachAckDataStruct = StructDescriptor.createDescriptor("NACHDBUSER.NACH_NPCI_ACK_OBJ", connection);


                STRUCT[] nachAckDataStructs = null;
                nachAckDataStructs = new STRUCT[nachAckDataList.size()];


                NachAckData nachAckData=null;
                TiffFileData tiffFileData=null;

                for (int index = 0; index < nachAckDataList.size(); index++) {
                    Object[] params = new Object[6];
                    nachAckData = (NachAckData) nachAckDataList.get(index);
                    if (nachAckData != null) {
                        log.debug("nachAckData :: "+nachAckData);
                        params[0] = nachAckData.getMsgId();
                        params[1] = nachAckData.getUmrnNo();
                        params[2] = nachAckData.getMerchantId();
                        params[3] = nachAckData.getConRefNo();
                        params[4] = nachAckData.getStatus();
                        params[5] = nachAckData.getReason();

                    }
                    STRUCT struct = new STRUCT(nachAckDataStruct, connection, params);
                    nachAckDataStructs[index] = struct;
                }

                ARRAY nachAckDataArray = new ARRAY(nachAckDataDescriptor, connection, nachAckDataStructs);

                if("ack".equalsIgnoreCase("")) {
                    stmt = connection.prepareCall("{ call " + ProcNameEnum.MANDATE_SERVICE_PROC_NAME_P_NACH_NPCI_ACK.getProcName() + "(?,?) }");
                }else{
                    stmt = connection.prepareCall("{ call " + ProcNameEnum.MANDATE_SERVICE_PROC_NAME_P_NACG_NPCI_FINAL_RESPONSE.getProcName() + "(?,?) }");
                }
                log.debug("nachAckDataArray : "+nachAckDataArray);

                stmt.setArray(1, nachAckDataArray);
                stmt.registerOutParameter(2, Types.VARCHAR);
                stmt.execute();
                log.debug("Executed the sql query:");
                log.debug("Status :"+stmt.getString(2));
                return stmt.getString(2);
            }else{
                return  "Not able to establish connection please contact administrator";
            }

        }catch (Exception e){
            log.error("Exception :",e);
        }finally {
            if(connection !=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    log.error("SQLException :",e);
                }
            }
            if(stmt!=null){
                try {
                    stmt.close();
                } catch (SQLException e) {
                    log.error("SQLException :",e);
                }
            }
        }
        return null;
    }
}
