package com.abipb.invoiceportal.repository.impl;

import com.abipb.invoiceportal.dto.*;
import com.abipb.invoiceportal.repository.PaymentRepository;
import com.abipb.invoiceportal.util.ConnectionFactory;
import com.abipb.invoiceportal.util.PropertiesValue;

import oracle.jdbc.OracleTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhay.kumar-v on 11/29/2017.
 */
@Component
public class PaymentRepositoryImpl implements PaymentRepository {

    private static final Logger log= LoggerFactory.getLogger(PaymentRepositoryImpl.class);

    @Resource
    private PropertiesValue propertiesValue;

    @Autowired
    private ConnectionFactory connectionFactory;

    @Override
    public PaymentModesResponse getPaymentModes(Long customerId, String establishmentId, String type) {
        Connection connection = null;
        CallableStatement stmt=null;
        ResultSet rs = null;
        PaymentModesResponse paymentModesResponse = new PaymentModesResponse();
        Mode mode=null;
        List<BankDetails> bankDetailsList=new ArrayList();
        List<UPIDetails> upiDetailsList=new ArrayList();
        BankDetails bankDetails=null;
        UPIDetails upiDetails=null;
        PaymentModes paymentModes=null;
        List<Mode> modeList=new ArrayList<>();
        List<String> types=null;
        try{

            connection=connectionFactory.getConnection();

           if(connection!=null) {
                stmt = connection.prepareCall("{ call P_B2B_DISPLAY_BANK_UPI_DETAILS(?,?,?,?,?,?,?,?) } ");
                stmt.setLong(1, customerId);//p_mer_id
                stmt.setString(2, establishmentId);//p_est_id
                stmt.setString(3, type);//p_payment_type
                stmt.registerOutParameter(4, OracleTypes.CURSOR);//p_bank_accounts
                stmt.registerOutParameter(5, OracleTypes.CURSOR);//p_upi_accounts
                stmt.registerOutParameter(6, Types.VARCHAR);//p_cc
                stmt.registerOutParameter(7, Types.VARCHAR);//p_dc
                stmt.registerOutParameter(8, Types.VARCHAR);//p_msg
                stmt.executeUpdate();
                if(stmt!=null) {
                    paymentModesResponse.setEntityId(customerId);
                    paymentModesResponse.setEstablishmentId(establishmentId);
                    if (stmt.getString(6) != null)
                        paymentModesResponse.setCreditCardStatus(stmt.getString(6));
                    else
                        paymentModesResponse.setCreditCardStatus("disabled");
                    if (stmt.getString(7) != null)
                        paymentModesResponse.setDebitCardStatus(stmt.getString(7));
                    else
                        paymentModesResponse.setDebitCardStatus("disabled");

                    log.debug("Proc Status : ", stmt.getString(8));

                    if (!"PULL".equals(type)){
                        rs = (ResultSet) stmt.getObject(4);
                    while (rs.next()) {
                        bankDetails = new BankDetails();
                        bankDetails.setAccountNo(rs.getString("abd_account_no"));
                        bankDetails.setBankName(rs.getString("abd_bank_name"));
                        String bankFlag=rs.getString("abd_preferred_bank_flg");
                        if (!StringUtils.isEmpty(bankFlag) && !"N".equalsIgnoreCase(bankFlag)) {
                            bankDetails.setPreferredFlag(bankFlag);
                            bankDetailsList.add(bankDetails);
                        } else {
                            bankDetails.setPreferredFlag("N");
                        }
                        // bankDetailsList.add(bankDetails);
                    }
                    if (bankDetailsList.size() > 0) {
                        mode = new Mode();
                        mode.setMode("NB");
                        types=new ArrayList<>(1);
                        types.add(StringUtils.isEmpty(type)?"PUSH":type);
                        mode.setType(types);
                        mode.setBankDetails(bankDetailsList);

                        modeList.add(mode);
                    }
                }
                    rs = (ResultSet) stmt.getObject(5);
                    while (rs.next()) {
                        upiDetails=new UPIDetails();
                        upiDetails.setHandlerName( rs.getString("aud_upa_handler"));
                        String upiFlag=rs.getString("aud_upa_dft_flg");
                        if(!StringUtils.isEmpty(upiFlag) && !"N".equalsIgnoreCase(upiFlag) ) {
                            upiDetails.setPreferredFlag(upiFlag);
                            upiDetailsList.add(upiDetails);
                        }
                        else {
                            upiDetails.setPreferredFlag("N");
                        }

                        //upiDetailsList.add(upiDetails);

                    }
                    if(upiDetailsList.size()>0){
                        mode=new Mode();
                        mode.setMode("UPI");
                        mode.setUpiDetails(upiDetailsList);
                        if(StringUtils.isEmpty(type)){
                            types=new ArrayList<>(2);
                            types.add("PULL");
                            types.add("PUSH");
                        }else {
                            types=new ArrayList<>(1);
                            types.add(type);
                        }
                        mode.setType(types);;
                        modeList.add(mode);
                    }

                    if(modeList!=null&&modeList.size()>0){
                        paymentModes=new PaymentModes();
                        paymentModes.setMode(modeList);
                        paymentModesResponse.setPaymentModes(paymentModes);
                    }
                }
                return paymentModesResponse;
            }

        }catch (Exception e){
            log.error("Exception :",e);

        }finally {
           connectionFactory.closeConnection(connection,stmt,rs);
        }
        return paymentModesResponse;
    }
}
