package com.abipb.invoiceportal.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the NACH_LOGIN_MASTER database table.
 * 
 */
@Entity
@Table(name="ABPG_LOGIN_MASTER")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ALM_USER_ID")
	private String nlmUserId;

	@Column(name="ALM_PASSWORD")
	private String nlmPassword;

	@Column(name="ALM_STATUS")
	private String nlmStatus;

	@Column(name="ALM_CREATED_BY")
	private String nlmCreatedBy;

	@Column(name="ALM_CREATED_DATE")
	private Timestamp nlmCreatedDate;

	@Column(name="ALM_ENTITY_ID")
	private String almEntityId;

	public String getAlmEntityId() {
		return almEntityId;
	}

	public void setAlmEntityId(String almEntityId) {
		this.almEntityId = almEntityId;
	}




	@Column(name="ALM_UPDATED_BY")
	private String nlmUpdatedBy;

	@Column(name="ALM_UPDATED_DATE")
	private Timestamp nlmUpdatedDate;

	@Column(name="ALM_MERCHANT_ID")
	private String nlmMerchantId;

	//bi-directional many-to-one association to NachRoleTable
//	@ManyToOne
//	@JoinColumn(name="NLM_ROLE_ID")
//	private NachRoleTable nachRoleTable;


	//bi-directional one-to-one association to Role
	@OneToOne
//    @JoinColumn(name="CLR_ID")
	@JoinColumn(name="ALM_ROLE_ID")
	private Role role;

	public User() {
	}

	public String getNlmUserId() {
		return this.nlmUserId;
	}

	public void setNlmUserId(String nlmUserId) {
		this.nlmUserId = nlmUserId;
	}

	public String getNlmCreatedBy() {
		return this.nlmCreatedBy;
	}

	public void setNlmCreatedBy(String nlmCreatedBy) {
		this.nlmCreatedBy = nlmCreatedBy;
	}

	public Timestamp getNlmCreatedDate() {
		return this.nlmCreatedDate;
	}

	public void setNlmCreatedDate(Timestamp nlmCreatedDate) {
		this.nlmCreatedDate = nlmCreatedDate;
	}

	public String getNlmMerchantId() {
		return this.nlmMerchantId;
	}

	public void setNlmMerchantId(String nlmMerchantId) {
		this.nlmMerchantId = nlmMerchantId;
	}

	public String getNlmPassword() {
		return this.nlmPassword;
	}

	public void setNlmPassword(String nlmPassword) {
		this.nlmPassword = nlmPassword;
	}

	public String getNlmStatus() {
		return this.nlmStatus;
	}

	public void setNlmStatus(String nlmStatus) {
		this.nlmStatus = nlmStatus;
	}

	public String getNlmUpdatedBy() {
		return this.nlmUpdatedBy;
	}

	public void setNlmUpdatedBy(String nlmUpdatedBy) {
		this.nlmUpdatedBy = nlmUpdatedBy;
	}

	public Timestamp getNlmUpdatedDate() {
		return this.nlmUpdatedDate;
	}

	public void setNlmUpdatedDate(Timestamp nlmUpdatedDate) {
		this.nlmUpdatedDate = nlmUpdatedDate;
	}

//	public NachRoleTable getNachRoleTable() {
//		return this.nachRoleTable;
//	}
//
//	public void setNachRoleTable(NachRoleTable nachRoleTable) {
//		this.nachRoleTable = nachRoleTable;
//	}


	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}