package com.abipb.invoiceportal.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the CRS_FINAL_CUSTOMER database table.
 * 
 */
@Entity
@Table(name="CRS_FINAL_CUSTOMER")
@NamedQuery(name="CrsFinalCustomer.findAll", query="SELECT c FROM CrsFinalCustomer c")
public class CrsFinalCustomer implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CrsFinalCustomerPK id;

	@Column(name="FC_BILL_AMOUNT")
	private BigDecimal fcBillAmount;

	@Temporal(TemporalType.DATE)
	@Column(name="FC_BILL_DUEDATE")
	private Date fcBillDuedate;

	@Column(name="FC_BILL_MONTH")
	private String fcBillMonth;

	@Column(name="FC_BILL_NO")
	private String fcBillNo;

	@Column(name="FC_BILL_STAGE")
	private String fcBillStage;

	@Temporal(TemporalType.DATE)
	@Column(name="FC_BILL_STAGE_DUEDATE")
	private Date fcBillStageDuedate;

	@Column(name="FC_CALC_AMT")
	private BigDecimal fcCalcAmt;

	@Column(name="FC_CHANNEL")
	private String fcChannel;

	@Column(name="FC_CREATED_BY")
	private String fcCreatedBy;

	@Column(name="FC_CREATED_DATE")
	private Timestamp fcCreatedDate;

	@Column(name="FC_CUST_ID")
	private String fcCustId;

	@Column(name="FC_CUST_MOBNO")
	private BigDecimal fcCustMobno;

	@Column(name="FC_CUST_NAME")
	private String fcCustName;

	@Column(name="FC_CUST_VPA")
	private String fcCustVpa;

	@Column(name="FC_DUEDATE_FLG")
	private String fcDuedateFlg;

	@Column(name="FC_RECEIVED_AMT")
	private BigDecimal fcReceivedAmt;

	@Column(name="FC_REMARK")
	private String fcRemark;

	@Column(name="FC_STATUS")
	private String fcStatus;

	@Column(name="FC_UPDATED_BY")
	private String fcUpdatedBy;

	@Column(name="FC_UPDATED_DATE")
	private Timestamp fcUpdatedDate;

	public CrsFinalCustomer() {
	}

	public CrsFinalCustomerPK getId() {
		return this.id;
	}

	public void setId(CrsFinalCustomerPK id) {
		this.id = id;
	}

	public BigDecimal getFcBillAmount() {
		return this.fcBillAmount;
	}

	public void setFcBillAmount(BigDecimal fcBillAmount) {
		this.fcBillAmount = fcBillAmount;
	}

	public Date getFcBillDuedate() {
		return this.fcBillDuedate;
	}

	public void setFcBillDuedate(Date fcBillDuedate) {
		this.fcBillDuedate = fcBillDuedate;
	}

	public String getFcBillMonth() {
		return this.fcBillMonth;
	}

	public void setFcBillMonth(String fcBillMonth) {
		this.fcBillMonth = fcBillMonth;
	}

	public String getFcBillNo() {
		return this.fcBillNo;
	}

	public void setFcBillNo(String fcBillNo) {
		this.fcBillNo = fcBillNo;
	}

	public String getFcBillStage() {
		return this.fcBillStage;
	}

	public void setFcBillStage(String fcBillStage) {
		this.fcBillStage = fcBillStage;
	}

	public Date getFcBillStageDuedate() {
		return this.fcBillStageDuedate;
	}

	public void setFcBillStageDuedate(Date fcBillStageDuedate) {
		this.fcBillStageDuedate = fcBillStageDuedate;
	}

	public BigDecimal getFcCalcAmt() {
		return this.fcCalcAmt;
	}

	public void setFcCalcAmt(BigDecimal fcCalcAmt) {
		this.fcCalcAmt = fcCalcAmt;
	}

	public String getFcChannel() {
		return this.fcChannel;
	}

	public void setFcChannel(String fcChannel) {
		this.fcChannel = fcChannel;
	}

	public String getFcCreatedBy() {
		return this.fcCreatedBy;
	}

	public void setFcCreatedBy(String fcCreatedBy) {
		this.fcCreatedBy = fcCreatedBy;
	}

	public Timestamp getFcCreatedDate() {
		return this.fcCreatedDate;
	}

	public void setFcCreatedDate(Timestamp fcCreatedDate) {
		this.fcCreatedDate = fcCreatedDate;
	}

	public String getFcCustId() {
		return this.fcCustId;
	}

	public void setFcCustId(String fcCustId) {
		this.fcCustId = fcCustId;
	}

	public BigDecimal getFcCustMobno() {
		return this.fcCustMobno;
	}

	public void setFcCustMobno(BigDecimal fcCustMobno) {
		this.fcCustMobno = fcCustMobno;
	}

	public String getFcCustName() {
		return this.fcCustName;
	}

	public void setFcCustName(String fcCustName) {
		this.fcCustName = fcCustName;
	}

	public String getFcCustVpa() {
		return this.fcCustVpa;
	}

	public void setFcCustVpa(String fcCustVpa) {
		this.fcCustVpa = fcCustVpa;
	}

	public String getFcDuedateFlg() {
		return this.fcDuedateFlg;
	}

	public void setFcDuedateFlg(String fcDuedateFlg) {
		this.fcDuedateFlg = fcDuedateFlg;
	}

	public BigDecimal getFcReceivedAmt() {
		return this.fcReceivedAmt;
	}

	public void setFcReceivedAmt(BigDecimal fcReceivedAmt) {
		this.fcReceivedAmt = fcReceivedAmt;
	}

	public String getFcRemark() {
		return this.fcRemark;
	}

	public void setFcRemark(String fcRemark) {
		this.fcRemark = fcRemark;
	}

	public String getFcStatus() {
		return this.fcStatus;
	}

	public void setFcStatus(String fcStatus) {
		this.fcStatus = fcStatus;
	}

	public String getFcUpdatedBy() {
		return this.fcUpdatedBy;
	}

	public void setFcUpdatedBy(String fcUpdatedBy) {
		this.fcUpdatedBy = fcUpdatedBy;
	}

	public Timestamp getFcUpdatedDate() {
		return this.fcUpdatedDate;
	}

	public void setFcUpdatedDate(Timestamp fcUpdatedDate) {
		this.fcUpdatedDate = fcUpdatedDate;
	}

}