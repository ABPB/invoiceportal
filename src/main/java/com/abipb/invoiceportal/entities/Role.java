package com.abipb.invoiceportal.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the NACH_ROLE_TABLE database table.
 * 
 */
@Entity
@Table(name="ABPG_ROLE_TABLE")
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

//ART_ROLE_ID
//ART_ROLE_NAME
//ART_CREATED_BY
//ART_CREATED_DATE
//ART_UPDATED_BY
//ART_UPDATED_DATE

	@Id
	@Column(name="ART_ROLE_ID")
	private long nrtRoleId;

	@Column(name="ART_ROLE_NAME")
	private String nrtRoleName;

	@Column(name="ART_CREATED_BY")
	private String nrtCreatedBy;

	@Column(name="ART_CREATED_DATE")
	private Timestamp nrtCreatedDate;


	@Column(name="ART_UPDATED_BY")
	private String nrtUpdatedBy;

	@Column(name="ART_UPDATED_DATE")
	private Timestamp nrtUpdatedDate;

	//bi-directional many-to-one association to NachLoginMaster
//	@OneToMany(mappedBy="Role")
//	private List<User> users;

	@OneToOne(mappedBy="role")
	private User user;
	public Role() {
	}

	public long getNrtRoleId() {
		return this.nrtRoleId;
	}

	public void setNrtRoleId(long nrtRoleId) {
		this.nrtRoleId = nrtRoleId;
	}

	public String getNrtCreatedBy() {
		return this.nrtCreatedBy;
	}

	public void setNrtCreatedBy(String nrtCreatedBy) {
		this.nrtCreatedBy = nrtCreatedBy;
	}

	public Timestamp getNrtCreatedDate() {
		return this.nrtCreatedDate;
	}

	public void setNrtCreatedDate(Timestamp nrtCreatedDate) {
		this.nrtCreatedDate = nrtCreatedDate;
	}

	public String getNrtRoleName() {
		return this.nrtRoleName;
	}

	public void setNrtRoleName(String nrtRoleName) {
		this.nrtRoleName = nrtRoleName;
	}

	public String getNrtUpdatedBy() {
		return this.nrtUpdatedBy;
	}

	public void setNrtUpdatedBy(String nrtUpdatedBy) {
		this.nrtUpdatedBy = nrtUpdatedBy;
	}

	public Timestamp getNrtUpdatedDate() {
		return this.nrtUpdatedDate;
	}

	public void setNrtUpdatedDate(Timestamp nrtUpdatedDate) {
		this.nrtUpdatedDate = nrtUpdatedDate;
	}

//	public List<User> getNachLoginMasters() {
//		return this.nachLoginMasters;
//	}
//
//	public void setNachLoginMasters(List<User> nachLoginMasters) {
//		this.nachLoginMasters = nachLoginMasters;
//	}
//
//	public User addNachLoginMaster(User nachLoginMaster) {
//		getNachLoginMasters().add(nachLoginMaster);
//		nachLoginMaster.setNachRoleTable(this);
//
//		return nachLoginMaster;
//	}
//
//	public NachLoginMaster removeNachLoginMaster(NachLoginMaster nachLoginMaster) {
//		getNachLoginMasters().remove(nachLoginMaster);
//		nachLoginMaster.setNachRoleTable(null);
//
//		return nachLoginMaster;
//	}
public void setUser(User user) {
	this.user = user;
}

	public User getUser() {
		return user;
	}
}