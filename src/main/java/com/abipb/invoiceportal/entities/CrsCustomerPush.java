package com.abipb.invoiceportal.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the CRS_CUSTOMER_PUSH database table.
 * 
 */
@Entity
@Table(name="CRS_CUSTOMER_PUSH")
@NamedQuery(name="CrsCustomerPush.findAll", query="SELECT c FROM CrsCustomerPush c")
public class CrsCustomerPush implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CrsCustomerPushPK id;

	@Column(name="CC_AMOUNT")
	private BigDecimal ccAmount;

	@Column(name="CC_CHANNEL")
	private String ccChannel;

	@Column(name="CC_CREATED_BY")
	private String ccCreatedBy;

	@Column(name="CC_CREATED_DATE")
	private Timestamp ccCreatedDate;

	@Column(name="CC_CUSTID")
	private String ccCustid;

	@Column(name="CC_CUSTOMER_ACC")
	private String ccCustomerAcc;

	@Column(name="CC_CUSTOMER_IFSC")
	private String ccCustomerIfsc;

	@Column(name="CC_CUSTOMER_NAME")
	private String ccCustomerName;

	@Column(name="CC_CUSTOMER_VPA")
	private String ccCustomerVpa;

	@Column(name="CC_RECEIVED_AMT")
	private BigDecimal ccReceivedAmt;

	@Column(name="CC_REMARK")
	private String ccRemark;

	@Column(name="CC_STATUS")
	private String ccStatus;

	@Column(name="CC_TXN_MODE")
	private String ccTxnMode;

	@Column(name="CC_UPDATED_BY")
	private String ccUpdatedBy;

	@Column(name="CC_UPDATED_DATE")
	private Timestamp ccUpdatedDate;

	@Column(name="CC_VPA_FLG")
	private String ccVpaFlg;

	public CrsCustomerPush() {
	}

	public BigDecimal getCcAmount() {
		return this.ccAmount;
	}

	public void setCcAmount(BigDecimal ccAmount) {
		this.ccAmount = ccAmount;
	}

	public String getCcChannel() {
		return this.ccChannel;
	}

	public void setCcChannel(String ccChannel) {
		this.ccChannel = ccChannel;
	}

	public String getCcCreatedBy() {
		return this.ccCreatedBy;
	}

	public void setCcCreatedBy(String ccCreatedBy) {
		this.ccCreatedBy = ccCreatedBy;
	}

	public Timestamp getCcCreatedDate() {
		return this.ccCreatedDate;
	}

	public void setCcCreatedDate(Timestamp ccCreatedDate) {
		this.ccCreatedDate = ccCreatedDate;
	}

	public String getCcCustid() {
		return this.ccCustid;
	}

	public void setCcCustid(String ccCustid) {
		this.ccCustid = ccCustid;
	}

	public String getCcCustomerAcc() {
		return this.ccCustomerAcc;
	}

	public void setCcCustomerAcc(String ccCustomerAcc) {
		this.ccCustomerAcc = ccCustomerAcc;
	}

	public String getCcCustomerIfsc() {
		return this.ccCustomerIfsc;
	}

	public void setCcCustomerIfsc(String ccCustomerIfsc) {
		this.ccCustomerIfsc = ccCustomerIfsc;
	}

	public String getCcCustomerName() {
		return this.ccCustomerName;
	}

	public void setCcCustomerName(String ccCustomerName) {
		this.ccCustomerName = ccCustomerName;
	}

	public String getCcCustomerVpa() {
		return this.ccCustomerVpa;
	}

	public void setCcCustomerVpa(String ccCustomerVpa) {
		this.ccCustomerVpa = ccCustomerVpa;
	}

	public BigDecimal getCcReceivedAmt() {
		return this.ccReceivedAmt;
	}

	public void setCcReceivedAmt(BigDecimal ccReceivedAmt) {
		this.ccReceivedAmt = ccReceivedAmt;
	}

	public String getCcRemark() {
		return this.ccRemark;
	}

	public void setCcRemark(String ccRemark) {
		this.ccRemark = ccRemark;
	}

	public String getCcStatus() {
		return this.ccStatus;
	}

	public void setCcStatus(String ccStatus) {
		this.ccStatus = ccStatus;
	}

	public String getCcTxnMode() {
		return this.ccTxnMode;
	}

	public void setCcTxnMode(String ccTxnMode) {
		this.ccTxnMode = ccTxnMode;
	}

	public String getCcUpdatedBy() {
		return this.ccUpdatedBy;
	}

	public void setCcUpdatedBy(String ccUpdatedBy) {
		this.ccUpdatedBy = ccUpdatedBy;
	}

	public Timestamp getCcUpdatedDate() {
		return this.ccUpdatedDate;
	}

	public void setCcUpdatedDate(Timestamp ccUpdatedDate) {
		this.ccUpdatedDate = ccUpdatedDate;
	}

	public String getCcVpaFlg() {
		return this.ccVpaFlg;
	}

	public void setCcVpaFlg(String ccVpaFlg) {
		this.ccVpaFlg = ccVpaFlg;
	}

}