package com.abipb.invoiceportal.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the CRS_STATUS database table.
 * 
 */
@Entity
@Table(name="CRS_STATUS")
@NamedQuery(name="CrsStatus.findAll", query="SELECT c FROM CrsStatus c")
public class CrsStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CS_STATUS_ID")
	private long csStatusId;

	@Column(name="CS_STATUS")
	private String csStatus;

	@Column(name="CS_STATUS_DESCRIPTION")
	private String csStatusDescription;

	public CrsStatus() {
	}

	public long getCsStatusId() {
		return this.csStatusId;
	}

	public void setCsStatusId(long csStatusId) {
		this.csStatusId = csStatusId;
	}

	public String getCsStatus() {
		return this.csStatus;
	}

	public void setCsStatus(String csStatus) {
		this.csStatus = csStatus;
	}

	public String getCsStatusDescription() {
		return this.csStatusDescription;
	}

	public void setCsStatusDescription(String csStatusDescription) {
		this.csStatusDescription = csStatusDescription;
	}

}