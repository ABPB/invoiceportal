package com.abipb.invoiceportal.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;


/**
 * Created by Venkatesh.R on 8/30/2017.
 */

    @Embeddable
    public class CrsFinalCustomerPK implements Serializable {

    @Column(name = "FC_BATCH_ID")
    private String batchId;

    @Column(name = "FC_RUNG_SEQ")
    private Long runSeq;

    public CrsFinalCustomerPK() {
    }

    public CrsFinalCustomerPK(String batchId, Long runSeq) {
        this.batchId = batchId;
        this.runSeq = runSeq;
    }

    public String getBatchId() {

        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Long getRunSeq() {
        return runSeq;
    }

    public void setRunSeq(Long runSeq) {
        this.runSeq = runSeq;
    }

    @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CrsFinalCustomerPK that = (CrsFinalCustomerPK) o;

            if (!batchId.equals(that.batchId)) return false;
            return runSeq.equals(that.runSeq);
        }

        @Override
        public int hashCode() {
            int result = batchId.hashCode();
            result = 31 * result + runSeq.hashCode();
            return result;
    }

}

