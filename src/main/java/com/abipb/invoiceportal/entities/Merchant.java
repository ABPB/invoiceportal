package com.abipb.invoiceportal.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the CRS_CLIENT_CONFIG database table.
 *
 */
@Entity
@Table(name="CRS_CLIENT_CONFIG")
@NamedQuery(name="Merchant.findAll", query="SELECT c FROM Merchant c")
public class Merchant implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String cid;

//	public Integer getVersion() {
//		return version;
//	}
//
//	public void setVersion(Integer version) {
//		this.version = version;
//	}
//
//	@Version
//	private Integer version;

	@Column(name="ACCOUNT_VERIFICATION")
	private String accountVerification;

	@Column(name="CEILING_PAYMENT")
	private BigDecimal ceilingPayment;

	@Column(name="DAYSAFTER_DUEDATE")
	private BigDecimal daysafterDuedate;

	@Column(name="DAYSINCENTIVE_VALID")
	private BigDecimal daysincentiveValid;

	@Column(name="EARLY_INCENTIVE_AMT")
	private BigDecimal earlyIncentiveAmt;

	@Column(name="EARLY_INCENTIVE_PERCNT")
	private BigDecimal earlyIncentivePercnt;

	@Column(name="FLOOR_PAYMENT")
	private BigDecimal floorPayment;

	@Column(name="LATEFEE_AMT")
	private BigDecimal latefeeAmt;

	@Column(name="LATEFEE_PERCNT")
	private BigDecimal latefeePercnt;

	@Column(name="ONLINEPAYMENT_CONFIRMATION")
	private String onlinepaymentConfirmation;

	@Column(name="PARTIALPAY")
	private String partialpay;

	//	@Temporal(TemporalType.DATE)
	@Column(name="PAYAFTER_DUEDATE")
	// @DateTimeFormat(pattern="dd-MM-YYYY")
	private String payafterDuedate;

	@Column(name="PAYMENT_CONFIRMATIONURL")
	private String paymentConfirmationurl;

	@Column(name="MERCHANT_CATEGORYCODE")
	private String merchantCategorycode;

	@Column(name="CLIENT_ACCOUNTNO")
	private String clientAccountno;

	@Column(name="CLIENT_BANK")
	private String clientBank;

	@Column(name="CLIENT_DEVICEID")
	private String clientDeviceid;

	@Column(name="CLIENT_IFSC_CODE")
	private String clientIfscCode;

	@Column(name="CLIENT_MOBNO")
	private BigDecimal clientMobno;

	@Column(name="CLIENT_SECRETKEY")
	private String clientSecretkey;

	@Column(name="CLIENT_VPA")
	private String clientVpa;

	@Column(name="CNAME")
	private String cname;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Column(name="CREATED_DATE")
	private Timestamp createdDate;

	@Column(name="SETTLEMENT_ACCOUNTNO")
	private String settlementAccountno;

	@Column(name="SETTLEMENT_BANK")
	private String settlementBank;

	@Column(name="SETTLEMENT_IFSC_CODE")
	private String settlementIfscCode;

	@Column(name="UPDATED_BY")
	private String updatedBy;

	@Column(name="UPDATED_DATE")
	private Timestamp updatedDate;

	@Column(name="MDR_CEILING")
	private BigDecimal mdrCeiling;

	@Column(name="MDR_PERC")
	private BigDecimal mdrPerc;

	@Column(name="MDR_AMOUNT")
	private BigDecimal mdrAmount;

	@Column(name="MDR_FLOOR")
	private BigDecimal mdrFloor;

	@Column(name="GROSS_NET")
	private String grossNet;

	@Column(name="MERCHANT_TAGLINE")
	private String merchantTagline;

	@Column(name="MERCHANT_LOGO")
	private String merchantLogo;

	@Column(name="REDIRECT_URL")
	private String redirectUrl;

	@Column(name="CARTCHECKOUT_FLG")
	private String cartcheckoutFlg;

	@Column(name="CASHON_DELIVERY_FLG")
	private String cashonDeliveryFlg;

	@Column(name="MERCHANTPULL_FLG")
	private String merchantpullFlg;

	@Column(name="MERCHANTPUSH_FLG")
	private String merchantpushFlg;


/*
	@PreUpdate
	@PrePersist
	public void setFlags() {
		if (merchantPullCheck)
			setMerchantpullFlg("Y");
		else
			setMerchantpullFlg("N");

		if (merchantPushCheck)
			setMerchantpushFlg("Y");
		else
			setMerchantpushFlg("N");
		if (cartCheckoutCheck)
			setCartcheckoutFlg("Y");
		else
			setCartcheckoutFlg("N");
	}
*/

	@PostLoad
	public void setPostLoadFlags() {
		if ("Y".equals(merchantpullFlg))
			setMerchantPullCheck(true);

		if ("Y".equals(merchantpushFlg))
			setMerchantPushCheck(true);

		if ("Y".equals(cartcheckoutFlg))
			setCartCheckoutCheck(true);

		if ("Y".equals(cashonDeliveryFlg))
		    setCashOnDeliveryCheck(true);
	}


	public void setCartCheckoutCheck(boolean cartCheckoutCheck) {
		this.cartCheckoutCheck = cartCheckoutCheck;
	}

	@Transient
	private boolean merchantPullCheck;
	@Transient
	private boolean merchantPushCheck;
	@Transient
	private boolean cartCheckoutCheck;

    @Transient
    private boolean cashOnDeliveryCheck;

    public boolean isCashOnDeliveryCheck() {
        return cashOnDeliveryCheck;
    }

    public void setCashOnDeliveryCheck(boolean cashOnDeliveryCheck) {
        this.cashOnDeliveryCheck = cashOnDeliveryCheck;
    }

    public String getCartcheckoutFlg() {
		return cartcheckoutFlg;
	}

	public void setCartcheckoutFlg(String cartcheckoutFlg) {
		this.cartcheckoutFlg = cartcheckoutFlg;
	}

	public String getCashonDeliveryFlg() {
		return cashonDeliveryFlg;
	}

	public void setCashonDeliveryFlg(String cashonDeliveryFlg) {
		this.cashonDeliveryFlg = cashonDeliveryFlg;
	}

	public String getMerchantpullFlg() {
		return merchantpullFlg;
	}

	public void setMerchantpullFlg(String merchantpullFlg) {
		this.merchantpullFlg = merchantpullFlg;
	}

	public String getMerchantpushFlg() {
		return merchantpushFlg;
	}

	public void setMerchantpushFlg(String merchantpushFlg) {
		this.merchantpushFlg = merchantpushFlg;
	}

	public Merchant() {
	}

	public String getCid() {
		return this.cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getAccountVerification() {
		return this.accountVerification;
	}

	public void setAccountVerification(String accountVerification) {
		this.accountVerification = accountVerification;
	}

	public BigDecimal getCeilingPayment() {
		return this.ceilingPayment;
	}

	public void setCeilingPayment(BigDecimal ceilingPayment) {
		this.ceilingPayment = ceilingPayment;
	}

	public String getClientAccountno() {
		return this.clientAccountno;
	}

	public void setClientAccountno(String clientAccountno) {
		this.clientAccountno = clientAccountno;
	}

	public String getClientBank() {
		return this.clientBank;
	}

	public void setClientBank(String clientBank) {
		this.clientBank = clientBank;
	}

	public String getClientDeviceid() {
		return this.clientDeviceid;
	}

	public void setClientDeviceid(String clientDeviceid) {
		this.clientDeviceid = clientDeviceid;
	}

	public String getClientIfscCode() {
		return this.clientIfscCode;
	}

	public void setClientIfscCode(String clientIfscCode) {
		this.clientIfscCode = clientIfscCode;
	}

	public BigDecimal getClientMobno() {
		return this.clientMobno;
	}

	public void setClientMobno(BigDecimal clientMobno) {
		this.clientMobno = clientMobno;
	}

	public String getClientSecretkey() {
		return this.clientSecretkey;
	}

	public void setClientSecretkey(String clientSecretkey) {
		this.clientSecretkey = clientSecretkey;
	}

	public String getClientVpa() {
		return this.clientVpa;
	}

	public void setClientVpa(String clientVpa) {
		this.clientVpa = clientVpa;
	}

	public String getCname() {
		return this.cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getDaysafterDuedate() {
		return this.daysafterDuedate;
	}

	public void setDaysafterDuedate(BigDecimal daysafterDuedate) {
		this.daysafterDuedate = daysafterDuedate;
	}

	public BigDecimal getDaysincentiveValid() {
		return this.daysincentiveValid;
	}

	public void setDaysincentiveValid(BigDecimal daysincentiveValid) {
		this.daysincentiveValid = daysincentiveValid;
	}

	public BigDecimal getEarlyIncentiveAmt() {
		return this.earlyIncentiveAmt;
	}

	public void setEarlyIncentiveAmt(BigDecimal earlyIncentiveAmt) {
		this.earlyIncentiveAmt = earlyIncentiveAmt;
	}

	public BigDecimal getEarlyIncentivePercnt() {
		return this.earlyIncentivePercnt;
	}

	public void setEarlyIncentivePercnt(BigDecimal earlyIncentivePercnt) {
		this.earlyIncentivePercnt = earlyIncentivePercnt;
	}

	public BigDecimal getFloorPayment() {
		return this.floorPayment;
	}

	public void setFloorPayment(BigDecimal floorPayment) {
		this.floorPayment = floorPayment;
	}

	public BigDecimal getLatefeeAmt() {
		return this.latefeeAmt;
	}

	public void setLatefeeAmt(BigDecimal latefeeAmt) {
		this.latefeeAmt = latefeeAmt;
	}

	public BigDecimal getLatefeePercnt() {
		return this.latefeePercnt;
	}

	public void setLatefeePercnt(BigDecimal latefeePercnt) {
		this.latefeePercnt = latefeePercnt;
	}

	public String getMerchantCategorycode() {
		return this.merchantCategorycode;
	}

	public void setMerchantCategorycode(String merchantCategorycode) {
		this.merchantCategorycode = merchantCategorycode;
	}

	public String getOnlinepaymentConfirmation() {
		return this.onlinepaymentConfirmation;
	}

	public void setOnlinepaymentConfirmation(String onlinepaymentConfirmation) {
		this.onlinepaymentConfirmation = onlinepaymentConfirmation;
	}

	public String getPartialpay() {
		return this.partialpay;
	}

	public void setPartialpay(String partialpay) {
		this.partialpay = partialpay;
	}

	public String getPayafterDuedate() {
		return this.payafterDuedate;
	}

	public void setPayafterDuedate(String payafterDuedate) {
		this.payafterDuedate = payafterDuedate;
	}

	public String getPaymentConfirmationurl() {
		return this.paymentConfirmationurl;
	}

	public void setPaymentConfirmationurl(String paymentConfirmationurl) {
		this.paymentConfirmationurl = paymentConfirmationurl;
	}

	public String getSettlementAccountno() {
		return this.settlementAccountno;
	}

	public void setSettlementAccountno(String settlementAccountno) {
		this.settlementAccountno = settlementAccountno;
	}

	public String getSettlementBank() {
		return this.settlementBank;
	}

	public void setSettlementBank(String settlementBank) {
		this.settlementBank = settlementBank;
	}

	public String getSettlementIfscCode() {
		return this.settlementIfscCode;
	}

	public void setSettlementIfscCode(String settlementIfscCode) {
		this.settlementIfscCode = settlementIfscCode;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

	public BigDecimal getMdrCeiling() {
		return mdrCeiling;
	}

	public void setMdrCeiling(BigDecimal mdrCeiling) {
		this.mdrCeiling = mdrCeiling;
	}

	public BigDecimal getMdrPerc() {
		return mdrPerc;
	}

	public void setMdrPerc(BigDecimal mdrPerc) {
		this.mdrPerc = mdrPerc;
	}

	public BigDecimal getMdrAmount() {
		return mdrAmount;
	}

	public void setMdrAmount(BigDecimal mdrAmount) {
		this.mdrAmount = mdrAmount;
	}

	public BigDecimal getMdrFloor() {
		return mdrFloor;
	}

	public void setMdrFloor(BigDecimal mdrFloor) {
		this.mdrFloor = mdrFloor;
	}

	public String getGrossNet() {
		return grossNet;
	}

	public void setGrossNet(String grossNet) {
		this.grossNet = grossNet;
	}

	public String getMerchantTagline() {
		return merchantTagline;
	}

	public void setMerchantTagline(String merchantTagline) {
		this.merchantTagline = merchantTagline;
	}

	public String getMerchantLogo() {
		return merchantLogo;
	}

	public void setMerchantLogo(String merchantLogo) {
		this.merchantLogo = merchantLogo;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public boolean isMerchantPullCheck() {
		return merchantPullCheck;
	}

	public void setMerchantPullCheck(boolean merchantPullCheck) {
		this.merchantPullCheck = merchantPullCheck;
	}

	public boolean isMerchantPushCheck() {
		return merchantPushCheck;
	}

	public void setMerchantPushCheck(boolean merchantPushCheck) {
		this.merchantPushCheck = merchantPushCheck;
	}

	public boolean isCartCheckoutCheck() {
		return cartCheckoutCheck;
	}


}