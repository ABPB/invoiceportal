package com.abipb.invoiceportal.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the CRS_LOGIN_TABLE database table.
 * 
 */
@Entity
@Table(name="CRS_LOGIN_TABLE")
@NamedQuery(name="CrsLoginTable.findAll", query="SELECT c FROM CrsLoginTable c")
public class CrsLoginTable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CLR_USER")
	private String clrUser;

	@Column(name="CLR_PWD")
	private String clrPwd;

	public CrsLoginTable() {
	}

	public String getClrUser() {
		return this.clrUser;
	}

	public void setClrUser(String clrUser) {
		this.clrUser = clrUser;
	}

	public String getClrPwd() {
		return this.clrPwd;
	}

	public void setClrPwd(String clrPwd) {
		this.clrPwd = clrPwd;
	}

}