package com.abipb.invoiceportal.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the CRS_SUMMARY database table.
 * 
 */
@Entity
@Table(name="CRS_SUMMARY")
@NamedQuery(name="CrsSummary.findAll", query="SELECT c FROM CrsSummary c")
public class CrsSummary implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CS_ID")
	private long csId;

	@Column(name="CS_BATCH_ID")
	private String csBatchId;

	@Column(name="CS_CREATED_BY")
	private String csCreatedBy;

	@Column(name="CS_CREATED_DATE")
	private Timestamp csCreatedDate;

	@Column(name="CS_LOT_NUMBER")
	private BigDecimal csLotNumber;

	@Column(name="CS_MERCH_ID")
	private String csMerchId;

	@Column(name="CS_RECORD_CNT")
	private BigDecimal csRecordCnt;

	@Column(name="CS_STATUS")
	private String csStatus;

	@Column(name="CS_UPDATED_BY")
	private String csUpdatedBy;

	@Column(name="CS_UPDATED_DATE")
	private Timestamp csUpdatedDate;

	public CrsSummary() {
	}

	public long getCsId() {
		return this.csId;
	}

	public void setCsId(long csId) {
		this.csId = csId;
	}

	public String getCsBatchId() {
		return this.csBatchId;
	}

	public void setCsBatchId(String csBatchId) {
		this.csBatchId = csBatchId;
	}

	public String getCsCreatedBy() {
		return this.csCreatedBy;
	}

	public void setCsCreatedBy(String csCreatedBy) {
		this.csCreatedBy = csCreatedBy;
	}

	public Timestamp getCsCreatedDate() {
		return this.csCreatedDate;
	}

	public void setCsCreatedDate(Timestamp csCreatedDate) {
		this.csCreatedDate = csCreatedDate;
	}

	public BigDecimal getCsLotNumber() {
		return this.csLotNumber;
	}

	public void setCsLotNumber(BigDecimal csLotNumber) {
		this.csLotNumber = csLotNumber;
	}

	public String getCsMerchId() {
		return this.csMerchId;
	}

	public void setCsMerchId(String csMerchId) {
		this.csMerchId = csMerchId;
	}

	public BigDecimal getCsRecordCnt() {
		return this.csRecordCnt;
	}

	public void setCsRecordCnt(BigDecimal csRecordCnt) {
		this.csRecordCnt = csRecordCnt;
	}

	public String getCsStatus() {
		return this.csStatus;
	}

	public void setCsStatus(String csStatus) {
		this.csStatus = csStatus;
	}

	public String getCsUpdatedBy() {
		return this.csUpdatedBy;
	}

	public void setCsUpdatedBy(String csUpdatedBy) {
		this.csUpdatedBy = csUpdatedBy;
	}

	public Timestamp getCsUpdatedDate() {
		return this.csUpdatedDate;
	}

	public void setCsUpdatedDate(Timestamp csUpdatedDate) {
		this.csUpdatedDate = csUpdatedDate;
	}

}